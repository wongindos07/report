<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marketing extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model("Drop_model");
		$this->load->model("Mainmodel");
		$this->load->library('session');
		
		if($this->session->userdata('status') !== 'masuk'){
			redirect('Login');
		}
	}

	// public function index(){
	// 	$this->session->set_flashdata('aktif','active');
	// 	$this->recap();

	// }
	public function index()
	{	
		$getinisialPMB = $this->Mainmodel->getinisialPMB()->result();
		foreach($getinisialPMB as $inisial){
			$inisialPMB=$inisial->ta;
		}
		$data['inisialPMB'] = $inisialPMB;
		$data['konten'] = 'form/pmb';
		$this->load->view('Dashboard',$data);
	}

	public function getTahunPMB(){
		$getTahunPMB = $this->Mainmodel->getTahunPMB()->result();
			echo "<option>-- Pilih Tahun PMB --</option>";
		foreach($getTahunPMB as $rowTahun){
			echo '<option value="'.$rowTahun->ta.'">'.$rowTahun->ta.'</option>';
		}
	}
	public function getPeriodeRekapPMB(){
		$kriteria = $this->input->post('kriteria');
		$getTglKal = $this->Mainmodel->getTglKalkulasi($kriteria)->result();
			echo "<option>-- Periode Kalkulasi --</option>";
		foreach($getTglKal as $Periodekal){
			$tglkal=$Periodekal->tglkalkulasi;
			echo "<option value='".$tglkal."'>".$Periodekal->tglkalkulasi."</option>";

		}
	}
	
	function getrekapPMB(){
		
		$kriteria			= $this->input->post('kriteria');
		$tahun			= $this->input->post('tahun');
		$tglkalkulasi			= $this->input->post('tglkalkulasi');
		$kdcabang			= $this->uri->segment(6);
		$milik			= $this->input->post('milik');

		$dpregis	= "select * from konfigurasi_dp_registrasi where tahunakademik='$tahun' and flag='1'";
		$dp= $this->Mainmodel->tampildatacollege($dpregis);
		
		foreach($dp as $dpr)
		{$ket=$dpr->keterangan;}
		if(!$dp)
		{$data['ket'] ="" ;}else{$data['ket'] =$ket ;}

		if($milik==1)
		{$stsmilik="and aplikanperolehannasional.kodecabang in (select kodecabang from cabang where (kepemilikan ='1' or kepemilikan='2'))";}else
		{$stsmilik="";}

			if($kriteria=='junior')
			{$data['nkriteria'] = "Perolehan Jumlah Junior";
				$ambil				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30,kelompok FROM aplikanperolehannasional where tahunpmb='$tahun' 
				and direktorat='2' and tglkalkulasi='$tglkalkulasi' and kelompok='1' $stsmilik order by (`regjunior`+`u-30`) desc";
				
				$ambil2				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30 ,targetd3,regd3 FROM aplikanperolehannasional where tahunpmb='$tahun' 
				and direktorat='3' and tglkalkulasi='$tglkalkulasi' $stsmilik order by (`regjunior`+`u-30`) desc";

				// $ambil3				= "SELECT 
				// kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				// ,direktorat,tglupdate,`u-30` as u30,kelompok FROM aplikanperolehannasional where tahunpmb='$tahun' 
				// and direktorat='2' and kelompok='4' order by kodecabang in 
				// (select kodecabang from cabang where kelompok='4' order by namacabang asc) ";

				$ambil3				="SELECT 
				aplikanperolehannasional.kodecabang, 
				aplikanperolehannasional.aplikan,
				aplikanperolehannasional.daftar,
				aplikanperolehannasional.ujian,
				aplikanperolehannasional.lulus,
				aplikanperolehannasional.regjunior,
				aplikanperolehannasional.regsenior,
				aplikanperolehannasional.targetjunior,
				aplikanperolehannasional.targetsenior,
				aplikanperolehannasional.tahunpmb,
				aplikanperolehannasional.direktorat,
				aplikanperolehannasional.tglupdate,
				aplikanperolehannasional.`u-30` as u30,
				aplikanperolehannasional.kelompok 

				FROM aplikanperolehannasional join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang 
				where aplikanperolehannasional.tahunpmb='$tahun' 
				and aplikanperolehannasional.direktorat='2' and aplikanperolehannasional.kelompok='4' and tglkalkulasi='$tglkalkulasi'  
				$stsmilik  order by cabang.namacabang asc,(aplikanperolehannasional.`regjunior`+aplikanperolehannasional.`u-30`) desc ";

			$data['cetakk']= $this->Mainmodel->tampildatacollege($ambil);
			$data['cetakk2']= $this->Mainmodel->tampildatapoltek($ambil2);
			$data['group']= $this->Mainmodel->tampildatacollege($ambil3);
			}
			else if($kriteria=='target')
			{$data['nkriteria'] = "Perolehan Target PMB";
				$ambil				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30,kelompok FROM aplikanperolehannasional where tahunpmb='$tahun' 
				and direktorat='2' and tglkalkulasi='$tglkalkulasi' and kelompok='1' $stsmilik order by (((`regjunior`+`u-30`)/targetjunior)*100) desc";
				
				$ambil2				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30,targetd3,regd3 FROM aplikanperolehannasional where tahunpmb='$tahun' 
				and direktorat='3' and tglkalkulasi='$tglkalkulasi' $stsmilik order by (((`regjunior`+`u-30`)/targetjunior)*100) desc";
				
				// $ambil3				= "SELECT 
				// kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				// ,direktorat,tglupdate,`u-30` as u30,kelompok FROM aplikanperolehannasional where tahunpmb='$tahun' 
			    // and direktorat='2' and kelompok='4' order by kodecabang in 
				// (select kodecabang from cabang where kelompok='4' order by namacabang asc) ";
				$ambil3				="SELECT 
				aplikanperolehannasional.kodecabang, 
				aplikanperolehannasional.aplikan,
				aplikanperolehannasional.daftar,
				aplikanperolehannasional.ujian,
				aplikanperolehannasional.lulus,
				aplikanperolehannasional.regjunior,
				aplikanperolehannasional.regsenior,
				aplikanperolehannasional.targetjunior,
				aplikanperolehannasional.targetsenior,
				aplikanperolehannasional.tahunpmb,
				aplikanperolehannasional.direktorat,
				aplikanperolehannasional.tglupdate,
				aplikanperolehannasional.`u-30` as u30,
				aplikanperolehannasional.kelompok 
				
				FROM aplikanperolehannasional join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang 
				where aplikanperolehannasional.tahunpmb='$tahun' 
			    and aplikanperolehannasional.direktorat='2' and aplikanperolehannasional.kelompok='4' and tglkalkulasi='$tglkalkulasi' 
				$stsmilik 	order by cabang.namacabang asc,(((aplikanperolehannasional.`regjunior`+aplikanperolehannasional.`u-30`)/targetjunior)*100) desc ";

			$data['cetakk']= $this->Mainmodel->tampildatacollege($ambil);
			$data['cetakk2']= $this->Mainmodel->tampildatapoltek($ambil2);	
			$data['group']= $this->Mainmodel->tampildatacollege($ambil3);
		}else
			{$data['nkriteria'] = "";
				$data['cetakk']= "";
				$data['cetakk2']= "";
			}

		//ini untuk menampilkan detail
		$ambildetail	= "select presenter.Presenter,
		presenter.target,count(aplikan.id) as 'aplikan',
		sum((case when aplikan.Keterangan='1' then 1 else 0 END)) as 'Daftar',
		sum((case when aplikan.Ujian='1' then 1 else 0 END)) as 'Ujian',
		sum((case when aplikan.Lulus='1' then 1 else 0 END)) as 'Wawancara'
		from aplikan inner join presenter on aplikan.KdPresenter=presenter.KdPresenter
		where aplikan.ta='$tahun' and aplikan.kodecabang='$kdcabang' and presenter.target <>'0' group by aplikan.kdpresenter";
		$data['zz']= $this->Mainmodel->tampildatacollege($ambildetail);

		//cekkodecabang
		$ceks="select count(*) as jd from cabang where kodecabang='$kdcabang'";
		$datacol= $this->Mainmodel->tampildatacollege($ceks);
		foreach($datacol as $ada)
		{$adacabang=$ada->jd;}	

		if($adacabang==0)
		{$data['zz']= $this->Mainmodel->tampildatapoltek($ambildetail);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatapoltek($cek);
		}
		else
		{$data['zz']= $this->Mainmodel->tampildatacollege($ambildetail);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatacollege($cek);
		}
			
		foreach($datcek as $adacab)
		{$nmcabang=$adacab->namacabang;}

		$data['nmcabang'] = $kdcabang;
		$data['kriteria'] = $kriteria;
		$data['tahun'] = $tahun;
		$data['targetd3'] ="";
		// $data['u30'] =  $u30;

		$this->load->view('form/rekapPMBnew' ,$data);
	}

	function recap(){
		
		$kriteria			= $this->uri->segment(5);
		$tahun				= $this->uri->segment(3).'/'.$this->uri->segment(4);
		$kdcabang			= $this->uri->segment(6);
		$combo_thpmb		= $this->Drop_model->combo_pmbmkt();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_thpmb);
		//$data['list_kpi'] 	= $this->MainModel->tampildatacollege($pdata);
		//$krit=array('','Perolehan Jumlah Junior','Perolehan Target PMB');
		
			if($kriteria=='junior')
			{$data['nkriteria'] = "Perolehan Jumlah Junior";
				$ambil				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30,kelompok FROM aplikanperolehannasional where tahunpmb='$tahun' and direktorat='2' order by kelompok asc,(`regjunior`+`u-30`) desc";
				
				$ambil2				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30 ,targetd3,regd3 FROM aplikanperolehannasional where tahunpmb='$tahun' and direktorat='2' order by (`regjunior`+`u-30`) desc";
			
			$data['cetakk']= $this->Mainmodel->tampildatacollege($ambil);
			$data['cetakk2']= $this->Mainmodel->tampildatapoltek($ambil2);
			}
			else if($kriteria=='target')
			{$data['nkriteria'] = "Perolehan Target PMB";
				$ambil				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30,kelompok FROM aplikanperolehannasional where tahunpmb='$tahun' and direktorat='2' order by kelompok asc,(((`regjunior`+`u-30`)/targetjunior)*100) desc";
				
				$ambil2				= "SELECT 
				kodecabang, aplikan,daftar,ujian,lulus,regjunior,regsenior,targetjunior,targetsenior,tahunpmb
				,direktorat,tglupdate,`u-30` as u30,targetd3,regd3 FROM aplikanperolehannasional where tahunpmb='$tahun' and direktorat='2' order by (((`regjunior`+`u-30`)/targetjunior)*100) desc";
				
				$data['cetakk']= $this->Mainmodel->tampildatacollege($ambil);
				$data['cetakk2']= $this->Mainmodel->tampildatapoltek($ambil2);	
		}else
			{$data['nkriteria'] = "";
				$data['cetakk']= "";
				$data['cetakk2']= "";
			}

			// $cetak= $this-> MainModel->tampildatacollege($ambil);
			// $data['cetakk']= $this-> MainModel->tampildatacollege($ambil);
			// $data['cetakk2']= $this-> MainModel->tampildatapoltek($ambil2);	
		// foreach ($cetak as $cetaks ) {
		// 	$kodecabang=$cetaks->kodecabang;
		// 	$aplikan=$cetaks->aplikan;
		// 	$daftar=$cetaks->daftar;
		// 			$ujian=$cetaks->ujian;
		// 			$lulus=$cetaks->lulus;
		// 			$regjunior=$cetaks->regjunior;
		// 			$regsenior=$cetaks->regsenior;
		// 			$targetjunior=$cetaks->targetjunior;
		// 			$targetsenior=$cetaks->targetsenior;
		// 			$tahunpmb=$cetaks->tahunpmb;
		// 			$direktorat=$cetaks->direktorat;
		// 			$tglupdate=$cetaks->tglupdate;
					
		// 			$u30=$cetaks->u30;

		// 	}


		//ini untuk menampilkan detail
		$ambildetail	= "select presenter.Presenter,
		presenter.target,count(aplikan.id) as 'aplikan',
		sum((case when aplikan.Keterangan='1' then 1 else 0 END)) as 'Daftar',
		sum((case when aplikan.Ujian='1' then 1 else 0 END)) as 'Ujian',
		sum((case when aplikan.Lulus='1' then 1 else 0 END)) as 'Wawancara'
		from aplikan inner join presenter on aplikan.KdPresenter=presenter.KdPresenter
		where aplikan.ta='$tahun' and aplikan.kodecabang='$kdcabang' and presenter.target <>'0' group by aplikan.kdpresenter";
		$data['zz']= $this->Mainmodel->tampildatacollege($ambildetail);

		//cekkodecabang
		$ceks="select count(*) as jd from cabang where kodecabang='$kdcabang'";
		$datacol= $this->Mainmodel->tampildatacollege($ceks);
		foreach($datacol as $ada)
		{$adacabang=$ada->jd;}	

		if($adacabang==0)
		{$data['zz']= $this->Mainmodel->tampildatapoltek($ambildetail);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatapoltek($cek);
		}
		else
		{$data['zz']= $this->Mainmodel->tampildatacollege($ambildetail);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatacollege($cek);
		}
			
		foreach($datcek as $adacab)
		{$nmcabang=$adacab->namacabang;}

		$data['nmcabang'] = $kdcabang;
		$data['kriteria'] = $kriteria;
		$data['tahun'] = $tahun;
		// $data['kodecabang'] =$kodecabang ;
		// $data['aplikan'] =$aplikan ;
		// $data['daftar'] = $daftar;
		// $data['ujian'] = $ujian;
		// $data['lulus'] = $lulus;
		// $data['regjunior'] = $regjunior;
		// $data['regsenior'] = $regsenior;
		// $data['targetjunior'] = $targetjunior;
		// $data['targetsenior'] = $targetsenior;
		// $data['tahunpmb'] = $tahunpmb;
		// $data['direktorat'] = $direktorat;
		// $data['tglupdate'] = $tglupdate;
		$data['regd3'] ="" ;
		$data['targetd3'] ="";
		// $data['u30'] =  $u30;

		$data['konten'] = 'form/rekapPMBnew';
		$this->load->view('Dashboard',$data);
		// $this->load->view('home',$data);
	}

	public function simpanperolehannas(){
		$kriteria			= $this->input->post('kriteria');
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->input->post('tahun');

		 date_default_timezone_set("Asia/Jakarta");
			$datetime = date("Y-m-d h:i:s");
			$thn = substr($tahun,2,2);
			$bln = date('m');
			$periodekal=substr($datetime,0,7);
		
			//cek ada ga di tabel rekappmb	
		$cekapl="select count(*) as jd from aplikanperolehannasional where direktorat='2' and tahunpmb='$tahun' and substr(tglkalkulasi,1,7)='$periodekal' and kelompok='1'";
		$cekperolehcol = $this->Mainmodel->tampildatacollege($cekapl);	

		foreach ($cekperolehcol as $ceks ) {
			$ada=$ceks->jd;}

			if($ada<>0)
			{ 
				$kondisi = array('direktorat'=>'2','tahunpmb'=>$tahun, 'kelompok'=>'1', 'substr(tglkalkulasi,1,7)'=>$periodekal);				
				$table	 = "aplikanperolehannasional";
				$this->Mainmodel->del_datacoll($table,$kondisi);
			}			
			//cabang college----------------------------------------------------------------------------
		$cabng="select * from cabang where kelompok='1' and jeniskampus='col'";
		$cabank = $this->Mainmodel->tampildatacollege($cabng);
		if($cabank == false){
			die('Gagal ambil data: ' . mysql_error());
		}
		foreach ($cabank as $kdc ) 
		{
			# code...
		
			$kodecb=$kdc->kodecabang;
			$nmcabang=$kdc->namacabang;
			$kel=$kdc->kelompok;

			$perolehcoll="select kodecabang, count(*) as aplikan, 
			sum((case when (Keterangan='1')  then Keterangan else 0 end)) as 'daftar',
			sum((case when (Ujian='1') then Ujian else 0 end)) as 'ujian',
			sum((case when (Lulus='1')  then Lulus else 0 end)) as 'lulus',
			tglinput from aplikan where ta='$tahun' and kodecabang='$kodecb' group by kodecabang";
		
			$apl = $this->Mainmodel->tampildatacollege($perolehcoll);
			
			if(!$apl)
			{
				$totapl=0;
				$totdaftar=0;
				$totujian=0;
				$totlulus=0;
				$tglinput ='0000-00-00';

			}
			foreach ($apl as $aplikan ) 
			{	
				$totapl=$aplikan->aplikan;
				$totdaftar=$aplikan->daftar;
				$totujian=$aplikan->ujian;
				$totlulus=$aplikan->lulus;
				$tglinput=$aplikan->tglinput;
			}
			
			//target cabang-------------------------------------------------
			$target="select targetjunior, targetsenior,tahunpmb
			from perolehan where tahunpmb='$tahun' and kodecabang='$kodecb' group by kodecabang";
			$trg = $this->Mainmodel->tampildatacollege($target);
			
			if(!$trg)
			{$trgjuncol='0'; $trgsencol='0';}

			foreach ($trg as $targetnya ) {
				
				$trgjuncol=$targetnya->targetjunior;
				$trgsencol=$targetnya->targetsenior;
			}
			//registrasijunior
			$regjuncol="select sum((case when (status='Aktif') then 1 else 0 end)) as 'regjunior',
			sum((case when (status='U-30') then 1 else 0 end)) as 'junu30'
			from viewregistrasi where ta='$tahun' and kodecabang='$kodecb' group by kodecabang";
			$perjuncol = $this->Mainmodel->tampildatacollege($regjuncol);
			//echo $regjuncol;
			
			if(!$perjuncol)
			{$juncolaktif='0'; $juncolu30='0';}

			foreach ($perjuncol as $perolehjuncol ) {
				$juncolaktif=$perolehjuncol->regjunior;
				$juncolu30=$perolehjuncol->junu30;		
					
			}
			
			//registrasiSenior
			$regsencol="select kodecabang, 
			sum((case when (status='Aktif') then 1 else 0 end)) as 'regsenior'
			from viewregsenior where ta='$tahun' and kodecabang='$kodecb' group by kodecabang";
			$persencol = $this->Mainmodel->tampildatacollege($regsencol);
			
			if(!$persencol)
			{$sencolaktif='0';}	
			foreach ($persencol as $perolehsencol ) {
				$sencolaktif=$perolehsencol->regsenior;
			}
			

				//hitung total2nya
				$totaljunkotor=($juncolaktif*1)+($juncolu30*1);
				$totaltarget=($trgjuncol*1)+($trgsencol*1);
				$totalregisaktif=($totaljunkotor*1)+($sencolaktif*1);
				//echo $juncolaktif;
				//kondisi
				if($trgjuncol==0)
				{$rasiojunaktif=0;}else{$rasiojunaktif=(($juncolaktif*1)/($trgjuncol*1))*100;}
				if($trgsencol==0)
				{$rasiosenaktif=0;}else{$rasiosenaktif=(($sencolaktif*1)/($trgsencol*1))*100;}
				if($totaltarget==0)
				{$rasioall=0;}else{$rasioall=(($totalregisaktif*1)/($totaltarget*1))*100;}

				$data = array(
					"kodecabang"=>$kodecb,
					"aplikan"=>$totapl,
					"daftar"=>$totdaftar,
					"ujian"=>$totujian,
					"lulus"=>$totlulus,
					"regjunior"=>$juncolaktif,
					"regsenior"=>$sencolaktif,
					"targetjunior"=>$trgjuncol,
					"targetsenior"=>$trgsencol,
					"tahunpmb"=>$tahun,
					"direktorat"=>"2",
					"tglupdate"=>$tglinput,
					"u-30"=>$juncolu30,
					"kelompok"=>$kel,
					"tglkalkulasi"=>$datetime
				);
				$datagroup = array(
					"tglkalkulasi"=>$datetime
				);
				


				$where=array(
					"kelompok"=>4,
					"tahunpmb"=>$tahun,
					"direktorat"=>2,
					"substr(tglkalkulasi,1,7)"=>$periodekal
					 );
		$insert = $this->Mainmodel->set_data("aplikanperolehannasional",$data);
		$update = $this->Mainmodel->update_data($where,$datagroup,"aplikanperolehannasional","Coll");

		}
		//end college--------------------------------

		//cabang PLJ--------------------------------------------------------------------------------------------
		$cabng2="select * from cabang where kelompok='aktif'";
		$cabank2 = $this->Mainmodel->tampildatapoltek($cabng2);
		if($cabank2 == false){
			die('Gagal ambil data: ' . mysql_error());
		}
		
		//cek ada ga di tabel rekappmb	
		$cekapl2="select count(*) as jd2 from aplikanperolehannasional where direktorat='3' and substr(tglkalkulasi,1,7)='$periodekal' and tahunpmb='$tahun'";
		$cekperolehpol2 = $this->Mainmodel->tampildatapoltek($cekapl2);

		foreach ($cekperolehpol2 as $cekspol ) {
			$ada2=$cekspol->jd2;}
		
			if($ada2<>0)
			{ 
				$kondisi2 = array('direktorat'=>'3','tahunpmb'=>$tahun, 'substr(tglkalkulasi,1,7)'=>$periodekal);				
				$table2	 = "aplikanperolehannasional";
				//delete
				$this->Mainmodel->del_dataplj($table2,$kondisi2);
			}
		foreach ($cabank2 as $kdc2 ) 
		{
			# code...
		
			$kodecb2=$kdc2->kodecabang;
			$nmcabang=$kdc->namacabang;

			$perolehpol="select kodecabang, count(*) as aplikan, 
			sum((case when (Keterangan='1') then Keterangan else 0 end)) as 'daftar',
			sum((case when (Ujian='1') then Ujian else 0 end)) as 'ujian',
			sum((case when (Lulus='1') then Lulus else 0 end)) as 'lulus',
			tglinput from aplikan where ta='$tahun' and kodecabang='$kodecb2' group by kodecabang";
		
			$aplpol = $this->Mainmodel->tampildatapoltek($perolehpol);
			if(!$aplpol)
			{$totaplpol='0'; $totdaftarpol='0'; $totujianpol=0; $totluluspol=0;}
			
			foreach ($aplpol as $aplikanpol ) {
				
				$totaplpol=$aplikanpol->aplikan;
				$totdaftarpol=$aplikanpol->daftar;
				$totujianpol=$aplikanpol->ujian;
				$totluluspol=$aplikanpol->lulus;
				$tglinputpol=$aplikanpol->tglinput;
			}
			//target cabang-------------------------------------------------
			$targetpol="select targetjunior, targetsenior, targettingkat3
			from perolehan where tahunpmb='$tahun' and kodecabang='$kodecb2' group by kodecabang";
			$trgpol = $this->Mainmodel->tampildatapoltek($targetpol);
			if(!$trgpol)
			{$trgjunpol='0'; $trgsenpol='0'; $trgtk3pol=0;}

			foreach ($trgpol as $targetnyapol ) {
				
				$trgjunpol=$targetnyapol->targetjunior;
				$trgsenpol=$targetnyapol->targetsenior;
				$trgtk3pol=$targetnyapol->targettingkat3;

			}
			//registrasijunior
			$regjunpol="select
			sum((case when (status='Aktif') then 1 else 0 end)) as 'regjunior',
			sum((case when (status='U-30') then 1 else 0 end)) as 'jun_u30'
			from viewregistrasi where ta='$tahun' and kodecabang='$kodecb2' group by kodecabang";
			$perjunpol = $this->Mainmodel->tampildatapoltek($regjunpol);
			if(!$perjunpol)
			{$junaktifpol='0'; $junu30pol='0';}
			
			foreach ($perjunpol as $perolehjunpol ) {
				
				$junaktifpol=$perolehjunpol->regjunior;
				$junu30pol=$perolehjunpol->jun_u30;
			}

			//registrasiSenior
			$regsenpol="select kodecabang, 
			sum((case when (status='Aktif') then 1 else 0 end)) as 'regsenior'
			from viewregsenior where ta='$tahun' and kodecabang='$kodecb2' group by kodecabang";
			$persenpol = $this->Mainmodel->tampildatapoltek($regsenpol);
			if(!$persenpol)
			{$senaktifpol='0'; }

			foreach ($persenpol as $perolehsenpol ) {
				$senaktifpol=$perolehsenpol->regsenior;
			}

			//registrasiTINGKAT3
			$regtk3pol="select count(*) as regpoltek from regispoltek where ta='$tahun' and kodecabang='$kodecb2' group by kodecabang";
			$perstk3pol = $this->Mainmodel->tampildatapoltek($regtk3pol);
			if(!$perstk3pol)
			{$tk3polaktif='0'; }

			foreach ($perstk3pol as $peroletk3pol ) {
				$tk3polaktif=$peroletk3pol->regpoltek;
			}

			

				//hitung total2nya
				$totaljunkotorpol=$junaktifpol+$junu30pol;
				$totaltargetpol=$trgjunpol+$trgsenpol+$trgtk3pol;
				$totalregisaktif=$totaljunkotor+$senaktifpol+$tk3polaktif;

				//kondisi
				if($trgjunpol==0)
				{$rasiojunaktifpol=0;}else{$rasiojunaktifpol=(($junaktifpol*1)/($trgjunpol*1))*100;}
				if($trgsenpol==0)
				{$rasiosenaktifpol=0;}else{$rasiosenaktifpol=(($senaktifpol*1)/($trgsenpol*1))*100;}
				if($totaltargetpol==0)
				{$rasioallpol=0;}else{$rasioallpol=(($totalregisaktif*1)/($totaltargetpol*1))*100;}

				$data = array(
					"kodecabang"=>$kodecb2,
					"aplikan"=>$totaplpol,
					"daftar"=>$totdaftarpol,
					"ujian"=>$totujianpol,
					"lulus"=>$totluluspol,
					"regjunior"=>$junaktifpol,
					"regsenior"=>$senaktifpol,
					"targetjunior"=>$trgjunpol,
					"targetsenior"=>$trgsenpol,
					"tahunpmb"=>$tahun,
					"direktorat"=>"3",
					"tglupdate"=>$tglinput,
					"regd3"=>$tk3polaktif,
					"targetd3"=>$trgtk3pol,
					"u-30"=>$junu30pol,
					"tglkalkulasi"=>$datetime
					
				);

				//insert BARU
				$insert = $this->Mainmodel->set_datapol("aplikanperolehannasional",$data);

		}
		

		//tampilkan ke tampilan
		$data['kriteria'] = $kriteria;
		$data['tahun'] = $tahun;

		redirect('Marketing');
		
	}
	//end rekap PMB

	//DETAIL REKAP PMB-----------------------------------------
	function detail_rekappmb(){
		$kriteria			= $this->input->post('kriteria');
		$kdcabang			= $this->input->post('kocab');
		$tahun				= $this->input->post('tahun');
		$tipe				= $this->input->post('tipe');

		// $kriteria			= $this->uri->segment(5);
		// $kdcabang			= $this->uri->segment(6);
		// $tahun				= $this->uri->segment(3).'/'.$this->uri->segment(4);

		// $combo_thpmb		= $this->Drop_model->combo_pmbmkt();		
		// $data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_thpmb);

		// $ambil	= "select presenter.Presenter as 'presenter',
		// presenter.target as 'target',count(aplikan.id)  'aplikan',
		// sum((case when aplikan.Keterangan='1' then 1 else 0 END)) as 'Daftar',
		// sum((case when aplikan.Ujian='1' then 1 else 0 END)) as 'Ujian',
		// sum((case when aplikan.Lulus='1' then 1 else 0 END)) as 'Wawancara'
		// from aplikan inner join presenter on aplikan.KdPresenter=Presenter.KdPresenter
		// where aplikan.ta='$tahun' and aplikan.kodecabang='$kdcabang' and presenter.target <>'0' group by aplikan.kdpresenter";

		$ambil	= "select * from presenter where ta='$tahun' and kodecabang='$kdcabang' group by kdpresenter";
		$dpregis	= "select * from konfigurasi_dp_registrasi where tahunakademik='$tahun' and flag='1'";
		$dp= $this->Mainmodel->tampildatacollege($dpregis);

		foreach($dp as $dp)
		{$ket=$dp->keterangan;}	
		
		//cekkodecabang
		// $cek="select count(*) as jd from cabang where kodecabang='$kdcabang'";
		// $datacol= $this->Mainmodel->tampildatacollege($cek);
		// foreach($datacol as $ada)
		// {$adacabang=$ada->jd;}	

		// if($adacabang==0)
		if($tipe=='pol')
		{$data['pmb']= $this->Mainmodel->tampildatapoltek($ambil);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatapoltek($cek);
			//$data['pmb2']= $this-> MainModel->tampildatapoltek($ambil2);
		}
		else
		{$data['pmb']= $this->Mainmodel->tampildatacollege($ambil);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatacollege($cek);
			//$data['pmb2']= $this-> MainModel->tampildatacollege($ambil2);
		}
			
		foreach($datcek as $adacab)
		{$nmcabang=$adacab->namacabang;}

		$data['nmcabang'] = $nmcabang;
		$data['tahun'] = $tahun;
		$data['kdcabang'] = $kdcabang;
		$data['ket'] = $ket;
		$data['kriteria'] = $kriteria;
		$data['tipe'] = $tipe;
		// $data['konten'] = 'form/rekapPMBdetail';
		// $this->load->view('Dashboard',$data);
		$this->load->view('form/rekapPMBdetail',$data);

	}

	public function printRekapPmb(){
		$kriteria			= $this->uri->segment(5);
		$kdcabang			= $this->uri->segment(6);
		$tahun				= $this->uri->segment(3).'/'.$this->uri->segment(4);
		$combo_thpmb		= $this->Drop_model->combo_pmbmkt();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_thpmb);


		$ambil	= "select * from presenter where ta='$tahun' and kodecabang='$kdcabang'  group by kdpresenter";
		$dpregis	= "select * from konfigurasi_dp_registrasi where tahunakademik='$tahun' and flag='1'";
		$dp= $this->Mainmodel->tampildatacollege($dpregis);

		foreach($dp as $dp)
		{$ket=$dp->keterangan;}	
		
		//cekkodecabang
		$cek="select count(*) as jd from cabang where kodecabang='$kdcabang'";
		$datacol= $this->Mainmodel->tampildatacollege($cek);
		foreach($datacol as $ada)
		{$adacabang=$ada->jd;}	

		if($adacabang==0)
		{$data['pmb']= $this->Mainmodel->tampildatapoltek($ambil);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatapoltek($cek);
	
		}
		else
		{$data['pmb']= $this->Mainmodel->tampildatacollege($ambil);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatacollege($cek);
	
		}
			
		foreach($datcek as $adacab)
		{$nmcabang=$adacab->namacabang;}

		$data['nmcabang'] = $nmcabang;
		$data['tahun'] = $tahun;
		$data['kdcabang'] = $kdcabang;
		$data['ket'] = $ket;
		$data['kriteria'] = $kriteria;
		$data['konten'] = 'laporan/rekapdetailpmb';
		$this->load->view('laporan',$data);
	}

	function printpdfpmb(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('L');

		$kriteria			= $this->uri->segment(5);
		$kdcabang			= $this->uri->segment(6);
		$tahun				= $this->uri->segment(3).'/'.$this->uri->segment(4);
		$combo_thpmb		= $this->Drop_model->combo_pmbmkt();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_thpmb);


		$ambil	= "select * from presenter where ta='$tahun' and kodecabang='$kdcabang' and target <>'0' group by kdpresenter";
		$dpregis	= "select * from konfigurasi_dp_registrasi where tahunakademik='$tahun' and flag='1'";
		$dp= $this->Mainmodel->tampildatacollege($dpregis);

		foreach($dp as $dp)
		{$ket=$dp->keterangan;}	
		
		//cekkodecabang
		$cek="select count(*) as jd from cabang where kodecabang='$kdcabang'";
		$datacol= $this->Mainmodel->tampildatacollege($cek);
		foreach($datacol as $ada)
		{$adacabang=$ada->jd;}	

		if($adacabang==0)
		{$data['pmb']= $this->Mainmodel->tampildatapoltek($ambil);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatapoltek($cek);
	
		}
		else
		{$data['pmb']= $this->Mainmodel->tampildatacollege($ambil);
			$cek="select * from cabang where kodecabang='$kdcabang'";
			$datcek= $this->Mainmodel->tampildatacollege($cek);
	
		}
			
		foreach($datcek as $adacab)
		{$nmcabang=$adacab->namacabang;}

		$data['nmcabang'] = $nmcabang;
		$data['tahun'] = $tahun;
		$data['kdcabang'] = $kdcabang;
		$data['ket'] = $ket;
		$data['kriteria'] = $kriteria;
		$data['konten'] = 'laporan/pmbdetailpdf';
		// $this->load->view('laporan',$data);
		$html = $this->load->view('laporan', $data, TRUE);

		//generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login');
	}

	public function detailCab(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('kriteria');
		$ket = $this->input->post('ket');
		$jeniscabang = $this->input->post('jeniscabang');
		$keterangan = $this->input->post('keterangan');

		if($kriteria == 'Coll' || $kriteria == 'group'){
			// cari tgl kalkulasi
			$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahun' and kelompok='1' and direktorat='2' order by tglkalkulasi desc limit 1";
			$tglkal = $this->Mainmodel->tampildatacollege($gettglkalkulasi);
		}else{
			// cari tgl kalkulasi
			$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahun' and direktorat='3' order by tglkalkulasi desc limit 1";
			$tglkal = $this->Mainmodel->tampildatapoltek($gettglkalkulasi);
		}
		
			foreach($tglkal as $tglnya){
				$datekal = $tglnya->tglkalkulasi;
			}

			
		if($jeniscabang == 'milker'){
			$data['u30'] = $this->Mainmodel->pmbcabangmilker($kriteria,$tahun,$datekal)->result();
			$data['label'] = "EWS PEROLEHAN PMB CABANG MILKER";
		}else{
			$data['u30'] = $this->Mainmodel->pmbcabang($kriteria,$tahun,$datekal)->result();
			$data['label'] = "EWS PEROLEHAN PMB CABANG NASIONAL";
		}


		$data['keterangan'] = $keterangan;
		$data['tahun'] = $tahun;
		$data['ket'] = $ket;
		$data['jeniscabang'] = $jeniscabang;
		$data['kriteria'] = $kriteria; 
		$data['tglkalkulasi'] = $datekal;

		if($keterangan == 'U-30'){
			$this->load->view('form/marketing/pmb', $data);
		}else{
			$this->load->view('form/marketing/pmb_aktif', $data);
		}

		
		
	}


	public function detailMhsPmb(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('kriteria');
		$ket = $this->input->post('ket');
		$jeniscabang = $this->input->post('jeniscabang');
		$keterangan = $this->input->post('keterangan');
		$kocab = $this->input->post('kocab');

		if($kriteria == 'Coll' || $kriteria == 'group'){
			// cari tgl kalkulasi
			$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahun' and kelompok='1' and direktorat='2' order by tglkalkulasi desc limit 1";
			$tglkal = $this->Mainmodel->tampildatacollege($gettglkalkulasi);
		}else{
			// cari tgl kalkulasi
			$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahun' and direktorat='3' order by tglkalkulasi desc limit 1";
			$tglkal = $this->Mainmodel->tampildatapoltek($gettglkalkulasi);
		}
	
			foreach($tglkal as $tglnya){
				$datekal = $tglnya->tglkalkulasi;
			}

		$getCabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($getCabang as $rcabang);
		$namacabang = $rcabang->namacabang;


		$data['aplikan'] = $this->Mainmodel->pmbMhs($kriteria,$keterangan,$tahun,$keterangan,$kocab)->result();
		$data['label'] = "EWS PEROLEHAN PMB ".substr($namacabang,7,50);

		$data['keterangan'] = $keterangan;
		$data['tahun'] = $tahun;
		$data['ket'] = $ket;
		$data['jeniscabang'] = $jeniscabang;
		$data['kriteria'] = $kriteria; 
		$data['tglkalkulasi'] = $datekal;
		$data['kocab'] = $kocab;


		$this->load->view('form/marketing/pmb_mhs', $data);

	}

	public function detailMhsPmbJurusan(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('kriteria');
		$ket = $this->input->post('ket');
		$jeniscabang = $this->input->post('jeniscabang');
		$keterangan = $this->input->post('keterangan');
		$kocab = $this->input->post('kocab');
		$kojur = $this->input->post('kojur');

		if($kriteria == 'Coll' || $kriteria == 'group'){
			// cari tgl kalkulasi
			$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahun' and kelompok='1' and direktorat='2' order by tglkalkulasi desc limit 1";
			$tglkal = $this->Mainmodel->tampildatacollege($gettglkalkulasi);
		}else{
			// cari tgl kalkulasi
			$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahun' and direktorat='3' order by tglkalkulasi desc limit 1";
			$tglkal = $this->Mainmodel->tampildatapoltek($gettglkalkulasi);
		}
	
			foreach($tglkal as $tglnya){
				$datekal = $tglnya->tglkalkulasi;
			}

		$getCabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($getCabang as $rcabang);
		$namacabang = $rcabang->namacabang;

		$data['aplikan'] = $this->Mainmodel->pmbMhsJurusan($kriteria,$keterangan,$tahun,$keterangan,$kocab,$kojur)->result();
		$data['label'] = "EWS PEROLEHAN PMB ".strtoupper($namacabang);

		$data['keterangan'] = $keterangan;
		$data['tahun'] = $tahun;
		$data['ket'] = $ket;
		$data['jeniscabang'] = $jeniscabang;
		$data['kriteria'] = $kriteria; 
		$data['tglkalkulasi'] = $datekal;
		$data['kocab'] = $kocab;


		$this->load->view('form/marketing/pmb_mhs_jurusan', $data);
	}

	
}



