<?php
class Cnp extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Mainmodel');
		$this->load->library('session');
		if($this->session->userdata('status') !== 'masuk'){
 			redirect('Login');
 		}

 		$cookie = get_cookie('sesilogin');
 		// jika cookie tidak ada maka akan pindah ke logout
        if($cookie == false){
           redirect('Main/logout');
           }

         delete_cookie('sesilogin');

         $nilai	=  $this->session->userdata('username');
 	     $waktu  =  1800;
 		 set_cookie('sesilogin',$nilai,$waktu);
	}

	function wajibKerjaCabang(){
		
		$kriteria = $this->input->post('kriteria');
		$jeniscabang = $this->input->post('jeniscabang');
		$tahun = $this->input->post('tahun');
		$jenis = $this->input->post('jenis');
	
		if($jeniscabang == "milker"){
			$data['label'] = "Perolehan realisasi Kerja CABANG MILKER";
			if($kriteria == "Coll" || $kriteria == "group"){
				$tahun = $this->input->post('tahun');
				$where = "select * from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2') order by namacabang asc";
				$data['cabangcnp'] = $this->Mainmodel->tampildatacollege($where);
			}else{
				$tahun = $this->input->post('tahun');
				$where = "select * from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2') order by namacabang asc";
		 		$data['cabangcnp'] = $this->Mainmodel->tampildatapoltek($where);
			}
			
		}else{
			$data['label'] = "Perolehan realisasi Kerja CABANG NASIONAL";
			if($kriteria == "Coll" || $kriteria == "group"){
				$tahun = $this->input->post('tahun');
				$where = "select * from cabang where kelompok='1' order by namacabang asc";
				$data['cabangcnp'] = $this->Mainmodel->tampildatacollege($where);
			}else{
				$tahun = $this->input->post('tahun');
				$where = "select * from cabang where kelompok='aktif' order by namacabang asc";
		 		$data['cabangcnp'] = $this->Mainmodel->tampildatapoltek($where);
			}
		}


		$data['kriteria'] = $kriteria;
		$data['jeniscabang'] = $jeniscabang;
		$data['tahun'] = $tahun;
		$data['jenis'] = $jenis;

		$this->load->view('form/cnp/cabang_penempatan', $data);
	}

	public function detailPenempatanMhs(){
		$kriteria = $this->input->post("kriteria"); 
		$tahun = $this->input->post("tahun");
		$jenis = $this->input->post("jenis");
		$kocab = $this->input->post("kocab");
		$jeniscabang = $this->input->post("jeniscabang");
		$table = "view_kandidatpenempatan".$kocab;

		$cabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($cabang as $row);

		if($jenis == "wajib"){
			$data['penempatan'] = $this->Mainmodel->mhsWajib($kriteria,$tahun,$table)->result();
			$data['label'] = "DETAIL PENEMPATAN WAJIB KERJA ".$row->namacabang;
		}else{
			$data['penempatan'] = $this->Mainmodel->mhsDibantu($kriteria,$tahun,$table)->result();
			$data['label'] = "DETAIL PENEMPATAN DIBANTU KERJA ".$row->namacabang;
		}

		
		$data['kriteria'] = $kriteria;
		$data['tahun'] = $tahun;
		$data['jenis'] = $jenis;
		$data['jeniscabang'] = $jeniscabang;
		$data['kocab'] = $kocab;

		$this->load->view("form/cnp/mhs_penempatan",$data);
	}

	public function wajibKerjaJurusan(){
		$kriteria = $this->input->post("kriteria"); 
		$tahun = $this->input->post("tahun");
		$jenis = $this->input->post("jenis");
		$kocab = $this->input->post("kocab");
		$jeniscabang = $this->input->post("jeniscabang");
		$kojur = $this->input->post("kojur");
		$table = "view_kandidatpenempatan".$kocab;

		$cabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($cabang as $row);

		if($jenis == "wajib"){
			$data['penempatan'] = $this->Mainmodel->mhsWajibJurusan($kriteria,$tahun,$table,$kojur)->result();
			$data['label'] = "DETAIL PENEMPATAN WAJIB KERJA ".$row->namacabang;
		}else{
			$data['penempatan'] = $this->Mainmodel->mhsDibantuJurusan($kriteria,$tahun,$table,$kojur)->result();
			$data['label'] = "DETAIL PENEMPATAN DIBANTU KERJA ".$row->namacabang;
		}

		
		$data['kriteria'] = $kriteria;
		$data['tahun'] = $tahun;
		$data['jenis'] = $jenis;
		$data['jeniscabang'] = $jeniscabang;
		$data['kocab'] = $kocab;

		$this->load->view("form/cnp/mhs_penempatan_jurusan",$data);
	}
}


?>