<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HRD extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model("Drop_model");
		$this->load->model("Mainmodel");
		$this->load->library('session');
		
		if($this->session->userdata('status') !== 'masuk'){
 			redirect('Login');
 		}

 		$cookie = get_cookie('sesilogin');
 		// jika cookie tidak ada maka akan pindah ke logout
        if($cookie == false){
           redirect('Main/logout');
           }

         delete_cookie('sesilogin');

         $nilai	=  $this->session->userdata('username');
 	     $waktu  =  1800;
 		 set_cookie('sesilogin',$nilai,$waktu);
	}

	function index(){
        $data['konten'] = 'form/hrd/karyawan';
		$this->load->view('Dashboard',$data);
    }

    function bagian(){
        $kriteria = $this->input->post("kriteria");
        if($kriteria == "PST"){
            $bagian = $this->Mainmodel->tampildatacollege("select * from kodepusat_bagian");
            echo "<option value=''>-- Pilih Bagian --</option>";
            foreach($bagian as $row){
                echo "<option value='$row->idbagian'>$row->namabagian</option>";
            }
        }else{
            $bagian = $this->Mainmodel->tampildatacollege("select * from kategori_jabatan");
            echo "<option value=''>-- Pilih Bagian --</option>";
            foreach($bagian as $row){
                echo "<option value='$row->kodejabatan'>$row->namajabatan</option>";
            }
        }
    }

    function carikaryawan(){
        $kriteria = $this->input->post("kriteria");
        $status = $this->input->post("status");
        $bagian = $this->input->post("bagian");
        $kocab = $this->input->post("kocab");

        if($kriteria == "PST"){
            if($status == "Aktif" || $status == "Tidak Aktif"){

                $query1 = "select * from profilkaryawan where bagian='$bagian' and cabang='$kriteria' and statuskerja='$status'";
                $data["karyawan"] = $this->Mainmodel->tampildatacollege($query1);

                // $data["karyawan"] = $this->Mainmodel->getWheres("profilkaryawan",array("statuskerja"=>$status,"bagian"=>$bagian,"cabang"=>$kriteria),"Coll")->result();
                $data['label'] = "list karyawan pusat ".$status;
            }else{

                $query1 = "select * from profilkaryawan where bagian='$bagian' and cabang='$kriteria'";
                $data["karyawan"] = $this->Mainmodel->tampildatacollege($query1);
                // $data["karyawan"] = $this->Mainmodel->getWheres("profilkaryawan",array("bagian"=>$bagian,"cabang"=>$kriteria),"Coll")->result();
                $data['label'] = "list karyawan pusat aktif dan tidak aktif";
            }
        }elseif($kriteria == "milker"){
                if($status == "Aktif" || $status == "Tidak Aktif"){
                    $query1 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where (kepemilikan='1' or kepemilikan='2') and kelompok='4') and statuskerja='$status' and bagian like '%$bagian%' and cabang='$kocab' order by cabang";
                    $data["karyawanpts1"] = $this->Mainmodel->tampildatacollege($query1);

                    $query2 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where (kepemilikan='1' or kepemilikan='2') and kelompok='aktif') and statuskerja='$status' and bagian like '%$bagian%' and cabang='$kocab' order by cabang";
                    $data["karyawanpts2"] = $this->Mainmodel->tampildatapoltek($query2);
                    
                    $query3 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where (kepemilikan='1' or kepemilikan='2') and kelompok='1') and statuskerja='$status' and bagian like '%$bagian%' and cabang='$kocab' order by cabang";
                    $data["karyawancoll"] = $this->Mainmodel->tampildatacollege($query3);
                    $data['label'] = "list karyawan milker ".$status;
                }else{
                    $query1 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where (kepemilikan='1' or kepemilikan='2') and kelompok='4') and bagian like '%$bagian%' order by cabang";
                    $data["karyawanpts1"] = $this->Mainmodel->tampildatacollege($query1);

                    $query2 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where (kepemilikan='1' or kepemilikan='2') and kelompok='aktif') and bagian like '%$bagian%' order by cabang";
                    $data["karyawanpts2"] = $this->Mainmodel->tampildatapoltek($query2);
                    
                    $query3 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where (kepemilikan='1' or kepemilikan='2') and kelompok='1') and bagian like '%$bagian%' order by cabang";
                    $data["karyawancoll"] = $this->Mainmodel->tampildatacollege($query3);
                    $data['label'] = "list karyawan milker aktif dan tidak aktif";
                }

                
        }elseif($kriteria == "Coll"){
           if($status == "Aktif" || $status == "Tidak Aktif"){
                $query1 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where kelompok='1') and bagian like '%$bagian%' and statuskerja='$status' order by cabang";
                    $data["karyawan"] = $this->Mainmodel->tampildatacollege($query1);
                $data['label'] = "list karyawan college ".$status;
            }else{
                $query1 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where kelompok='1') and bagian like '%$bagian%' order by cabang";
                    $data["karyawan"] = $this->Mainmodel->tampildatacollege($query1);
                $data['label'] = "list karyawan college aktif dan tidak aktif";
            }
        }else{
           if($status == "Aktif" || $status == "Tidak Aktif"){
                $query1 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where kelompok='4') and bagian like '%$bagian%' and statuskerja='$status' order by cabang";
                    $data["karyawan1"] = $this->Mainmodel->tampildatacollege($query1);

                $query2 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where kelompok='aktif') and bagian like '%$bagian%' and statuskerja='$status' order by cabang";
                    $data["karyawan2"] = $this->Mainmodel->tampildatapoltek($query2);
                
                $data['label'] = "list karyawan PTS ".$status;
            }else{
                $query1 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where kelompok='4') and bagian like '%$bagian%' order by cabang";
                    $data["karyawan1"] = $this->Mainmodel->tampildatacollege($query1);

                $query2 = "select * from profilkaryawan where cabang in(select kodecabang from cabang where kelompok='aktif') and bagian like '%$bagian%' order by cabang";
                    $data["karyawan2"] = $this->Mainmodel->tampildatapoltek($query2);

                $data['label'] = "list karyawan PTS aktif dan tidak aktif";
            } 
        }

        $data['kriteria'] = $kriteria;
        $data['status'] = $status;
        $data['bagian'] = $bagian;
		$this->load->view('form/hrd/hasil_karyawan',$data);
    }

    function cabangmilker(){
        $kriteria = $this->input->post("kriteria");
        if($kriteria == "milker"){
           $col = $this->Mainmodel->tampildatacollege("select * from cabang where (kelompok='1' or kelompok='4') and (kepemilikan='1' or kepemilikan='2')");
            echo "<option>-- Pilih Cabang --</option>";
            foreach($col as $row){
                $nama = substr($row->namacabang,7,40);
            echo "<option value='$row->kodecabang'>$nama</option>";
            }

            $pol = $this->Mainmodel->tampildatapoltek("select * from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')");
            foreach($pol as $row2){
                $nama2 = substr($row2->namacabang,7,40);
                echo "<option value='$row2->kodecabang'>$nama2</option>";
            }
        }

    }


}
