<style type="text/css">
    .note-warning ol li{
        font-size: 13px;
    }
</style>
<div class="portlet box blue-hoki">
     <div class="portlet-title">
         <div class="caption">
             <i class="fa fa-gift"></i>Rekapitulasi PMB</div>
     </div> 

     <form method="POST" action="<?=base_url();?>index.php/Marketing/simpanperolehannas">
            <div class="form-body">
            <div class="portlet light bordered">
                 <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                            <div class="form-group">
                                <label class="col-md-5 control-label">Kriteria</label>
                                <div class="col-md-7">
                                <select name="kriteria" id="kriteria" class="form-control" required="required">
                                <?php
                                                            $krit=array('','Perolehan Jumlah Junior','Perolehan Target PMB');
                                                            $alias=array('','junior','target');
                                                            for($i=1; $i<=2; $i++)
                                                            {
                                                                $kritpmb=$krit[$i];    
                                                        ?>
                                                            <option value="<?=$alias[$i];?>" title='<?=$alias[$i];?>'><?=$kritpmb;?></option>
                                                        <?php }?>
                                </select>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="form-group" style="margin-top: 10px !important">
                                <label class="col-md-5 control-label">Tahun PMB</label>
                                <div class="col-md-7">
                                <?php
                                    $js = 'class="form-control" style="width:200px" ';
                                     echo form_dropdown('tahun', $cmbtahun, '', $js);
                                ?>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="form-group" style="margin-top: 10px !important">
                                <label class="col-md-5 control-label"></label>
                                <div class="col-md-4">
                                    <input type="submit" value="Tampilkan" name="cari" id="modal" class="btn red-thunderbird">
                                </div>
                                <div class="col-md-4">
                                    <input type="submit" value="kalkulasi" name="kalkulasi" id="kalkulasi" class="btn red-thunderbird">
                                </div>
                            </div>
                            </div>
                    
                        </div>
                        <div class="col-md-6">
                            <div class="note note-warning">
                                <ol>
                                    <li>R. = Realisasi</li>
                                    <li>Rasio TK1 = Diambil dari total registrasi dengan DP (diatas & dibawah 30%)</li>
                                    <li>Target TK1 NAS = Target Tingkat 1 Nasional Sesuai Keputusan Rakernas</li>
                                    <li>Target TK1 CAB = Target Cabang Per-Presenter</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
            </div>
        </form>
</div>   
                
            
              <br>                          
            <!-- <?php foreach($nambidang as $nambid);
                        ?> -->
                <span style="font-size: 16px">Diurutkan dari <b><?=$nkriteria?></b>- Tahun <b><?php echo $tahun;?></b></span>
            <div class="portlet-body flip-scroll" style="margin-top:20px">
                                       <table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>No</th>
                                                    <th>Nama Cabang</th>
                                                    <th>Target TK1 Nas</th>
                                                    <th>R.TK1</th>
                                                    <th>Rasio TK1</th>
                                                    <th>Target TK2</th>
                                                    <th>R.TK2</th>
                                                    <th>Rasio TK2</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $n=0;
                                            $ttargetjunior =0;  
                                            $tregjunior =0;
                                            $ttargetsenior =0;
                                            $tregsenior =0;
                                            $registk1=0;
                                            // if(!$cetakk){
                                            //     $kodecabang="";
                                            //     $aplikan=0;
                                            //     $daftar=0;
                                            //             $ujian=0;
                                            //             $lulus=0;
                                            //             $regjunior=0;
                                            //             $regsenior=0;
                                            //             $targetjunior=0;
                                            //             $targetsenior=0;
                                            //             $tahunpmb="";
                                            //             $direktorat=2;
                                            //             $tglupdate="";
                                            //             $rasiotk1=0;
                                            //             $rasiotk2=0;
                                            //             $u30=$cetakss->u30;}
                                            if($cetakk == false){
                                                die('Tidak ada data: ' . mysql_error());
                                            }   
                                            foreach ($cetakk as $cetakss ) {
                                                $n++;
                                                $kodecabang=$cetakss->kodecabang;
                                                $aplikan=$cetakss->aplikan;
                                                $daftar=$cetakss->daftar;
                                                        $ujian=$cetakss->ujian;
                                                        $lulus=$cetakss->lulus;
                                                        $regjunior=$cetakss->regjunior;
                                                        $regsenior=$cetakss->regsenior;
                                                        $targetjunior=$cetakss->targetjunior;
                                                        $targetsenior=$cetakss->targetsenior;
                                                        $tahunpmb=$cetakss->tahunpmb;
                                                        $direktorat=$cetakss->direktorat;
                                                        $tglupdate=$cetakss->tglupdate;
                                                        $kelompok=$cetakss->kelompok;

                                                        $u30=$cetakss->u30;
                                                        $registk1=($regjunior*1)+($u30*1);

                                                        if($targetjunior==0)
                                                       {$rasiotk1=0;}else {$rasiotk1=(($regjunior+$u30)/($targetjunior*1))*100;}
                                                    
                                                        if($targetsenior=='0')
                                                        {$rasiotk2=0;}else {$rasiotk2= ($regsenior/$targetsenior)*100;} 

                                                            if($rasiotk1 >= 80){
                                                                $tcolor='bg-green-jungle bg-font-green-jungle';
                                                                $fontcol="#ffffff";
                                                            }elseif($rasiotk1 >= 50){
                                                                $tcolor="bg-yellow-crusta bg-font-yellow-crusta";
                                                                $fontcol="#000000";
                                                            }elseif($rasiotk1 <= 49){
                                                                $tcolor='bg-red-thunderbird bg-font-red-thunderbird';
                                                                $fontcol="#ffffff";
                                                            }
                                                            else
                                                            { $tcolor='bg-grey-gallery bg-font-grey-gallery'; $fontcol="#ffffff";}  

                                                                if($rasiotk2 >= 80){
                                                                    $tcolor2="bg-green-jungle bg-font-green-jungle";
                                                                    $fontcol2="#ffffff";
                                                                }elseif($rasiotk2 >= 50){
                                                                    $tcolor2="g-yellow-crusta bg-font-yellow-crusta";
                                                                    $fontcol2="#000000";
                                                                }elseif($rasiotk2 <= 49){
                                                                    $tcolor2="bg-red-thunderbird bg-font-red-thunderbird";
                                                                    $fontcol2="#ffffff";
                                                                }else
                                                                { $tcolor2="bg-grey-gallery bg-font-grey-gallery"; $fontcol2="#ffffff";}

                                                        if($kelompok=='1')
                                                        {
                                                            $action='detail';
                                                            $moda='#basic';
                                                            $link = base_url('index.php/Marketing/detail_rekappmb/'.$tahun.'/'.$kriteria.'/'.$kodecabang);
                                                            $linkk = '<a class="btn red btn-outline bold" href="'.$link.'">'.$action.'</a>';
                                                        }else
                                                        {
                                                            $action='';
                                                            $moda='#';
                                                            $link='#';
                                                            $linkk='';
                                                        }
                                                        $namacab="select * from cabang where kodecabang='$kodecabang'";
			                                            $nmcab= $this->Mainmodel->tampildatacollege($namacab);
                                                        foreach ($nmcab as $nmcab ) {
                                                            $nama=$nmcab->namacabang;
                                                          

                                            ?>    
                                                <tr >
                                                    <td><?php echo $n;?></td>
                                                    <td><?php echo substr($nama,6);?> </td>
                                                    <td><?php echo $targetjunior;?></td>
                                                    <td><?php echo $registk1;?> </td>
                                                    <td class="<?php echo $tcolor;?>" width='80px'><b><font color="<?=$fontcol?>"><?php echo number_format($rasiotk1)."%";?></font></b></td>
                                                    <td> <?=$targetsenior;?></td>
                                                    <td><?=$regsenior?></td>
                                                    <td class="<?php echo $tcolor2?>" width='80px'><b><font color="<?=$fontcol2?>"><?php echo number_format($rasiotk2)."%";?></font></b> </td>
                                                    <td><?php echo $linkk?></td>
                                                    
                                                </tr><?php
                                                   $ttargetjunior += $targetjunior;  
                                                   $tregjunior +=$registk1;
                                                   $ttargetsenior +=$targetsenior;
                                                   $tregsenior +=$regsenior;
                                                   
                                                  if($ttargetjunior==0)
                                                  {$trasiotk1=0;}else{$trasiotk1 =($tregjunior/$ttargetjunior)*100; }
                                                  if($ttargetsenior==0)
                                                  {$trasiotk2=0;}else{$trasiotk2 =($tregsenior/$ttargetsenior)*100; } 
                                                   
                                                   $rastk1=number_format($trasiotk1,1);
                                                   $rastk2=number_format($trasiotk2,1);
                                                    if($n==30)
                                                    {
                                                        echo "<tr class='bg-grey bg-font-grey'>
                                                        <td > <b>*</b></td>
                                                        <td ><b>Total</b> </td>                               
                                                        <td><b>$ttargetjunior</b></td>
                                                        <td><b>$tregjunior</b></td>
                                                        <td><b>$rastk1%</b></td>
                                                        <td><b> $ttargetsenior</b></td>
                                                        <td><b>$tregsenior</b></td>
                                                        <td><b>$rastk2%</b></td>
                                                        <td>*</td>
                                                    
                                                        </tr>";

                                                        echo "<tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                        <td colspan='9' ><b>GROUP</b></td>                                                
                                                        ";
                                                        echo "
                                                        <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                        <th>No</th>
                                                        <th>Nama Institusi</th>
                                                        <th>Target TK1 NAS</th>
                                                        <th>R.TK1</th>
                                                        <th>Rasio TK1</th>
                                                        <th>Target TK2</th>
                                                        <th>R.TK2</th>
                                                        <th>Rasio TK2</th>
                                                        <th>Action</th>
                                                    </tr>";
                                                    $ttargetjunior =0;  
                                                   $tregjunior =0;
                                                   $trasiotk1 =0; 
                                                   $ttargetsenior=0;
                                                   $ttregsenior=0;
                                                   $trasiotk2=0;
                                                    }
                                                }   
                                                

                                            };
                                            
                                            ?>
                                            <tr class='bg-grey bg-font-grey'>
                                                    <td > <b>*</b></td>
                                                    <td ><b>Total</b> </td>                               
                                                    <td><b><?=$ttargetjunior?></b></td>
                                                    <td><b><?=$tregjunior?></b></td>
                                                    <td><b><?=number_format($trasiotk1,1)."%"?></b></td>
                                                    <td><b> <?=$ttargetsenior?></b></td>
                                                    <td><b><?=$ttregsenior?></b></td>
                                                    <td><b><?=number_format($trasiotk2,1)."%"?></b></td>
                                                    <td>*</td>
                                                    
                                                </tr>
                                            <?php    
                                            if($cetakk2 == false){
                                                die('Tidak ada data: ' . mysql_error());
                                            }else
                                            {
                                                echo "<tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <td colspan=9 ><b>POLITEKNIK LP3I JAKARTA</b></td>                                                
                                                    ";
                                                    echo "
                                                    <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>No</th>
                                                    <th>Nama Kampus</th>
                                                    <th>Target TK1 NAS</th>
                                                    <th>R.TK1</th>
                                                    <th>Rasio TK1</th>
                                                    <th>Target TK2</th>
                                                    <th>R.TK2</th>
                                                    <th>Rasio TK2</th>
                                                    <th>Action</th>
                                                </tr>";
                                                $n2=0;
                                                $ttargetjuniorpol =0; 
                                               $tregjuniorpol =0;
                                                $ttargetseniorpol =0;
                                                $tregseniorpol =0;
                                                $registk1pol=0;
                                                foreach ($cetakk2 as $cetakss2 ) {
                                                    $n2++;
                                                    $kodecabang2=$cetakss2->kodecabang;
                                                    $aplikan2=$cetakss2->aplikan;
                                                    $daftar2=$cetakss2->daftar;
                                                            $ujian2=$cetakss2->ujian;
                                                            $lulus2=$cetakss2->lulus;
                                                            $regjunior2=$cetakss2->regjunior;
                                                            $regsenior2=$cetakss2->regsenior;
                                                            $targetjunior2=$cetakss2->targetjunior;
                                                            $targetsenior2=$cetakss2->targetsenior;
                                                            $tahunpmb2=$cetakss2->tahunpmb;
                                                            $direktorat2=$cetakss2->direktorat;
                                                            $tglupdate2=$cetakss2->tglupdate;
                                                            $targettk3=$cetakss2->targetd3;
                                                            $registk3=$cetakss2->regd3;
                                                            $u30pol=$cetakss2->u30;

                                                            $registk1pol=($regjunior2+$u30pol)*1;
                                                            if($targetjunior2==0)
                                                        {$rasiotk1pol=0;}else {$rasiotk1pol=(($regjunior2+$u30pol)/($targetjunior2*1))*100;}
                                                        
                                                            if($targetsenior2=='0')
                                                            {$rasiotk2pol=0;}else {$rasiotk2pol= ($regsenior2/$targetsenior2)*100;} 

                                                                if($rasiotk1pol >= 80){
                                                                    $tcolorpol='bg-green-jungle bg-font-green-jungle';
                                                                    $fontpol="#ffffff";
                                                                }elseif($rasiotk1pol >= 50){
                                                                    $tcolorpol="g-yellow-crusta bg-font-yellow-crusta";
                                                                    $fontpol="#000000";                                                                
                                                                }elseif($rasiotk1pol <= 49){
                                                                    $tcolorpol='bg-red-thunderbird bg-font-red-thunderbird';
                                                                    $fontpol="#ffffff";                                                                
                                                                }
                                                                else
                                                                { $tcolorpol='bg-grey-gallery bg-font-grey-gallery'; $fontpol="#ffffff";}  

                                                                    if($rasiotk2pol >= 80){
                                                                        $tcolor2pol="bg-green-jungle bg-font-green-jungle";
                                                                        $fontpol2="#ffffff";
                                                                    }elseif($rasiotk2pol >= 50){
                                                                        $tcolor2pol="g-yellow-crusta bg-font-yellow-crusta";
                                                                        $fontpol2="#000000";
                                                                    }elseif($rasiotk2pol <= 49){
                                                                        $tcolor2pol="bg-red-thunderbird bg-font-red-thunderbird";
                                                                        $fontpol2="#ffffff";
                                                                    }else
                                                                    { $tcolor2pol="bg-grey-gallery bg-font-grey-gallery";  $fontpol2="#ffffff";                                                                    }
                                                                    
                                                                        //link url detail
                                                                        $action='detail';
                                                                        $moda='#basic';
                                                                        $link = base_url('index.php/Marketing/detail_rekappmb/'.$tahun.'/'.$kriteria.'/'.$kodecabang2);
                                                                        $linkk = '<a class="btn red btn-outline bold" href="'.$link.'">'.$action.'</a>';
                                                                   

                                                            $namacabpol="select * from cabang where kodecabang='$kodecabang2'";
                                                            $nmcabpol= $this->Mainmodel->tampildatapoltek($namacabpol);
                                                            foreach ($nmcabpol as $nmcabpol ) {
                                                                $namapol=$nmcabpol->namacabang;
                                            ?>
                                                <tr>
                                                <td><?=$n2?></td>
                                                    <td><?=substr($namapol,6)?></td>
                                                    <td><?=$targetjunior2;?></td>
                                                    <td><?=$registk1pol;?> </td>
                                                    <td class="<?=$tcolorpol?>" width='80px'><b><font color="<?=$fontpol?>"><?=number_format($rasiotk1pol)."%";?></font></b></td>
                                                    <td> <?=$targetsenior2;?></td>
                                                    <td><?=$regsenior2?></td>
                                                    <td class="<?=$tcolor2pol?>" width='80px'><b><font color="<?=$fontpol2?>"><?=number_format($rasiotk2pol)."%";?></font></b> </td>
                                                    <!-- <td><?=$targettk3?></td>
                                                    <td><?=$registk3?></td>
                                                    <td></td> -->
                                                    <td><?php echo $linkk?></td>
                                                    
                                                </tr>
                                            <?php 
                                                 } 
                                                 $ttargetjuniorpol += $targetjunior2;  
                                                   $tregjuniorpol +=$registk1pol;
                                                   $ttargetseniorpol +=$targetsenior2;
                                                   $tregseniorpol +=$regsenior2;
                                                   
                                                  if($ttargetjuniorpol==0)
                                                  {$trasiotk1pol=0;}else{$trasiotk1pol =($tregjuniorpol/$ttargetjuniorpol)*100; }
                                                  if($ttargetseniorpol==0)
                                                  {$trasiotk2pol=0;}else{$trasiotk2pol =($tregseniorpol/$ttargetseniorpol)*100; }
                                                }
                                                 
                                                   
                                                //    $rastk1pol=number_format($trasiotk1,1);
                                                //    $rastk2=number_format($trasiotk2,1);
                                             }
                                             
                                             ?>   
                                                <tr class='bg-grey bg-font-grey'>
                                                    <td > <b>*</b></td> 
                                                    <td ><b>Total</b></td>                         
                                                    <td ><b><?=$ttargetjuniorpol?></b></td>
                                                    <td><b><?=$tregjuniorpol?></b></td>
                                                    <td><b><?=number_format($trasiotk1pol,1)."%"?></b></td>
                                                    <td><b> <?=$ttargetseniorpol?></b></td>
                                                    <td><b><?=$tregseniorpol?></b></td>
                                                    <td><b><?=number_format($trasiotk2pol,1)."%"?></b></td>
                                                    <td>*</td>
                                                    
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    