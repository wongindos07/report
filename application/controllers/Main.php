<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	private $_table = "kpi_nilai_kampus";

	Public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model("drop_model");
		$this->load->model("Mainmodel");	
		$this->load->library('session');
		if($this->session->userdata('status') !== 'masuk'){
 			redirect('Login');
 		}
	}

	function index(){
		$this->session->set_flashdata('aktif','active');
		$this->recap();

	}

	function dashboard(){
		$data['title'] = "Dashboard";
		$data['include'] = "form/dashboard";
		$data['sbkpi'] = "";
		$data['selkpi'] = "";
		$data['sbdb'] = "start active open";
		$data['seldb'] = "selected";
		$data['sbrecap'] = "";
		$data['selrecap'] = "";

		$this->load->view('home',$data);
	}

	public function dkpi(){
		$user = $_GET['user'];
		$kode = $_GET['kocab'];
		$grp = $_GET['grp'];

		// create session
		$newdata = array(
        'username'  => $user,
        'kocab'     => $kode,
        'grp'       => $grp
		);

		$this->session->set_userdata($newdata);
		// selected
		$bidang				= $this->input->post('kodebidang');
		$tahun				= $this->input->post('tahun');
		$kdcabang			= $this->input->post('cabang');
		$hasil				= $this->input->post('hasil');
		$kodecapai			= $this->input->post('kodecapai');
		$datetime			= date("Y-m-d h:i:s a");
		$usernya			= "ABCD";

		$tbidang			= "SELECT namabidang FROM kpi_bidang where kodebidang='$bidang' limit 1";
		$tcabang			= "SELECT mid(namacabang,8,LENGTH(namacabang)-1) as namkampus FROM cabang where kodecabang='$kdcabang' limit 1";
		 

		$tdkpi				= "SELECT a.kodecapai,b.kodestandar,b.tahun,mid(c.namacabang,8,LENGTH(c.namacabang)-1) as kampus,d.namabidang,b.goal,b.satuan,a.hasil,b.bobot,
format(if(a.hasil>=b.std2,b.bobot,if(a.hasil<b.std2,b.bobot*(a.hasil/b.std2),0)),2) as skor,if(a.hasil>=b.std2,'#00FF00',if(a.hasil>=b.std1,'Yellow','#FF0000')) as warnabg,if(a.hasil>=b.std2,'#000000',if(a.hasil>=b.std1,'#000000','#FFFFFF')) as warnahrf,
a.lampiran,a.`user`,a.datetime,concat(b.std1,' ',b.satuan) as minimal,concat(b.std2,' ',b.satuan) as standar,b.sumber,b.skrip,b.std1,b.std2,b.satuan, d.kodebidang, (a.kodecapai and b.skrip) as ab
from kpi_nilai_kampus as a,kpi_standar as b,cabang as c,kpi_bidang as d
where a.kodestandar=b.kodestandar and a.kodecabang=c.kodecabang and b.kodebidang=d.kodebidang and a.kodecabang='$kdcabang' and a.kodebidang='$bidang' and b.tahun='$tahun' ";
		     

		$combo_bidang=$this->drop_model->combo_bidang();
		$combo_tahun=$this->drop_model->combo_tahun();
		$combo_cabang=$this->drop_model->combo_cabang();

		$data=array('cmbbidang'=>$combo_bidang,'cmbtahun'=>$combo_tahun,'cmbcabang'=>$combo_cabang);

		$data['nambid'] = $this->Mainmodel->tampildatacollege($tbidang);
		$data['namcab'] = $this->Mainmodel->tampildatacollege($tcabang);
		$data['list_kpi'] 	= $this->Mainmodel->tampildatacollege($tdkpi);
		$data['vtahun'] = $tahun;
		$data['title'] = "Key Performance Indicator";
		$data['kodekampus'] = $kdcabang;
		$data['include'] = "form/kpi";
		$data['sbkpi'] = "start active open";
		$data['selkpi'] = "selected";
		$data['sbdb'] = "";
		$data['seldb'] = "";
		$data['sbrecap'] = "";
		$data['selrecap'] = "";
		$where=array('kodecapai'=>$kodecapai);
		$this->load->view('home',$data);
	}

		
	function updatekpidata(){
		$bidang				= $this->input->post('kodebidang');
		$tahun				= $this->input->post('tahun');
		$hasil				= $this->input->post('hasil');
		$kodecapai			= $this->input->post('kodecapai');
		$datetime			= date("Y-m-d h:i:s a");
		$usernya			= "ABCD";
		$lampiran 			= $this->input->post('kodecapai');
		//-- UPLOAD FILE
		$tipenya			= $_FILES['lampiran']['type'];
		$lokasi				= $_FILES['lampiran']['tmp_name'];
		$ukuran				= $_FILES['lampiran']['size'];
		$namanya			= $_FILES['lampiran']['name'];

        $nim 				= $kodecapai;
        $namabaru 			= $nim.".".end(explode(".", $namanya));
        $tempatnya			= "dokumenews/$namabaru";
        $uploadDir			= "documents/dokumenews/";

        $user = $this->session->userdata('username');
        $kocab = $this->session->userdata('kocab');
        $grp = $this->session->userdata('grp');
        

        $this->load->library('upload', $config);

        if(empty($lokasi)){

        	$data=array(
        		'hasil'=>$hasil,
        		'kodecapai'=>$kodecapai,
        		'datetime'=>$datetime,
        		'user'=>$usernya);
			$where=array('kodecapai'=>$kodecapai);
			$this->Mainmodel->updatedatapoltek('kpi_nilai_kampus',$data,$where);

        }else{
        	//tampung data

        	if(is_uploaded_file($_FILES['lampiran']['tmp_name'])){
        	$uploadFile = $_FILES['lampiran'];
        	//extract nama file
        	$extractFile = pathinfo($namabaru);

        	if($tipenya != "application/pdf"){
        		$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
			  <strong>MAAF!!!</strong> Format File harus PDF.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>');
        		$call=base_url('index.php/main/dkpi?user='.$user.'&kocab='.$kocab.'&grp='.$grp.'');
				redirect($call);      
        	}elseif($ukuran > 100000){
        		$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
			  <strong>MAAF!!!</strong> Ukuran File Anda Terlalu Besar.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>');
        		$call=base_url('index.php/main/dkpi?user='.$user.'&kocab='.$kocab.'&grp='.$grp.'');
				redirect($call);
        	}

        	$sameName 	= 0; //jika menyimpan file dengan nama yang sama
        	$handle		= opendir($uploadDir);

        	while(false !== ($file = readdir($handle))){//melaukan looping data ke direktori
        		//jika memiliki nama yang sama saat melakukan upload
        		if(strpos($file,$extractFile['filename']) !== false)
        			$sameName++; //menambah data file
        	}
        	$newName = empty($sameName) ? $namabaru : $extractFile['filename'].'(' .$sameName. ').' .$extractFile['extension'];


	        	if(move_uploaded_file($uploadFile['tmp_name'], $uploadDir.$newName)){
	        	if($tipenya == "application/pdf"){
	        		$tipe = 'pdf';
	        	}

	        	$data=array(
	        	'hasil'=>$hasil,
	        	'kodecapai'=>$kodecapai,
	        	'datetime'=>$datetime,
	        	'user'=>$usernya,
	        	'tipelampiran'=>$tipe,
	        	'lampiran'=>$uploadDir ."/" .$newName); 
	        	$where=array('kodecapai'=>$kodecapai);
				$this->Mainmodel->updatedatapoltek('kpi_nilai_kampus',$data,$where);

				$call=base_url('index.php/main/dkpi?user='.$user.'&kocab='.$kocab.'&grp='.$grp.'');
				redirect($call);

	        	}


        }
     }
        
}


	function recap(){


		$bidang				= $this->input->post('kodebidang');
		$tahun				= $this->input->post('tahun');
		$kdcabang			= $this->input->post('kodecabang');
		$combo_bidang		= $this->drop_model->combo_bidang();
		$combo_tahun		= $this->drop_model->combo_tahun();
		$data=array('cmbbidang'=>$combo_bidang,'cmbtahun'=>$combo_tahun);
		$ambil				= "SELECT namabidang FROM kpi_bidang where kodebidang='$bidang' limit 1";
		
		$pdata				= 	
		"SELECT cabang.kodecabang, MID(cabang.namacabang,8,LENGTH(cabang.namacabang)-1) as namakampus,AVG(kpi_nilai_kampus.hasil) AS rasio, kpi_standar.tahun, kpi_nilai_kampus.kodebidang, format(if(kpi_nilai_kampus.hasil>=kpi_standar.std2,kpi_standar.bobot,if(kpi_nilai_kampus.hasil<kpi_standar.std2, SUM(kpi_standar.bobot*(kpi_nilai_kampus.hasil/kpi_standar.std2)),0)),2) as skor 
		FROM kpi_standar 
		INNER JOIN kpi_nilai_kampus ON kpi_nilai_kampus.kodestandar=kpi_standar.kodestandar
		INNER JOIN cabang ON cabang.kodecabang=kpi_nilai_kampus.kodecabang 
		WHERE kpi_standar.tahun='$tahun' AND kpi_nilai_kampus.kodebidang ='$bidang' GROUP BY kpi_nilai_kampus.kodecabang ORDER BY kpi_nilai_kampus.kodecabang ASC";

		$data['nambidang'] = $this->Mainmodel->tampildatacollege($ambil);
		$data['list_kpi'] 	= $this->Mainmodel->tampildatacollege($pdata);

		$data['vtahun'] = $tahun;
		$data['title'] = "Recapitulations";
		// $data['include'] = "form/recap";
		$data['sbkpi'] = "";
		$data['selkpi'] = "";
		$data['sbdb'] = "";
		$data['seldb'] = "";
		$data['sbrecap'] = "start active open";
		$data['selrecap'] = "selected";

		$data['konten'] = 'form/recap';
		$this->load->view('Dashboard',$data);
		// $this->load->view('home',$data);
	}


	function detailkpi(){
		
		// selected
		$bidang				= $this->input->get('kodebidang');
		$tahun				= $this->input->get('tahun');
		$kdcabang			= $this->input->get('kdcabang');
		$hasil				= $this->input->post('hasil');
		$kodecapai			= $this->input->post('kodecapai');
		$datetime			= date("Y-m-d h:i:s a");
		$usernya			= "ABCD";

		$tbidang			= "SELECT namabidang FROM kpi_bidang where kodebidang='ICT' limit 1";
		$tcabang			= "SELECT mid(namacabang,8,LENGTH(namacabang)-1) as namkampus FROM cabang where kodecabang='$kdcabang' limit 1";
		 

		$tdkpi				= "SELECT a.kodecapai,b.kodestandar,b.tahun,mid(c.namacabang,8,LENGTH(c.namacabang)-1) as kampus,d.namabidang,b.goal,b.satuan,a.hasil,b.bobot,
format(if(a.hasil>=b.std2,b.bobot,if(a.hasil<b.std2,b.bobot*(a.hasil/b.std2),0)),2) as skor,if(a.hasil>=b.std2,'#00FF00',if(a.hasil>=b.std1,'Yellow','#FF0000')) as warnabg,if(a.hasil>=b.std2,'#000000',if(a.hasil>=b.std1,'#000000','#FFFFFF')) as warnahrf,
a.lampiran,a.`user`,a.datetime,concat(b.std1,' ',b.satuan) as minimal,concat(b.std2,' ',b.satuan) as standar,b.sumber,b.skrip,b.std1,b.std2,b.satuan, d.kodebidang, (a.kodecapai and b.skrip) as ab
from kpi_nilai_kampus as a,kpi_standar as b,cabang as c,kpi_bidang as d
where a.kodestandar=b.kodestandar and a.kodecabang=c.kodecabang and b.kodebidang=d.kodebidang and a.kodecabang='$kdcabang' and a.kodebidang='$bidang' and b.tahun='$tahun' ";
		     

		$combo_bidang=$this->drop_model->combo_bidang();
		$combo_tahun=$this->drop_model->combo_tahun();
		$combo_cabang=$this->drop_model->combo_cabang();

		$data=array('cmbbidang'=>$combo_bidang,'cmbtahun'=>$combo_tahun,'cmbcabang'=>$combo_cabang);

		$data['nambid'] = $this->Mainmodel->tampildatacollege($tbidang);
		$data['namcab'] = $this->Mainmodel->tampildatacollege($tcabang);
		$data['list_kpi'] 	= $this->Mainmodel->tampildatacollege($tdkpi);
		$data['vtahun'] = $tahun;
		$data['title'] = "Detail Key Performance Indicator";
		$data['kodekampus'] = $kdcabang;
		// $data['include'] = "form/kpi_list";
		$data['sbkpi'] = "";
		$data['selkpi'] = "";
		$data['sbdb'] = "";
		$data['seldb'] = "";
		$data['sbrecap'] = "start active open";
		$data['selrecap'] = "selected";
		$where=array('kodecapai'=>$kodecapai);

		// $this->load->view('home',$data);
		$data['konten'] = 'form/kpi_list';
		$this->load->view('Dashboard',$data);
	}

	public function logout(){
 		$user =  get_cookie('sesilogin');
 		delete_cookie('sesilogin');
 		
 		$this->session->sess_destroy();
 		redirect('Login');
 	}


	public function inputstandar(){
		$data['konten'] = 'form/inputstandar';
		$this->load->view('Dashboard',$data);
	}

	public function simpanstandar(){

		$usernya = $this->session->userdata('username');
		$tahun = $this->input->post('tahun');
		$goal = addslashes($this->input->post('goal'));
		$standar = $this->input->post('standar');
		$target = $this->input->post('target');
		$satuan = $this->input->post('satuan');
		$bobot = $this->input->post('bobot');
		$sumber = $this->input->post('sumber');
		$keterangan = $this->input->post('keterangan');

		 date_default_timezone_set("Asia/Jakarta");
			$datetime = date("Y-m-d h:i:s");
			$thn = substr($tahun,2,2);
			$bln = date('m');

		$sadmin = "select direktorat from useradmin where user='$usernya'";
		$qadmin = $this->Mainmodel->tampildatacollege($sadmin);
		foreach($qadmin as $radmin){
			$dir = $radmin->direktorat;
		}

		$kepala="select * from kpi_bidang where tingkat='$dir'";
		$qkepala = $this->Mainmodel->tampildatacollege($kepala);
		foreach($qkepala as $rkepala){
			$kodebidang = $rkepala->kodebidang;
		}


		$sql = "select kodestandar from kpi_standar where substr(kodestandar,1,2)='$thn' and 
				substr(kodestandar,3,3)='$kodebidang' order by kodestandar desc limit 1";
		$qkpi = $this->Mainmodel->tampildatacollege($sql);
		$kodestandar = null;
		foreach($qkpi as $rkpi){
			$kodestandar = $rkpi->kodestandar;
		}

		
		if ($kodestandar == null){
					$newID  = $thn."".$kodebidang."001";
			}else{
					$getNumberr = substr($kodestandar, -3); ///no urut terakhir 
					$getNumber = intval($getNumberr) + 1;  //Penambahan intval untuk mengkonversi string ke number
					$newID = $thn."". $kodebidang."".sprintf('%03s', $getNumber);
			}


		$scabang = "select kodecabang from cabang";
		$qcabang = $this->Mainmodel->tampildatacollege($scabang);
		if($qcabang == false){
			die('Gagal ambil data: ' . mysql_error());
		}



			$data = array(
				"kodestandar"=>$newID,
				"kodebidang"=>$kodebidang,
				"tahun"=>$tahun,
				"goal"=>$goal,
				"bobot"=>$bobot,
				"std1"=>$standar,
				"std2"=>$target,
				"satuan"=>$satuan,
				"ket"=>$keterangan,
				"sumber"=>$sumber,
				"skrip"=>"",
				"user"=>$usernya,
				"datetime"=>$datetime
			);

			$insert = $this->Mainmodel->set_data("kpi_standar",$data);



		foreach($qcabang as $rcabang){
			$kocab = $rcabang->kodecabang;
			$kcapai=$newID."-".$kocab;



				$data2 = array(
				   array(
					"kodecapai"=>$kcapai,
					"kodebidang"=>$kodebidang,
					"kodestandar"=>$newID,
					"hasil"=>"0",
					"lampiran"=>"",
					"user"=>$usernya,
					"datetime"=>$datetime,
					"tipelampiran"=>"",
					"kodecabang"=>$kocab
					)
				);
				
				$this->db->insert_batch('kpi_nilai_kampus', $data2); 

		}

		if($insert){
			$this->session->set_flashdata('message','<div class="alert alert-success" role="alert">
			  Data KPI STANDAR berhasil terinput.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>');
			redirect('Main/inputstandar');
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger" role="alert">
			  <strong>MAAF!!!</strong> Data KPI STANDAR gagal terinput.
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
			    <span aria-hidden="true">&times;</span>
			  </button>
			</div>');
			redirect('Main/inputstandar');
		}
	}

}



