<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model("Drop_model");
		$this->load->model("Mainmodel");
		$this->load->library('session');
		
		if($this->session->userdata('status') !== 'masuk'){
 			redirect('Login');
 		}
	}


	function rekappdd(){

        $kriteria			= $this->input->post('kriteria');
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->input->post('tahun');
		$data['kriteria'] = $kriteria;
		$data['tahun'] = $tahun; 
		
		$combo_tabiodata		= $this->Drop_model->combo_tabiodata();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_tabiodata);
					$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);


        if($kriteria=='Coll')
		{
			$ambil="select * from cabang where kelompok='1'";
			$dbnya="tampildatacollege"; 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from cabang where kelompok='aktif'";
			$dbnya="tampildatapoltek";
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from cabang where kelompok='aktif'";
			$dbnya="tampildatapoltek";
			$label='';
		}
		$data['cabang']=$this->Mainmodel->$dbnya($ambil);
		$data['cabangc'] = $this->Mainmodel->getCabang()->result();
		
	

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['kriteria']=$kriteria;

		$data['konten'] = 'konten/rekappddtab';
		$this->load->view('Dashboard',$data);
	}


	function rekappddjurus(){

        $kriteria			= $this->input->post('kriteria');
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->input->post('tahun'); 

		$combo_tabiodata		= $this->Drop_model->combo_tabiodata();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_tabiodata);
					$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);


        if($kriteria=='Coll')
		{
			$ambil="select * from jurusan where status='BC'";
			$dbnya="tampildatacollege"; 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from jurusan ";
			$dbnya="tampildatapoltek";
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from jurusan";
			$dbnya="tampildatapoltek";
			$label='';
		}
		$data['jurusan']=$this->MainModel->$dbnya($ambil);
		


		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['kriteria']=$kriteria;
		if(!$aktif)
		{$data['aktif']="tab3";}else
		{$data['aktif']=$aktif;}

		$data['konten'] = 'konten/rekappddtab';
		$this->load->view('Dashboard',$data);

	}


	public function rekappddjur(){
		$kriteria			= $this->input->post('kriteria');
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->input->post('tahun');



					$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);




        if($kriteria=='Coll')
		{
			$ambil="select * from jurusan where status='BC'";
			// $dbnya="tampildatacollege";
			$link = $this->Mainmodel->tampildatacollege($ambil); 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from jurusan ";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from jurusan";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='';
		}

		$data['jurusan']=$link;
		

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['kriteria']=$kriteria;

		$this->load->view('form/pendidikan/isipddjurus',$data);
		//$this->load->view('home',$data);
	}

	public function rekappddjurreport(){
		$kriteria			= $this->uri->segment(3);
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->uri->segment(4);

					$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);


        if($kriteria=='Coll')
		{
			$ambil="select * from jurusan where status='BC'";
			// $dbnya="tampildatacollege";
			$link = $this->Mainmodel->tampildatacollege($ambil); 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from jurusan ";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from jurusan";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='';
		}

		$data['jurusan']=$link;
		

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['kriteria']=$kriteria;

		//$this->load->view('home',$data);
		$data['konten'] = 'laporan/rekappddjurreport';
		$this->load->view('laporan',$data);
	}

	
	public function rekappddjurpdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('L');

		$kriteria			= $this->uri->segment(3);
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->uri->segment(4);

					$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);


        if($kriteria=='Coll')
		{
			$ambil="select * from jurusan where status='BC'";
			// $dbnya="tampildatacollege";
			$link = $this->Mainmodel->tampildatacollege($ambil); 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from jurusan ";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from jurusan";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='';
		}

		$data['jurusan']=$link;
		

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['kriteria']=$kriteria;

		//$this->load->view('home',$data);
		$data['konten'] = 'laporan/rekappddjurreport';
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}


	public function detailcabjur(){
		$kriteria			= $this->input->post('kriteria');
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->input->post('tahun');
		$jurusan			= $this->input->post('jurusan');
		$tipe				= $this->input->post('tipe');

		$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);


        if($kriteria=='Coll')
		{
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$label='';
		}

		// $data['jurusan']=$link;
		

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['jurusan']=$jurusan;
		$data['kriteria']=$kriteria;
		$data['tipe']=$tipe;


		$this->load->view('form/pendidikan/detailcabjur',$data);
	}

	public function detailmhsjur(){
		$kriteria			= $this->input->post('kriteria');
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->input->post('tahun');
		$jurusan			= $this->input->post('jurusan');
		$tipe				= $this->input->post('tipe');
		$kocab 				= $this->input->post('kocab');

		$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);


        if($kriteria=='Coll')
		{
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$label='';
		}

		// $data['jurusan']=$link;
		

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['jurusan']=$jurusan;
		$data['kriteria']=$kriteria;
		$data['tipe']=$tipe;
		$data['kocab'] = $kocab;


		$this->load->view('form/pendidikan/detailmhsjur',$data);
	}

	public function detailmhs(){
		$nim = $this->input->post('nim');
		$kocab = $this->input->post('kocab');
		$kriteria = $this->input->post('kriteria');

		 if($kriteria=='Coll')
		{
			$ambil="select * from biodata where nim='$nim'";
			// $dbnya="tampildatacollege";
			$link = $this->Mainmodel->tampildatacollege($ambil); 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from biodata where nim='$nim'";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from biodata where nim='$nim'";
			// $dbnya="tampildatapoltek";
			$link = $this->Mainmodel->tampildatapoltek($ambil);
			$label='';
		}

		$data['kriteria'] = $kriteria;
		$data['biodata'] = $link;
		$data['kocab'] = $kocab;

		$this->load->view('form/pendidikan/detailmhs',$data);
		
	}


	public function logout(){

		$this->session->sess_destroy();
		redirect('Login');

	}

	public function printPpdb(){
		$kriteria			= $this->uri->segment(4);
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->uri->segment(3); 
		
		$combo_tabiodata		= $this->Drop_model->combo_tabiodata();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_tabiodata);
					$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);

			
		



        if($kriteria=='Coll')
		{
			$ambil="select * from cabang where kelompok='1'";
			$dbnya="tampildatacollege"; 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from cabang where kelompok='aktif'";
			$dbnya="tampildatapoltek";
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from cabang where kelompok='aktif'";
			$dbnya="tampildatapoltek";
			$label='';
		}
		$data['cabang']=$this->Mainmodel->$dbnya($ambil);
		
		

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['kriteria']=$kriteria;

		$data['konten'] = 'laporan/printppdb';
		$this->load->view('laporan',$data);
	}

	public function printPpdbPdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('L');
		$kriteria			= $this->uri->segment(4);
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->uri->segment(3); 
		
		$combo_tabiodata		= $this->Drop_model->combo_tabiodata();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_tabiodata);
					$thj1=$tahun;
					$thj2=$tahun-1;
					$thj3=$tahun-2;
					$thsen1=$tahun."/".($tahun+1);
					$thsen2=($tahun-1)."/".$tahun;
					$thsen3=($tahun-2)."/".($tahun-1);

	
        if($kriteria=='Coll')
		{
			$ambil="select * from cabang where kelompok='1'";
			$dbnya="tampildatacollege"; 
			$label='Data Peserta Didik LP3I College';
			$tingkat3="";
		}
        else if($kriteria=='PLJ')
        {
			$ambil="select * from cabang where kelompok='aktif'";
			$dbnya="tampildatapoltek";
			$label='Data Mahasiswa PLJ';
		}
		else
		{
			$ambil="select * from cabang where kelompok='aktif'";
			$dbnya="tampildatapoltek";
			$label='';
		}
		$data['cabang']=$this->Mainmodel->$dbnya($ambil);
		

		$data['nkriteria']=$label;
		$data['tahun']=$tahun;
		$data['kritpot']=$kriteria;
		$data['thj1']=$thj1;
		$data['thj2']=$thj2;
		$data['thj3']=$thj3;
		$data['thsen1']=$thsen1;
		$data['thsen2']=$thsen2;
		$data['thsen3']=$thsen3;
		$data['kriteria']=$kriteria;

		$data['konten'] = 'laporan/printppdbpdf';
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}
	

	public function rekapLapBul(){
		$data['cabang'] = $this->Mainmodel->getCabang()->result();

		$data['konten'] = 'form/pendidikan/rekLapBul';
		$this->load->view('Dashboard',$data);
	}

	public function getTak(){
		$cab = $this->input->post('cab');

		$tak = $this->Mainmodel->getTak($cab)->result();
		echo "<option>-- Pilih Tahun Akademik --</option>";
		foreach($tak as $rtak){
			echo "<option value=".$rtak->tahunakademik.">".$rtak->tahunakademik."</option>";
		}
	}



	public function FormJmlMhs(){
		$cab = $this->input->post('cab');
		$tak = $this->input->post('tak');
		$periode = $this->input->post('periode');

		$isi1 = $cab;
		$isi2 = $tak;
		$isi3 = $periode;
		$ta = substr($tak,0,4);

		$bull=array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
		$thnx = substr($periode,0,4)*1;
		$blnx = substr($periode,5,2)*1;
		$tglx = substr($periode,8,2)*1;

		if($blnx == "01"){
			$bln_mundur = '12';
		}else{
			$bln_mundur=($blnx*1)-1;
		}
		
		$gabb = $tglx." ".$bull[$blnx*1]." ".$thnx;
		$gabb2 = $bull[$blnx*1]." ".$thnx;
		$link = base_url('index.php/Pendidikan/lapbulreport/'.$isi1.'/'.$isi2.'/'.$isi3);
		$linkpdf = base_url('index.php/Pendidikan/lapbulpdf/'.$isi1.'/'.$isi2.'/'.$isi3);


		$data['getCabang'] = $this->Mainmodel->getWhere('cabang',array('kodecabang'=>$cab))->result();
		$getInisialisasi = $this->Mainmodel->getWhere('inisialisasi',array('kodecabang'=>$cab))->result();
		foreach($getInisialisasi as $rInisialisasi);
		$asmen = $rInisialisasi->Nama_asmen1;
		$pincab = $rInisialisasi->NamaPincab;

			echo '
			<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">JUMLAH MAHASISWA PROFESI</span>
				</div>	
			</div>
            <div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">Laporan Bulanan : '.$gabb2.'</span>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">FR - PDK - 079</span>
				</div>	
			</div>  
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default" target="_blank" href="'.$link.'">
                <i class="icon-printer"></i>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default" target="_blank" href="'.$linkpdf.'">
                <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>
	</div>
	<div class="portlet-body" style="overflow-x: auto;">
			';

		for($x=1;$x<=2;$x++){
			

			echo '
			<div>
				<table border="1" class="table table-bordered table-striped table-condensed flip-content">
				<b>Peserta Didik Tingkat '.$x.'</b>
                <thead>
                	<tr class="bg-grey-gallery bg-font-grey-gallery">
                	<td>No</td>
                	<td>Bidang Keahlian</td>
                	<td>Kelas</td>
                	<td>Jml Awal Bulan</td>
                	<td>Jml Bulan Januari</td>
                	<td>Jml Bulan Februari</td>
                	<td>Jml Out Bulan Ini</td>
                	<td>Total Out</td>
                	<td>%Out Bln Ini</td>
                	<td>%Total Out</td>
                	<td>Nama PA</td>
                	<td>Wkt Kuliah</td>
                	</tr>
                </thead>
                <tbody>
				';

			if($x == 1){
				$tot1x=0;
				$tot2x=0;
				$tot3x=0;
				$tot4x=0;
				$tot5x=0;

				$tot1=0;
				$tot2=0;
				$tot3=0;
				$tot4=0;
				$tot5=0;
				$no = 0;
				$persen_skrxx = 0;
				$persen_allxx = 0;
				$jurcab = $this->db->query("select biodata.kode from biodata where TahunAngkatan='$ta' and kodecabang='$cab' group by biodata.kode")->result();
				foreach($jurcab as $rjurcab){
					$kojur = $rjurcab->kode;

				$klscab = $this->db->query("select kelas from mhs_serahterima where kode='$kojur' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak' group by kelas")->result();
				foreach($klscab as $rklscab){
					$klscab = $rklscab->kelas;

				$getjur = $this->db->query("select namajurusan from jurusan where kodejurusan='$kojur'")->result();
				foreach($getjur as $rgetjur);
				$najur = $rgetjur->namajurusan;

				//JUMLAH AWAL BULAN
				$cekjml = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak'");
				$jumlah1 = $cekjml->num_rows();
				if($jumlah1 == 0){
					$hasil1 = 0;
				}else{
					$jmledu = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak'")->result();
					foreach($jmledu as $rjmledu);
					if($rjmledu->jml_edu == ""){
						$hasil1 = 0;
					}else{
						$hasil1 = $rjmledu->jml_edu;
					}

				}


			//JUMLAH AWAL BULAN MUNDUR
			$cekjmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'");
				$jumlah2 = $cekjmlbln->num_rows();

				if($jumlah2 == 0){
					$hasil2 = 0;
				}else{

				$jmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'")->result();
				foreach($jmlbln as $rjmlbln);
				if($rjmlbln->jml_edu == ""){
						$hasil2 = 0;
					}else{
						$hasil2 = $rjmlbln->jml_edu;
					}

				}

				
				//JUMLAH AWAL BULAN SAAT INI
				$cekblnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'");
				$jumlah3 = $cekblnnow->num_rows();
				if($jumlah3 == 0){
					$hasil3 = 0;
				}else{
					$blnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'")->result();
					foreach($blnnow as $rblnnow);
					if($rblnnow->jml_edu == ""){
						$hasil3 = 0;
					}else{
						$hasil3 = $rblnnow->jml_edu;
					}
				}


				//JUMLAH OUT SEKARANG
				$cekout = $this->db->query("select * from mhs_statusdo where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' and month(tgl_pengajuan)='$blnx' and year(tgl_pengajuan)='$thnx'");
				$jumlah4 = $cekout->num_rows();
				if($jumlah4 == ""){
					$hasil4 = 0;
				}else{
					$hasil4 = $jumlah4;
				}


				//JUMLAH OUT HINGGA SEKARANG
				$cekoutnow = $this->db->query("select * from mhs_statusdo where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' or tgl_pengajuan  ='$periode'");
				$jumlah5 = $cekoutnow->num_rows();
				if($jumlah5 == ""){
					$hasil5 = 0;
				}else{
					$hasil5 = $jumlah5;
				}


				//CARI KELAS, PA, WAKTU
				$cekjmlpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'");
				$jumlah6 = $cekjmlpa->num_rows();
				if($jumlah6 == ""){
					$pa = "-";
					$waktu = "-";
				}else{

				$cekpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'")->result();
				foreach($cekpa as $rcekpa);
				// validasi pa
				if($rcekpa->PA == ""){
					$pa = "-";
				}else{
					$pa = $rcekpa->PA;
				}

				// validasi waktu
				if($rcekpa->Waktu == ""){
					$waktu = "-";
				}else{
					$waktu = $rcekpa->Waktu;
				}

				}

				// MENCARI PERSENTASE
				if($hasil1 == 0){
					//%OUT BULAN INI
					$persen_skr = 0;
					
					//% TOTAL OUT
					$persen_all = 0;
				}else{
					//%OUT BULAN INI
					$persen_skr = ($hasil4/$hasil1)*100;
					if($persen_skr >= 100)
					{$persen_skr="100%";}else{$persen_skr=number_format($persen_skr,0)."%";}
					//% TOTAL OUT
					
					$persen_all = ($hasil5/$hasil1)*100;
					if($persen_all >= 100)
					{$persen_all="100%";}else{$persen_all=number_format($persen_all,0)."%";}
				}
				
				$no++;
		
				echo '
				    <tr>
				    	<td>'.$no.'</td>
				    	<td>'.$najur.'</td>
				    	<td>'.$klscab.'</td>
				    	<td>'.$hasil1.'</td>
				    	<td>'.$hasil2.'</td>
				    	<td>'.$hasil3.'</td>
				    	<td>'.$hasil4.'</td>
				    	<td>'.$hasil5.'</td>
				    	<td>'.$persen_skr.'</td>
				    	<td>'.$persen_all.'</td>
				    	<td>'.$pa.'</td>
				    	<td>'.$waktu.'</td>
				    </tr>
				    ';

				  $tot1+=$hasil1;
				  $tot2+=$hasil2;
				  $tot3+=$hasil3;
				  $tot4+=$hasil4;
				  $tot5+=$hasil5;
				

				}
				// tutup foreach klscab

				}
				// tutup foreach jurcab

				if($tot1==0)
					{
					//%OUT BULAN INI
					$persen_skrx = 0;
					//% TOTAL OUT
					$persen_allx = 0;
					}
					else
					{
					//%OUT BULAN INI
					$persen_skrx = ($tot4/$tot1)*100;
					//% TOTAL OUT
					$persen_allx = ($tot5/$tot1)*100;
					}

					echo '
					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL</td>
					<td>'.$tot1.'</td>
					<td>'.$tot2.'</td>
					<td>'.$tot3.'</td>
					<td>'.$tot4.'</td>
					<td>'.$tot5.'</td>
					<td>'.number_format($persen_skrx,2).'</td>
					<td>'.number_format($persen_allx,2).'</td>
					<td></td>
					<td></td>
					</tr>';

			}elseif($x == 2){
			

			$no = 0;
			$jurcab = $this->db->query("select regissenior.kode from regissenior inner join biodata on regissenior.kode=regissenior.kode where regissenior.ta='$tak' and regissenior.kodecabang='$cab' group by regissenior.kode")->result();
			foreach($jurcab as $rjurcab){
				$kojur = $rjurcab->kode;
				$klscab = $this->db->query("select kelas from mhs_serahterima where kode='$kojur' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak' group by kelas")->result();
				foreach($klscab as $rklscab){
					$klscab = $rklscab->kelas;

				$getjur = $this->db->query("select namajurusan from jurusan where kodejurusan='$kojur'")->result();
				foreach($getjur as $rgetjur);
				$najur = $rgetjur->namajurusan;

				//JUMLAH AWAL BULAN
				$cekjml = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak'");
				$jumlah1 = $cekjml->num_rows();
				if($jumlah1 == 0){
					$hasil1 = 0;
				}else{
					$jmledu = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak'")->result();
					foreach($jmledu as $rjmledu);
					if($rjmledu->jml_edu == ""){
						$hasil1 = 0;
					}else{
						$hasil1 = $rjmledu->jml_edu;
					}

				}


			//JUMLAH AWAL BULAN MUNDUR
			$cekjmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'");
				$jumlah2 = $cekjmlbln->num_rows();

				if($jumlah2 == 0){
					$hasil2 = 0;
				}else{

				$jmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'")->result();
				foreach($jmlbln as $rjmlbln);
				if($rjmlbln->jml_edu == ""){
						$hasil2 = 0;
					}else{
						$hasil2 = $rjmlbln->jml_edu;
					}

				}

				
				//JUMLAH AWAL BULAN SAAT INI
				$cekblnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'");
				$jumlah3 = $cekblnnow->num_rows();
				if($jumlah3 == 0){
					$hasil3 = 0;
				}else{
					$blnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'")->result();
					foreach($blnnow as $rblnnow);
					if($rblnnow->jml_edu == ""){
						$hasil3 = 0;
					}else{
						$hasil3 = $rblnnow->jml_edu;
					}
				}


				//JUMLAH OUT SEKARANG
				$cekout = $this->db->query("select * from mhs_statusdo where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' and month(tgl_pengajuan)='$blnx' and year(tgl_pengajuan)='$thnx'");
				$jumlah4 = $cekout->num_rows();
				if($jumlah4 == ""){
					$hasil4 = 0;
				}else{
					$hasil4 = $jumlah4;
				}


				//JUMLAH OUT HINGGA SEKARANG
				$cekoutnow = $this->db->query("select * from mhs_statusdo where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' or tgl_pengajuan  ='$periode'");
				$jumlah5 = $cekoutnow->num_rows();
				if($jumlah5 == ""){
					$hasil5 = 0;
				}else{
					$hasil5 = $jumlah5;
				}


				//CARI KELAS, PA, WAKTU
				$cekjmlpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'");
				$jumlah6 = $cekjmlpa->num_rows();
				if($jumlah6 == ""){
					$pa = "-";
					$waktu = "-";
				}else{

				$cekpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'")->result();
				foreach($cekpa as $rcekpa);
				// validasi pa
				if($rcekpa->PA == ""){
					$pa = "-";
				}else{
					$pa = $rcekpa->PA;
				}

				// validasi waktu
				if($rcekpa->Waktu == ""){
					$waktu = "-";
				}else{
					$waktu = $rcekpa->Waktu;
				}

				}

				// MENCARI PERSENTASE
				if($hasil1 == 0){
					//%OUT BULAN INI
					$persen_skr = 0;
					
					//% TOTAL OUT
					$persen_all = 0;
				}else{
					//%OUT BULAN INI
					$persen_skr = ($hasil4/$hasil1)*100;
					if($persen_skr >= 100)
					{$persen_skr="100%";}else{$persen_skr=number_format($persen_skr,0)."%";}
					//% TOTAL OUT
					
					$persen_all = ($hasil5/$hasil1)*100;
					if($persen_all >= 100)
					{$persen_all="100%";}else{$persen_all=number_format($persen_all,0)."%";}
				}
				
				$no++;
		
				echo '
				    <tr>
				    	<td>'.$no.'</td>
				    	<td>'.$najur.'</td>
				    	<td>'.$klscab.'</td>
				    	<td>'.$hasil1.'</td>
				    	<td>'.$hasil2.'</td>
				    	<td>'.$hasil3.'</td>
				    	<td>'.$hasil4.'</td>
				    	<td>'.$hasil5.'</td>
				    	<td>'.$persen_skr.'</td>
				    	<td>'.$persen_all.'</td>
				    	<td>'.$pa.'</td>
				    	<td>'.$waktu.'</td>
				    </tr>
				    ';

				  $tot1x+=$hasil1;
				  $tot2x+=$hasil2;
				  $tot3x+=$hasil3;
				  $tot4x+=$hasil4;
				  $tot5x+=$hasil5;
				

				}
				// tutup foreach klscab

			}
			// tutup foreach jurcab
			if($tot1x==0)
					{
					//%OUT BULAN INI
					$persen_skrxx = 0;
					//% TOTAL OUT
					$persen_allxx = 0;
					}
					else
					{
					//%OUT BULAN INI
					$persen_skrxx = ($tot4x/$tot1x)*100;
					//% TOTAL OUT
					$persen_allxx = ($tot5x/$tot1x)*100;
					}

					echo '
					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL</td>
					<td>'.$tot1x.'</td>
					<td>'.$tot2x.'</td>
					<td>'.$tot3x.'</td>
					<td>'.$tot4x.'</td>
					<td>'.$tot5x.'</td>
					<td>'.number_format($persen_skrxx,2).'</td>
					<td>'.number_format($persen_allxx,2).'</td>
					<td></td>
					<td></td>
					</tr>';

				

			}
			// tutup if $x
			$tott1 = $tot1 + $tot1x;
			$tott2 = $tot2 + $tot2x;
			$tott3 = $tot3 + $tot3x;
			$tott4 = $tot4 + $tot4x;
			$tott5 = $tot5 + $tot5x;
			$persen1 = $persen_skrx + $persen_skrxx;
			$persen2 = $persen_allx + $persen_allxx;
		
		}
		// tutup for

		echo '
		<tr style: border: none>
					<td colspan="3"></td?>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					</tr>

					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL PROFESI</td>
					<td>'.$tott1.'</td>
					<td>'.$tott2.'</td>
					<td>'.$tott3.'</td>
					<td>'.$tott4.'</td>
					<td>'.$tott5.'</td>
					<td>'.number_format($persen1,2).'</td>
					<td>'.number_format($persen2,2).'</td>
					<td></td>
					<td></td>
					</tr>
				</tbody>
			</table>
		</div>

			<div class="row">
				<div class="col-md-4 col-sm-6"><b style="color: red">Last Validasi : '.$gabb.'</b></div>
				<div class="col-md-4 col-sm-6"></div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 col-sm-6">Dibuat Oleh,</div>
				<div class="col-md-4 col-sm-6">Mengetahui</div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6"><b>'.$asmen.'</b></div>
				<div class="col-md-4 col-sm-6"><b>'.$pincab.'</b></div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
			<div class="row" style="margin-bottom: 100px">
				<div class="col-md-4 col-sm-6">Head Of Education</div>
				<div class="col-md-4 col-sm-6">Branch Manager</div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
					</div>
						</div>
		';



			
		
					
		}


public function lapbulreport(){
	$cab = $this->uri->segment(3);
	$tak = $this->uri->segment(4)."/".$this->uri->segment(5);
	$periode = $this->uri->segment(6)."/".$this->uri->segment(7)."/".$this->uri->segment(8);
	
	$ta = substr($tak,0,4);

		$bull=array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
		$thnx = substr($periode,0,4)*1;
		$blnx = substr($periode,5,2)*1;
		$tglx = substr($periode,8,2)*1;

		if($blnx == "01"){
			$bln_mundur = '12';
		}else{
			$bln_mundur=($blnx*1)-1;
		}
		
		$gabb = $tglx." ".$bull[$blnx*1]." ".$thnx;
		$gabb2 = $bull[$blnx*1]." ".$thnx;


		$data['getCabang'] = $this->Mainmodel->getWhere('cabang',array('kodecabang'=>$cab))->result();
		$getInisialisasi = $this->Mainmodel->getWhere('inisialisasi',array('kodecabang'=>$cab))->result();
		foreach($getInisialisasi as $rInisialisasi);
		$asmen = $rInisialisasi->Nama_asmen1;
		$pincab = $rInisialisasi->NamaPincab;

		echo '
	<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">JUMLAH MAHASISWA PROFESI</span>
				</div>	
			</div>
            <div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">Laporan Bulanan : '.$gabb2.'</span>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">FR - PDK - 079</span>
				</div>	
			</div>  
        </div>
        
	</div>
	<div class="portlet-body">
			';

		for($x=1;$x<=2;$x++){
			

			echo '
			<div>
				<table border="1" class="table table-bordered table-striped table-condensed flip-content">
				<b>Peserta Didik Tingkat '.$x.'</b>
                <thead>
                	<tr class="bg-grey-gallery bg-font-grey-gallery">
                	<td>No</td>
                	<td>Bidang Keahlian</td>
                	<td>Kelas</td>
                	<td>Jml Awal Bulan</td>
                	<td>Jml Bulan Januari</td>
                	<td>Jml Bulan Februari</td>
                	<td>Jml Out Bulan Ini</td>
                	<td>Total Out</td>
                	<td>%Out Bln Ini</td>
                	<td>%Total Out</td>
                	<td>Nama PA</td>
                	<td>Wkt Kuliah</td>
                	</tr>
                </thead>
                <tbody>
				';

			if($x == 1){
				$tot1x=0;
				$tot2x=0;
				$tot3x=0;
				$tot4x=0;
				$tot5x=0;

				$tot1=0;
				$tot2=0;
				$tot3=0;
				$tot4=0;
				$tot5=0;
				$no = 0;
				$persen_skrxx = 0;
				$persen_allxx = 0;
				$jurcab = $this->db->query("select biodata.kode from biodata where TahunAngkatan='$ta' and kodecabang='$cab' group by biodata.kode")->result();
				foreach($jurcab as $rjurcab){
					$kojur = $rjurcab->kode;

				$klscab = $this->db->query("select kelas from mhs_serahterima where kode='$kojur' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak' group by kelas")->result();
				foreach($klscab as $rklscab){
					$klscab = $rklscab->kelas;

				$getjur = $this->db->query("select namajurusan from jurusan where kodejurusan='$kojur'")->result();
				foreach($getjur as $rgetjur);
				$najur = $rgetjur->namajurusan;

				//JUMLAH AWAL BULAN
				$cekjml = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak'");
				$jumlah1 = $cekjml->num_rows();
				if($jumlah1 == 0){
					$hasil1 = 0;
				}else{
					$jmledu = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak'")->result();
					foreach($jmledu as $rjmledu);
					if($rjmledu->jml_edu == ""){
						$hasil1 = 0;
					}else{
						$hasil1 = $rjmledu->jml_edu;
					}

				}


			//JUMLAH AWAL BULAN MUNDUR
			$cekjmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'");
				$jumlah2 = $cekjmlbln->num_rows();

				if($jumlah2 == 0){
					$hasil2 = 0;
				}else{

				$jmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'")->result();
				foreach($jmlbln as $rjmlbln);
				if($rjmlbln->jml_edu == ""){
						$hasil2 = 0;
					}else{
						$hasil2 = $rjmlbln->jml_edu;
					}

				}

				
				//JUMLAH AWAL BULAN SAAT INI
				$cekblnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'");
				$jumlah3 = $cekblnnow->num_rows();
				if($jumlah3 == 0){
					$hasil3 = 0;
				}else{
					$blnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'")->result();
					foreach($blnnow as $rblnnow);
					if($rblnnow->jml_edu == ""){
						$hasil3 = 0;
					}else{
						$hasil3 = $rblnnow->jml_edu;
					}
				}


				//JUMLAH OUT SEKARANG
				$cekout = $this->db->query("select * from mhs_statusdo where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' and month(tgl_pengajuan)='$blnx' and year(tgl_pengajuan)='$thnx'");
				$jumlah4 = $cekout->num_rows();
				if($jumlah4 == ""){
					$hasil4 = 0;
				}else{
					$hasil4 = $jumlah4;
				}


				//JUMLAH OUT HINGGA SEKARANG
				$cekoutnow = $this->db->query("select * from mhs_statusdo where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' or tgl_pengajuan  ='$periode'");
				$jumlah5 = $cekoutnow->num_rows();
				if($jumlah5 == ""){
					$hasil5 = 0;
				}else{
					$hasil5 = $jumlah5;
				}


				//CARI KELAS, PA, WAKTU
				$cekjmlpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'");
				$jumlah6 = $cekjmlpa->num_rows();
				if($jumlah6 == ""){
					$pa = "-";
					$waktu = "-";
				}else{

				$cekpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'")->result();
				foreach($cekpa as $rcekpa);
				// validasi pa
				if($rcekpa->PA == ""){
					$pa = "-";
				}else{
					$pa = $rcekpa->PA;
				}

				// validasi waktu
				if($rcekpa->Waktu == ""){
					$waktu = "-";
				}else{
					$waktu = $rcekpa->Waktu;
				}

				}

				// MENCARI PERSENTASE
				if($hasil1 == 0){
					//%OUT BULAN INI
					$persen_skr = 0;
					
					//% TOTAL OUT
					$persen_all = 0;
				}else{
					//%OUT BULAN INI
					$persen_skr = ($hasil4/$hasil1)*100;
					if($persen_skr >= 100)
					{$persen_skr="100%";}else{$persen_skr=number_format($persen_skr,0)."%";}
					//% TOTAL OUT
					
					$persen_all = ($hasil5/$hasil1)*100;
					if($persen_all >= 100)
					{$persen_all="100%";}else{$persen_all=number_format($persen_all,0)."%";}
				}
				
				$no++;
		
				echo '
				    <tr>
				    	<td>'.$no.'</td>
				    	<td>'.$najur.'</td>
				    	<td>'.$klscab.'</td>
				    	<td>'.$hasil1.'</td>
				    	<td>'.$hasil2.'</td>
				    	<td>'.$hasil3.'</td>
				    	<td>'.$hasil4.'</td>
				    	<td>'.$hasil5.'</td>
				    	<td>'.$persen_skr.'</td>
				    	<td>'.$persen_all.'</td>
				    	<td>'.$pa.'</td>
				    	<td>'.$waktu.'</td>
				    </tr>
				    ';

				  $tot1+=$hasil1;
				  $tot2+=$hasil2;
				  $tot3+=$hasil3;
				  $tot4+=$hasil4;
				  $tot5+=$hasil5;
				

				}
				// tutup foreach klscab

				}
				// tutup foreach jurcab

				if($tot1==0)
					{
					//%OUT BULAN INI
					$persen_skrx = 0;
					//% TOTAL OUT
					$persen_allx = 0;
					}
					else
					{
					//%OUT BULAN INI
					$persen_skrx = ($tot4/$tot1)*100;
					//% TOTAL OUT
					$persen_allx = ($tot5/$tot1)*100;
					}

					echo '
					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL</td>
					<td>'.$tot1.'</td>
					<td>'.$tot2.'</td>
					<td>'.$tot3.'</td>
					<td>'.$tot4.'</td>
					<td>'.$tot5.'</td>
					<td>'.number_format($persen_skrx,2).'</td>
					<td>'.number_format($persen_allx,2).'</td>
					<td></td>
					<td></td>
					</tr>';

			}elseif($x == 2){
			

			$no = 0;
			$jurcab = $this->db->query("select regissenior.kode from regissenior inner join biodata on regissenior.kode=regissenior.kode where regissenior.ta='$tak' and regissenior.kodecabang='$cab' group by regissenior.kode")->result();
			foreach($jurcab as $rjurcab){
				$kojur = $rjurcab->kode;
				$klscab = $this->db->query("select kelas from mhs_serahterima where kode='$kojur' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak' group by kelas")->result();
				foreach($klscab as $rklscab){
					$klscab = $rklscab->kelas;

				$getjur = $this->db->query("select namajurusan from jurusan where kodejurusan='$kojur'")->result();
				foreach($getjur as $rgetjur);
				$najur = $rgetjur->namajurusan;

				//JUMLAH AWAL BULAN
				$cekjml = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak'");
				$jumlah1 = $cekjml->num_rows();
				if($jumlah1 == 0){
					$hasil1 = 0;
				}else{
					$jmledu = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak'")->result();
					foreach($jmledu as $rjmledu);
					if($rjmledu->jml_edu == ""){
						$hasil1 = 0;
					}else{
						$hasil1 = $rjmledu->jml_edu;
					}

				}


			//JUMLAH AWAL BULAN MUNDUR
			$cekjmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'");
				$jumlah2 = $cekjmlbln->num_rows();

				if($jumlah2 == 0){
					$hasil2 = 0;
				}else{

				$jmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'")->result();
				foreach($jmlbln as $rjmlbln);
				if($rjmlbln->jml_edu == ""){
						$hasil2 = 0;
					}else{
						$hasil2 = $rjmlbln->jml_edu;
					}

				}

				
				//JUMLAH AWAL BULAN SAAT INI
				$cekblnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'");
				$jumlah3 = $cekblnnow->num_rows();
				if($jumlah3 == 0){
					$hasil3 = 0;
				}else{
					$blnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'")->result();
					foreach($blnnow as $rblnnow);
					if($rblnnow->jml_edu == ""){
						$hasil3 = 0;
					}else{
						$hasil3 = $rblnnow->jml_edu;
					}
				}


				//JUMLAH OUT SEKARANG
				$cekout = $this->db->query("select * from mhs_statusdo where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' and month(tgl_pengajuan)='$blnx' and year(tgl_pengajuan)='$thnx'");
				$jumlah4 = $cekout->num_rows();
				if($jumlah4 == ""){
					$hasil4 = 0;
				}else{
					$hasil4 = $jumlah4;
				}


				//JUMLAH OUT HINGGA SEKARANG
				$cekoutnow = $this->db->query("select * from mhs_statusdo where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' or tgl_pengajuan  ='$periode'");
				$jumlah5 = $cekoutnow->num_rows();
				if($jumlah5 == ""){
					$hasil5 = 0;
				}else{
					$hasil5 = $jumlah5;
				}


				//CARI KELAS, PA, WAKTU
				$cekjmlpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'");
				$jumlah6 = $cekjmlpa->num_rows();
				if($jumlah6 == ""){
					$pa = "-";
					$waktu = "-";
				}else{

				$cekpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'")->result();
				foreach($cekpa as $rcekpa);
				// validasi pa
				if($rcekpa->PA == ""){
					$pa = "-";
				}else{
					$pa = $rcekpa->PA;
				}

				// validasi waktu
				if($rcekpa->Waktu == ""){
					$waktu = "-";
				}else{
					$waktu = $rcekpa->Waktu;
				}

				}

				// MENCARI PERSENTASE
				if($hasil1 == 0){
					//%OUT BULAN INI
					$persen_skr = 0;
					
					//% TOTAL OUT
					$persen_all = 0;
				}else{
					//%OUT BULAN INI
					$persen_skr = ($hasil4/$hasil1)*100;
					if($persen_skr >= 100)
					{$persen_skr="100%";}else{$persen_skr=number_format($persen_skr,0)."%";}
					//% TOTAL OUT
					
					$persen_all = ($hasil5/$hasil1)*100;
					if($persen_all >= 100)
					{$persen_all="100%";}else{$persen_all=number_format($persen_all,0)."%";}
				}
				
				$no++;
		
				echo '
				    <tr>
				    	<td>'.$no.'</td>
				    	<td>'.$najur.'</td>
				    	<td>'.$klscab.'</td>
				    	<td>'.$hasil1.'</td>
				    	<td>'.$hasil2.'</td>
				    	<td>'.$hasil3.'</td>
				    	<td>'.$hasil4.'</td>
				    	<td>'.$hasil5.'</td>
				    	<td>'.$persen_skr.'</td>
				    	<td>'.$persen_all.'</td>
				    	<td>'.$pa.'</td>
				    	<td>'.$waktu.'</td>
				    </tr>
				    ';

				  $tot1x+=$hasil1;
				  $tot2x+=$hasil2;
				  $tot3x+=$hasil3;
				  $tot4x+=$hasil4;
				  $tot5x+=$hasil5;
				

				}
				// tutup foreach klscab

			}
			// tutup foreach jurcab
			if($tot1x==0)
					{
					//%OUT BULAN INI
					$persen_skrxx = 0;
					//% TOTAL OUT
					$persen_allxx = 0;
					}
					else
					{
					//%OUT BULAN INI
					$persen_skrxx = ($tot4x/$tot1x)*100;
					//% TOTAL OUT
					$persen_allxx = ($tot5x/$tot1x)*100;
					}

					echo '
					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL</td>
					<td>'.$tot1x.'</td>
					<td>'.$tot2x.'</td>
					<td>'.$tot3x.'</td>
					<td>'.$tot4x.'</td>
					<td>'.$tot5x.'</td>
					<td>'.number_format($persen_skrxx,2).'</td>
					<td>'.number_format($persen_allxx,2).'</td>
					<td></td>
					<td></td>
					</tr>';

				

			}
			// tutup if $x
			$tott1 = $tot1 + $tot1x;
			$tott2 = $tot2 + $tot2x;
			$tott3 = $tot3 + $tot3x;
			$tott4 = $tot4 + $tot4x;
			$tott5 = $tot5 + $tot5x;
			$persen1 = $persen_skrx + $persen_skrxx;
			$persen2 = $persen_allx + $persen_allxx;
		
		}
		// tutup for

		echo '
		<tr style: border: none>
					<td colspan="3"></td?>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					</tr>

					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL PROFESI</td?>
					<td>'.$tott1.'</td>
					<td>'.$tott2.'</td>
					<td>'.$tott3.'</td>
					<td>'.$tott4.'</td>
					<td>'.$tott5.'</td>
					<td>'.number_format($persen1,2).'</td>
					<td>'.number_format($persen2,2).'</td>
					<td></td>
					<td></td>
					</tr>
		</tbody>
			</table>
				</div>

			<div class="row">
				<div class="col-md-4 col-sm-6"><b style="color: red">Last Validasi : '.$gabb.'</b></div>
				<div class="col-md-4 col-sm-6"></div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
			<br>
			<div class="row">
				<div class="col-md-4 col-sm-6">Dibuat Oleh,</div>
				<div class="col-md-4 col-sm-6">Mengetahui</div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-6"><b>'.$asmen.'</b></div>
				<div class="col-md-4 col-sm-6"><b>'.$pincab.'</b></div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
			<div class="row" style="margin-bottom: 100px">
				<div class="col-md-4 col-sm-6">Head Of Education</div>
				<div class="col-md-4 col-sm-6">Branch Manager</div>
				<div class="col-md-4 col-sm-6"></div>
			</div>
					</div>
						</div>
		';
	

		$data['konten'] = 'laporan/lapbul-print';
		$this->load->view('laporan',$data);


}


public function lapbulpdf(){
	$this->load->library('pdf');
	$this->pdf = new mPDF();
    $this->pdf->AddPage('L');

	$data['cab'] = $this->uri->segment(3);
	$cab = $this->uri->segment(3);
	$data['tak'] = $this->uri->segment(4)."/".$this->uri->segment(5);
	$data['periode'] = $this->uri->segment(6)."/".$this->uri->segment(7)."/".$this->uri->segment(8);
	
	
		


		$getInisialisasi = $this->Mainmodel->getWhere('inisialisasi',array('kodecabang'=>$cab))->result();
		foreach($getInisialisasi as $rInisialisasi);
		$data['asmen'] = $rInisialisasi->Nama_asmen1;
		$data['pincab'] = $rInisialisasi->NamaPincab;

		$data['konten'] = 'laporan/lapbul-pdf';
		// $this->load->view('laporan',$data);
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");

}

public function lmsstudent(){
	$periode = $this->input->post('periode');
	$tahunaka = $this->input->post('tahunaka');

	//cari tanggal max di dasboard bulan dan tahunnya-------------------------------------------------------
	$real2="select max(substr(tglposting,1,7)) as tglposting from dasboard_mhs where ta='$tahunaka' and ta <>'0000-00-00' and periode='$periode'";
	$rela2 = $this->Mainmodel->tampildatacollege($real2);
		foreach($rela2 as $realis2){
			$tglposting = $realis2->tglposting;
		}

	$data['getLmsCabang'] = $this->Mainmodel->getLmsCabang($periode,$tahunaka,$tglposting)->result();

	$getkocabtk = $this->Mainmodel->getKocabTk($periode,$tahunaka)->result();
	foreach($getkocabtk as $rkocabtk){
		$kodecabang = $rkocabtk->kodecabang;
	}

	
	// $data['kodecabang'] = $kodecabang;
	$data['periode'] = $periode;
	$data['tahunaka'] = $tahunaka;

	$this->load->view('form/pendidikan/lmsstudent',$data);
	
}

public function Gettk(){
	$periode = $this->input->post('periode');
	$tahunaka = $this->input->post('tak');
	$kodecabang = $this->input->post('kocab');

	// cari mhs tingkat1
	$jmltk1 = $this->Mainmodel->getTk1($kodecabang,substr($tahunaka,0,4))->result();
	foreach($jmltk1 as $rjmltk1);
		$jumlahtk1 = $rjmltk1->jumlahtk1;
	
	// cari reg tingkat1
	$regTk1 = $this->Mainmodel->regTk1($kodecabang,substr($tahunaka,0,4))->result();
	foreach($regTk1 as $rregTk1);
		$registk1 = $rregTk1->nim;
	

	if($jumlahtk1 == '0'){
		$data['tk1'] = '0';
	}else{
		$data['tk1'] = ($registk1/$jumlahtk1)*100;
	}


	// cari mhs tingkat 2
	$jmltk2 = $this->Mainmodel->getTk2($kodecabang,$tahunaka)->result();
	foreach($jmltk2 as $rjmltk2);
		$jumlahtk2 = $rjmltk2->jumlahtk2;
	
	// cari reg tingkat2
	$regTk2 = $this->Mainmodel->regTk2($kodecabang,$tahunaka)->result();
	foreach($regTk2 as $rregtk2);
		$registk2 = $rregtk2->jumlah;
	

	$namacabang = $this->Mainmodel->getWhere('cabang',array('kodecabang'=>$kodecabang))->result();
	foreach($namacabang as $rnama);
		$data['namacabang'] = $rnama->namacabang;
	

	if($jumlahtk2 == '0'){
		$data['tk2'] = '0';
	}else{
		$data['tk2'] = ($registk2/$jumlahtk2)*100;
	}

	$this->load->view('form/pendidikan/detailtingkat',$data);
}

public function GetJurusanTk(){
	$periode = $this->input->post('periode');
	$tak = $this->input->post('tak');
	$kocab = $this->input->post('kocab');
	$tingkat = $this->input->post('tingkat');
	$ta = substr($tak,0,4);

	$data['kocab'] = $kocab;
	$data['tak'] = $tak;
	$data['tingkat'] = $tingkat;
	$data['ta'] = $ta;

	$data['ambiljurusantk'] = $this->Mainmodel->getJurusanTk($kocab,$tak,$tingkat,$ta)->result();

	$this->load->view('form/pendidikan/lmsstudent-jurusan',$data);

}

function rekapdosen(){
	$usernya			= $this->session->userdata('username');
	//$tahun				= $this->input->post('tahun');
	// $data['kriteria'] = $kriteria;
	$waktu=date('y-m-d');
//	$data['tahun'] = $tahun; 


	$data['konten'] = 'konten/rekapdosentab';
	$this->load->view('Dashboard',$data);
}
				
function getrekapdosen(){

	$kriteria			= $this->input->post('kriteria');
	$usernya			= $this->session->userdata('username');
	//$tahun				= $this->input->post('tahun');
	$data['kriteria'] = $kriteria;
	$waktu=date('y-m-d');
//	$data['tahun'] = $tahun; 
	
	// $combo_tabiodata		= $this->Drop_model->combo_tabiodata();		
	// $data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_tabiodata);
	// 			$thj1=$tahun;
	// 			$thj2=$tahun-1;
	// 			$thj3=$tahun-2;
	// 			$thsen1=$tahun."/".($tahun+1);
	// 			$thsen2=($tahun-1)."/".$tahun;
	// 			$thsen3=($tahun-2)."/".($tahun-1);


	if($kriteria=='Coll')
	{
		$getperiode = $this->Mainmodel->getPeriode($waktu,'Coll')->result();
		foreach($getperiode as $kperiode){
			$tahun = $kperiode->ta;
			$periodeta = $kperiode->periode;
		}
		$perjadwal=substr($tahun,0,4);
		$perjadwal1=$perjadwal."1";
		$perjadwal2=$perjadwal."2";
		$ambil="select cabang.kodecabang,cabang.namacabang, count((namadosen)) as Total,
					sum((case when (dosen.jenisdosen='1') then 1 else 0 end)) as 'fulltime',
					sum((case when (dosen.jenisdosen='2') then 1 else 0 end)) as 'partime'
					from dosen join cabang on dosen.kodecabang=cabang.kodecabang 
				where (cabang.kelompok='1' or kelompok='3') GROUP BY dosen.kodecabang";
		$dbnya="tampildatacollege"; 
		
		$label='Data Tenaga Pendidik';
	}
	else
	{
		$getperiode = $this->Mainmodel->getPeriode($waktu,'group')->result();
		foreach($getperiode as $kperiode){
			$tahun = $kperiode->ta;
			$periodeta = $kperiode->periode;
		}
		$perjadwal=substr($tahun,0,4);
		$perjadwal1=$perjadwal."1";
		$perjadwal2=$perjadwal."2";
		$ambil="SELECT  jadual.kodecabang,
				count(DISTINCT(IF(dosen.jenisdosen=1, jadual.kodedosen, NULL)))  AS Tetap, 
				count(DISTINCT(IF(dosen.jenisdosen=2, jadual.kodedosen, NULL)))  AS Tidak_tetap, 
				count(DISTINCT(IF(dosen.jenisdosen=3, jadual.kodedosen, NULL)))  AS Lokal, 
				count(DISTINCT(IF(dosen.jenisdosen=0, jadual.kodedosen, NULL)))  AS Takteridentifikasi, 
				count(DISTINCT(jadual.kodedosen)) AS TOTAL 
					FROM jadual join dosen on (jadual.kodedosen=dosen.kodedosen)
					where (jadual.periode='$periodeta')
						and dosen.kodecabang in (select kodecabang from cabang where kelompok='aktif')
					GROUP BY jadual.kodecabang";
		$dbnya="tampildatapoltek";
		$label='Data Dosen PLJ';

		//cari jumlah total dosen aktif
		$ket="select count(DISTINCT(dosen.kodedosen)) as totalall ,
			  		 count(DISTINCT(IF(dosen.nidn<>'', dosen.kodedosen, NULL)))  AS bernidn,
			  		 count(DISTINCT(IF(jadual.periode='$periodeta', jadual.kodedosen, NULL)))  AS aktifngajar
			  FROM jadual join dosen on (jadual.kodecabang=dosen.kodecabang)
			  where  dosen.kodecabang in (select kodecabang from cabang where kelompok='aktif')";

	  $data['master']=$this->Mainmodel->$dbnya($ket);

	}
	

	$data['cabang']=$this->Mainmodel->$dbnya($ambil);
	$data['cabangc'] = $this->Mainmodel->getCabang()->result();
	
	
	
	$data['getperiode']=$getperiode;
	$data['nkriteria']=$label;
	$data['tahun']=$tahun;
	$data['kritpot']=$kriteria;
	$data['dbnya']=$dbnya;
	$data['periodeta']=$periodeta;
	// $data['thj2']=$thj2;
	// $data['thj3']=$thj3;
	// $data['thsen1']=$thsen1;
	// $data['thsen2']=$thsen2;
	// $data['thsen3']=$thsen3;
	// $data['kriteria']=$kriteria;

	$this->load->view('form/pendidikan/rekapdosen' ,$data);

}		

function getlistdosen(){

	$kriteria			= $this->input->post('kriteria');
	$kdcabang			= $this->input->post('kdcabang');
	$tahun				= $this->input->post('ta');
	$periode			= $this->input->post('periode');
	$usernya			= $this->session->userdata('username');
	$data['kriteria'] = $kriteria;
	$waktu=date('Y-m-d');

	if($kriteria=='Coll')
	{

		$perjadwal=substr($tahun,0,4);
		$perjadwal1=$perjadwal."1";
		$perjadwal2=$perjadwal."2";
		$ambil="select * from dosen where validasi='ya' and kodedosen in
		(select kodedosen from jadual where kodecabang='$kdcabang' and periode='$periode')";
		$dbnya="tampildatacollege"; 
		$namacabang="select * from cabang where kodecabang='$kdcabang'";

		$label='Daftar Data Tenaga Pendidik Cabang';
		$data['nmcab'] = $this->Mainmodel->tampildatacollege($namacabang);

	}
	else
	{

		$perjadwal=substr($tahun,0,4);
		$perjadwal1=$perjadwal."1";
		$perjadwal2=$perjadwal."2";

		$ambil="select * from dosen where kodedosen in
		(select kodedosen from jadual where kodecabang='$kdcabang' and periode='$periode' group by kodedosen)";

		$dbnya="tampildatapoltek";
		$label='Data Dosen PLJ';
		$namacabang="select * from cabang where kodecabang='$kdcabang'";
		$data['nmcab'] = $this->Mainmodel->tampildatapoltek($namacabang);


	}
	// echo $kriteria;

	$data['cabang']=$this->Mainmodel->$dbnya($ambil);
	$data['cabangc'] = $this->Mainmodel->getCabang()->result();
	
	
	
	$data['getperiode']=$periode;
	$data['nkriteria']=$label;
	$data['tahun']=$tahun;
	$data['kritpot']=$kriteria;
	$data['dbnya']=$dbnya;

	$this->load->view('form/pendidikan/listdosen' ,$data);

}			



			

	
	
}
