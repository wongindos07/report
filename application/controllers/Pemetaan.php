<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemetaan extends CI_Controller {
	/**
	IT LP3I Development Teams
	 */

	function __construct(){
		parent::__construct();
		$this->load->model('Mainmodel');
		$this->load->library('session');
		if($this->session->userdata('status') !== 'masuk'){
 			redirect('Login');
 		}
	}

	public function index()
	{   

		// $data['getNama'] = $this->Mainmodel->getNama($this->session->userdata('nik'))->result();

		$dates=Date('Y-m-d');
		// 	COLLEGE
		// inisial TA
		$real1="select * from inisialpmb";
		$ta = $this->Mainmodel->tampildatacollege($real1);
		foreach($ta as $rota){
			$tapmb = $rota->ta;
            $tahun = substr($tapmb,0,4);

		}
		$gel = "select * from gelombangmaster order by gelombang desc limit 1";
		$gelom = $this->Mainmodel->tampildatacollege($gel);
		foreach($gelom as $bang){
			$tglgelahir = $bang->tglakhir;
		}
		$data['tglgelahir'] = $tglgelahir;

		// =================================================grafik aplikan MARKETING college=========================================
		// grafik aplikan
		$jenis = "select PddkAkhir,count(PddkAkhir) as jumlah from aplikan where ta='$tapmb' and aplikan.kodecabang in (select kodecabang from cabang where kelompok='1') 
		          group by PddkAkhir order by PddkAkhir asc";
		$data['sekolah'] = $this->Mainmodel->tampildatacollege($jenis);

        $tot = "select count(*) as total from aplikan where ta='$tapmb' and aplikan.kodecabang in (select kodecabang from cabang where kelompok='1')";
		$totaplikan = $this->Mainmodel->tampildatacollege($tot);
		foreach($totaplikan as $tota){
			$total = $tota->total;
		}

		$jenispoltek = "select PddkAkhir,count(PddkAkhir) as jumlah from aplikan where ta='$tapmb' and aplikan.kodecabang in (select kodecabang from cabang where kelompok='aktif') 
		          group by PddkAkhir order by PddkAkhir asc";
		$data['sekolahpoltek'] = $this->Mainmodel->tampildatapoltek($jenispoltek);

        $totpoltek = "select count(*) as totalpoltek from aplikan where ta='$tapmb' and aplikan.kodecabang in (select kodecabang from cabang where kelompok='aktif')";
		$totaplikanpoltek = $this->Mainmodel->tampildatapoltek($totpoltek);
		foreach($totaplikanpoltek as $totapoltek){
			$totalpoltek = $totapoltek->totalpoltek;
		}

		//------------------------- PHP sekolah BIODATA-------------------------------  ----------------------  

		$tot2 = "select count(*) as totalbio from biodata where tahunangkatan='$tahun ' and (status='u-30' or status='aktif')
		         and kodecabang in (select kodecabang from cabang where kelompok='1')";
		$totbio = $this->Mainmodel->tampildatacollege($tot2);
		foreach($totbio as $totabio){
			$totalbio = $totabio->totalbio;
		}
		
		$jenis2 = "select jenissekolah.Pendidikan as js,count(biodata.nim) as totalbio, biodata.TahunAngkatan as tapmb, 
		aplikan.ta as ta from biodata join aplikan on biodata.id=aplikan.id join jenissekolah on aplikan.PddkAkhir=jenissekolah.PddkAkhir 
		where aplikan.ta='$tapmb' and (biodata.`status`='aktif' or biodata.`status`='u-30') 
		and biodata.kodecabang in (select kodecabang from cabang where kelompok='1') group by aplikan.PddkAkhir desc";
		$data['sekolahbio'] = $this->Mainmodel->tampildatacollege($jenis2);
		
		$data['totalbiodata']= $totalbio;

		$tot2poltek = "select count(*) as totalbiopoltek from biodata where tahunangkatan='$tahun ' and (status='u-30' or status='aktif')
		         and kodecabang in (select kodecabang from cabang where kelompok='aktif')";
		$totbiopoltek = $this->Mainmodel->tampildatapoltek($tot2poltek);
		foreach($totbiopoltek as $totabiopoltek){
			$totalbiopoltek = $totabiopoltek->totalbiopoltek;
		}
		
		$jenis2poltek = "select jenissekolah.Pendidikan as js,count(biodata.nim) as totalbio, biodata.TahunAngkatan as tapmb, 
		aplikan.ta as ta from biodata join aplikan on biodata.id=aplikan.id join jenissekolah on aplikan.PddkAkhir=jenissekolah.PddkAkhir 
		where aplikan.ta='$tapmb' and (biodata.`status`='aktif' or biodata.`status`='u-30') 
		and biodata.kodecabang in (select kodecabang from cabang where kelompok='aktif') group by aplikan.PddkAkhir desc";
		$data['sekolahbiopoltek'] = $this->Mainmodel->tampildatapoltek($jenis2poltek);
		
		$data['totalbiodatapoltek']= $totalbiopoltek;
		//-------------------------end PHP sekolah BIODATA-------------------------------  ----------------------  

		//------------------------- PHP umur -------------------------------  ----------------------  
	   
	   $umap = "SELECT
		   CASE
		   WHEN umur < 18 THEN '< 18'
		   WHEN umur BETWEEN 18 and 20 THEN '18 - 20'
		   WHEN umur BETWEEN 21 and 22 THEN '21 - 22'
		   WHEN umur >= 23 THEN '>= 23 '
		   WHEN umur IS NULL THEN 'Not Identified'
			END as range_umur,
			COUNT(*) AS jumlahapumur
			FROM (select TIMESTAMPDIFF(YEAR, tgllahir, '$tglgelahir') AS umur from aplikan
			where ta='$tapmb' and KodeCabang in (select kodecabang from cabang where kelompok='1')
			)  as aplikan_umur
			GROUP BY range_umur
			ORDER BY range_umur";

	   $data['umuraplikan'] = $this->Mainmodel->tampildatacollege($umap);


	   $umbio = "SELECT
		   CASE
		   WHEN umur < 17 THEN '< 17'
		   WHEN umur BETWEEN 17 and 20 THEN '17 - 20'
		   WHEN umur BETWEEN 21 and 24 THEN '21 - 24'
		   WHEN umur >= 25 THEN '>= 25'
		   WHEN (umur IS NULL or umur=0 or umur >=100) THEN 'Not Identified'
			END as range_umurbio,
			COUNT(*) AS jumlahbioumur
			FROM (select TIMESTAMPDIFF(YEAR, tanggal_lahir, '$tglgelahir') AS umur from biodata
			where tahunangkatan='$tahun' and (status ='u-30' or status='aktif') and KodeCabang in 
			(select kodecabang from cabang where kelompok='1'))  as biodata_umur
			GROUP BY range_umurbio
			ORDER BY range_umurbio";

	   $data['umurbiodata'] = $this->Mainmodel->tampildatacollege($umbio);


	   $umapoltek = "SELECT
		   CASE
		   WHEN umur < 18 THEN '< 18'
		   WHEN umur BETWEEN 18 and 20 THEN '18 - 20'
		   WHEN umur BETWEEN 21 and 22 THEN '21 - 22'
		   WHEN umur >= 23 THEN '>= 23 '
		   WHEN umur IS NULL THEN 'Not Identified'
			END as range_umur,
			COUNT(*) AS jumlahapumur
			FROM (select TIMESTAMPDIFF(YEAR, tgllahir, '$tglgelahir') AS umur from aplikan
			where ta='$tapmb' and KodeCabang in (select kodecabang from cabang where kelompok='aktif')
			)  as aplikan_umur
			GROUP BY range_umur
			ORDER BY range_umur";

	   $data['umuraplikanpoltek'] = $this->Mainmodel->tampildatapoltek($umapoltek);


	   $umbiopoltek = "SELECT
		   CASE
		   WHEN umur < 17 THEN '< 17'
		   WHEN umur BETWEEN 17 and 20 THEN '17 - 20'
		   WHEN umur BETWEEN 21 and 24 THEN '21 - 24'
		   WHEN umur >= 25 THEN '>= 25'
		   WHEN (umur IS NULL or umur=0 or umur >=100) THEN 'Not Identified'
			END as range_umurbio,
			COUNT(*) AS jumlahbioumur
			FROM (select TIMESTAMPDIFF(YEAR, tanggal_lahir, '$tglgelahir') AS umur from biodata
			where tahunangkatan='$tahun' and (status ='u-30' or status='aktif') and KodeCabang in 
			(select kodecabang from cabang where kelompok='aktif'))  as biodata_umur
			GROUP BY range_umurbio
			ORDER BY range_umurbio";

	   $data['umurbiodatapoltek'] = $this->Mainmodel->tampildatapoltek($umbiopoltek);
	   //$data['umbio'] = $umbio;

	   //-------------------------end PHP UMUR------------------------------  ----------------------  

//-------------PHP SEX ----------------------------------------------------------------------------
$sexapl = "SELECT
	   CASE
	   WHEN sex =1 THEN 'Laki-laki'
	   WHEN sex =2 THEN 'Perempuan'
	   WHEN (sex IS NULL or sex='' or sex >2) THEN 'Not Identified'
		END as sexapli,
		COUNT(*) AS jumlahsex
		FROM (select sex AS sex from aplikan
		where ta='$tapmb' and KodeCabang in 
		(select kodecabang from cabang where kelompok='1'))  as aplikan_sex
		GROUP BY sexapli
		ORDER BY sexapli asc";

	   $data['sexaplikan'] = $this->Mainmodel->tampildatacollege($sexapl);

$sexbio = "SELECT
	   CASE
	   WHEN sex =1 THEN 'Laki-laki'
	   WHEN sex =2 THEN 'Perempuan'
	   WHEN (sex IS NULL or sex='0' or sex >2) THEN 'Not Identified'
		END as sexbiod,
		COUNT(*) AS jumlahsexbio
		FROM (select sex AS sex from biodata
		where tahunangkatan='$tahun' and (status ='u-30' or status='aktif') and KodeCabang in 
			(select kodecabang from cabang where kelompok='1')) as biodata_sex
		GROUP BY sexbiod
		ORDER BY sexbiod asc";

$data['sexbiodata'] = $this->Mainmodel->tampildatacollege($sexbio);


$sexaplpoltek = "SELECT
	   CASE
	   WHEN sex =1 THEN 'Laki-laki'
	   WHEN sex =2 THEN 'Perempuan'
	   WHEN (sex IS NULL or sex='' or sex >2) THEN 'Not Identified'
		END as sexapli,
		COUNT(*) AS jumlahsex
		FROM (select sex AS sex from aplikan
		where ta='$tapmb' and KodeCabang in 
		(select kodecabang from cabang where kelompok='aktif'))  as aplikan_sex
		GROUP BY sexapli
		ORDER BY sexapli asc";

	   $data['sexaplikanpoltek'] = $this->Mainmodel->tampildatapoltek($sexaplpoltek);

$sexbiopoltek = "SELECT
	   CASE
	   WHEN sex =1 THEN 'Laki-laki'
	   WHEN sex =2 THEN 'Perempuan'
	   WHEN (sex IS NULL or sex='0' or sex >2) THEN 'Not Identified'
		END as sexbiod,
		COUNT(*) AS jumlahsexbio
		FROM (select sex AS sex from biodata
		where tahunangkatan='$tahun' and (status ='u-30' or status='aktif') and KodeCabang in 
			(select kodecabang from cabang where kelompok='aktif')) as biodata_sex
		GROUP BY sexbiod
		ORDER BY sexbiod asc";

$data['sexbiodatapoltek'] = $this->Mainmodel->tampildatapoltek($sexbiopoltek);
//------------------END PHP SEX----------------------------------------------------------------------    


//-------------PHP sumber INFO ----------------------------------------------------------------------------
$infom = "SELECT
CASE
WHEN kategori = 10 THEN 'Iklan Cetak'
  WHEN kategori = 11 THEN 'Iklan Radio'
  WHEN kategori = 12 THEN 'Eksebisi'
  WHEN kategori = 13 THEN 'Operasi Pasar'
  WHEN kategori = 14 THEN 'Informasi Sekolah'
  WHEN kategori = 15 THEN 'Direct Selling'
  WHEN kategori = 16 THEN 'Market Channel'
  WHEN kategori = 17 THEN 'Iklan Televisi'
  WHEN kategori = 18 THEN 'Media Luar Ruangan'
  WHEN kategori = 19 THEN 'Iklan Digital'
  WHEN (kategori = '' or kategori is NULL) THEN 'Not Identified'
 END as info,
 count(DISTINCT(id)) AS jumlahinfo
 FROM (select  DISTINCT(aplikan.id) as id,info.kategori AS kategori from info inner join aplikan 
      on (aplikan.kodecabang=info.kodecabang and aplikan.ta=info.ta) 
		where aplikan.ta='$tapmb' and aplikan.KodeCabang in 
		(select kodecabang from cabang where kelompok='1') group by id)  as aplikan_info
		group by info";

$data['infomaster'] = $this->Mainmodel->tampildatacollege($infom);

$infom2 = "SELECT
CASE
WHEN kategori = 10 THEN 'Iklan Cetak'
  WHEN kategori = 11 THEN 'Iklan Radio'
  WHEN kategori = 12 THEN 'Eksebisi'
  WHEN kategori = 13 THEN 'Operasi Pasar'
  WHEN kategori = 14 THEN 'Informasi Sekolah'
  WHEN kategori = 15 THEN 'Direct Selling'
  WHEN kategori = 16 THEN 'Market Channel'
  WHEN kategori = 17 THEN 'Iklan Televisi'
  WHEN kategori = 18 THEN 'Media Luar Ruangan'
  WHEN kategori = 19 THEN 'Iklan Digital'
  WHEN (kategori = '' or kategori is NULL) THEN 'Not Identified'
 END as info2,
 count(DISTINCT(nim)) AS jumlahinfo2
 FROM (select  DISTINCT(biodata.nim) as nim,info.kategori AS kategori,info.ta 
	from biodata inner join aplikan on ( aplikan.id=biodata.id) join info 
	on ( aplikan.kodecabang=info.kodecabang and aplikan.ta=info.ta) 
	where biodata.tahunangkatan='$tahun' and  biodata.KodeCabang in 
	(select kodecabang from cabang where kelompok='1') and 
	(biodata.status='U-30' or biodata.`status`='aktif') group by nim)  as bio_info
	group by info2";

$data['infomaster2'] = $this->Mainmodel->tampildatacollege($infom2);


$infompoltek = "SELECT
CASE
WHEN kategori = 10 THEN 'Iklan Cetak'
  WHEN kategori = 11 THEN 'Iklan Radio'
  WHEN kategori = 12 THEN 'Eksebisi'
  WHEN kategori = 13 THEN 'Operasi Pasar'
  WHEN kategori = 14 THEN 'Informasi Sekolah'
  WHEN kategori = 15 THEN 'Direct Selling'
  WHEN kategori = 16 THEN 'Market Channel'
  WHEN kategori = 17 THEN 'Iklan Televisi'
  WHEN kategori = 18 THEN 'Media Luar Ruangan'
  WHEN kategori = 19 THEN 'Iklan Digital'
  WHEN (kategori = '' or kategori is NULL) THEN 'Not Identified'
 END as info,
 count(DISTINCT(id)) AS jumlahinfo
 FROM (select  DISTINCT(aplikan.id) as id,info.kategori AS kategori from info inner join aplikan 
      on (aplikan.kodecabang=info.kodecabang and aplikan.ta=info.ta) 
		where aplikan.ta='$tapmb' and aplikan.KodeCabang in 
		(select kodecabang from cabang where kelompok='aktif') group by id)  as aplikan_info
		group by info";

$data['infomasterpoltek'] = $this->Mainmodel->tampildatapoltek($infompoltek);

$infom2poltek = "SELECT
CASE
WHEN kategori = 10 THEN 'Iklan Cetak'
  WHEN kategori = 11 THEN 'Iklan Radio'
  WHEN kategori = 12 THEN 'Eksebisi'
  WHEN kategori = 13 THEN 'Operasi Pasar'
  WHEN kategori = 14 THEN 'Informasi Sekolah'
  WHEN kategori = 15 THEN 'Direct Selling'
  WHEN kategori = 16 THEN 'Market Channel'
  WHEN kategori = 17 THEN 'Iklan Televisi'
  WHEN kategori = 18 THEN 'Media Luar Ruangan'
  WHEN kategori = 19 THEN 'Iklan Digital'
  WHEN (kategori = '' or kategori is NULL) THEN 'Not Identified'
 END as info2,
 count(DISTINCT(nim)) AS jumlahinfo2
 FROM (select  DISTINCT(biodata.nim) as nim,info.kategori AS kategori,info.ta 
	from biodata inner join aplikan on ( aplikan.id=biodata.id) join info 
	on ( aplikan.kodecabang=info.kodecabang and aplikan.ta=info.ta) 
	where biodata.tahunangkatan='$tahun' and  biodata.KodeCabang in 
	(select kodecabang from cabang where kelompok='aktif') and 
	(biodata.status='U-30' or biodata.`status`='aktif') group by nim)  as bio_info
	group by info2";

$data['infomaster2poltek'] = $this->Mainmodel->tampildatapoltek($infom2poltek);

//------------------END PHP SEX----------------------------------------------------------------------   


$infojurus = "select jurusan.namajurusan, count(biodata.nim) as jumlahjur from biodata join jurusan on 
			biodata.kode=jurusan.kodejurusan where biodata.TahunAngkatan='$tahun' and 
			(biodata.`status`='u-30' or biodata.status='aktif') and biodata.kode <> '' 
			and biodata.kodecabang in (SELECT kodecabang from cabang where kelompok='1') group by biodata.kode";

$data['infojurusan'] = $this->Mainmodel->tampildatacollege($infojurus);

$infojuruspoltek = "select jurusan.namajurusan, count(biodata.nim) as jumlahjur from biodata join jurusan on 
			biodata.kode=jurusan.kodejurusan where biodata.TahunAngkatan='$tahun' and 
			(biodata.`status`='u-30' or biodata.status='aktif') and biodata.kode <> '' 
			and biodata.kodecabang in (SELECT kodecabang from cabang where kelompok='aktif') group by biodata.kode";

$data['infojurusanpoltek'] = $this->Mainmodel->tampildatapoltek($infojuruspoltek);
//------------------END PHP jurusan---------------------------------------------------------------------- 

		$data['totalap']= $total;
		$data['totalappoltek']= $totalpoltek;
        $data['tapmb']  = $tapmb;
		//$tahunipk = "select tahunangkatan from ipk group by tahunangkatan order by tahunangkatan desc limit 1";
		//$ipkp	  = $this->Mainmodel->tampildatacollege($tahunipk);
		//foreach($ipkp as $ipktahun);
		//$tahun2 = $ipktahun->tahunangkatan;


		// ============================================= BATAS EDU ================================================



		$data['konten'] = 'laporan/grafik-pemetaan';
		$this->load->view('grafik',$data);
	}





	
}
