<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->model("Drop_model");
		$this->load->model("Mainmodel");
		$this->load->library('session');
		
		if($this->session->userdata('status') !== 'masuk'){
 			redirect('Login');
 		}

 		$cookie = get_cookie('sesilogin');
 		// jika cookie tidak ada maka akan pindah ke logout
        if($cookie == false){
           redirect('Main/logout');
           }

         delete_cookie('sesilogin');

         $nilai	=  $this->session->userdata('username');
 	     $waktu  =  1800;
 		 set_cookie('sesilogin',$nilai,$waktu);
	}

	public function index()
	{	
		$data['konten'] = 'form/keuangan/piutang';
		$this->load->view('Dashboard',$data);
	}

	public function potensi(){
        $kriteria			= $this->input->post('kriteria');
		$usernya			= $this->session->userdata('username');
		$tahun				= $this->input->post('tahun'); 
		$combo_thpmb		= $this->Drop_model->combo_pmbmkt();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_thpmb);
        
        if($kriteria=='J')
        {$label="Junior";}
        else if($kriteria=='S')
        {$label="Senior";}
        elseif($kriteria=='A')
		{$label="Junior & Senior";}
		else {$label="";}

		$ambil="select cabang.kodecabang,cabang.namacabang,
		sum((case when viewpotensiincome.status='u-30' and ket='Registrasi Mahasiswa Baru' then 1 else 0 END)) as 'u30',
		sum((case when viewpotensiincome.status='aktif' and ket='Registrasi Mahasiswa Baru' then 1 else 0 END)) as 'aktif',
		sum((case when viewpotensiincome.status='aktif' and ket='Registrasi Mahasiswa Senior' then 1 else 0 END)) as 'senaktif',
		sum((case when viewpotensiincome.status='u-30' and viewpotensiincome.ket='Registrasi Mahasiswa Baru' then (viewpotensiincome.jumlahbiaya) else 0 END)) as 'potensi_u30',
        sum((case when viewpotensiincome.status='aktif' and viewpotensiincome.ket='Registrasi Mahasiswa Baru' then (viewpotensiincome.jumlahbiaya) else 0 END)) as 'potensi_aktif',
        sum((case when (viewpotensiincome.status='aktif' and viewpotensiincome.ket='Registrasi Mahasiswa Baru' or viewpotensiincome.status='u-30') then (viewpotensiincome.jumlahbiaya) else 0 END)) as 'potensi_all',
        case when (viewpotensiincome.status='u-30' or viewpotensiincome.status='aktif' and viewpotensiincome.ket='Registrasi Mahasiswa Baru') then round(avg(viewpotensiincome.jumlahbiaya)) else 0 END as 'hrjalljun',
        sum((case when (viewpotensiincome.status='aktif' ) and viewpotensiincome.ket='Registrasi Mahasiswa Senior' then (viewpotensiincome.jumlahbiaya) else 0 END)) as 'potensi_allsen',
        case when (viewpotensiincome.status='aktif') and viewpotensiincome.ket='Registrasi Mahasiswa Senior' then round(avg(viewpotensiincome.jumlahbiaya)) else 0 END as 'hrjallsen',
        sum((case when (viewpotensiincome.status='u-30' or viewpotensiincome.status='aktif' )  then (viewpotensiincome.jumlahbiaya) else 0 END)) as 'potensiall',
        case when (viewpotensiincome.status='u-30' or viewpotensiincome.status='aktif')  then round(avg(viewpotensiincome.jumlahbiaya)) else 0 END as 'hrjall'
        from viewpotensiincome  join cabang on viewpotensiincome.kodecabang=cabang.kodecabang where viewpotensiincome.tahunakademik='$tahun'  
        and viewpotensiincome.jumlahbiaya <>'0' group by viewpotensiincome.kodecabang";	

       $ambil2="select * from konfigurasi_dp_registrasi where flag='1'";

        $data['potensi']= $this->Mainmodel->tampildatacollege($ambil);
        $data['potensi2']=$this->Mainmodel->tampildatapoltek($ambil);
        $dp=$this->Mainmodel->tampildatapoltek($ambil2);

		foreach($dp as $dp)
		{$dpr=$dp->keterangan;}

        $data['kritpot'] =$kriteria;
        $data['nkriteria'] =$label;

		$data['tahun'] =$tahun;
		$data['dpr'] =$dpr;
        
		// $data['u30'] =  $u30;

		$data['konten'] = 'form/keuangan/rekapptensiINC';
		$this->load->view('Dashboard',$data);
		// $this->load->view('home',$data);
	}

	public function getTahun(){
		$tipe = $this->input->post('tipe');
		$kriteria = $this->input->post('kriteria');
		$getTahun = $this->Mainmodel->getTahun($tipe,$kriteria)->result();
			echo "<option>-- Pilih Tahun Akademik --</option>";
		foreach($getTahun as $rowTahun){
			echo "<option>".$rowTahun->tahunakademik."</option>";
		}
	}

	public function getTipePiutang(){
		$kriteria = $_POST['kriteria'];
		if($kriteria == "Coll" || $kriteria == "group"){
			echo "<option value='0'>-- Pilih Tipe --</option>
                  <option value='Registrasi Mahasiswa Baru'>Junior</option>
                  <option value='Registrasi Mahasiswa Senior'>Senior</option>";
		}else{
			echo "<option value='0'>-- Pilih Tipe --</option>
                  <option value='Registrasi Mahasiswa Baru'>Tingkat 1</option>
                  <option value='Registrasi Mahasiswa Senior'>Tingkat 2</option>
                  <option value='Registrasi Mahasiswa Tingkat 3'>Tingkat 3</option>";
		}
	}


	public function getPiutang(){
		$tahun = $this->input->post('tahun');
		$tipe = $this->input->post('tipe');
		$kriteria = $this->input->post('kriteria');
		$Dashboard = $this->input->post('dashboard');
		$jeniscabang = $this->input->post('jeniscabang');

		if($Dashboard == ''){
			$data['dashboard'] = '';
		}else{
			$data['dashboard'] = 'dashboard';
		}

		$tahunangkatan = substr($tahun,0,4);
		$waktu = date("Y-m-d");

		if($jeniscabang == "milker"){
			// BEGIN
			if($kriteria=='Coll'){
			
			if($tipe == "Registrasi Mahasiswa Baru"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join biaya on cabang.kodecabang=biaya.kodecabang inner join biodata on biaya.nim=biodata.nim where ta='$tahun' and kelompok='1' and (kepemilikan='1' or kepemilikan='2') group by biaya.kodecabang";
			}else{
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regissenior on cabang.kodecabang=regissenior.kodecabang inner join biaya on regissenior.nim=biaya.nim where regissenior.ta='$tahun' and kelompok='1' and (kepemilikan='1' or kepemilikan='2') group by regissenior.kodecabang";
			}	
			$dbnya="tampildatacollege"; 
			$label='Rekapitulasi Piutang Peserta Didik LP3I College Per Cabang';
		}
        elseif($kriteria=='PLJ'){
			if($tipe == "Registrasi Mahasiswa Baru"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join biaya on cabang.kodecabang=biaya.kodecabang inner join biodata on biaya.nim=biodata.nim where ta='$tahun' and kelompok='aktif' and (kepemilikan='1' or kepemilikan='2') group by biaya.kodecabang";
			}elseif($tipe == "Registrasi Mahasiswa Senior"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regissenior on cabang.kodecabang=regissenior.kodecabang inner join biaya on regissenior.nim=biaya.nim where regissenior.ta='$tahun' and kelompok='aktif' and (kepemilikan='1' or kepemilikan='2') group by regissenior.kodecabang";
			}else{
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regispoltek on cabang.kodecabang=regispoltek.kodecabang inner join biaya on regispoltek.nim=biaya.nim where regispoltek.ta='2017/2018' and kelompok='aktif' and (kepemilikan='1' or kepemilikan='2') group by regispoltek.kodecabang";
			}
			$dbnya="tampildatapoltek";
			$label='Rekapitulasi Piutang Peserta Didik LP3I Politeknik Per Kampus';
		}else{
			if($tipe == "Registrasi Mahasiswa Baru"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join biaya on cabang.kodecabang=biaya.kodecabang inner join biodata on biaya.nim=biodata.nim where ta='$tahun' and kelompok='4' and (kepemilikan='1' or kepemilikan='2') group by biaya.kodecabang";
			}else{
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regissenior on cabang.kodecabang=regissenior.kodecabang inner join biaya on regissenior.nim=biaya.nim where regissenior.ta='$tahun' and kelompok='4' and (kepemilikan='1' or kepemilikan='2') group by regissenior.kodecabang";
			}	
			$dbnya="tampildatacollege"; 
			$label='Rekapitulasi Piutang Peserta Didik LP3I Group Per Kampus';
		}
			// END
		}else{
			// BEGIN
			if($kriteria=='Coll'){
			
			if($tipe == "Registrasi Mahasiswa Baru"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join biaya on cabang.kodecabang=biaya.kodecabang inner join biodata on biaya.nim=biodata.nim where ta='$tahun' and kelompok='1' group by biaya.kodecabang";
			}else{
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regissenior on cabang.kodecabang=regissenior.kodecabang inner join biaya on regissenior.nim=biaya.nim where regissenior.ta='$tahun' and kelompok='1' group by regissenior.kodecabang";
			}	
			$dbnya="tampildatacollege"; 
			$label='Rekapitulasi Piutang Peserta Didik LP3I College Per Cabang';
		}
        elseif($kriteria=='PLJ'){
			if($tipe == "Registrasi Mahasiswa Baru"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join biaya on cabang.kodecabang=biaya.kodecabang inner join biodata on biaya.nim=biodata.nim where ta='$tahun' and kelompok='aktif' group by biaya.kodecabang";
			}elseif($tipe == "Registrasi Mahasiswa Senior"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regissenior on cabang.kodecabang=regissenior.kodecabang inner join biaya on regissenior.nim=biaya.nim where regissenior.ta='$tahun' and kelompok='aktif' group by regissenior.kodecabang";
			}else{
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regispoltek on cabang.kodecabang=regispoltek.kodecabang inner join biaya on regispoltek.nim=biaya.nim where regispoltek.ta='2017/2018' and kelompok='aktif' group by regispoltek.kodecabang";
			}
			$dbnya="tampildatapoltek";
			$label='Rekapitulasi Piutang Peserta Didik LP3I Politeknik Per Kampus';
		}else{
			if($tipe == "Registrasi Mahasiswa Baru"){
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join biaya on cabang.kodecabang=biaya.kodecabang inner join biodata on biaya.nim=biodata.nim where ta='$tahun' and kelompok='4' group by biaya.kodecabang";
			}else{
				$ambil="select cabang.namacabang, cabang.kodecabang from cabang inner join regissenior on cabang.kodecabang=regissenior.kodecabang inner join biaya on regissenior.nim=biaya.nim where regissenior.ta='$tahun' and kelompok='4' group by regissenior.kodecabang";
			}	
			$dbnya="tampildatacollege"; 
			$label='Rekapitulasi Piutang Peserta Didik LP3I Group Per Kampus';
		}
			// END
		}

		


		$data['tipe'] = $tipe;
		$data['kriteria'] = $kriteria;
		$data['tahun'] = $tahun;
		$data['tahunangkatan'] = $tahunangkatan;
		$data['label'] = $label;
		$data['waktu'] = $waktu;
		$data['cabang'] = $this->Mainmodel->$dbnya($ambil);
		$data['jeniscabang'] = $jeniscabang;

		$this->load->view('form/keuangan/piutang-pd', $data);
		
	}

	public function getDetailPiutangJunior(){
		$kocab = $this->input->post('kocab');
		$tahun = $this->input->post('tahun');
		$tipe = $this->input->post('tipe');
		$kriteria = $this->input->post('kriteria');
		$data['dashboard'] = $this->input->post('dashboard');
		$waktu = date("Y-m-d");
		$data['tahun'] = $tahun;
		$jeniscabang = $this->input->post('jeniscabang');

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$kocab = $rownama->kodecabang;
		$data['namacabang'] = $rownama->namacabang;

		$getPiutangCab = $this->Mainmodel->getSumPiutangJunior($tahun,$waktu,$kocab,$kriteria)->result();
		foreach($getPiutangCab as $rowsum);
		$sebesar = $rowsum->sebesar;
		$terbayar = $rowsum->terbayar;
		$data['total'] = $sebesar - $terbayar;

		$data['detailPiutang'] = $this->Mainmodel->getDetailPiutangjunior($tahun,$waktu,$kocab,$kriteria)->result(); 
		$data['links'] = base_url('index.php/Keuangan/pdfDetailJunior/'.$kocab.'/'.$tahun.'/'.$kriteria);
		$data['linkprint'] = base_url('index.php/Keuangan/printDetailJunior/'.$kocab.'/'.$tahun.'/'.$kriteria);
		$data['kriteria'] = $kriteria;
		$data['tipe'] = $tipe;
		$data['jeniscabang'] = $jeniscabang;
		

		if($kriteria == 'Coll'){
			$this->load->view('form/keuangan/detailpiutang',$data);
		}else{
			$this->load->view('form/keuangan/detailpiutangpol',$data);
		}
		// $this->load->view('form/keuangan/detailpiutang',$data);
	}

	public function getDetailPiutangSenior(){
		$kocab = $this->input->post('kocab');
		$tahun = $this->input->post('tahun');
		$tipe = $this->input->post('tipe');
		$kriteria = $this->input->post('kriteria');
		$data['dashboard'] = $this->input->post('dashboard');
		$waktu = date("Y-m-d");
		$data['tahun'] = $tahun;
		$jeniscabang = $this->input->post('jeniscabang');

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;
		$kocab = $rownama->kodecabang;

		$getPiutangCab = $this->Mainmodel->getSumPiutangSenior($tahun,$waktu,$kocab,$kriteria)->result();
		foreach($getPiutangCab as $rowsum);
		$sebesar = $rowsum->sebesar;
		$terbayar = $rowsum->terbayar;
		$data['total'] = $sebesar - $terbayar;  

		$data['detailPiutang'] = $this->Mainmodel->getDetailPiutangSenior($tahun,$waktu,$kocab,$kriteria)->result();
		$data['links'] = base_url('index.php/Keuangan/pdfDetailSenior/'.$kocab.'/'.$tahun);
		$data['linkprint'] = base_url('index.php/Keuangan/printDetailSenior/'.$kocab.'/'.$tahun); 
		$data['kriteria'] = $kriteria;
		$data['tipe'] = $tipe;
		$data['jeniscabang'] = $jeniscabang;

		if($kriteria == 'Coll'){
			$this->load->view('form/keuangan/detailpiutang',$data);
		}else{
			$this->load->view('form/keuangan/detailpiutangpol',$data);
		}
	
		

	}

	public function getDetailPiutangPoltek(){
		$kocab = $this->input->post('kocab');
		$tahun = $this->input->post('tahun');
		$tipe = $this->input->post('tipe');
		$kriteria = $this->input->post('kriteria');
		$data['dashboard'] = $this->input->post('dashboard');
		$waktu = date("Y-m-d");
		$data['tahun'] = $tahun;
		$jeniscabang = $this->input->post('jeniscabang');

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;
		$kocab = $rownama->kodecabang;

		$getPiutangCab = $this->Mainmodel->getSumPiutangPoltek($tahun,$waktu,$kocab)->result();
		foreach($getPiutangCab as $rowsum);
		$sebesar = $rowsum->sebesar;
		$terbayar = $rowsum->terbayar;
		$data['total'] = $sebesar - $terbayar;

		$data['detailPiutang'] = $this->Mainmodel->getDetailPiutangPoltek($tahun,$waktu,$kocab)->result();
		$data['links'] = base_url('index.php/Keuangan/pdfDetailSenior/'.$kocab.'/'.$tahun);
		$data['linkprint'] = base_url('index.php/Keuangan/printDetailSenior/'.$kocab.'/'.$tahun); 
		$data['kriteria'] = $kriteria;
		$data['tipe'] = $tipe;
		$data['jeniscabang'] = $jeniscabang;
	
		$this->load->view('form/keuangan/detailpiutangpol',$data);
	}


	public function pdfDetailJunior(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	// $this->pdf->AddPage('L');

		$kocab = $this->uri->segment(3);
		$tahun1 = $this->uri->segment(4);
		$tahun2 = $this->uri->segment(5);
		$kriteria = $this->uri->segment(6);
		$tahun = $tahun1.'/'.$tahun2;
		$waktu = date("Y-m-d");
		$data['tahun'] = $tahun;
		$data['kriteria'] = 'Junior';

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;

		$getPiutangCab = $this->Mainmodel->getSumPiutangJunior($tahun,$waktu,$kocab,$kriteria)->result();
		foreach($getPiutangCab as $rowsum);
		$sebesar = $rowsum->sebesar;
		$terbayar = $rowsum->terbayar;
		$data['total'] = $sebesar - $terbayar;

		$data['detailPiutang'] = $this->Mainmodel->getDetailPiutangjunior($tahun,$waktu,$kocab,$kriteria)->result(); 

		$data['konten'] = 'laporan/detail-pdf';
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	public function pdfDetailSenior(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	// $this->pdf->AddPage('L');

		$kocab = $this->uri->segment(3);
		$tahun1 = $this->uri->segment(4);
		$tahun2 = $this->uri->segment(5);
		$kriteria = $this->uri->segment(6);
		$tahun = $tahun1.'/'.$tahun2;
		$waktu = date("Y-m-d");
		$data['tahun'] = $tahun;
		$data['kriteria'] = 'Senior';

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;
		$kocab = $rownama->kodecabang;

		$getPiutangCab = $this->Mainmodel->getSumPiutangSenior($tahun,$waktu,$kocab,$kriteria)->result();
		foreach($getPiutangCab as $rowsum);
		$sebesar = $rowsum->sebesar;
		$terbayar = $rowsum->terbayar;
		$data['total'] = $sebesar - $terbayar;  

		$data['detailPiutang'] = $this->Mainmodel->getDetailPiutangSenior($tahun,$waktu,$kocab,$kriteria)->result(); 

		$data['konten'] = 'laporan/detail-pdf';
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");

	}

	public function printDetailSenior(){

		$kocab = $this->uri->segment(3);
		$tahun1 = $this->uri->segment(4);
		$tahun2 = $this->uri->segment(5);
		$kriteria = $this->uri->segment(6);
		$tahun = $tahun1.'/'.$tahun2;
		$waktu = date("Y-m-d");
		$data['tahun'] = $tahun;
		$data['kriteria'] = 'Senior';

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;
		$kocab = $rownama->kodecabang;

		$getPiutangCab = $this->Mainmodel->getSumPiutangSenior($tahun,$waktu,$kocab,$kriteria)->result();
		foreach($getPiutangCab as $rowsum);
		$sebesar = $rowsum->sebesar;
		$terbayar = $rowsum->terbayar;
		$data['total'] = $sebesar - $terbayar;  

		$data['detailPiutang'] = $this->Mainmodel->getDetailPiutangSenior($tahun,$waktu,$kocab,$kriteria)->result(); 

		$data['konten'] = 'laporan/detail-print';
		$html = $this->load->view('laporan',$data);

	}

	public function printDetailJunior(){
		$kocab = $this->uri->segment(3);
		$tahun1 = $this->uri->segment(4);
		$tahun2 = $this->uri->segment(5);
		$kriteria = $this->uri->segment(6);
		$tahun = $tahun1.'/'.$tahun2;
		$waktu = date("Y-m-d");
		$data['tahun'] = $tahun;
		$data['kriteria'] = 'Junior';

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;

		$getPiutangCab = $this->Mainmodel->getSumPiutangJunior($tahun,$waktu,$kocab,$kriteria)->result();
		foreach($getPiutangCab as $rowsum);
		$sebesar = $rowsum->sebesar;
		$terbayar = $rowsum->terbayar;
		$data['total'] = $sebesar - $terbayar;

		$data['detailPiutang'] = $this->Mainmodel->getDetailPiutangjunior($tahun,$waktu,$kocab,$kriteria)->result(); 

		$data['konten'] = 'laporan/detail-print';
		$this->load->view('laporan',$data);

	}

	function registrasiPmb(){
		$combo_tabiodata		= $this->Drop_model->combo_tabiodata();		
		$data=array('cmbkriteriapmb'=>'','cmbtahun'=>$combo_tabiodata);

		$data['konten'] = 'form/keuangan/registrasiPmb';
		$this->load->view('Dashboard',$data);
	}

	function getTakPmb(){
		$kriteria = $this->input->post('tipe');

		if($kriteria=='Coll'){
			$ambil="select ta from aplikan where ta <> '' group by ta order by ta desc limit 5";
			$dbnya="tampildatacollege";
		}elseif($kriteria=='PLJ'){
			$ambil="select ta from aplikan where ta <> '' group by ta order by ta desc limit 5";
			$dbnya="tampildatapoltek";
		}else{
			$ambil="select ta from aplikan where ta <> '' group by ta order by ta desc limit 5";
			$dbnya="tampildatacollege";
		}

		$data = $this->Mainmodel->$dbnya($ambil);
			echo "<option value=''>-- Pilih Tahun --</option>";
		foreach($data as $nilai){
			?>
			<option value="<?=$nilai->ta?>"><?=$nilai->ta?></option>
			<?php
		}
	}

	function regCabangPmb(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('tipe');

		if($kriteria=='Coll'){
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='1' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Cabang';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='aktif' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Cabang';
		}else{
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='4' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Cabang';
		}

		$data['tak'] = $tahun;
		$data['kriteria'] = $kriteria;
		$data['dp'] = $this->Mainmodel->dpreg($kriteria)->result();
		$data['label'] = $label;
		$data['cabang'] = $this->Mainmodel->$dbnya($ambil);
		
		$this->load->view('form/keuangan/cabang-pmb', $data);
	}

	function regJurusanPmb(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('tipe');
		$kocab = $this->input->post('kocab');
		$jenis = $this->input->post('jenis');
		$status = $this->input->post('status');
		$ket = $this->input->post('ket');

		if($kriteria=='Coll'){
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' group by biodata.kode";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Jurusan';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' group by biodata.kode";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Jurusan';
		}else{
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' group by biodata.kode";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Jurusan';
		}

		$data['tak'] 		= $tahun;
		$data['kriteria'] 	= $kriteria;
		$data['jenis'] 		= $jenis;
		$data['kocab'] 		= $kocab;
		$data['status'] 	= $status;
		$data['ket'] 		= $ket;
		$data['dp'] 		= $this->Mainmodel->dpreg($kriteria)->result();
		$data['label']	    = $label;
		$data['jurusan'] 	= $this->Mainmodel->$dbnya($ambil);
		$this->load->view('form/keuangan/jurusan-pmb', $data);

	}

	function regMhsPmb(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('tipe');
		$kocab = $this->input->post('kocab');
		$jenis = $this->input->post('jenis');
		$status = $this->input->post('status');
		$ket = $this->input->post('ket');
		$kojur = $this->input->post('kojur');

		if($kriteria=='Coll'){
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Mahasiswa';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Mahasiswa';
		}else{
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Mahasiswa';
		}

		$data['tak'] 		= $tahun;
		$data['kriteria'] 	= $kriteria;
		$data['jenis'] 		= $jenis;
		$data['kocab'] 		= $kocab;
		$data['status'] 	= $status;
		$data['ket'] 		= $ket;
		$data['kojur']      = $kojur;
		$data['dp'] 		= $this->Mainmodel->dpreg($kriteria)->result();
		$data['label']	    = $label;
		$data['mahasiswa'] 	= $this->Mainmodel->$dbnya($ambil);
		$this->load->view('form/keuangan/mahasiswa-pmb', $data);
	}

	function printcabangpmb(){
		$tahun = $this->uri->segment('4').'/'.$this->uri->segment('5');
		$kriteria = $this->uri->segment('3');

		if($kriteria=='Coll'){
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='1' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Cabang';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='aktif' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Cabang';
		}else{
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='4' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Cabang';
		}

		$data['tak'] = $tahun;
		$data['kriteria'] = $kriteria;
		$data['dp'] = $this->Mainmodel->dpreg($kriteria)->result();
		$data['label'] = $label;
		$data['cabang'] = $this->Mainmodel->$dbnya($ambil);
		
		$data['konten'] = 'laporan/cabang-pmb';
		$this->load->view('laporan',$data);
	}

	function cabangpmbpdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('P');

    	$tahun = $this->uri->segment('4').'/'.$this->uri->segment('5');
		$kriteria = $this->uri->segment('3');


    	if($kriteria=='Coll'){
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='1' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi PMB LP3I College Per Cabang';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='aktif' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi PMB Politeknik Per Cabang';
		}else{
			$ambil="select namacabang, bayarkuliah.kodecabang, bayarkuliah.tahunakademik from cabang inner join bayarkuliah on cabang.kodecabang=bayarkuliah.kodecabang where cabang.kelompok='4' and bayarkuliah.tahunakademik='$tahun' group by namacabang order by namacabang asc";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi PMB Group LP3I Per Cabang';
		}

		$data['tak'] = $tahun;
		$data['kriteria'] = $kriteria;
		$data['dp'] = $this->Mainmodel->dpreg($kriteria)->result();
		$data['label'] = $label;
		$data['cabang'] = $this->Mainmodel->$dbnya($ambil);

    	//$this->load->view('home',$data);
		$data['konten'] = 'laporan/cabang-pmbpdf';
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	function printjurusanpmb(){
		$tahun = $this->uri->segment('4').'/'.$this->uri->segment('5');
		$kriteria = $this->uri->segment('3');
		$kocab = $this->uri->segment('6');
		$jenis = $this->uri->segment('7');
		$status = $this->uri->segment('8');
		$ket = $this->uri->segment('9');

		if($jenis == 'up'){
			$jenis = '>=30%';
		}elseif($jenis == 'under'){
			$jenis = '<30%';
		}else{
			$jenis = 'TK 2';
		}

		if($ket == 'baru'){
			$kett = 'Registrasi Mahasiswa Baru';
		}else{
			$kett = 'Registrasi Mahasiswa Senior';
		}

		if($kriteria=='Coll'){
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$kett' group by biodata.kode";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Jurusan';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$kett' group by biodata.kode";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Jurusan';
		}else{
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$kett' group by biodata.kode";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Jurusan';
		}

		$data['tak'] 		= $tahun;
		$data['kriteria'] 	= $kriteria;
		$data['jenis'] 		= $jenis;
		$data['kocab'] 		= $kocab;
		$data['status'] 	= $status;
		$data['ket'] 		= $kett;
		$data['dp'] 		= $this->Mainmodel->dpreg($kriteria)->result();
		$data['label']	    = $label;
		$data['jurusan'] 	= $this->Mainmodel->$dbnya($ambil);

		$data['konten'] = 'laporan/jurusan-pmb';
		$this->load->view('laporan',$data);
	}

	function jurusanpmbpdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('P');

    	$tahun = $this->uri->segment('4').'/'.$this->uri->segment('5');
		$kriteria = $this->uri->segment('3');
		$kocab = $this->uri->segment('6');
		$jenis = $this->uri->segment('7');
		$status = $this->uri->segment('8');
		$ket = $this->uri->segment('9');

		if($jenis == 'up'){
			$jenis = '>=30%';
		}elseif($jenis == 'under'){
			$jenis = '<30%';
		}else{
			$jenis = 'TK 2';
		}

		if($ket == 'baru'){
			$ket = 'Registrasi Mahasiswa Baru';
		}else{
			$ket = 'Registrasi Mahasiswa Senior';
		}

		if($kriteria=='Coll'){
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' group by biodata.kode";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Jurusan';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' group by biodata.kode";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Jurusan';
		}else{
			$ambil="select biodata.kode, bayarkuliah.kodecabang from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$kett' group by biodata.kode";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Jurusan';
		}

		$data['tak'] 		= $tahun;
		$data['kriteria'] 	= $kriteria;
		$data['jenis'] 		= $jenis;
		$data['kocab'] 		= $kocab;
		$data['status'] 	= $status;
		$data['ket'] 		= $ket;
		$data['dp'] 		= $this->Mainmodel->dpreg($kriteria)->result();
		$data['label']	    = $label;
		$data['jurusan'] 	= $this->Mainmodel->$dbnya($ambil);

		//$this->load->view('home',$data);
		$data['konten'] = 'laporan/jurusan-pmbpdf';
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	function printmhspmb(){
		$tahun = $this->uri->segment('4').'/'.$this->uri->segment('5');
		$kriteria = $this->uri->segment('3');
		$kocab = $this->uri->segment('6');
		$jenis = $this->uri->segment('7');
		$status = $this->uri->segment('8');
		$ket = $this->uri->segment('9');
		$kojur = $this->uri->segment('10');

		if($jenis == 'up'){
			$jenis = '>=30%';
		}elseif($jenis == 'under'){
			$jenis = '<30%';
		}else{
			$jenis = 'TK 2';
		}

		if($ket == 'baru'){
			$ket = 'Registrasi Mahasiswa Baru';
		}else{
			$ket = 'Registrasi Mahasiswa Senior';
		}

		if($kriteria=='Coll'){
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Mahasiswa';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Mahasiswa';
		}else{
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Mahasiswa';
		}

		$data['tak'] 		= $tahun;
		$data['kriteria'] 	= $kriteria;
		$data['jenis'] 		= $jenis;
		$data['kocab'] 		= $kocab;
		$data['status'] 	= $status;
		$data['ket'] 		= $ket;
		$data['dp'] 		= $this->Mainmodel->dpreg($kriteria)->result();
		$data['label']	    = $label;
		$data['mahasiswa'] 	= $this->Mainmodel->$dbnya($ambil);

		$data['konten'] = 'laporan/mhs-pmb';
		$this->load->view('laporan',$data);
	}

	function mhspmbpdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('P');

    	$tahun = $this->uri->segment('4').'/'.$this->uri->segment('5');
		$kriteria = $this->uri->segment('3');
		$kocab = $this->uri->segment('6');
		$jenis = $this->uri->segment('7');
		$status = $this->uri->segment('8');
		$ket = $this->uri->segment('9');
		$kojur = $this->uri->segment('10');

    	if($jenis == 'up'){
			$jenis = '>=30%';
		}elseif($jenis == 'under'){
			$jenis = '<30%';
		}else{
			$jenis = 'TK 2';
		}

		if($ket == 'baru'){
			$ket = 'Registrasi Mahasiswa Baru';
		}else{
			$ket = 'Registrasi Mahasiswa Senior';
		}

		if($kriteria=='Coll'){
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatacollege"; 
			$label='Data Pembayaran Registrasi LP3I College Per Mahasiswa';
			$tingkat3="";
		}
        elseif($kriteria=='PLJ'){
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Politeknik Per Mahasiswa';
		}else{
			$ambil="select biodata.nim, biodata.Nama_Mahasiswa, bayarkuliah.jumlahbayar from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tahun' and ket='$ket' and biodata.kode='$kojur' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' group by bayarkuliah.nim";
			$dbnya="tampildatapoltek";
			$label='Data Pembayaran Registrasi Group LP3I Per Mahasiswa';
		}

		$data['tak'] 		= $tahun;
		$data['kriteria'] 	= $kriteria;
		$data['jenis'] 		= $jenis;
		$data['kocab'] 		= $kocab;
		$data['status'] 	= $status;
		$data['ket'] 		= $ket;
		$data['dp'] 		= $this->Mainmodel->dpreg($kriteria)->result();
		$data['label']	    = $label;
		$data['mahasiswa'] 	= $this->Mainmodel->$dbnya($ambil);

    	//$this->load->view('home',$data);
		$data['konten'] = 'laporan/mhs-pmbpdf';
		$html = $this->load->view('laporan',$data, TRUE);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	function transaksiPembayaran(){
		$data['konten'] = 'form/keuangan/transaksi-pembayaran';
		$this->load->view('Dashboard',$data);
	}

	function transaksiRegistrasi(){
		$from = $this->input->post('from');
		$to = $this->input->post('to');

		$querycol = "SELECT * FROM cabang WHERE kelompok='1'";
		$data['college'] = $this->Mainmodel->tampildatacollege($querycol);

		$querypol = "SELECT * FROM cabang WHERE kelompok='aktif'";
		$data['poltek'] = $this->Mainmodel->tampildatapoltek($querypol);

		$querygrup = "SELECT * FROM cabang WHERE kelompok='4'";
		$data['grup'] = $this->Mainmodel->tampildatacollege($querygrup);

		$data['label'] =  "Rekap Transaksi Registrasi Kuliah per Cabang";
		$data['from'] = $from;
		$data['to'] = $to;
		$data['label2'] = $from." s/d ".$to;


		$this->load->view('form/keuangan/transaksi-registrasi',$data);
	}

	function printregistrasi(){
		$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kriteria =  $this->uri->segment(9);

		$querycol = "SELECT * FROM cabang WHERE kelompok='1'";
		$data['college'] = $this->Mainmodel->tampildatacollege($querycol);

		$querypol = "SELECT * FROM cabang WHERE kelompok='aktif'";
		$data['poltek'] = $this->Mainmodel->tampildatapoltek($querypol);

		$querygrup = "SELECT * FROM cabang WHERE kelompok='4'";
		$data['grup'] = $this->Mainmodel->tampildatacollege($querygrup);

		$data['label'] =  "Rekap Transaksi Registrasi Kuliah per Cabang";
		$data['from'] = $from;
		$data['to'] = $to;
		$data['kriteria'] = $kriteria;
		if($kriteria ==  "Coll"){
			$tipekampus = 'College';
		}elseif($kriteria == "PLJ"){
			$tipekampus = 'Politeknik';
		}else{
			$tipekampus = 'PTS';
		}

		$data['tipekampus'] = $tipekampus;
		$data['konten'] = 'laporan/transaksi-registrasi';
		$this->load->view('laporan',$data);
	}

	function printregistrasipdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();

    	

		$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kriteria =  $this->uri->segment(9);
		// if($kriteria == 'group'){
			$this->pdf->AddPage('L');
		// }else{
		// 	$this->pdf->AddPage('P');
		// }

		$querycol = "SELECT * FROM cabang WHERE kelompok='1'";
		$data['college'] = $this->Mainmodel->tampildatacollege($querycol);

		$querypol = "SELECT * FROM cabang WHERE kelompok='aktif'";
		$data['poltek'] = $this->Mainmodel->tampildatapoltek($querypol);

		$querygrup = "SELECT * FROM cabang WHERE kelompok='4'";
		$data['grup'] = $this->Mainmodel->tampildatacollege($querygrup);

		$data['label'] =  "Rekap Transaksi Registrasi Kuliah per Cabang";
		$data['from'] = $from;
		$data['to'] = $to;
		$data['kriteria'] = $kriteria;
		if($kriteria ==  "Coll"){
			$tipekampus = 'College';
		}elseif($kriteria == "PLJ"){
			$tipekampus = 'Politeknik';
		}else{
			$tipekampus = 'PTS';
		}

		$data['kriteria'] = $kriteria;
		$data['tipekampus'] = $tipekampus;
		$data['konten'] = 'laporan/transaksi-registrasipdf';
		$html = $this->load->view('laporan',$data, true);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}


	function detailRegistrasi(){
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$kocab = $this->input->post('kocab');
		$kriteria = $this->input->post('kriteria');
		$tingkat = $this->input->post('tingkat');


		$data['registrasi'] = $this->Mainmodel->getRegMhs($from,$to,$kocab,$kriteria,$tingkat)->result();
		$cabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($cabang as $row);
		$namacabang = $row->namacabang;
		$data['label'] =  "data registrasi mahasiswa ".$namacabang;
		$data['label2'] = $from." s/d ".$to;
		$data['kriteria'] = $kriteria;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['tingkat'] = $tingkat;
		$data['kodecabang'] = $kocab;

		$this->load->view('form/keuangan/transaksi-detailmhs',$data);
	}

	function printDetailRegistrasi(){
		$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kocab = $this->uri->segment(10);
		$kriteria =  $this->uri->segment(9);
		$tingkat = $this->uri->segment(11);


		$data['registrasi'] = $this->Mainmodel->getRegMhs($from,$to,$kocab,$kriteria,$tingkat)->result();
		$cabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($cabang as $row);
		$namacabang = $row->namacabang;
		$data['label'] =  "data registrasi mahasiswa ".$namacabang;
		$data['label2'] = $from." s/d ".$to;
		$data['kriteria'] = $kriteria;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['tingkat'] = $tingkat;

		if($kriteria ==  "Coll"){
			$tipekampus = 'College';
		}elseif($kriteria == "PLJ"){
			$tipekampus = 'Politeknik';
		}else{
			$tipekampus = 'PTS';
		}

		$data['tipekampus'] = $tipekampus;

		$data['konten'] = 'laporan/transaksi-detailregistrasi';
		$this->load->view('laporan',$data);
	}

	function printDetailRegistrasiPdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('P');

		$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kriteria =  $this->uri->segment(9);
		$kocab = $this->uri->segment(10);
		$tingkat = $this->uri->segment(11);

		$data['registrasi'] = $this->Mainmodel->getRegMhs($from,$to,$kocab,$kriteria,$tingkat)->result();
		$cabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($cabang as $row);
		$namacabang = $row->namacabang;
		$data['label'] =  "data registrasi mahasiswa ".$namacabang;
		$data['label2'] = $from." s/d ".$to;
		$data['kriteria'] = $kriteria;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['tingkat'] = $tingkat;

		if($kriteria ==  "Coll"){
			$tipekampus = 'College';
		}elseif($kriteria == "PLJ"){
			$tipekampus = 'Politeknik';
		}else{
			$tipekampus = 'PTS';
		}

		$data['tipekampus'] = $tipekampus;

		$data['konten'] = 'laporan/transaksi-detailregistrasipdf';
		$html = $this->load->view('laporan', $data, true);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	function transaksiDaftar(){
		$from = $this->input->post('from');
		$to = $this->input->post('to');


		$querycol = "SELECT * FROM cabang WHERE kelompok='1'";
		$data['college'] = $this->Mainmodel->tampildatacollege($querycol);

		$querypol = "SELECT * FROM cabang WHERE kelompok='aktif'";
		$data['poltek'] = $this->Mainmodel->tampildatapoltek($querypol);

		$querygrup = "SELECT * FROM cabang WHERE kelompok='4'";
		$data['grup'] = $this->Mainmodel->tampildatacollege($querygrup);

		$data['label'] =  "Rekap Transaksi Pendaftaran Kuliah per Cabang";
		$data['from'] = $from;
		$data['to'] = $to;
		$data['label2'] = $from." s/d ".$to;

		$this->load->view('form/keuangan/transaksi-daftar',$data);
	}

	function printdaftar(){
		$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kriteria =  $this->uri->segment(9);

		if($kriteria ==  "Coll"){
			$tipekampus = 'College';
		}elseif($kriteria == "PLJ"){
			$tipekampus = 'Politeknik';
		}else{
			$tipekampus = 'PTS';
		}


		$querycol = "SELECT * FROM cabang WHERE kelompok='1'";
		$data['college'] = $this->Mainmodel->tampildatacollege($querycol);

		$querypol = "SELECT * FROM cabang WHERE kelompok='aktif'";
		$data['poltek'] = $this->Mainmodel->tampildatapoltek($querypol);

		$querygrup = "SELECT * FROM cabang WHERE kelompok='4'";
		$data['grup'] = $this->Mainmodel->tampildatacollege($querygrup);

		$data['label'] =  "Rekap Transaksi Pendaftaran Kuliah per Cabang";
		$data['from'] = $from;
		$data['to'] = $to;
		$data['kriteria'] = $kriteria;
		$data['tipekampus'] = $tipekampus;

		$data['konten'] = 'laporan/transaksi-daftar';
		$this->load->view('laporan',$data);
	}

	function printdaftarpdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('P');

    	$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kriteria =  $this->uri->segment(9);

    	if($kriteria ==  "Coll"){
			$tipekampus = 'College';
		}elseif($kriteria == "PLJ"){
			$tipekampus = 'Politeknik';
		}else{
			$tipekampus = 'PTS';
		}


		$querycol = "SELECT * FROM cabang WHERE kelompok='1'";
		$data['college'] = $this->Mainmodel->tampildatacollege($querycol);

		$querypol = "SELECT * FROM cabang WHERE kelompok='aktif'";
		$data['poltek'] = $this->Mainmodel->tampildatapoltek($querypol);

		$querygrup = "SELECT * FROM cabang WHERE kelompok='4'";
		$data['grup'] = $this->Mainmodel->tampildatacollege($querygrup);

		$data['label'] =  "Rekap Transaksi Pendaftaran Kuliah per Cabang";
		$data['from'] = $from;
		$data['to'] = $to;
		$data['kriteria'] = $kriteria;
		$data['tipekampus'] = $tipekampus;

		$data['konten'] = 'laporan/transaksi-daftarpdf';
		$html = $this->load->view('laporan',$data,true);

    	// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	function daftarAplikan(){
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$kocab = $this->input->post('kocab');
		$kriteria = $this->input->post('kriteria');

		if($kriteria == "Coll"){
			$nama = 'College';
		}elseif($kriteria == 'PLJ'){
			$nama = 'Politeknik';
		}else{
			$nama = 'PTS';
		}
		$data['aplikan'] = $this->Mainmodel->getAplikanDaftar($from,$to,$kocab,$kriteria)->result();
		$data['label'] =  "data pendaftaran aplikan ".$nama;
		$data['label2'] = $from." s/d ".$to;
		$data['kriteria'] = $kriteria;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['kodecabang'] = $kocab;


		$this->load->view('form/keuangan/transaksi-aplikan',$data);
	}

	function printdaftaraplikan(){
		$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kriteria =  $this->uri->segment(9);
		$kocab = $this->uri->segment(10);

		if($kriteria == "Coll"){
			$nama = 'College';
		}elseif($kriteria == 'PLJ'){
			$nama = 'Politeknik';
		}else{
			$nama= 'PTS';
		}
		$data['aplikan'] = $this->Mainmodel->getAplikanDaftar($from,$to,$kocab,$kriteria)->result();
		$data['label'] =  "data pendaftaran aplikan ".$nama;
		$data['label2'] = $from." s/d ".$to;
		$data['kriteria'] = $kriteria;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['kodecabang'] = $kocab;
		$data['tipekampus'] = $nama;

		$data['konten'] = 'laporan/printdaftaraplikan';
		$this->load->view('laporan',$data);
	}

	function printdaftaraplikanpdf(){
		$this->load->library('pdf');
		$this->pdf = new mPDF();
    	$this->pdf->AddPage('P');

		$from = $this->uri->segment(3).'/'.$this->uri->segment(4).'/'.$this->uri->segment(5);
		$to = $this->uri->segment(6).'/'.$this->uri->segment(7).'/'.$this->uri->segment(8);
		$kriteria =  $this->uri->segment(9);
		$kocab = $this->uri->segment(10);

		if($kriteria == "Coll"){
			$nama = 'College';
		}elseif($kriteria == 'PLJ'){
			$nama = 'Politeknik';
		}else{
			$nama= 'PTS';
		}
		$data['aplikan'] = $this->Mainmodel->getAplikanDaftar($from,$to,$kocab,$kriteria)->result();
		$data['label'] =  "data pendaftaran aplikan ".$nama;
		$data['label2'] = $from." s/d ".$to;
		$data['kriteria'] = $kriteria;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['kodecabang'] = $kocab;
		$data['tipekampus'] = $nama;

		$data['konten'] = 'laporan/printdaftaraplikanpdf';
		$html = $this->load->view('laporan',$data, true);

		// generate the PDF from the given html
		$this->pdf->WriteHTML($html);
		//download it.
		$this->pdf->Output($data.'.pdf', "I");
	}

	function transaksiRealisasi(){
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$dates=Date('Y-m-d');
		$jeniscabang = $this->input->post('jeniscabang');

		$querycol = "SELECT * FROM cabang WHERE kelompok='1'";
		$data['college'] = $this->Mainmodel->tampildatacollege($querycol);

		$querypol = "SELECT * FROM cabang WHERE kelompok='aktif'";
		$data['poltek'] = $this->Mainmodel->tampildatapoltek($querypol);

		$querygrup = "SELECT * FROM cabang WHERE kelompok='4'";
		$data['grup'] = $this->Mainmodel->tampildatacollege($querygrup);

		// mencari periode
		 $d = $this->Mainmodel->tampildatapoltek("select ta from inisialperiode where tgl_awal <= '$dates' and tgl_akhir >= '$dates'");
         foreach($d as $rperiode);
         $periodepol = $rperiode->ta;

          $d = $this->Mainmodel->tampildatacollege("select ta from inisialperiode where tgl_awal <= '$dates' and tgl_akhir >= '$dates'");
          foreach($d as $rperiode);
          $periodecol = $rperiode->ta;

		$data['label'] =  "Rekap Realisasi Pembayaran Kuliah per Cabang";
		$data['from'] = $from;
		$data['to'] = $to;
		$data['label2'] = $from." s/d ".$to;
		$data['dates'] = $dates;
		$data['jeniscabang'] =$jeniscabang;
		

		$this->load->view('form/keuangan/cabang-realisasi',$data);
	}

	// function trxJurusanRealisasi(){
	// 	$from = $_POST['from'];
	// 	$to = $_POST['to'];
	// 	$ta = $_POST['ta'];
	// 	$kocab = $_POST['kocab'];
	// 	$kriteria = $_POST['kriteria'];
	// 	$dates = Date('Y-m-d');


	// 	$data['jurusan'] = $this->Mainmodel->trxJurusanRealisasi($kriteria,$kocab,$ta,$dates,$from,$to)->result();

	// 	$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
	// 	foreach($getNamaCabang as $rownama);
	// 	$data['namacabang'] = $rownama->namacabang;
	// 	$kocab = $rownama->kodecabang;




	// 	$data['from'] = $from;
	// 	$data['to'] = $to;
	// 	$data['ta'] = $ta;
	// 	$data['kocab'] = $kocab;
	// 	$data['kriteria'] = $kriteria;
	// 	$data['dates'] = $dates; 

	// 	$this->load->view('form/keuangan/jurusan-realisasi',$data);

	// }

	function trxJurusanRealisasi(){
		$from = $_POST['from'];
		$to = $_POST['to'];
		$ta = $_POST['ta'];
		$kocab = $_POST['kocab'];
		$kriteria = $_POST['kriteria'];
		$dates = Date('Y-m-d');
		$kojur = $_POST['kojur'];

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;

		$data['mahasiswa'] = $this->Mainmodel->trxMhsRealisasiJurusan($kriteria,$kocab,$ta,$dates,$from,$to,$kojur)->result();

		$data['tahun'] = $ta;
		$data['kocab'] = $kocab;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['ta'] = $ta;
		$data['kriteria'] = $kriteria;
		$data['dates'] = $dates; 

		$this->load->view('form/keuangan/jurusan-realisasi',$data);

	}

	function trxMhsRealisasi(){
		$from = $_POST['from'];
		$to = $_POST['to'];
		$ta = $_POST['ta'];
		$kocab = $_POST['kocab'];
		$kriteria = $_POST['kriteria'];
		$dates = Date('Y-m-d');
		// $kojur = $_POST['kojur'];

		$getNamaCabang = $this->Mainmodel->getNamaCabang($kocab,$kriteria)->result();
		foreach($getNamaCabang as $rownama);
		$data['namacabang'] = $rownama->namacabang;

		// $getNamaJurusan = $this->Mainmodel->getWheres('jurusan',array('kodejurusan'=>$kojur),$kriteria)->result();
		// foreach($getNamaJurusan as $jurusan);
		// $data['jurusan'] = $jurusan->namajurusan;

		$data['mahasiswa'] = $this->Mainmodel->trxMhsRealisasi($kriteria,$kocab,$ta,$dates,$from,$to)->result();

		$data['tahun'] = $ta;
		$data['kocab'] = $kocab;
		$data['from'] = $from;
		$data['to'] = $to;
		$data['ta'] = $ta;
		$data['kriteria'] = $kriteria;
		$data['dates'] = $dates; 

		$this->load->view('form/keuangan/mahasiswa-realisasi',$data);
	}


public function getPiutangNot(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('kriteria');
		$Dashboard = $this->input->post('dashboard');
		$jeniscabang = $this->input->post('jeniscabang');

		if($Dashboard == ''){
			$data['dashboard'] = '';
		}else{
			$data['dashboard'] = 'dashboard';
		}

		if($jeniscabang == 'milker'){
			// BEGIN
		if($kriteria == 'Coll' || $kriteria == 'group'){
		 	$data['cabang'] = $this->Mainmodel->getCabangMilker()->result();
		 	$data['label'] = 'REKAPITULASI PIUTANG PESERTA DIDIK YANG BELUM DIBUATKAN RENCANA BAYAR';
		 }else{
		 	$data['cabang'] = $this->Mainmodel->tampildatapoltek("select * from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')");
		 	$data['label'] = 'REKAPITULASI MAHASISWA DIDIK YANG BELUM DIBUATKAN RENCANA BAYAR';
		 }
			// END
		}else{
			// BEGIN
		if($kriteria == 'Coll' || $kriteria == 'group'){
		 	$data['cabang'] = $this->Mainmodel->getCabang()->result();
		 	$data['label'] = 'REKAPITULASI PIUTANG PESERTA DIDIK YANG BELUM DIBUATKAN RENCANA BAYAR';
		 }else{
		 	$data['cabang'] = $this->Mainmodel->getWheres('cabang',array('kelompok'=>'aktif'),$kriteria)->result();
		 	$data['label'] = 'REKAPITULASI MAHASISWA DIDIK YANG BELUM DIBUATKAN RENCANA BAYAR';
		 }
			// END
		}

		
		$data['tahun'] = $tahun;
		$data['kriteria'] = $kriteria;
		$data['dashboard'] = $Dashboard;
		$data['jeniscabang'] = $jeniscabang;

		$this->load->view('form/keuangan/piutang-not1', $data);
	}

	public function detailpiutangnot(){
		$tahun = $this->input->post('tahun');
		$kriteria = $this->input->post('kriteria');
		$Dashboard = $this->input->post('dashboard');
		$kocab = $this->input->post("kocab");
		$jeniscabang = $this->input->post('jeniscabang');

		$namacabang = $this->Mainmodel->getWheres('cabang',array('kodecabang'=>$kocab),$kriteria)->result();
		foreach($namacabang as $row);
		$data['namacabang'] = $row->namacabang;



		$detail = $this->Mainmodel->sumPiutangNotMhs($kriteria,$tahun,$kocab)->result();
		foreach($detail as $row);
		$jumlahbiaya = $row->jumlahbiaya;
		$sebesar = $row->terbayar;
		$data['total'] = $jumlahbiaya - $sebesar;


		$data['tahun'] = $tahun;
		$data['kriteria'] = $kriteria;
		$data['dashboard'] = $Dashboard;
		$data['kocab'] = $kocab;
		$data['jeniscabang'] = $jeniscabang;
		
		$this->load->view('form/keuangan/piutang-notdetail', $data);
	}


}
