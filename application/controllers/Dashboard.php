<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('Mainmodel');
		$this->load->library('session');
		if($this->session->userdata('status') !== 'masuk'){
 			redirect('Login');
 		}

 		$cookie = get_cookie('sesilogin');
 		// jika cookie tidak ada maka akan pindah ke logout
        if($cookie == false){
           redirect('Main/logout');
           }

         delete_cookie('sesilogin');

         $nilai	=  $this->session->userdata('username');
 	     $waktu  =  1800;
 		 set_cookie('sesilogin',$nilai,$waktu);
	}

	

	public function index()
	{   

		// $data['getNama'] = $this->Mainmodel->getNama($this->session->userdata('nik'))->result();

		$dates=Date('Y-m-d');
		// 	COLLEGE
		// inisial TA
		$real1="select * from inisialta";
		$ta = $this->Mainmodel->tampildatacollege($real1);
		foreach($ta as $rota){
			$inisialta = $rota->ta;
			$tahun = substr($inisialta,0,4);
		}

		// =================================================EWS IT=========================================
		// inisial periode
		$iniperiod = "select * from inisialperiode where tgl_awal <='$dates' and tgl_akhir >='$dates'";
		$iniper = $this->Mainmodel->tampildatacollege($iniperiod);
		foreach ($iniper as $nilaiperiode) {
			$periode = $nilaiperiode->periode;
		}

		//cari tanggal max di dasboard bulan dan tahunnya-------------------------------------------------------
		$real2="select max(substr(tglposting,1,7)) as tglposting from dasboard_mhs where ta='$inisialta' and ta <>'0000-00-00' and periode='$periode'";
		$rela2 = $this->Mainmodel->tampildatacollege($real2);
		foreach($rela2 as $realis2){
			$tglposting = $realis2->tglposting;
		}

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE-----------------------------------------------------------------------------------------------------------------------------
		$real3="SELECT avg(rasiolmsmhs) as lmsmhs,avg(rasiowebmhs) as webmhs from dasboard_mhs where ta='$inisialta' and tglposting <>'0000-00-00' and periode='$periode' and kodecabang <> '109' and kodecabang <> '102' AND substr(tglposting,1,7)='$tglposting'";
		$rela3=$this->Mainmodel->tampildatacollege($real3);
		foreach($rela3 as $realis3){
			$data['lmsmhs'] = $realis3->lmsmhs;
			$data['lmsweb'] = $realis3->webmhs;
			$data['periode'] = $periode;

		}
			$data['tahunaka'] = $inisialta;
		//cari tanggal max di dasboard bulan dan tahunnya---------------------------------------------
		$realdos="select max(substr(tglposting,1,7)) as tglpost from dasboard_dos where ta='$inisialta' and tglposting <>'0000-00-00' and periode='$periode'";
		$realdose=$this->Mainmodel->tampildatacollege($realdos);
		foreach($realdose as $realdosen){
			$tglpost = $realdosen->tglpost;
		}

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE-------------------------------
		$real3="SELECT * from dasboard_dos where ta='$inisialta' and ta <>'0000-00-00' and periode='$periode' AND substr(tglposting,1,7)='$tglpost'";
		$reall = $this->Mainmodel->tampildatacollege($real3);
		

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE--------------------------------------
		$rasdos="SELECT avg(rasiowebdosen) as lmsdos,avg(rasiolmsdosen) as webdos from dasboard_dos where ta='$inisialta' and ta <>'0000-00-00' and periode='$periode' 
			and kodecabang <> '109' and kodecabang <> '102' AND substr(tglposting,1,7)='$tglposting'";
		$rasdose = $this->Mainmodel->tampildatacollege($rasdos);
		foreach($rasdose as $rasdosen){
			$data['rasiodoselec'] = $rasdosen->lmsdos;
			$data['rasiodoslms'] = $rasdosen->webdos;
		}

		// =================================================================================================

		$dates=Date('Y-m-d');
		$tahunipk = $tahun - 1;

		$tahunipk = "select tahunangkatan from ipk group by tahunangkatan order by tahunangkatan desc limit 1";
		$ipkp	  = $this->Mainmodel->tampildatacollege($tahunipk);
		foreach($ipkp as $ipktahun);
		$tahun2 = $ipktahun->tahunangkatan;


								// COLLEGE EDU
								$qview = "SELECT COUNT(DISTINCT nim) as total from ipk where tahunangkatan='$tahun2' and
												 ipk>=3.50";
								$viewp = $this->Mainmodel->tampildatacollege($qview);
								foreach($viewp as $pview);
								$totall = $pview->total;
								

								$qview2 = "SELECT COUNT(DISTINCT nim) as total1 from ipk where tahunangkatan='$tahun2' and
												 (ipk>=3.00 and ipk<3.50)";
								$viewp = $this->Mainmodel->tampildatacollege($qview2);
								foreach($viewp as $pview);
								$total2 = $pview->total1;

								$qview3 = "SELECT COUNT(DISTINCT nim) as total2 from ipk where tahunangkatan='$tahun2' and
												 (ipk>=2.30 and ipk<3.00)";
								$viewp  = $this->Mainmodel->tampildatacollege($qview3);
								foreach($viewp as $pview);
								$total3 = $pview->total2;

								$qview4 = "SELECT COUNT(DISTINCT nim) as total3 from ipk where tahunangkatan='$tahun2' and
												 ipk<2.30";
								$viewp  = $this->Mainmodel->tampildatacollege($qview4);
								foreach($viewp as $pview);
								$total4 = $pview->total3;

								$data['ttl1'] = $totall;
								$data['ttl2'] = $total2;
								$data['ttl3'] = $total3;
								$data['ttl4'] = $total4;
		// =============================================== BATAS EDU COLLEGE#


		// EWS Marketing college
		// cari tahun akademik marketing
		$datetime = date("Y-m-d h:i:s");
		$periodekal=substr($datetime,0,7);

		$inispmb = "select * from inisialpmb";
		$pmb1 = $this->Mainmodel->tampildatacollege($inispmb);
		foreach($pmb1 as $rpmb1){
			$tahunpmb = $rpmb1->ta;
		}
		// // konfigurasi dp
		$konfdp = "select * from konfigurasi_dp_registrasi where flag='1'";
		$dp1 = $this->Mainmodel->tampildatacollege($konfdp);
		foreach($dp1 as $rowdb){
			$ket = $rowdb->keterangan;
		}

		$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahunpmb' and kelompok='1' and direktorat='2' order by tglkalkulasi desc limit 1";
		$tglkal = $this->Mainmodel->tampildatacollege($gettglkalkulasi);
	
			foreach($tglkal as $tglnya){
				$datekal = $tglnya->tglkalkulasi;
			}

	
		// hitung jumlah pmb
		$cari1 = "select tahunpmb,sum(targetjunior) as targetjunior,
				sum(regjunior) as registrasisah,
				sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang
				where aplikanperolehannasional.kelompok='1' and tahunpmb='$tahunpmb' and direktorat='2' and tglkalkulasi='$datekal' and (kepemilikan='1' or kepemilikan='2') group by aplikanperolehannasional.kelompok";
		 $datapmb = $this->Mainmodel->tampildatacollege($cari1);
		 if(!$datapmb){
		 	$ttltargetjunior = 0;
			$ttlregsah = 0;
			$ttlregboking = 0;
		 }else{
		 	foreach($datapmb as $rpmb2){
			$ttltargetjunior = $rpmb2->targetjunior;
			$ttlregsah = $rpmb2->registrasisah;
			$ttlregboking = $rpmb2->registrasibooking;
			 }
		 }
		 
		 if($ttltargetjunior == 0){
			 $rasio = '0';
		 }else{
			 $rasio = ((($ttlregsah+$ttlregboking)*1)/$ttltargetjunior)*100;
		 }

		// Data target PMB  
		$data['ket'] = $ket;
		$data['satu'] = $ttltargetjunior;
		$data['dua'] = $ttlregsah;
		$data['tiga'] = $ttlregboking;
		$data['rasio'] = $rasio;
		$data['tes1'] = $tahunpmb;

		$data['query'] = $cari1;
		// ===============================

		// EWS Marketing poltek
		// cari tahun akademik marketing
		$inispmbpol = "select * from inisialpmb";
		$pmb1pol = $this->Mainmodel->tampildatapoltek($inispmbpol);
		foreach($pmb1pol as $rpmb1){
			$tahunpmbpol = $rpmb1->ta;
		}
		// konfigurasi dp
		$konfdppol = "select * from konfigurasi_dp_registrasi where flag='1'";
		$dp1pol = $this->Mainmodel->tampildatapoltek($konfdppol);
		foreach($dp1pol as $rowdb){
			$ketpol = $rowdb->keterangan;
		}
		// hitung jumlah pmb
		$cari1pol = "select tahunpmb,sum(targetjunior) as targetjunior,
				sum(regjunior) as registrasisah,
				sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang
				where tahunpmb='$tahunpmbpol' and kelompok='aktif' and direktorat='3' and substr(tglkalkulasi,1,7)='$periodekal' and (kepemilikan='1' or kepemilikan='2') group by direktorat";
		$datapmbpol = $this->Mainmodel->tampildatapoltek($cari1pol);
		if(!$datapmbpol){
			$ttltargetjuniorpol = 0;
			$ttlregsahpol = 0;
			$ttlregbokingpol = 0;
		}else{
			foreach($datapmbpol as $rpmb2){
			$ttltargetjuniorpol = $rpmb2->targetjunior;
			$ttlregsahpol = $rpmb2->registrasisah;
			$ttlregbokingpol = $rpmb2->registrasibooking;
		 	}
		}
		 

		 if($ttltargetjuniorpol == 0 || $ttltargetjuniorpol == null){
			 $rasio2 = '0';
		 }else{
			 $rasio2 = ((($ttlregsahpol+$ttlregbokingpol)*1)/$ttltargetjuniorpol)*100;
		 }

		// Data target PMB  
		$data['ketpol'] = $ketpol;
		$data['satupol'] = $ttltargetjuniorpol;
		$data['duapol'] = $ttlregsahpol;
		$data['tigapol'] = $ttlregbokingpol;
		$data['rasio2'] = $rasio2;
		$data['tes'] = $tahunpmbpol;

		// KEU PIUTANG 
		 $waktu = date('Y-m-d');
		 $iniperiod = "select ta from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'";
		 $qper = $this->Mainmodel->tampildatacollege($iniperiod);
		 foreach($qper as $rowper);
		 $periodes = $rowper->ta;
		 $tahunangkatan = substr($periodes,0,4);
		 $data['periodes'] = $periodes;
		 
			$biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       		 from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$periodes' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatacollege($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
			 $totsebesar = $rowpiutang->ttlsebesar;
			 $totterbayar = $rowpiutang->ttlterbayar;
			 $totpiutang = $totsebesar - $totterbayar;
			 $totalpiutang = $totalpiutang += $totpiutang;
			 $totalterbayar = $totalterbayar += $totterbayar;
			}
		
		
			$data['limapersen'] = ($totalterbayar * 5) / 100;
			$data['sepuluhpersen'] = ($totalterbayar * 10) / 100;
		 	$data['sebesar'] = $totalpiutang;

		 // tingkat 2 college
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       	from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
		 $cpiutang = $this->Mainmodel->tampildatacollege($biayapiutang);
		 $totalpiutang = 0;
		 $totalterbayar = 0;
		 foreach($cpiutang as $rowpiutang){
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;
			$totalpiutang = $totalpiutang += $totpiutang;
			$totalterbayar = $totalterbayar += $totterbayar;
		 }

		 $data['limapersen2'] = ($totalterbayar * 5) / 100;
		 $data['sepuluhpersen2'] = ($totalterbayar * 10) / 100;
		 $data['sebesar2'] = $totalpiutang;

		 // tingkat 1 college
		 $sumtk1col = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='1' and NPM = '' and biodata.status='aktif' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemua'] = $this->Mainmodel->tampildatacollege($sumtk1col);

		 $sumtk2col = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='2' and biodata.status='aktif'and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemua2'] = $this->Mainmodel->tampildatacollege($sumtk2col);

		 $data['periodecol'] = $periodes;

		 // cari data piutang mahasiswa yang belum dibuatkan rencana
		 $data['piutangnotbiaya'] = $this->Mainmodel->piutangnotbiayamilker('Coll',$periodes)->result();
		 // end piutang tidak ada rencana


		 // piutang poltek
		 $iniperiod = "select ta from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'";
		 $qper = $this->Mainmodel->tampildatapoltek($iniperiod);
		 foreach($qper as $rowper);
		 $periodes = $rowper->ta;
		 

		 // tingkat 1 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$periodes' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
		 foreach($cpiutang as $rowpiutang){
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;
			$totalpiutang = $totalpiutang += $totpiutang;
			$totalterbayar = $totalterbayar += $totterbayar;
		 }	
		 	$data['limapersenpol'] = ($totalterbayar * 5) / 100;
			$data['sepuluhpersenpol'] = ($totalterbayar * 10) / 100;
		 	$data['sebesarpol'] = $totalpiutang;

		 // tingkat 2 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
	       from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
				$totsebesar = $rowpiutang->ttlsebesar;
				$totterbayar = $rowpiutang->ttlterbayar;
				$totpiutang = $totsebesar - $totterbayar;
				$totalpiutang = $totalpiutang += $totpiutang;
				$totalterbayar = $totalterbayar += $totterbayar;
			 }	
			 	$data['limapersen2pol'] = ($totalterbayar * 5) / 100;
				$data['sepuluhpersen2pol'] = ($totalterbayar * 10) / 100;
		        $data['sebesar2pol'] = $totalpiutang;

		// tingkat 3 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar from biaya inner join regispoltek on biaya.nim=regispoltek.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
		 $totalpiutang = 0;
		 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
				$totsebesar = $rowpiutang->ttlsebesar;
				$totterbayar = $rowpiutang->ttlterbayar;
				$totpiutang = $totsebesar - $totterbayar;
				$totalpiutang = $totalpiutang += $totpiutang;
				$totalterbayar = $totalterbayar += $totterbayar;
			 }
			 $data['limapersenpol3'] = ($totalterbayar * 5) / 100;
			 $data['sepuluhpersenpol3'] = ($totalterbayar * 10) / 100;
		     $data['sebesarpoltk3'] = $totalpiutang;

		 // tingkat 1 poltek
		 $sumtk1pol = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='1' and NPM = '' and biodata.status='aktif' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemuapol'] = $this->Mainmodel->tampildatapoltek($sumtk1pol);

		 $sumtk2pol = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='2' and biodata.status='aktif'and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemua2pol'] = $this->Mainmodel->tampildatapoltek($sumtk2pol);

		 
		 $data['tak'] = $waktu;
		 $data['periodespol'] = $periodes;

		 // piutang not biaya poltek 
		 $data['piutangnotbiayapol'] = $this->Mainmodel->piutangnotbiayamilker('PLJ',$periodes)->result();
		 
		 // ambil periode 
		 $getperiode = $this->Mainmodel->getPeriode($waktu,'Coll')->result();
		 foreach($getperiode as $rowperiode);
		 $data['tak'] = $rowperiode->ta;
		 $data['ta'] = substr($rowperiode->ta,0,4);
		 $ta = substr($rowperiode->ta,0,4);
		 $tafix = $ta - 1; 

		 $getperiodepol = $this->Mainmodel->getPeriode($waktu,'PLJ')->result();
		 foreach($getperiodepol as $rowperiodepol);
		 $tapol = substr($rowperiodepol->ta,0,4);
		 $tafixpol = $tapol - 2;

		 // BEGIN EDU IPK
		
		 // begin college ipk
		$datetime = date("Y-m-d h:i:s");
		$periodekal=substr($datetime,0,7);
		$getperiode = $this->Mainmodel->getPeriode($dates,'Coll')->result();
		foreach($getperiode as $periode);
		$periode = $periode->ta;
		// cari rata ipk tingkat 1
		$getKalkulasi = $this->Mainmodel->getRataKal('Coll',$periode,1,1)->result();
		foreach($getKalkulasi as $rowkal1);
		$data['ratakal1'] = $rowkal1->rataipk;
		// cari rata ipk tingkat 2
		$getKalkulasi = $this->Mainmodel->getRataKal('Coll',$periode,2,1)->result();
		foreach($getKalkulasi as $rowkal2);
		$data['ratakal2'] = $rowkal2->rataipk;
		// end college ipk
		// begin poltek ipk
		$getperiode = $this->Mainmodel->getPeriode($dates,'PLJ')->result();
		foreach($getperiode as $periode);
		$periode = $periode->ta;
		// cari rata ipk tingkat 1
		$getKalkulasi = $this->Mainmodel->getRataKal('PLJ',$periode,1,null)->result();
		foreach($getKalkulasi as $rowkal1);
		$data['ratakalpol1'] = $rowkal1->rataipk;
		// cari rata ipk tingkat 2
		$getKalkulasi = $this->Mainmodel->getRataKal('PLJ',$periode,2,null)->result();
		foreach($getKalkulasi as $rowkal2);
		$data['ratakalpol2'] = $rowkal2->rataipk;
		// cari rata ipk tingkat 3
		$getKalkulasi = $this->Mainmodel->getRataKal('PLJ',$periode,3,null)->result();
		foreach($getKalkulasi as $rowkal3);
		$data['ratakalpol3'] = $rowkal3->rataipk;
		 
		 // END EDU IPK

		 // BEGIN CNP
		 // college
		 // college
		 $where = "select * from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2')";
		 $data['cabangcnp'] = $this->Mainmodel->tampildatacollege($where);
		 $data['tahun'] = $tafix;

		 // poltek
		 $where = "select * from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')";
		 $data['cabangcnppol'] = $this->Mainmodel->tampildatapoltek($where);
		 $data['tahunpol'] = $tafixpol;
		 // END CNP
		 // END CNP

		 $data['jeniscabang'] = 'milker';


		

		$data['konten'] = 'konten/Dashboard';
		$this->load->view('Dashboard',$data);
	}

	public function pmbnasional(){
		// EWS Marketing college
		// cari tahun akademik marketing
		$datetime = date("Y-m-d h:i:s");
		$periodekal=substr($datetime,0,7);

		$inispmb = "select * from inisialpmb";
		$pmb1 = $this->Mainmodel->tampildatacollege($inispmb);
		foreach($pmb1 as $rpmb1){
			$tahunpmb = $rpmb1->ta;
		}
		// // konfigurasi dp
		$konfdp = "select * from konfigurasi_dp_registrasi where flag='1'";
		$dp1 = $this->Mainmodel->tampildatacollege($konfdp);
		foreach($dp1 as $rowdb){
			$ket = $rowdb->keterangan;
		}

		$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahunpmb' and kelompok='1' and direktorat='2' order by tglkalkulasi desc limit 1";
		$tglkal = $this->Mainmodel->tampildatacollege($gettglkalkulasi);
	
			foreach($tglkal as $tglnya){
				$datekal = $tglnya->tglkalkulasi;
			}

	

		// hitung jumlah pmb
		$cari1 = "select tahunpmb,sum(targetjunior) as targetjunior,
				sum(regjunior) as registrasisah,
				sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang
				where aplikanperolehannasional.kelompok='1' and tahunpmb='$tahunpmb' and direktorat='2' and tglkalkulasi='$datekal' group by aplikanperolehannasional.kelompok";
		 $datapmb = $this->Mainmodel->tampildatacollege($cari1);
		 if(!$datapmb){
		 	$ttltargetjunior = 0;
			$ttlregsah = 0;
			$ttlregboking = 0;
		 }else{
		 	foreach($datapmb as $rpmb2){
			$ttltargetjunior = $rpmb2->targetjunior;
			$ttlregsah = $rpmb2->registrasisah;
			$ttlregboking = $rpmb2->registrasibooking;
			 }
		 }
		 
		 if($ttltargetjunior == 0){
			 $rasio = '0';
		 }else{
			 $rasio = ((($ttlregsah+$ttlregboking)*1)/$ttltargetjunior)*100;
		 }

		// Data target PMB  
		$data['ket'] = $ket;
		$data['satu'] = $ttltargetjunior;
		$data['dua'] = $ttlregsah;
		$data['tiga'] = $ttlregboking;
		$data['rasio'] = $rasio;
		$data['tes1'] = $tahunpmb;

		$data['query'] = $cari1;
		// ===============================

		// EWS Marketing poltek
		// cari tahun akademik marketing
		$inispmbpol = "select * from inisialpmb";
		$pmb1pol = $this->Mainmodel->tampildatapoltek($inispmbpol);
		foreach($pmb1pol as $rpmb1){
			$tahunpmbpol = $rpmb1->ta;
		}
		// konfigurasi dp
		$konfdppol = "select * from konfigurasi_dp_registrasi where flag='1'";
		$dp1pol = $this->Mainmodel->tampildatapoltek($konfdppol);
		foreach($dp1pol as $rowdb){
			$ketpol = $rowdb->keterangan;
		}
		// hitung jumlah pmb
		$cari1pol = "select tahunpmb,sum(targetjunior) as targetjunior,
				sum(regjunior) as registrasisah,
				sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang
				where tahunpmb='$tahunpmbpol' and kelompok='aktif' and direktorat='3' and substr(tglkalkulasi,1,7)='$periodekal' group by direktorat";
		$datapmbpol = $this->Mainmodel->tampildatapoltek($cari1pol);
		if(!$datapmbpol){
			$ttltargetjuniorpol = 0;
			$ttlregsahpol = 0;
			$ttlregbokingpol = 0;
		}else{
			foreach($datapmbpol as $rpmb2){
			$ttltargetjuniorpol = $rpmb2->targetjunior;
			$ttlregsahpol = $rpmb2->registrasisah;
			$ttlregbokingpol = $rpmb2->registrasibooking;
		 	}
		}
		 

		 if($ttltargetjuniorpol == 0 || $ttltargetjuniorpol == null){
			 $rasio2 = '0';
		 }else{
			 $rasio2 = ((($ttlregsahpol+$ttlregbokingpol)*1)/$ttltargetjuniorpol)*100;
		 }

		// Data target PMB  
		$data['ketpol'] = $ketpol;
		$data['satupol'] = $ttltargetjuniorpol;
		$data['duapol'] = $ttlregsahpol;
		$data['tigapol'] = $ttlregbokingpol;
		$data['rasio2'] = $rasio2;
		$data['tes'] = $tahunpmbpol;
		$data['jeniscabang'] = 'nasional';

		$this->load->view('form/dashboard/pmb', $data);
	}

	public function pmbmilker(){
		// EWS Marketing college
		// cari tahun akademik marketing
		$datetime = date("Y-m-d h:i:s");
		$periodekal=substr($datetime,0,7);

		$inispmb = "select * from inisialpmb";
		$pmb1 = $this->Mainmodel->tampildatacollege($inispmb);
		foreach($pmb1 as $rpmb1){
			$tahunpmb = $rpmb1->ta;
		}
		// // konfigurasi dp
		$konfdp = "select * from konfigurasi_dp_registrasi where flag='1'";
		$dp1 = $this->Mainmodel->tampildatacollege($konfdp);
		foreach($dp1 as $rowdb){
			$ket = $rowdb->keterangan;
		}

		$gettglkalkulasi = "select tglkalkulasi from aplikanperolehannasional where tahunpmb='$tahunpmb' and kelompok='1' and direktorat='2' order by tglkalkulasi desc limit 1";
		$tglkal = $this->Mainmodel->tampildatacollege($gettglkalkulasi);
	
			foreach($tglkal as $tglnya){
				$datekal = $tglnya->tglkalkulasi;
			}

	

		// hitung jumlah pmb
		$cari1 = "select tahunpmb,sum(targetjunior) as targetjunior,
				sum(regjunior) as registrasisah,
				sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang
				where aplikanperolehannasional.kelompok='1' and tahunpmb='$tahunpmb' and direktorat='2' and tglkalkulasi='$datekal' and (kepemilikan='1' or kepemilikan='2') group by aplikanperolehannasional.kelompok";
		 $datapmb = $this->Mainmodel->tampildatacollege($cari1);
		 if(!$datapmb){
		 	$ttltargetjunior = 0;
			$ttlregsah = 0;
			$ttlregboking = 0;
		 }else{
		 	foreach($datapmb as $rpmb2){
			$ttltargetjunior = $rpmb2->targetjunior;
			$ttlregsah = $rpmb2->registrasisah;
			$ttlregboking = $rpmb2->registrasibooking;
			 }
		 }
		 
		 if($ttltargetjunior == 0){
			 $rasio = '0';
		 }else{
			 $rasio = ((($ttlregsah+$ttlregboking)*1)/$ttltargetjunior)*100;
		 }

		// Data target PMB  
		$data['ket'] = $ket;
		$data['satu'] = $ttltargetjunior;
		$data['dua'] = $ttlregsah;
		$data['tiga'] = $ttlregboking;
		$data['rasio'] = $rasio;
		$data['tes1'] = $tahunpmb;

		$data['query'] = $cari1;
		// ===============================

		// EWS Marketing poltek
		// cari tahun akademik marketing
		$inispmbpol = "select * from inisialpmb";
		$pmb1pol = $this->Mainmodel->tampildatapoltek($inispmbpol);
		foreach($pmb1pol as $rpmb1){
			$tahunpmbpol = $rpmb1->ta;
		}
		// konfigurasi dp
		$konfdppol = "select * from konfigurasi_dp_registrasi where flag='1'";
		$dp1pol = $this->Mainmodel->tampildatapoltek($konfdppol);
		foreach($dp1pol as $rowdb){
			$ketpol = $rowdb->keterangan;
		}
		// hitung jumlah pmb
		$cari1pol = "select tahunpmb,sum(targetjunior) as targetjunior,
				sum(regjunior) as registrasisah,
				sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang
				where tahunpmb='$tahunpmbpol' and kelompok='aktif' and direktorat='3' and substr(tglkalkulasi,1,7)='$periodekal' and (kepemilikan='1' or kepemilikan='2') group by direktorat";
		$datapmbpol = $this->Mainmodel->tampildatapoltek($cari1pol);
		if(!$datapmbpol){
			$ttltargetjuniorpol = 0;
			$ttlregsahpol = 0;
			$ttlregbokingpol = 0;
		}else{
			foreach($datapmbpol as $rpmb2){
			$ttltargetjuniorpol = $rpmb2->targetjunior;
			$ttlregsahpol = $rpmb2->registrasisah;
			$ttlregbokingpol = $rpmb2->registrasibooking;
		 	}
		}
		 

		 if($ttltargetjuniorpol == 0 || $ttltargetjuniorpol == null){
			 $rasio2 = '0';
		 }else{
			 $rasio2 = ((($ttlregsahpol+$ttlregbokingpol)*1)/$ttltargetjuniorpol)*100;
		 }

		// Data target PMB  
		$data['ketpol'] = $ketpol;
		$data['satupol'] = $ttltargetjuniorpol;
		$data['duapol'] = $ttlregsahpol;
		$data['tigapol'] = $ttlregbokingpol;
		$data['rasio2'] = $rasio2;
		$data['tes'] = $tahunpmbpol;
		$data['jeniscabang'] = 'milker';

		$this->load->view('form/dashboard/pmb', $data);
	}

	public function cnpmilker(){
		// BEGIN CNP
		$waktu = date('Y-m-d');
		// ambil periode 
		 $getperiode = $this->Mainmodel->getPeriode($waktu,'Coll')->result();
		 foreach($getperiode as $rowperiode);
		 $data['tak'] = $rowperiode->ta;
		 $data['ta'] = substr($rowperiode->ta,0,4);
		 $ta = substr($rowperiode->ta,0,4);
		 $tafix = $ta - 1; 

		 $getperiodepol = $this->Mainmodel->getPeriode($waktu,'PLJ')->result();
		 foreach($getperiodepol as $rowperiodepol);
		 $tapol = substr($rowperiodepol->ta,0,4);
		 $tafixpol = $tapol - 2;
		 // college
		 $where = "select * from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2')";
		 $data['cabangcnp'] = $this->Mainmodel->tampildatacollege($where);
		 $data['tahun'] = $tafix;

		 // poltek
		 $where = "select * from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')";
		 $data['cabangcnppol'] = $this->Mainmodel->tampildatapoltek($where);
		 $data['tahunpol'] = $tafixpol;
		 $data['jeniscabang'] = 'milker';
		 // END CNP

		 $this->load->view('form/dashboard/cnp', $data);
	}

	public function cnpnasional(){
		// BEGIN CNP
		$waktu = date('Y-m-d');
		// ambil periode 
		 $getperiode = $this->Mainmodel->getPeriode($waktu,'Coll')->result();
		 foreach($getperiode as $rowperiode);
		 $data['tak'] = $rowperiode->ta;
		 $data['ta'] = substr($rowperiode->ta,0,4);
		 $ta = substr($rowperiode->ta,0,4);
		 $tafix = $ta - 1; 

		 $getperiodepol = $this->Mainmodel->getPeriode($waktu,'PLJ')->result();
		 foreach($getperiodepol as $rowperiodepol);
		 $tapol = substr($rowperiodepol->ta,0,4);
		 $tafixpol = $tapol - 2;
		 // college
		 $where = "select * from cabang where kelompok='1'";
		 $data['cabangcnp'] = $this->Mainmodel->tampildatacollege($where);
		 $data['tahun'] = $tafix;

		 // poltek
		 $where = "select * from cabang where kelompok='aktif'";
		 $data['cabangcnppol'] = $this->Mainmodel->tampildatapoltek($where);
		 $data['tahunpol'] = $tafixpol;
		 $data['jeniscabang'] = 'nasional';
		 // END CNP

		 $this->load->view('form/dashboard/cnp', $data);
	}


	public function itmilker(){
		// EWS IT
		// inisial TA
		$real1="select * from inisialta";
		$ta = $this->Mainmodel->tampildatacollege($real1);
		foreach($ta as $rota){
			$inisialta = $rota->ta;
			$tahun = substr($inisialta,0,4);
		}
		$dates = date("Y-m-d");
		// inisial periode
		$iniperiod = "select * from inisialperiode where tgl_awal <='$dates' and tgl_akhir >='$dates'";
		$iniper = $this->Mainmodel->tampildatacollege($iniperiod);
		foreach ($iniper as $nilaiperiode) {
			$periode = $nilaiperiode->periode;
		}

		//cari tanggal max di dasboard bulan dan tahunnya
		$real2="select max(substr(tglposting,1,7)) as tglposting from dasboard_mhs inner join cabang on dasboard_mhs.kodecabang=cabang.kodecabang where (kepemilikan='1' or kepemilikan='2') and ta='$inisialta' and ta <>'0000-00-00' and periode='$periode'";
		$rela2 = $this->Mainmodel->tampildatacollege($real2);
		foreach($rela2 as $realis2){
			$tglposting = $realis2->tglposting;
		}

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE
		$real3="SELECT avg(rasiolmsmhs) as lmsmhs,avg(rasiowebmhs) as webmhs from dasboard_mhs inner join cabang on dasboard_mhs.kodecabang=cabang.kodecabang where (kepemilikan='1' or kepemilikan='2') and ta='$inisialta' and tglposting <>'0000-00-00' and periode='$periode' and dasboard_mhs.kodecabang <> '109' and dasboard_mhs.kodecabang <> '102' AND substr(tglposting,1,7)='$tglposting'";
		$rela3=$this->Mainmodel->tampildatacollege($real3);
		foreach($rela3 as $realis3){
			$data['lmsmhs'] = $realis3->lmsmhs;
			$data['lmsweb'] = $realis3->webmhs;
			$data['periode'] = $periode;

		}
			$data['tahunaka'] = $inisialta;


		//cari tanggal max di dasboard bulan dan tahunnya
		$realdos="select max(substr(tglposting,1,7)) as tglpost from dasboard_dos inner join cabang on dasboard_dos.kodecabang=cabang.kodecabang where (kepemilikan='1' or kepemilikan='2') and ta='$inisialta' and tglposting <>'0000-00-00' and periode='$periode'";
		$realdose=$this->Mainmodel->tampildatacollege($realdos);
		foreach($realdose as $realdosen){
			$tglpost = $realdosen->tglpost;
		}


		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE
		$real3="SELECT * from dasboard_dos inner join cabang on dasboard_dos.kodecabang=cabang.kodecabang where (kepemilikan='1' or kepemilikan='2') and ta='$inisialta' and ta <>'0000-00-00' and periode='$periode' AND substr(tglposting,1,7)='$tglpost'";
		$reall = $this->Mainmodel->tampildatacollege($real3);
		

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE
		$rasdos="SELECT avg(rasiowebdosen) as lmsdos,avg(rasiolmsdosen) as webdos from dasboard_dos inner join cabang on dasboard_dos.kodecabang=cabang.kodecabang where (kepemilikan='1' or kepemilikan='2') and ta='$periode' and ta <>'0000-00-00' and periode='$periode' and cabang.kodecabang <> '109' and cabang.kodecabang <> '102' AND substr(tglposting,1,7)='$tglpost'";
		$rasdose = $this->Mainmodel->tampildatacollege($rasdos);
		foreach($rasdose as $rasdosen){
			$data['rasiodoselec'] = $rasdosen->lmsdos;
			$data['rasiodoslms'] = $rasdosen->webdos;
		}
		$data['jeniscabang'] = 'milker';

		$this->load->view('form/dashboard/it', $data);
	}

	public function itnasional(){
		// EWS IT
		// inisial TA
		$real1="select * from inisialta";
		$ta = $this->Mainmodel->tampildatacollege($real1);
		foreach($ta as $rota){
			$inisialta = $rota->ta;
			$tahun = substr($inisialta,0,4);
		}
		$dates = date("Y-m-d");
		// inisial periode
		$iniperiod = "select * from inisialperiode where tgl_awal <='$dates' and tgl_akhir >='$dates'";
		$iniper = $this->Mainmodel->tampildatacollege($iniperiod);
		foreach ($iniper as $nilaiperiode) {
			$periode = $nilaiperiode->periode;
		}

		//cari tanggal max di dasboard bulan dan tahunnya-------------------------------------------------------
		$real2="select max(substr(tglposting,1,7)) as tglposting from dasboard_mhs where ta='$inisialta' and ta <>'0000-00-00' and periode='$periode'";
		$rela2 = $this->Mainmodel->tampildatacollege($real2);
		foreach($rela2 as $realis2){
			$tglposting = $realis2->tglposting;
		}

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE-----------------------------------------------------------------------------------------------------------------------------
		$real3="SELECT avg(rasiolmsmhs) as lmsmhs,avg(rasiowebmhs) as webmhs from dasboard_mhs where ta='$inisialta' and tglposting <>'0000-00-00' and periode='$periode' and kodecabang <> '109' and kodecabang <> '102' AND substr(tglposting,1,7)='$tglposting'";
		$rela3=$this->Mainmodel->tampildatacollege($real3);
		foreach($rela3 as $realis3){
			$data['lmsmhs'] = $realis3->lmsmhs;
			$data['lmsweb'] = $realis3->webmhs;
			$data['periode'] = $periode;

		}
			$data['tahunaka'] = $inisialta;
		//cari tanggal max di dasboard bulan dan tahunnya---------------------------------------------
		$realdos="select max(substr(tglposting,1,7)) as tglpost from dasboard_dos where ta='$inisialta' and tglposting <>'0000-00-00' and periode='$periode'";
		$realdose=$this->Mainmodel->tampildatacollege($realdos);
		foreach($realdose as $realdosen){
			$tglpost = $realdosen->tglpost;
		}

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE-------------------------------
		$real3="SELECT * from dasboard_dos where ta='$inisialta' and ta <>'0000-00-00' and periode='$periode' AND substr(tglposting,1,7)='$tglpost'";
		$reall = $this->Mainmodel->tampildatacollege($real3);
		

		//PANGGIL DASBOARD_MHS SESUAI TANGGAL DAN PERIODE--------------------------------------
		$rasdos="SELECT avg(rasiowebdosen) as lmsdos,avg(rasiolmsdosen) as webdos from dasboard_dos where ta='$inisialta' and ta <>'0000-00-00' and periode='$periode' 
			and kodecabang <> '109' and kodecabang <> '102' AND substr(tglposting,1,7)='$tglposting'";
		$rasdose = $this->Mainmodel->tampildatacollege($rasdos);
		foreach($rasdose as $rasdosen){
			$data['rasiodoselec'] = $rasdosen->lmsdos;
			$data['rasiodoslms'] = $rasdosen->webdos;
		}
		$data['jeniscabang'] = 'nasional';

		$this->load->view('form/dashboard/it', $data);
		
	}


	function keuanganmilker(){
		// KEU PIUTANG 
		 $waktu = date('Y-m-d');
		 $iniperiod = "select ta from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'";
		 $qper = $this->Mainmodel->tampildatacollege($iniperiod);
		 foreach($qper as $rowper);
		 $periodes = $rowper->ta;
		 $tahunangkatan = substr($periodes,0,4);
		 $data['periodes'] = $periodes;
		 
			$biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       		 from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$periodes' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatacollege($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
			 $totsebesar = $rowpiutang->ttlsebesar;
			 $totterbayar = $rowpiutang->ttlterbayar;
			 $totpiutang = $totsebesar - $totterbayar;
			 $totalpiutang = $totalpiutang += $totpiutang;
			 $totalterbayar = $totalterbayar += $totterbayar;
			}
		
		
			$data['limapersen'] = ($totalterbayar * 5) / 100;
			$data['sepuluhpersen'] = ($totalterbayar * 10) / 100;
		 	$data['sebesar'] = $totalpiutang;

		 // tingkat 2 college
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       	from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
		 $cpiutang = $this->Mainmodel->tampildatacollege($biayapiutang);
		 $totalpiutang = 0;
		 $totalterbayar = 0;
		 foreach($cpiutang as $rowpiutang){
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;
			$totalpiutang = $totalpiutang += $totpiutang;
			$totalterbayar = $totalterbayar += $totterbayar;
		 }

		 $data['limapersen2'] = ($totalterbayar * 5) / 100;
		 $data['sepuluhpersen2'] = ($totalterbayar * 10) / 100;
		 $data['sebesar2'] = $totalpiutang;

		 // tingkat 1 college
		 $sumtk1col = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='1' and NPM = '' and biodata.status='aktif' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemua'] = $this->Mainmodel->tampildatacollege($sumtk1col);

		 $sumtk2col = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='2' and biodata.status='aktif'and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemua2'] = $this->Mainmodel->tampildatacollege($sumtk2col);

		 $data['periodecol'] = $periodes;

		 // cari data piutang mahasiswa yang belum dibuatkan rencana
		 $data['piutangnotbiaya'] = $this->Mainmodel->piutangnotbiayamilker('Coll',$periodes)->result();
		 // end piutang tidak ada rencana


		 // piutang poltek
		 $iniperiod = "select ta from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'";
		 $qper = $this->Mainmodel->tampildatapoltek($iniperiod);
		 foreach($qper as $rowper);
		 $periodes = $rowper->ta;
		 

		 // tingkat 1 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$periodes' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
		 foreach($cpiutang as $rowpiutang){
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;
			$totalpiutang = $totalpiutang += $totpiutang;
			$totalterbayar = $totalterbayar += $totterbayar;
		 }	
		 	$data['limapersenpol'] = ($totalterbayar * 5) / 100;
			$data['sepuluhpersenpol'] = ($totalterbayar * 10) / 100;
		 	$data['sebesarpol'] = $totalpiutang;

		 // tingkat 2 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
	       from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
				$totsebesar = $rowpiutang->ttlsebesar;
				$totterbayar = $rowpiutang->ttlterbayar;
				$totpiutang = $totsebesar - $totterbayar;
				$totalpiutang = $totalpiutang += $totpiutang;
				$totalterbayar = $totalterbayar += $totterbayar;
			 }	
			 	$data['limapersen2pol'] = ($totalterbayar * 5) / 100;
				$data['sepuluhpersen2pol'] = ($totalterbayar * 10) / 100;
		        $data['sebesar2pol'] = $totalpiutang;

		// tingkat 3 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar from biaya inner join regispoltek on biaya.nim=regispoltek.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')) group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
		 $totalpiutang = 0;
		 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
				$totsebesar = $rowpiutang->ttlsebesar;
				$totterbayar = $rowpiutang->ttlterbayar;
				$totpiutang = $totsebesar - $totterbayar;
				$totalpiutang = $totalpiutang += $totpiutang;
				$totalterbayar = $totalterbayar += $totterbayar;
			 }
			 $data['limapersenpol3'] = ($totalterbayar * 5) / 100;
			 $data['sepuluhpersenpol3'] = ($totalterbayar * 10) / 100;
		     $data['sebesarpoltk3'] = $totalpiutang;

		 // tingkat 1 poltek
		 $sumtk1pol = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='1' and NPM = '' and biodata.status='aktif' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemuapol'] = $this->Mainmodel->tampildatapoltek($sumtk1pol);

		 $sumtk2pol = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='2' and biodata.status='aktif'and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2'))";
		 $data['piutangsemua2pol'] = $this->Mainmodel->tampildatapoltek($sumtk2pol);

		 
		 $data['tak'] = $waktu;
		 $data['periodespol'] = $periodes;

		 // piutang not biaya poltek 
		 $data['piutangnotbiayapol'] = $this->Mainmodel->piutangnotbiayamilker('PLJ',$periodes)->result();

		 $data['jeniscabang'] = 'milker';

		 	$this->load->view('form/dashboard/keuangan', $data);
	}

	function keuangannasional(){
		// KEU PIUTANG 
		 $waktu = date('Y-m-d');
		 $iniperiod = "select ta from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'";
		 $qper = $this->Mainmodel->tampildatacollege($iniperiod);
		 foreach($qper as $rowper);
		 $periodes = $rowper->ta;
		 $tahunangkatan = substr($periodes,0,4);
		 $data['periodes'] = $periodes;
		 
			$biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       		 from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$periodes' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1') group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatacollege($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
			 $totsebesar = $rowpiutang->ttlsebesar;
			 $totterbayar = $rowpiutang->ttlterbayar;
			 $totpiutang = $totsebesar - $totterbayar;
			 $totalpiutang = $totalpiutang += $totpiutang;
			 $totalterbayar = $totalterbayar += $totterbayar;
			}
		
		
			$data['limapersen'] = ($totalterbayar * 5) / 100;
			$data['sepuluhpersen'] = ($totalterbayar * 10) / 100;
		 	$data['sebesar'] = $totalpiutang;

		 // tingkat 2 college
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       	from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1') group by biaya.kodecabang";
		 $cpiutang = $this->Mainmodel->tampildatacollege($biayapiutang);
		 $totalpiutang = 0;
		 $totalterbayar = 0;
		 foreach($cpiutang as $rowpiutang){
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;
			$totalpiutang = $totalpiutang += $totpiutang;
			$totalterbayar = $totalterbayar += $totterbayar;
		 }

		 $data['limapersen2'] = ($totalterbayar * 5) / 100;
		 $data['sepuluhpersen2'] = ($totalterbayar * 10) / 100;
		 $data['sebesar2'] = $totalpiutang;

		 // tingkat 1 college
		 $sumtk1col = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='1' and NPM = '' and biodata.status='aktif' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1')";
		 $data['piutangsemua'] = $this->Mainmodel->tampildatacollege($sumtk1col);

		 $sumtk2col = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='2' and biodata.status='aktif'and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='1')";
		 $data['piutangsemua2'] = $this->Mainmodel->tampildatacollege($sumtk2col);

		 $data['periodecol'] = $periodes;

		 // cari data piutang mahasiswa yang belum dibuatkan rencana
		 $data['piutangnotbiaya'] = $this->Mainmodel->piutangnotbiaya('Coll',$periodes)->result();
		 // end piutang tidak ada rencana


		 // piutang poltek
		 $iniperiod = "select ta from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'";
		 $qper = $this->Mainmodel->tampildatapoltek($iniperiod);
		 foreach($qper as $rowper);
		 $periodes = $rowper->ta;
		 

		 // tingkat 1 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$periodes' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif') group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
		 foreach($cpiutang as $rowpiutang){
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;
			$totalpiutang = $totalpiutang += $totpiutang;
			$totalterbayar = $totalterbayar += $totterbayar;
		 }	
		 	$data['limapersenpol'] = ($totalterbayar * 5) / 100;
			$data['sepuluhpersenpol'] = ($totalterbayar * 10) / 100;
		 	$data['sebesarpol'] = $totalpiutang;

		 // tingkat 2 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
	       from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif') group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
			 $totalpiutang = 0;
			 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
				$totsebesar = $rowpiutang->ttlsebesar;
				$totterbayar = $rowpiutang->ttlterbayar;
				$totpiutang = $totsebesar - $totterbayar;
				$totalpiutang = $totalpiutang += $totpiutang;
				$totalterbayar = $totalterbayar += $totterbayar;
			 }	
			 	$data['limapersen2pol'] = ($totalterbayar * 5) / 100;
				$data['sepuluhpersen2pol'] = ($totalterbayar * 10) / 100;
		        $data['sebesar2pol'] = $totalpiutang;

		// tingkat 3 poltek
		 $biayapiutang = "SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar from biaya inner join regispoltek on biaya.nim=regispoltek.nim where biaya.ta='$periodes' and tglrencana<='$waktu' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif') group by biaya.kodecabang";
			 $cpiutang = $this->Mainmodel->tampildatapoltek($biayapiutang);
		 $totalpiutang = 0;
		 $totalterbayar = 0;
			 foreach($cpiutang as $rowpiutang){
				$totsebesar = $rowpiutang->ttlsebesar;
				$totterbayar = $rowpiutang->ttlterbayar;
				$totpiutang = $totsebesar - $totterbayar;
				$totalpiutang = $totalpiutang += $totpiutang;
				$totalterbayar = $totalterbayar += $totterbayar;
			 }
			 $data['limapersenpol3'] = ($totalterbayar * 5) / 100;
			 $data['sepuluhpersenpol3'] = ($totalterbayar * 10) / 100;
		     $data['sebesarpoltk3'] = $totalpiutang;

		 // tingkat 1 poltek
		 $sumtk1pol = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='1' and NPM = '' and biodata.status='aktif' and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif')";
		 $data['piutangsemuapol'] = $this->Mainmodel->tampildatapoltek($sumtk1pol);

		 $sumtk2pol = "select sum(sebesar) as sebesar from biaya inner join biodata on biaya.nim=biodata.nim where tglrencana < '$waktu' and ta='$periodes' and tingkat='2' and biodata.status='aktif'and biaya.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif')";
		 $data['piutangsemua2pol'] = $this->Mainmodel->tampildatapoltek($sumtk2pol);

		 
		 $data['tak'] = $waktu;
		 $data['periodespol'] = $periodes;

		
		 // piutang not biaya poltek 
		 $data['piutangnotbiayapol'] = $this->Mainmodel->piutangnotbiaya('PLJ',$periodes)->result();

		 $data['jeniscabang'] = 'nasional';

		 	$this->load->view('form/dashboard/keuangan', $data);
	}



	
}
