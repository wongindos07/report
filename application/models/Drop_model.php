<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Drop_model extends CI_Model{
	function combo_bidang(){
		$grp = $this->session->userdata('grp');
		$myquery = "select kodebidang, namabidang from kpi_bidang";
		$kasus = $this->db->query($myquery);
		foreach ($kasus->result() as $row)
			{$kpi_bidang[$row->kodebidang] = $row->namabidang;}
		return $kpi_bidang;
	}

	function combo_tahun(){
		$myquery = "select distinct tahun from kpi_standar order by tahun DESC";
		$kasus = $this->db->query($myquery);
		foreach ($kasus->result() as $row)
			{$kpi_standar[$row->tahun] = $row->tahun;}
		return $kpi_standar;
	}

	function combo_cabang(){
		$kocab = $this->session->userdata('kocab');
		$myquery = "select kodecabang, mid(namacabang,8,LENGTH(namacabang)-1) as kampus from cabang";
		$kasus = $this->db->query($myquery);
		foreach ($kasus->result() as $row)
			{$cabang[$row->kodecabang] = $row->kampus;}
		return $cabang;
	}

	function combo_pmbmkt(){
		$kocab = $this->session->userdata('kocab');
		$myquery = "select ta from aplikan where ta<>'' group by ta desc limit 5";
		$kasus = $this->db->query($myquery);
		foreach ($kasus->result() as $row)
			{$tapmbmkt[$row->ta] = $row->ta;}
		return $tapmbmkt;
	}

	function combo_tabiodata(){
		$kocab = $this->session->userdata('kocab');
		$myquery = "select tahunangkatan from biodata where tahunangkatan <>'' group by tahunangkatan desc limit 5";
		$kasus = $this->db->query($myquery);
		foreach ($kasus->result() as $row)
			{$tabiodata[$row->tahunangkatan] = $row->tahunangkatan;}
		return $tabiodata;
	}


}
