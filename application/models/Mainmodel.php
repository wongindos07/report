<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmodel extends CI_Model {
 // db2 digunakan untuk mengakses database ke-2 (poltek)
 private $db2;

 public function __construct()
 {
  parent::__construct();
         $this->db2 = $this->load->database('poltek', TRUE);
 }

 function getPeriode($waktu,$kriteria){
    if($kriteria == 'Coll' || $kriteria == 'group'){
      return $this->db->query("select ta, periode from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'");
    }else{
      return $this->db2->query("select ta, periode from inisialperiode where tgl_awal <='$waktu' and tgl_akhir >='$waktu'");
    }
 }

 function getRataKal($kriteria,$periode,$tingkat,$tipe){
    if($kriteria == 'Coll' || $kriteria == 'group'){
      return $this->db->query("select tingkat, avg(ipk) as rataipk from ipkperolehannasional where tahunakademik='$periode' and tingkat='$tingkat' and kriteria='$tipe'");
    }else{
      return $this->db2->query("select tingkat, avg(ipk) as rataipk from ipkperolehannasional where tahunakademik='$periode' and tingkat='$tingkat'"); 
    }
 }

 function getTahunPenempatan($kriteria,$ta){
   if($kriteria == 'Coll' || $kriteria == 'group'){
      return $this->db->query("select ta from realisasi_nasional  where ta<'$ta' group by ta  order by ta desc limit 1
");
   }else{
      return $this->db2->query("select ta from realisasi_nasional  where ta<'$ta' group by ta  order by ta desc limit 1
");
   }
 }

 function penempatan($kriteria,$kocab,$tahun,$tgl){
    if($kriteria == 'Coll' || $kriteria == 'group'){
       return $this->db->query("select terget1, kerja1, target2, kerja2 from realisasi_nasional where kodecabang='$kocab' and tgl_input='$tgl' and ta='$tahun' group by kodecabang order by tgl_input desc");
    }else{
      return $this->db2->query("select terget1, kerja1, target2, kerja2 from realisasi_nasional where kodecabang='$kocab' and tgl_input='$tgl' and ta='$tahun' group by kodecabang order by tgl_input desc");
    }
 }

 function getWaktuInput($kriteria,$kocab,$tahun){
  if($kriteria == 'Coll' || $kriteria == 'group'){
       return $this->db->query("select * from realisasi_nasional where kodecabang='$kocab' and ta='$tahun' order by id desc limit 0,1");
    }else{
      return $this->db2->query("select * from realisasi_nasional where kodecabang='$kocab' and ta='$tahun' order by id desc limit 0,1");
    }
 }

 function tampildatacollege($sql)
		{
			$query	= $this->db->query($sql);
			return $query->result();	
		}

function tampildatapoltek($sql)
		{
			$query	= $this->db2->query($sql);
			return $query->result();	
		}

function set_data($table, $data){
        $this->db->insert($table, $data);
        return true;
        }
function set_datapol($table, $data){
         $this->db2->insert($table, $data);
         return true;
        }       
function del_datacoll($table, $kondisi){
        $this->db->delete($table, $kondisi);
        return true;
	}	
function del_dataplj($table, $kondisi){
        $this->db2->delete($table, $kondisi);
        return true;
    }		
function get_branch($table, $data){
        $this->db->select($table, $data);
        return true;
    }
function update_data($where,$data,$table,$kriteria){
    if($kriteria == 'Coll' || $kriteria == 'group'){
      $this->db->where($where);
      $this->db->update($table,$data);
      return true;
    }else{
      $this->db2->where($where);
      $this->db2->update($table,$data);
      return true;
    }
  }
function dpreg($kriteria){
    if($kriteria=='Coll'){
      $this->db->select('*');
      $this->db->from('konfigurasi_dp_registrasi');
      $this->db->order_by('tahunakademik','asc');
      $data = $this->db->get();
      return $data;
    }elseif($kriteria=='PLJ'){
      $this->db2->select('*');
      $this->db2->from('konfigurasi_dp_registrasi');
      $this->db2->order_by('tahunakademik','asc');
      $data = $this->db2->get();
      return $data;
    }else{
      $this->db->select('*');
      $this->db->from('konfigurasi_dp_registrasi');
      $this->db->order_by('tahunakademik','asc');
      $data = $this->db->get();
      return $data;
    }
}

function getNama($nik){
  $this->db->select('*');
  $this->db->from('profil_struktur');
  $this->db->where('nik',$nik);
  $data = $this->db->get();
  return $data;
}

function getPenempatan($year){
   $this->db->select('COUNT(DISTINCT nim) as totalpenempatan');
   $this->db->from('alumni_aktifitas');
   $this->db->where('ta',$year);
   $data = $this->db->get();
   return $data;
}

function getCabang(){
    return $this->db->query("select * from cabang where (kelompok='1' or kelompok='3')");
}

function getCabangMilker(){
    return $this->db->query("select * from cabang where (kelompok='1' or kelompok='3') and (kepemilikan='1' or kepemilikan='2')");
}

function getWhere($table,$val){
  return $this->db->get_where($table,$val);
}

function getWheres($table,$val,$kriteria){
  if($kriteria == 'Coll' || $kriteria == 'group'){
    return $this->db->get_where($table,$val);
  }else{
    return $this->db2->get_where($table,$val);
  } 
}

function detPiutang($nim2,$ta2,$kriteria){
  if($kriteria == 'Coll' || $kriteria == 'group'){
    $this->db->select("tglrencana, sebesar, terbayar, cicil");
    $this->db->from('biaya');
    $this->db->where('nim',$nim2);
    $this->db->where('ta',$ta2);
    $this->db->order_by('tglrencana','asc');
    $data = $this->db->get();
    return $data;
  }else{
    $this->db2->select("tglrencana, sebesar, terbayar, cicil");
    $this->db2->from('biaya');
    $this->db2->where('nim',$nim2);
    $this->db2->where('ta',$ta2);
    $this->db2->order_by('tglrencana','asc');
    $data = $this->db2->get();
    return $data;
  }
  
}

function getNamaCabang($kocab,$kriteria){
  if($kriteria == "Coll" || $kriteria == "group"){
     $this->db->select('namacabang,kodecabang');
     $this->db->from('cabang');
     $this->db->where('kodecabang',$kocab);
     $data = $this->db->get();
     return $data;
  }else{
     $this->db2->select('namacabang,kodecabang');
     $this->db2->from('cabang');
     $this->db2->where('kodecabang',$kocab);
     $this->db2->where('kelompok','aktif');
     $data = $this->db2->get();
     return $data;
  }


   
}

function getTagihan($tgl){
  $this->db->select('kodecabang, sum(sebesar) as totalsebesar');
  $this->db->from('viewtagihan');
  $this->db->where('tglrencana <=','$tgl');
  $data = $this->db->get();
  return $data;
}

function getTahun($tipe,$kriteria){
  if($kriteria == "Coll" || $kriteria == "group"){
    $this->db->select('tahunakademik');
    $this->db->from('biayakuliah');
    $this->db->where('ket', $tipe);
    $this->db->group_by('tahunakademik');
    $this->db->order_by('tahunakademik','desc');
    $this->db->limit(5);
    $data = $this->db->get();
    return $data;
  }else{
    $this->db2->select('tahunakademik');
    $this->db2->from('biayakuliah');
    $this->db2->where('ket', $tipe);
    $this->db2->group_by('tahunakademik');
    $this->db2->order_by('tahunakademik','desc');
    $this->db2->limit(5);
    $data = $this->db2->get();
    return $data;
  }
}
//GET PERIODE REKAP PMB MARKETING----------------
function getTahunPMB(){
  $this->db->select('ta');
  $this->db->from('aplikan');
  $this->db->group_by('ta');
  $this->db->order_by('ta','desc');
  $this->db->limit(5);
  $data = $this->db->get();
  return $data;
}

function getinisialPMB(){
  $this->db->select('ta');
  $this->db->from('inisialpmb');
  $data = $this->db->get();
  return $data;
}

function getTahunIpk($kriteria){
  if($kriteria == "Coll" || $kriteria == "group"){
    return $this->db->query("select tahunakademik from ipkperolehannasional group by tahunakademik order by tahunakademik desc limit 5");
  }else{
    return $this->db2->query("select tahunakademik from ipkperolehannasional group by tahunakademik order by tahunakademik desc limit 5");
  }
}

function getDataIpk($kriteria,$tak,$tingkat,$tglkalkulasi){
  if($kriteria == "Coll"){
    return $this->db->query("select * from ipkperolehannasional where tahunakademik='$tak' and tingkat='$tingkat' and tglkalkulasi='$tglkalkulasi' and kriteria='1'");
  }elseif($kriteria == "group"){
    return $this->db->query("select * from ipkperolehannasional where tahunakademik='$tak' and tingkat='$tingkat' and tglkalkulasi='$tglkalkulasi' and kriteria='2'");
  }else{
    return $this->db2->query("select * from ipkperolehannasional where tahunakademik='$tak' and tingkat='$tingkat' and tglkalkulasi='$tglkalkulasi'");
  }
}

function getIpk($kriteria,$kocab,$periode,$tingkat){
   if($kriteria == 'Coll' || $kriteria == 'group'){
      if($tingkat == 1){
         return $this->db->query("select sum(kumulatif)/sum(sks) as ttlipk from viewkhs$kocab where ta='$periode' and semester BETWEEN 1 and 2");
      }else{
         return $this->db->query("select sum(kumulatif)/sum(sks) as ttlipk from viewkhs$kocab where nim in (select nim from regissenior where ta='$periode') and semester BETWEEN 1 and 4");
      }
   }else{
       if($tingkat == 1){
         return $this->db2->query("select sum(kumulatif)/sum(sks) as ttlipk from viewkhs$kocab where ta='$periode' and semester BETWEEN 1 and 2");
      }elseif($tingkat == 2){
         return $this->db2->query("select sum(kumulatif)/sum(sks) as ttlipk from viewkhs$kocab where nim in (select nim from regissenior where ta='$periode') and semester BETWEEN 1 and 4");
      }else{
         return $this->db2->query("select sum(kumulatif)/sum(sks) as ttlipk from viewkhs$kocab where nim in (select nim from regispoltek where ta='$periode') and semester BETWEEN 1 and 6");
      }
   }
 }

function detailIpkMhs($kriteria,$kocab,$tak,$tingkat){
  if($kriteria == "Coll" || $kriteria == "group"){
      if($tingkat == 1){
         return $this->db->query("select nim, Nama_Mahasiswa, sum(kumulatif)/sum(sks) as ipk from viewkhs$kocab where ta='$tak' and semester BETWEEN 1 and 2 group by nim");
      }else{
         return $this->db->query("select nim, Nama_Mahasiswa, sum(kumulatif)/sum(sks) as ipk from viewkhs$kocab where nim in (select nim from regissenior where ta='$tak') and semester BETWEEN 1 and 4 group by nim ");
      }
  }else{
      if($tingkat == 1){
         return $this->db2->query("select nim, Nama_Mahasiswa, sum(kumulatif)/sum(sks) as ipk from viewkhs$kocab where ta='$tak' and semester BETWEEN 1 and 2 group by nim");
      }elseif($tingkat == 2){
         return $this->db2->query("select nim, Nama_Mahasiswa, sum(kumulatif)/sum(sks) as ipk from viewkhs$kocab where nim in (select nim from regissenior where ta='$tak') and semester BETWEEN 1 and 4 group by nim ");
      }else{
         return $this->db2->query("select nim, Nama_Mahasiswa, sum(kumulatif)/sum(sks) as ipk from viewkhs$kocab where nim in (select nim from regispoltek where ta='$tak') and semester BETWEEN 1 and 6 group by nim");
      }
  }
}

function getPeriodeKalkulasi($kriteria,$tipe,$tak){
    if($kriteria == "Coll" || $kriteria == "group"){
      return $this->db->query("select tglkalkulasi from ipkperolehannasional where tingkat='$tipe' and tahunakademik='$tak' order by tglkalkulasi desc limit 1");
    }else{
      return $this->db2->query("select tglkalkulasi from ipkperolehannasional where tingkat='$tipe' and tahunakademik='$tak' order by tglkalkulasi desc limit 1");
    }
}

function getTglKalkulasi($kriteria){
  $this->db->select('tglkalkulasi');
  $this->db->from('aplikanperolehannasional');
  $this->db->where('tahunpmb', $kriteria);
  $this->db->where('direktorat', '2');
  $this->db->group_by('tglkalkulasi');
  $this->db->order_by('tglkalkulasi','desc');
  $this->db->limit(31);
  $data = $this->db->get();
  return $data;
}
//END GET PERIODE REKAP PMB MARKETING----------------
function cekPA($kode,$kelas,$kocab,$kriteria){
  if($kriteria == 'Coll'){
    $this->db->select('PA');
    $this->db->from('kelas');
    $this->db->where('kodejurusan',$kode);
    $this->db->where('kelas',$kelas);
    $this->db->where('kodecabang',$kocab);
    $data = $this->db->get();
      return $data;
  }else{
    $this->db2->select('PA');
    $this->db2->from('kelas');
    $this->db2->where('kodejurusan',$kode);
    $this->db2->where('kelas',$kelas);
    $this->db2->where('kodecabang',$kocab);
    $data = $this->db2->get();
      return $data;
  }
  
}

function getPA($kode,$kelas,$kocab,$kriteria){
  if($kriteria == 'Coll'){
    $this->db->select('PA');
    $this->db->from('kelas');
    $this->db->where('kodejurusan',$kode);
    $this->db->where('kelas',$kelas);
    $this->db->where('kodecabang',$kocab);
    $data = $this->db->get();
      return $data;
  }else{
    $this->db2->select('PA');
    $this->db2->from('kelas');
    $this->db2->where('kodejurusan',$kode);
    $this->db2->where('kelas',$kelas);
    $this->db2->where('kodecabang',$kocab);
    $data = $this->db2->get();
      return $data;
  }
}

function getsmtCabang($kriteria,$nim,$angkatan,$nilaicab){
  if($kriteria == "Coll"){
    $this->db->select('*');
    $this->db->from($nilaicab);
    $this->db->where('nim',$nim);
    $this->db->where('TahunAngkatan',$angkatan);
    $this->db->group_by('semester');
    $this->db->order_by('semester','desc');
    $data = $this->db->get();
      return $data;
  }else{
    $this->db2->select('*');
    $this->db2->from($nilaicab);
    $this->db2->where('nim',$nim);
    $this->db2->where('TahunAngkatan',$angkatan);
    $this->db2->group_by('semester');
    $this->db2->order_by('semester','desc');
    $data = $this->db2->get();
      return $data;
  }
}

function getNilaiCabang($nilaicab,$nim,$angkatan,$smt,$kriteria){
  if($kriteria == "Coll"){
      $this->db->select("*");
      $this->db->from($nilaicab);
      $this->db->where('nim',$nim);
      $this->db->where('TahunAngkatan',$angkatan);
      $this->db->where('semester',$smt);
      $this->db->order_by('kodemtk');
      $data = $this->db->get();
      return $data;
  }else{
      $this->db2->select("*");
      $this->db2->from($nilaicab);
      $this->db2->where('nim',$nim);
      $this->db2->where('TahunAngkatan',$angkatan);
      $this->db2->where('semester',$smt);
      $this->db2->order_by('kodemtk');
      $data = $this->db2->get();
      return $data;
  }
}

function getMatkul($kodemtk,$kojur,$smt,$kriteria){
  if($kriteria == "Coll"){
      $this->db->select("*");
      $this->db->from('matakuliah');
      $this->db->where('kodemtk',$kodemtk);
      $this->db->where('kodejurusan',$kojur);
      $this->db->where('smt',$smt);
      $data = $this->db->get();
      return $data;
  }else{
      $this->db2->select("*");
      $this->db2->from('matakuliah');
      $this->db2->where('kodemtk',$kodemtk);
      $this->db2->where('kodejurusan',$kojur);
      $this->db2->where('smt',$smt);
      $data = $this->db2->get();
      return $data;
  }
}

function getLmsCabang($periode,$tahunaka,$tglposting){
  return $this->db->query("
SELECT dasboard_mhs.kodecabang, cabang.namacabang, avg(rasiolmsmhs) as lmsmhs from dasboard_mhs
inner join cabang on dasboard_mhs.kodecabang=cabang.kodecabang where ta='$tahunaka'
 and tglposting <>'0000-00-00' and periode='$periode' and dasboard_mhs.kodecabang <> '109' and dasboard_mhs.kodecabang <> '102'
 AND substr(tglposting,1,7)='$tglposting' group by dasboard_mhs.kodecabang order by avg(rasiolmsmhs) DESC");
}

function getKocabTk($periode,$tahunaka){
  return $this->db->query("SELECT dasboard_mhs.kodecabang from dasboard_mhs inner join cabang on dasboard_mhs.kodecabang=cabang.kodecabang where ta='$tahunaka' and tglposting <>'0000-00-00' and periode='$periode' and (kelompok='1' or kelompok='3') group by dasboard_mhs.kodecabang
 order by avg(rasiolmsmhs) DESC limit 1");
}

function getTk1($kocab,$ta){
  $this->db->select("count(nim) as jumlahtk1");
  $this->db->from("biodata");
  $this->db->where("TahunAngkatan",$ta);
  $this->db->where("status","aktif");
  $this->db->where("kodecabang",$kocab);
  $this->db->group_by("kodecabang");
  $data = $this->db->get();
  return $data;
}

function regTk1($kocab,$ta){
  return $this->db->query("SELECT count(DISTINCT(logmhs.nim)) as nim from logmhs inner join biodata on logmhs.nim=biodata.nim where biodata.TahunAngkatan='$ta'
          and logmhs.situsreferensi like '%lms%' and biodata.`status`='aktif' and logmhs.kodecabang='$kocab'");
}

function getTk2($kocab,$tak){
  $this->db->select("count(regissenior.nim) as jumlahtk2");
  $this->db->from("biodata");
  $this->db->join("regissenior","biodata.nim=regissenior.nim","inner");
  $this->db->where("regissenior.ta",$tak);
  $this->db->where("biodata.kodecabang",$kocab);
  $this->db->where("biodata.status","aktif");
  $data = $this->db->get();
  return $data;
}

function regTk2($kocab,$tak){
  return $this->db->query("SELECT count(DISTINCT(logmhs.nim)) as jumlah from logmhs inner join regissenior on logmhs.nim=regissenior.nim join biodata on logmhs.nim=biodata.nim where regissenior.ta='$tak' and (logmhs.situsreferensi like '%lms%') and biodata.`status`='aktif' and regissenior.kodecabang='$kocab'");
}


function getDetailPiutang($kocab,$tahun,$waktu){
  $this->db->select('
    nim, 
    kodecabang,
    sum(sebesar) as ttlsebesar,
    sum(terbayar) as ttlterbayar');
  $this->db->from('biaya');
  $this->db->where('ta', $tahun);
  $this->db->where('tglrencana <=', $waktu);
  $this->db->where('kodecabang', $kocab);
  $this->db->group_by('nim');
  $data = $this->db->get();
  return $data;
}

function getDetailPiutangSenior($tahun,$waktu,$kocab,$kriteria){
  if($kriteria == "Coll" || $kriteria == "group"){
    $this->db->select('
      biaya.nim, 
      biaya.kodecabang,
      biaya.ta,
      sum(sebesar) as ttlsebesar,
      sum(terbayar) as ttlterbayar');
    $this->db->from('biaya');
    $this->db->join('regissenior','biaya.nim=regissenior.nim','inner');
    $this->db->where('biaya.ta',$tahun);
    $this->db->where('tglrencana <=', $waktu);
    $this->db->group_by('biaya.nim');
    $this->db->where('biaya.kodecabang', $kocab);
    $data = $this->db->get();
    return $data;
  }else{
    $this->db2->select('
      biaya.nim, 
      biaya.kodecabang,
      biaya.ta,
      sum(sebesar) as ttlsebesar,
      sum(terbayar) as ttlterbayar');
    $this->db2->from('biaya');
    $this->db2->join('regissenior','biaya.nim=regissenior.nim','inner');
    $this->db2->where('biaya.ta',$tahun);
    $this->db2->where('tglrencana <=', $waktu);
    $this->db2->group_by('biaya.nim');
    $this->db2->where('biaya.kodecabang', $kocab);
    $data = $this->db2->get();
    return $data;
  }
  
}

function getDetailPiutangPoltek($tahun,$waktu,$kocab){
  $this->db2->select('
      biaya.nim, 
      biaya.kodecabang,
      biaya.ta,
      sum(sebesar) as ttlsebesar,
      sum(terbayar) as ttlterbayar');
    $this->db2->from('biaya');
    $this->db2->join('regispoltek','biaya.nim=regispoltek.nim','inner');
    $this->db2->where('biaya.ta',$tahun);
    $this->db2->where('tglrencana <=', $waktu);
    $this->db2->group_by('biaya.nim');
    $this->db2->where('biaya.kodecabang', $kocab);
    $data = $this->db2->get();
    return $data;
}

function getDetailPiutangJunior($tahun,$waktu,$kocab,$kriteria){
  $tahunangkatan = substr($tahun,0,4);

  if($kriteria == "Coll" || $kriteria == "group"){
    $this->db->select('
    biaya.nim, 
    biaya.kodecabang,
    biaya.ta,
    sum(sebesar) as ttlsebesar,
    sum(terbayar) as ttlterbayar');
    $this->db->from('biaya');
    $this->db->join('biodata','biaya.nim=biodata.nim','inner');
    $this->db->where('biaya.ta',$tahun);
    $this->db->where('biodata.status','aktif');
    $this->db->where('biodata.TahunAngkatan',$tahunangkatan);
    $this->db->where('biaya.kodecabang', $kocab);
    $this->db->where('tglrencana <=', $waktu);
    $this->db->group_by('biaya.nim');
    $data = $this->db->get();
    return $data;
  }else{
    $this->db2->select('
    biaya.nim, 
    biaya.kodecabang,
    biaya.ta,
    sum(sebesar) as ttlsebesar,
    sum(terbayar) as ttlterbayar');
    $this->db2->from('biaya');
    $this->db2->join('biodata','biaya.nim=biodata.nim','inner');
    $this->db2->where('biaya.ta',$tahun);
    $this->db2->where('biodata.status','aktif');
    $this->db2->where('biodata.TahunAngkatan',$tahunangkatan);
    $this->db2->where('biaya.kodecabang', $kocab);
    $this->db2->where('tglrencana <=', $waktu);
    $this->db2->group_by('biaya.nim');
    $data = $this->db2->get();
    return $data;
  }
  
}

function getSumPiutangSenior($tahun,$waktu,$kocab,$kriteria){
  if($kriteria == "Coll" || $kriteria == "group"){
    $this->db->select('sum(sebesar) as sebesar, sum(terbayar) as terbayar');
    $this->db->from('biaya');
    $this->db->join('regissenior','biaya.nim=regissenior.nim','inner');
    $this->db->where('biaya.ta',$tahun);
    $this->db->where('tglrencana <=', $waktu);
    $this->db->where('biaya.kodecabang', $kocab);
    $this->db->group_by('biaya.kodecabang');
    $data = $this->db->get();
    return $data;
  }else{
    $this->db2->select('sum(sebesar) as sebesar, sum(terbayar) as terbayar');
    $this->db2->from('biaya');
    $this->db2->join('regissenior','biaya.nim=regissenior.nim','inner');
    $this->db2->where('biaya.ta',$tahun);
    $this->db2->where('tglrencana <=', $waktu);
    $this->db2->where('biaya.kodecabang', $kocab);
    $this->db2->group_by('biaya.kodecabang');
    $data = $this->db2->get();
    return $data;
  }
  
}

function getSumPiutangPoltek($tahun,$waktu,$kocab){
    $this->db2->select('sum(sebesar) as sebesar, sum(terbayar) as terbayar');
    $this->db2->from('biaya');
    $this->db2->join('regispoltek','biaya.nim=regispoltek.nim','inner');
    $this->db2->where('biaya.ta',$tahun);
    $this->db2->where('tglrencana <=', $waktu);
    $this->db2->where('biaya.kodecabang', $kocab);
    $this->db2->group_by('biaya.kodecabang');
    $data = $this->db2->get();
    return $data;
}

function getSumPiutangJunior($tahun,$waktu,$kocab,$kriteria){
  if($kriteria == "Coll" || $kriteria == "group"){
    $tahunangkatan = substr($tahun,0,4);
    $this->db->select('sum(sebesar) as sebesar, sum(terbayar) as terbayar');
    $this->db->from('biaya');
    $this->db->join('biodata','biaya.nim=biodata.nim','inner');
    $this->db->where('biaya.ta',$tahun);
    $this->db->where('biodata.status','aktif');
    $this->db->where('biodata.TahunAngkatan',$tahunangkatan);
    $this->db->where('biaya.kodecabang', $kocab);
    $this->db->where('tglrencana <=', $waktu);
    $this->db->group_by('biaya.kodecabang');
    $data = $this->db->get();
    return $data;
  }else{
    $tahunangkatan = substr($tahun,0,4);
    $this->db2->select('sum(sebesar) as sebesar, sum(terbayar) as terbayar');
    $this->db2->from('biaya');
    $this->db2->join('biodata','biaya.nim=biodata.nim','inner');
    $this->db2->where('biaya.ta',$tahun);
    $this->db2->where('biodata.status','aktif');
    $this->db2->where('biodata.TahunAngkatan',$tahunangkatan);
    $this->db2->where('biaya.kodecabang', $kocab);
    $this->db2->where('tglrencana <=', $waktu);
    $this->db2->group_by('biaya.kodecabang');
    $data = $this->db2->get();
    return $data;
  }
  
}

function detailMhsSenior($nim,$ta){
  $this->db->select("tglrencana, sebesar, terbayar, cicil");
  $this->db->from('biaya');
  $this->db->where('nim', $nim);
  $this->db->where('ta', $ta);
  $data = $this->db->get();
  return $data;
}

function getTak($cab){
  $this->db->select("tahunakademik");
  $this->db->from('mhs_jml_bln');
  $this->db->where('kodecabang', $cab);
  $this->db->group_by('tahunakademik');
  $this->db->order_by('tahunakademik','desc');
  $data = $this->db->get();
  return $data;
}

function cekDataBio($ta,$x,$cab){
  return $this->db->query("select kelas from biodata where kodecabang='$cab' and tahunangkatan='$ta' and status='Aktif' and tingkat='$x' group by kelas");
}

function getDataBio($kode,$ta,$x,$cab){
  return $this->db->query("select kelas from biodata where kodecabang='$cab' and kode='$kode' and tahunangkatan='$ta' and status='Aktif' and tingkat='$x' group by kelas");
}

function getJmlEdu($kelas,$kode,$tak,$x,$cab){
  return $this->db->query("select jml_edu from mhs_serahterima where tingkat='$x' and kodecabang='$cab' and kelas='$kelas' and kode='$kode' and tahunakademik='$tak' ");
}

function getSqq2($kelas,$kode,$tak,$x,$cab,$bln_mundur,$thnx){
  return $this->db->query("select jml_edu from mhs_jml_bln where tingkat='$x' and kodecabang='$cab' and kelas='$kelas' and kode='$kode' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thn'");
}

function getJurusanTk($kocab,$tak,$tingkat,$ta){
    if($tingkat == 1){
        return $this->db->query("SELECT biodata.kode from logmhs inner join biodata on logmhs.nim=biodata.nim where biodata.TahunAngkatan='$ta' and logmhs.situsreferensi like '%lms%' and biodata.`status`='aktif' and logmhs.kodecabang='$kocab' group by biodata.kode");
    }else{

    }
}

function jmlJurusanTk($kocab,$tak,$tingkat,$ta,$kode){
  if($tingkat == 1){

    return $this->db->query("SELECT count(DISTINCT(logmhs.nim)) as nim,  from logmhs inner join biodata on logmhs.nim=biodata.nim where biodata.TahunAngkatan='$ta' and logmhs.situsreferensi like '%lms%' and biodata.`status`='aktif' and logmhs.kodecabang='$kocab' and biodata.kode='$kode'");

  }else{

  }
}

function getU30($kocab,$kriteria,$tak){
  if($kriteria == "Coll"){
      $this->db->select("sum(jumlahbayar) as total");
      $this->db->from("bayarkuliah");
      $this->db->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db->where('tahunakademik',$tak);
      $this->db->where('bayarkuliah.kodecabang',$kocab);
      $this->db->where('status','U-30');
      $this->db->where('ket','Registrasi Mahasiswa Baru');
      $this->db->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db->get();
      return $data;
  }elseif($kriteria == "PLJ"){
      $this->db2->select("sum(jumlahbayar) as total");
      $this->db2->from("bayarkuliah");
      $this->db2->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db2->where('tahunakademik',$tak);
      $this->db2->where('bayarkuliah.kodecabang',$kocab);
      $this->db2->where('status','U-30');
      $this->db2->where('ket','Registrasi Mahasiswa Baru');
      $this->db2->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db2->get();
      return $data;
  }else{
      $this->db->select("sum(jumlahbayar) as total");
      $this->db->from("bayarkuliah");
      $this->db->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db->where('tahunakademik',$tak);
      $this->db->where('bayarkuliah.kodecabang',$kocab);
      $this->db->where('status','U-30');
      $this->db->where('ket','Registrasi Mahasiswa Baru');
      $this->db->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db->get();
      return $data;
  }
}

function getA30($kocab,$kriteria,$tak){
  if($kriteria == "Coll"){
      $this->db->select("sum(jumlahbayar) as total");
      $this->db->from("bayarkuliah");
      $this->db->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db->where('tahunakademik',$tak);
      $this->db->where('bayarkuliah.kodecabang',$kocab);
      $this->db->where('status','Aktif');
      $this->db->where('ket','Registrasi Mahasiswa Baru');
      $this->db->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db->get();
      return $data;
  }elseif($kriteria == "PLJ"){
      $this->db2->select("sum(jumlahbayar) as total");
      $this->db2->from("bayarkuliah");
      $this->db2->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db2->where('tahunakademik',$tak);
      $this->db2->where('bayarkuliah.kodecabang',$kocab);
      $this->db2->where('status','Aktif');
      $this->db2->where('ket','Registrasi Mahasiswa Baru');
      $this->db2->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db2->get();
      return $data;
  }else{
      $this->db->select("sum(jumlahbayar) as total");
      $this->db->from("bayarkuliah");
      $this->db->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db->where('tahunakademik',$tak);
      $this->db->where('bayarkuliah.kodecabang',$kocab);
      $this->db->where('status','Aktif');
      $this->db->where('ket','Registrasi Mahasiswa Baru');
      $this->db->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db->get();
      return $data;
  }
}

function ambiltk2($kocab,$kriteria,$tak){
if($kriteria == "Coll"){
      $this->db->select("sum(jumlahbayar) as total");
      $this->db->from("bayarkuliah");
      $this->db->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db->where('tahunakademik',$tak);
      $this->db->where('bayarkuliah.kodecabang',$kocab);
      $this->db->where('status','Aktif');
      $this->db->where('ket','Registrasi Mahasiswa Senior');
      $this->db->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db->get();
      return $data;
  }elseif($kriteria == "PLJ"){
      $this->db2->select("sum(jumlahbayar) as total");
      $this->db2->from("bayarkuliah");
      $this->db2->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db2->where('tahunakademik',$tak);
      $this->db2->where('bayarkuliah.kodecabang',$kocab);
      $this->db2->where('status','Aktif');
      $this->db2->where('ket','Registrasi Mahasiswa Senior');
      $this->db2->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db2->get();
      return $data;
  }else{
      $this->db->select("sum(jumlahbayar) as total");
      $this->db->from("bayarkuliah");
      $this->db->join('biodata','bayarkuliah.nim=biodata.nim', 'inner');
      $this->db->where('tahunakademik',$tak);
      $this->db->where('bayarkuliah.kodecabang',$kocab);
      $this->db->where('status','Aktif');
      $this->db->where('ket','Registrasi Mahasiswa Senior');
      $this->db->order_by('count(DISTINCT bayarkuliah.nim)','desc');
      $data = $this->db->get();
      return $data;
  }
}

function jurusanPmb($kocab,$tak,$kriteria,$kojur,$status,$ket){

    if($kriteria == "Coll"){
      return $this->db->query("SELECT sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tak' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' and biodata.kode='$kojur'");
  }elseif($kriteria == "PLJ"){
      return $this->db2->query("SELECT sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tak' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' and biodata.kode='$kojur'");
  }else{
     return $this->db->query("SELECT sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where tahunakademik='$tak' and biodata.`status`='$status' and bayarkuliah.kodecabang='$kocab' and ket='$ket' and biodata.kode='$kojur'");
  }
}

function ambilpiutangcol($tipe,$tahun,$tahunangkatan,$kocab,$waktu){
  if($tipe == "Registrasi Mahasiswa Baru"){
        return $this->db->query("SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$tahun' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang='$kocab' group by biaya.kodecabang");
      }else{
        return $this->db->query("SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$tahun' and tglrencana<='$waktu' and biaya.kodecabang='$kocab' group by biaya.kodecabang");
      }
}

function ambilpiutangpol($tipe,$tahun,$tahunangkatan,$kocab,$waktu){
  if($tipe == "Registrasi Mahasiswa Baru"){
        return $this->db2->query("SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       from biaya inner join biodata on biaya.nim=biodata.nim where biaya.ta='$tahun' and biodata.`status`='aktif' and biodata.TahunAngkatan='$tahunangkatan' and tglrencana<='$waktu' and biaya.kodecabang='$kocab' group by biaya.kodecabang");
      }elseif($tipe == "Registrasi Mahasiswa Senior"){
        return $this->db2->query("SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar
       from biaya inner join regissenior on biaya.nim=regissenior.nim where biaya.ta='$tahun' and tglrencana<='$waktu' and biaya.kodecabang='$kocab' group by biaya.kodecabang");
      }else{
        return $this->db2->query("SELECT biaya.kodecabang, sum(sebesar) as ttlsebesar, sum(terbayar) as ttlterbayar from biaya inner join regispoltek on biaya.nim=regispoltek.nim where biaya.ta='$tahun' and tglrencana<='$waktu' and biaya.kodecabang='$kocab' group by biaya.kodecabang");
      }
}

function getRegistrasi($kocab,$from,$to,$kriteria,$tingkat){
  if($kriteria == "Coll"){

    if($tingkat == 1){
      return $this->db->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Baru' and deo_tg between '$from' and '$to' and kodecabang='$kocab'");
    }elseif($tingkat == 2){
      return $this->db->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Senior' and deo_tg between '$from' and '$to' and kodecabang='$kocab'");
    }

  }elseif($kriteria == "PLJ"){

    if($tingkat == 1){
      return $this->db2->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Baru' and deo_tg between '$from' and '$to' and kodecabang='$kocab'");
    }elseif($tingkat == 2){
      return $this->db2->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Senior' and deo_tg between '$from' and '$to' and kodecabang='$kocab'");
    }else{
       return $this->db2->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Tingkat 3' and deo_tg BETWEEN
      '$from' and '$to' and kodecabang='$kocab'");
    }

  }else{

    if($tingkat == 1){
      return $this->db->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Baru' and deo_tg between '$from' and '$to' and kodecabang='$kocab'");
    }elseif($tingkat == 2){
      return $this->db->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Senior' and deo_tg between '$from' and '$to' and kodecabang='$kocab'");
    }else{
       return $this->db->query("SELECT sum(terbayar) as total1, count(DISTINCT nim) as total2 from biayakuliah where ket='Registrasi Mahasiswa Tingkat 3' and deo_tg BETWEEN
      '$from' and '$to' and kodecabang='$kocab'");
    }

  }
}

function getDaftar($kocab,$from,$to,$kriteria){
  if($kriteria == "Coll" || $kriteria == "grup"){
    return $this->db->query("SELECT sum(total) as totaldaftar, count(DISTINCT idaplikan) as ttlaplikan from biayadaftar inner join aplikan ON biayadaftar.idaplikan=aplikan.id where TglDaftar between '$from' and '$to' and biayadaftar.kodecabang='$kocab'");
  }elseif($kriteria == "PLJ"){
    return $this->db2->query("SELECT sum(total) as totaldaftar, count(DISTINCT idaplikan) as ttlaplikan from biayadaftar inner join aplikan ON biayadaftar.idaplikan=aplikan.id where TglDaftar between '$from' and '$to' and biayadaftar.kodecabang='$kocab'");
  }
}

function getAplikanDaftar($from,$to,$kocab,$kriteria){
  if($kriteria == "Coll" || $kriteria == "grup"){
    return $this->db->query("SELECT * from biayadaftar inner join aplikan ON biayadaftar.idaplikan=aplikan.id where TglDaftar between '$from' and '$to' and biayadaftar.kodecabang='$kocab'");
  }elseif($kriteria == "PLJ"){
    return $this->db2->query("SELECT * from biayadaftar inner join aplikan ON biayadaftar.idaplikan=aplikan.id where TglDaftar between '$from' and '$to' and biayadaftar.kodecabang='$kocab'");
  }
}

function getRegMhs($from,$to,$kocab,$kriteria,$tingkat){
  if($kriteria == "Coll"){

    if($tingkat == 1){
      return $this->db->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Baru' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }elseif($tingkat == 2){
      return $this->db->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Senior' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }

  }elseif($kriteria == "PLJ"){

    if($tingkat == 1){
      return $this->db2->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Baru' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }elseif($tingkat == 2){
      return $this->db2->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Senior' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }else{
       return $this->db2->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Tingkat 3' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }

  }else{

    if($tingkat == 1){
      return $this->db->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Baru' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }elseif($tingkat == 2){
      return $this->db->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Senior' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }else{
       return $this->db->query("SELECT * from biayakuliah inner join biodata on biayakuliah.nim=biodata.nim where ket='Registrasi Mahasiswa Tingkat 3' and deo_tg between '$from' and '$to' and biayakuliah.kodecabang='$kocab'");
    }

  }
}


function trxJurusanRealisasi($kriteria,$kocab,$ta,$dates,$from,$to){
  if($kriteria == "Coll" || $kriteria == "grup"){
    if($from == '' && $to == ''){
         return $this->db->query("SELECT jurusan.kodejurusan, jurusan.namajurusan from bayarkuliah
 inner join biodata on bayarkuliah.nim=biodata.nim inner join jurusan on
biodata.kode=jurusan.kodejurusan where bayarkuliah.kodecabang='$kocab' and
 tahunakademik='$ta' and tanggalbayar <= '$dates' group by jurusan.kodejurusan");
    }elseif($from <> '' && $to <> ''){
         return $this->db->query("SELECT jurusan.kodejurusan, jurusan.namajurusan from bayarkuliah
 inner join biodata on bayarkuliah.nim=biodata.nim inner join jurusan on
biodata.kode=jurusan.kodejurusan where bayarkuliah.kodecabang='$kocab' and
 tahunakademik='$ta' and tanggalbayar between '$from' and '$to' group by jurusan.kodejurusan");
    }

  }else{

    if($from == '' && $to == ''){
        return $this->db2->query("SELECT jurusan.kodejurusan, jurusan.namajurusan from bayarkuliah
 inner join biodata on bayarkuliah.nim=biodata.nim inner join jurusan on
biodata.kode=jurusan.kodejurusan where bayarkuliah.kodecabang='$kocab' and
 tahunakademik='$ta' and tanggalbayar <= '$dates' group by jurusan.kodejurusan");
    }elseif($from <> '' && $to <> ''){
       return $this->db2->query("SELECT jurusan.kodejurusan, jurusan.namajurusan from bayarkuliah
 inner join biodata on bayarkuliah.nim=biodata.nim inner join jurusan on
biodata.kode=jurusan.kodejurusan where bayarkuliah.kodecabang='$kocab' and
 tahunakademik='$ta' and tanggalbayar between '$from' and '$to' group by jurusan.kodejurusan");
    }

  }
}

function jurusanRealisasi($kriteria,$kocab,$ta,$dates,$from,$to,$kojur){
  if($kriteria == "Coll" || $kriteria == "grup"){
    if($from == '' && $to == ''){

         return $this->db->query("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and ket='Pembayaran angsuran' and tanggalbayar <= '$dates' and biodata.kode='$kojur'");
    }elseif($from <> '' && $to <> ''){

        return $this->db->query("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and ket='Pembayaran angsuran' and tanggalbayar between '$from' and '$to' and biodata.kode='$kojur'");
    }

  }else{

    if($from == '' && $to == ''){

       return $this->db2->query("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and ket='Pembayaran angsuran' and tanggalbayar <= '$dates' and biodata.kode='$kojur'");
    }elseif($from <> '' && $to <> ''){

      return $this->db2->query("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and ket='Pembayaran angsuran' and tanggalbayar between '$from' and '$to' and biodata.kode='$kojur'");
    }

  }
}

// function trxMhsRealisasi($kriteria,$kocab,$ta,$dates,$from,$to,$kojur){
//   if($kriteria == "Coll" || $kriteria == "grup"){
//       if($from == '' && $to == ''){
//          return $this->db->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar <= '$dates' and biodata.kode='$kojur' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
//     }elseif($from <> '' && $to <> ''){
//          return $this->db->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar between '$from' and '$to' and biodata.kode='$kojur' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
//     }
//   }else{
//     if($from == '' && $to == ''){
//         return $this->db2->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar <= '$dates' and biodata.kode='$kojur' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
//     }elseif($from <> '' && $to <> ''){
//         return $this->db2->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar between '$from' and '$to' and biodata.kode='$kojur' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
//     }
//   }
// }

function trxMhsRealisasi($kriteria,$kocab,$ta,$dates,$from,$to){
  if($kriteria == "Coll" || $kriteria == "grup"){
      if($from == '' && $to == ''){
         return $this->db->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar = '$dates' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }elseif($from <> '' && $to <> ''){
         return $this->db->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar between '$from' and '$to' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }
  }else{
    if($from == '' && $to == ''){
        return $this->db2->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar = '$dates' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }elseif($from <> '' && $to <> ''){
        return $this->db2->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and tahunakademik='$ta' and tanggalbayar between '$from' and '$to' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }
  }
}

function trxMhsRealisasiJurusan($kriteria,$kocab,$ta,$dates,$from,$to,$kojur){
  if($kriteria == "Coll" || $kriteria == "grup"){
      if($from == '' && $to == ''){
         return $this->db->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and biodata.kode='$kojur' and tahunakademik='$ta' and tanggalbayar = '$dates' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }elseif($from <> '' && $to <> ''){
         return $this->db->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and biodata.kode='$kojur' and tahunakademik='$ta' and tanggalbayar between '$from' and '$to' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }
  }else{
    if($from == '' && $to == ''){
        return $this->db2->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and biodata.kode='$kojur' and tahunakademik='$ta' and tanggalbayar = '$dates' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }elseif($from <> '' && $to <> ''){
        return $this->db2->query("SELECT bayarkuliah.tahunakademik, bayarkuliah.nim, biodata.Nama_Mahasiswa, bayarkuliah.tingkat, sum(jumlahbayar) as total, biodata.kode from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab' and biodata.kode='$kojur' and tahunakademik='$ta' and tanggalbayar between '$from' and '$to' and ket='Pembayaran angsuran' group by bayarkuliah.nim");
    }
  }
}

function detBayar($nim2,$tak,$kriteria,$kocab,$from,$to){
  $hariini = date('Y-m-d');
  if($kriteria == "Coll" || $kriteria == "grup"){
    if($from == '' && $to == ''){
         return $this->db->query("select * from bayarkuliah where tahunakademik='$tak' and nim='$nim2' and ket='Pembayaran angsuran' and kodecabang='$kocab' and tanggalbayar='$hariini' group by akun order by tanggalbayar asc");
    }elseif($from <> '' && $to <> ''){
         return $this->db->query("select * from bayarkuliah where tahunakademik='$tak' and nim='$nim2' and ket='Pembayaran angsuran' and kodecabang='$kocab' and tanggalbayar between '$from' and '$to' group by akun order by tanggalbayar asc");
    }
  }else{
    if($from == '' && $to == ''){
        return $this->db2->query("select * from bayarkuliah where tahunakademik='$tak' and nim='$nim2' and ket='Pembayaran angsuran' and kodecabang='$kocab' and tanggalbayar='$hariini' group by akun order by tanggalbayar asc");
    }elseif($from <> '' && $to <> ''){
        return $this->db2->query("select * from bayarkuliah where tahunakademik='$tak' and nim='$nim2' and ket='Pembayaran angsuran' and kodecabang='$kocab' and tanggalbayar between '$from' and '$to' group by akun order by tanggalbayar asc");
    }
  }
}


// function detBayar($nim2,$tak,$kriteria,$kocab){
//   if($kriteria == "Coll" || $kriteria == "grup"){
//     return $this->db->query("select * from bayarkuliah where tahunakademik='$tak' and nim='$nim2' and ket='Pembayaran angsuran' and kodecabang='$kocab' order by tanggalbayar asc");
//   }else{
//     return $this->db2->query("select * from bayarkuliah where tahunakademik='$tak' and nim='$nim2' and ket='Pembayaran angsuran' and kodecabang='$kocab' order by tanggalbayar asc");
//   }
// }

function piutangnotbiaya($kriteria,$tak){
  if($kriteria == "Coll" || $kriteria == "grup"){
    return $this->db->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where 
biayakuliah.nim not in(select nim from biaya where ta='$tak') and
biayakuliah.tahunakademik='$tak' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
biayakuliah.kodecabang in(select cabang.kodecabang from cabang where (kelompok='1' or kelompok='3')) and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior')");
  }else{
    return $this->db2->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where 
biayakuliah.nim not in(select nim from biaya where ta='$tak') and
biayakuliah.tahunakademik='$tak' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
biayakuliah.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif') and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior' or biayakuliah.ket='Registrasi Mahasiswa Tingkat 3')");
  }
}

function piutangnotbiayamilker($kriteria,$tak){
 if($kriteria == "Coll" || $kriteria == "grup"){
    return $this->db->query("SELECT sum(biayakuliah.jumlahbiaya) as jumlahbiaya, sum(biayakuliah.terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon) and
(biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior' or biayakuliah.ket='Registrasi Mahasiswa Tingkat 3') and biayakuliah.tahunakademik='$tak'  and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus' ) and 
biayakuliah.nim not in(select nim from biaya where ta='$tak' ) and
biayakuliah.kodecabang in (select cabang.kodecabang from cabang where  (kelompok='1' or kelompok='3') and (kepemilikan='1' or kepemilikan='2'))");
  }else{
    return $this->db2->query("SELECT sum(biayakuliah.jumlahbiaya) as jumlahbiaya, sum(biayakuliah.terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon) and
(biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior' or biayakuliah.ket='Registrasi Mahasiswa Tingkat 3') and biayakuliah.tahunakademik='$tak'  and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus' ) and 
biayakuliah.nim not in(select nim from biaya where ta='$tak' ) and
biayakuliah.kodecabang in (select cabang.kodecabang from cabang where  kelompok='aktif' and (kepemilikan='1' or kepemilikan='2'))");
  }
}

// function piutangnotbiayamilker($kriteria,$tak){
//  if($kriteria == "Coll" || $kriteria == "grup"){
//     return $this->db->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
// inner join biodata on biayakuliah.nim=biodata.nim
// where 
// biayakuliah.nim not in(select nim from biaya where ta='$tak') and
// biayakuliah.tahunakademik='$tak' and
// (biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
// ((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
// biayakuliah.kodecabang in(select cabang.kodecabang from cabang where (kelompok='1' or kelompok='3') and (kepemilikan='1' or kepemilikan='2')) and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior')");
//   }else{
//     return $this->db2->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
// inner join biodata on biayakuliah.nim=biodata.nim
// where 
// biayakuliah.nim not in(select nim from biaya where ta='$tak') and
// biayakuliah.tahunakademik='$tak' and
// (biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
// ((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
// biayakuliah.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif' and (kepemilikan='1' or kepemilikan='2')) and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior' or biayakuliah.ket='Registrasi Mahasiswa Tingkat 3')");
//   }
// }

function piutangNotCabang($kriteria,$tak,$kocab){
  if($kriteria == "Coll" || $kriteria == "grup"){
    return $this->db->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where 
biayakuliah.nim not in(select nim from biaya where ta='$tak') and
biayakuliah.tahunakademik='$tak' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and biayakuliah.kodecabang='$kocab' and
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
biayakuliah.kodecabang in(select cabang.kodecabang from cabang where kelompok='1' or kelompok='3') and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior')");
  }else{
    return $this->db2->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where 
biayakuliah.nim not in(select nim from biaya where ta='$tak') and
biayakuliah.tahunakademik='$tak' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
biayakuliah.kodecabang in(select cabang.kodecabang from cabang where kelompok='aktif') and biayakuliah.kodecabang='$kocab' and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior' or biayakuliah.ket='Registrasi Mahasiswa Tingkat 3')");
  }
}

function piutangNotMhs($kriteria,$tahun,$kocab){
  if($kriteria == "Coll" || $kriteria == "grup"){
    return $this->db->query("SELECT biayakuliah.nim, biodata.Nama_Mahasiswa, biodata.kode, ket, sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar, biayakuliah.diskon from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where biayakuliah.nim not in(select nim from biaya where ta='$tahun') and
biayakuliah.tahunakademik='$tahun' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
biayakuliah.kodecabang='$kocab' and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior')
group by biayakuliah.nim");
  }else{
return $this->db2->query("SELECT biayakuliah.nim, biodata.Nama_Mahasiswa, biodata.kode, ket, sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar, biayakuliah.diskon from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where biayakuliah.nim not in(select nim from biaya where ta='$tahun') and
biayakuliah.tahunakademik='$tahun' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and
biayakuliah.kodecabang='$kocab' and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior' or biayakuliah.ket='Registrasi Mahasiswa Tingkat 3')
group by biayakuliah.nim");
  }
}

function sumPiutangNotMhs($kriteria,$tahun,$kocab){
  if($kriteria == "Coll" || $kriteria == "grup"){
    return $this->db->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where biayakuliah.nim not in(select nim from biaya where ta='$tahun') and
biayakuliah.tahunakademik='$tahun' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and biayakuliah.kodecabang='$kocab' and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior')");
  }else{
    return $this->db2->query("SELECT sum(jumlahbiaya) as jumlahbiaya, sum(terbayar) as terbayar from biayakuliah
inner join biodata on biayakuliah.nim=biodata.nim
where biayakuliah.nim not in(select nim from biaya where ta='$tahun') and
biayakuliah.tahunakademik='$tahun' and
(biodata.status='aktif' or biodata.status='cuti' or biodata.status='lulus') and 
((biayakuliah.jumlahbiaya <> (biayakuliah.terbayar + biayakuliah.diskon))) and biayakuliah.kodecabang='$kocab' and (biayakuliah.ket='Registrasi Mahasiswa Baru' or biayakuliah.ket='Registrasi Mahasiswa Senior' or biayakuliah.ket='Registrasi Mahasiswa Tingkat 3')");
  }
}


public function pmbcabang($kriteria,$tahun,$datekal){
    if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("SELECT cabang.kodecabang, cabang.namacabang, tahunpmb,sum(targetjunior) as targetjunior,sum(regjunior) as registrasisah,sum(`u-30`) as registrasibooking, aplikanperolehannasional.tglkalkulasi from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang where aplikanperolehannasional.kelompok='1' and tahunpmb='$tahun' and direktorat='2' and tglkalkulasi='$datekal' group by aplikanperolehannasional.kodecabang");
    }else{
      return $this->db2->query("SELECT cabang.kodecabang, cabang.namacabang,tahunpmb,sum(targetjunior) as targetjunior,sum(regjunior) as registrasisah,sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang where tahunpmb='$tahun' and kelompok='aktif' and direktorat='3' and tglkalkulasi='$datekal' group by aplikanperolehannasional.kodecabang");
    }
}

public function pmbcabangmilker($kriteria,$tahun,$datekal){
    if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("SELECT cabang.kodecabang, cabang.namacabang, tahunpmb,sum(targetjunior) as targetjunior,sum(regjunior) as registrasisah,sum(`u-30`) as registrasibooking, aplikanperolehannasional.tglkalkulasi from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang where aplikanperolehannasional.kelompok='1' and tahunpmb='$tahun' and direktorat='2' and tglkalkulasi='$datekal' and (cabang.kepemilikan='1' or cabang.kepemilikan='2') group by aplikanperolehannasional.kodecabang");
    }else{
      return $this->db2->query("SELECT cabang.kodecabang, cabang.namacabang,tahunpmb,sum(targetjunior) as targetjunior,sum(regjunior) as registrasisah,sum(`u-30`) as registrasibooking from aplikanperolehannasional inner join cabang on aplikanperolehannasional.kodecabang=cabang.kodecabang where tahunpmb='$tahun' and kelompok='aktif' and direktorat='3' and tglkalkulasi='$datekal' and (kepemilikan='1' or kepemilikan='2') group by aplikanperolehannasional.kodecabang");
    }
}

public function pmbMhs($kriteria,$keterangan,$tahun,$keterangan,$kocab){
//public function pmbMhs($kriteria,$keterangan,$tahun,$kocab){
  if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("select biodata.nim, aplikan.KdJurusan, biodata.Nama_Mahasiswa, aplikan.KdPresenter from aplikan inner join biodata on aplikan.id=biodata.ID where aplikan.ta='$tahun' and biodata.status='$keterangan' and aplikan.kodecabang='$kocab' order by biodata.Nama_Mahasiswa asc");
    }else{
      return $this->db2->query("select biodata.nim, aplikan.KdJurusan, biodata.Nama_Mahasiswa, aplikan.KdPresenter from aplikan inner join biodata on aplikan.id=biodata.ID where aplikan.ta='$tahun' and biodata.status='$keterangan' and aplikan.kodecabang='$kocab' order by biodata.Nama_Mahasiswa asc");
    }
}

public function pmbMhsJurusan($kriteria,$keterangan,$tahun,$keterangan,$kocab,$kojur){
  if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("select biodata.nim, biodata.kode, biodata.Nama_Mahasiswa, aplikan.KdPresenter from aplikan inner join biodata on aplikan.id=biodata.ID where aplikan.ta='$tahun' and biodata.status='$keterangan' and aplikan.kodecabang='$kocab' and biodata.kode='$kojur' order by biodata.Nama_Mahasiswa asc");
    }else{
      return $this->db2->query("select biodata.nim, biodata.kode, biodata.Nama_Mahasiswa, aplikan.KdPresenter from aplikan inner join biodata on aplikan.id=biodata.ID where aplikan.ta='$tahun' and biodata.status='$keterangan' and aplikan.kodecabang='$kocab' and biodata.kode='$kojur' order by biodata.Nama_Mahasiswa asc");
    }
}

public function mhsWajib($kriteria,$tahun,$table){
  if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	$table.ipk >=3 and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }else{
      return $this->db2->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	$table.ipk >=3 and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }
}

public function mhsDibantu($kriteria,$tahun,$table){
  if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	($table.ipk >=2 and $table.ipk < 3) and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }else{
      return $this->db2->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	($table.ipk >=2 and $table.ipk < 3) and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }
}

public function mhsWajibJurusan($kriteria,$tahun,$table,$kojur){
  if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.kodejurusan='$kojur' and $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	$table.ipk >=3 and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }else{
      return $this->db2->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.kodejurusan='$kojur' and $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	$table.ipk >=3 and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }
}

public function mhsDibantuJurusan($kriteria,$tahun,$table,$kojur){
  if($kriteria == "Coll" || $kriteria == "grup"){
      return $this->db->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.kodejurusan='$kojur' and $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	($table.ipk >=2 and $table.ipk < 3) and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }else{
      return $this->db2->query("SELECT * FROM alumni_aktifitas inner join $table on alumni_aktifitas.nim=$table.nim WHERE $table.kodejurusan='$kojur' and $table.totalsemester >='3' and ($table.StatusMahasiswa='Aktif' or $table.StatusMahasiswa='Lulus') and $table.TahunAngkatan='$tahun' and 	($table.ipk >=2 and $table.ipk < 3) and alumni_aktifitas.diterima='1' and alumni_aktifitas.status='1' and alumni_aktifitas.aktif='0' order by alumni_aktifitas.tglinput desc");
    }
}


} 
?>