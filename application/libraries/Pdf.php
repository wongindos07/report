<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
include_once APPPATH.'/libraries/MPDF57/mpdf.php';
 
class Pdf {
 
    public $param;
    public $pdf;
 
    public function __construct($param = '"utf-8","A4-L","","Arial",10,10,10,10,6,3')
    {
        $this->param = $param;
        $this->pdf = new mPDF($this->param);
    }
}