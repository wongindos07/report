<style>
.fin{
    margin-bottom: 20px;
}
</style>
<div class="col-lg-12 col-xs-12 col-sm-12" style="margin-top: 10px;">
                                <div class="portlet light bordered">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption">
                                            <i class="icon-bubbles font-dark hide"></i>
                                            <!-- <span class="caption-subject font-dark bold uppercase">Dashboard College</span> -->
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#portlet_comments_1" data-toggle="tab"> <b>Rekap PD Banding </b></a>
                                            </li>
                                            <li>
                                                <a href="#portlet_comments_2" data-toggle="tab"> <b>Rekap PD Per-Bulan </b></a>
                                            </li>
                                            <li>
                                                <a href="#portlet_comments_3" data-toggle="tab"> <b>Rekap PD Per-Jurusan </b></a>
                                            </li>
                                           <!--  <li>
                                                <a href="#portlet_comments_4" data-toggle="tab"><b> Berita Acara PD</b></a>
                                            </li> -->

                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="portlet_comments_1">
                                                <!-- Awal dashboard Education -->
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i>Rekapitulasi MHS/Peserta Didik Perbandingan 3 tahun</div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <!-- BEGIN Portlet PORTLET-->
                                                    
                                                       
                                                            <?php
                                                            $this->load->view('form/pendidikan/rekappdd');
                                                            ?>
                                                            
                                                    </div>
                                                </div>

                                                 
                                            </div>
                                            <div class="tab-pane" id="portlet_comments_2">
                                               <!-- Awal dashboard marketing -->
                                
                                               <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i>Rekapitulasi MHS/Peserta Didik Per-Bulan</div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                        
                                                    <?php
                                                            $this->load->view('form/pendidikan/rekLapBul');
                                                    ?>

                                                    </div>
                                                </div>
                                             
                                            </div>
                                            <div class="tab-pane" id="portlet_comments_3">
                                               <!-- Awal dashboard Finance -->
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i>Rekapitulasi MHS/Peserta Didik Per-Jurusan</div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                        
                                                    <?php
                                                        $this->load->view('form/pendidikan/rekapppdjurus');
                                                    ?>

                                                    </div>
                                                </div>
                                               
                                            </div>
                                            <div class="tab-pane" id="portlet_comments_4">
                                               <!-- Awal dashboard IT -->
                                               <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i>Berita Acara Serah Terima MHS/Peserta Didik</div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                                    

                  


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

