<!DOCTYPE html>
<html>
 <!-- TOP STYLESHEET-->
 <?php $this->load->view('themes/thead');?>
<!-- END TOP STYLESHEET-->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
<div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top" style="height: 90px;">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner" style="margin-top: 10px">
                    <!-- BEGIN LOGO -->
                    <!-- LOGO -->
                                <?php $this->load->view('themes/logo');?>
                                <!-- LOGO -->
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu" style="margin-top: -20px">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile">
                                       <font style="font-size: 21px"> <b> </b> </font>
                                   </span>
                                </a>
                                <font style="font-size: 15px;color: white;margin-left: 20px;margin-top: 10px">
                                    <b> </b></font>
                                    <!-- <button style="margin-left: 10px" type="button" class="btn btn-primary">
                                    <i class="icon-logout"></i> <a href="<?=base_url('index.php/Main/keluar')?>" target="_top"><font color="#FFFFFF" style="text-decoration: none;">Log Out</font></a></button> -->
                                    </li>
                                </ul>
                <!-- END HEADER INNER -->
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix" style="margin-top: 50px"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN BEGIN SIDEBAR -->
                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <?php $this->load->view('themes/sidebar');?>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END END SIDEBAR -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <div class="theme-panel hidden-xs hidden-sm">
                        </div>
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="#"><?php echo $title?></a>
                                </li>
                            </ul>
                            
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        <h1 class="page-title"> <?php echo $title?>
                        </h1>
                        <!-- END PAGE TITLE-->
                        <!-- END PAGE HEADER-->
                        
                        <div class="clearfix"></div>
                         <div class="row">
                        <!-- END DASHBOARD STATS 1-->
                        <div class="col-md-12">
                                <!-- BEGIN CONDENSED TABLE PORTLET-->
                                <?php $this->load->view($include);?> 
                                <!-- END CONDENSED TABLE PORTLET-->
                        </div>
                    </div>
                </div>

                 
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <div class="page-footer">
                
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
        </div>
        <!-- FOOTER STYLESHEET-->
        <?php $this->load->view('themes/tfoot');?>
        <!-- END FOOTER STYLESHEET-->
</body>
</html>