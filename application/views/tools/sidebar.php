
<div class="page-sidebar-wrapper" >
                    
                    <div class="page-sidebar navbar-collapse collapse" >
                        <!-- BEGIN SIDEBAR MENU -->
                   
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- <li class="nav-item <?php if($this->uri->segment(1) == 'Dashboard'){echo 'active';} ?>">
                                <a href="<?=base_url('index.php/Dashboard')?>" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li> -->
                            <li class="nav-item <?php if($this->uri->segment(1) == 'Dashboard'){echo 'active';} ?>">
                                <a href="<?=base_url('index.php/Dashboard/index')?>" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1) == 'Main' or $this->uri->segment(1) == 'Marketing' or $this->uri->segment(1) == 'Pendidikan' or $this->uri->segment(1) == 'Keuangan'){echo 'active';} ?>" >
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-graph"></i>
                                    <span class="title">Report & Rekapitulasi</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                <!-- INI UNTUK MARKETING -->
                                <li class="nav-item <?php if($this->uri->segment(1) == 'Marketing'){echo 'active';} ?>" >
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-line-chart"></i>
                                    <span class="title">Marketing</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                 <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Marketing')?>" class="nav-link ">
                                            <span class="title">Rekap PMB</span>
                                        </a>
                                    </li>   
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Pemetaan')?>" class="nav-link ">
                                            <span class="title">Grafik Pemetaan</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                                </li>
                                <!-- INI UNTUK KEUANGAN -->
                                <li class="nav-item <?php if($this->uri->segment(1) == 'Main' or $this->uri->segment(1) == 'Keuangan'){echo 'active';} ?>" >
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-money"></i>
                                    <span class="title">Keuangan</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                 <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Keuangan/registrasiPmb')?>" class="nav-link ">
                                            <span class="title">Rekap Registrasi PMB</span>
                                        </a>
                                    </li>   
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Keuangan/potensi')?>" class="nav-link ">
                                            <span class="title">Potensi Income</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Keuangan')?>" class="nav-link ">
                                            <span class="title">Rekap Piutang PD</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Keuangan/transaksiPembayaran')?>" class="nav-link ">
                                            <span class="title">Rekap Transaksi Pembayaran PD</span>
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Rekap Laporan Keungan</span>
                                        </a>
                                    </li> -->
                                </ul>
                                </li>
                                <!-- INI UNTUK PENDIDIKAN -->
                                <li class="nav-item <?php if($this->uri->segment(1) == 'Main' or $this->uri->segment(1) == 'Pendidikan'){echo 'active';} ?>" >
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-mortar-board"></i>
                                    <span class="title">Pendidikan</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Pendidikan/rekappdd')?>" class="nav-link ">
                                            <span class="title">Rekap Peserta Didik</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Pendidikan/rekapdosen')?>" class="nav-link ">
                                            <span class="title">Rekap Tenaga Pendidik</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Pendidikan/rekapIpk')?>" class="nav-link ">
                                            <span class="title">Rekap IPK</span>
                                        </a>
                                    </li>  
                                    <!-- <li class="nav-item">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Rekap Tenaga Pendidik</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Rekap Nilai</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" class="nav-link ">
                                            <span class="title">Rekap Laporan Bulanan</span>
                                        </a>
                                    </li> -->
                                </ul>
                                </li>
                                <!-- UNTUK CNP -->
                                <!-- INI UNTUK PENDIDIKAN -->
                                <li class="nav-item <?php if($this->uri->segment(1) == 'Main' or $this->uri->segment(1) == 'Cnp'){echo 'active';} ?>" >
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fa-users""></i>
                                    <span class="title">HRD</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/HRD')?>" class="nav-link ">
                                            <span class="title">Rekap Karyawan</span>
                                        </a>
                                    </li>  
                                </ul>
                                </li>
                                 <!-- <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Rekap KPI</span>
                                        </a>
                                    </li>  -->  
                                    <!-- <li class="nav-item">
                                        <a href="#" class="nav-link ">
                                            <i class="icon-bar-chart"></i>
                                            <span class="title">Rekap Ews Cabang</span>
                                        </a>
                                    </li> -->
                                </ul>
                            </li>
                      
   -->
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>