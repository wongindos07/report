<head>
        <meta charset="utf-8" />
        <title>ESR Directorate</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="ESR Directorate" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?php
        $this->load->view('css/style.php');
        ?>
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?=base_url()?>assets/img/esr_directorate_CxP_icon.ico" />
        <!-- <link rel="icon" type="image/png" href="namalogo.png">  -->
</head>