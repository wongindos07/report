
<div class="page-sidebar-wrapper" >
                    
                    <div class="page-sidebar navbar-collapse collapse" >
                        <!-- BEGIN SIDEBAR MENU -->
                   
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <li class="nav-item <?php if($this->uri->segment(1) == 'Dashboard'){echo 'active';} ?>">
                                <a href="<?=base_url('index.php/Dashboard')?>" class="nav-link nav-toggle">
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                </a>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1) == 'Main'){echo 'active';} ?>">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-check"></i>
                                    <span class="title">Input & Konfigurasi</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Main/inputstandar')?>" class="nav-link ">
                                            <span class="title">Input KPI Standar</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Input Cabang</span>
                                        </a>
                                    </li><hr>
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Posting EWS Cabang</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Konfigurasi Data PMB</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Konfigurasi Minimum DP PMB</span>
                                        </a>
                                    </li><hr>
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Daftar KPI Master</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Daftar User Cabang</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item <?php if($this->uri->segment(1) == 'Main'){echo 'active';} ?>" >
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-graph"></i>
                                    <span class="title">Report & Rekapitulasi</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                 <li class="nav-item <?php if($this->uri->segment(1) == 'Main'){echo 'active';} ?>" >
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-graph"></i>
                                    <span class="title">Marketing</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                 <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Rekap PMB</span>
                                        </a>
                                    </li>   
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Main/inputstandar')?>" class="nav-link ">
                                            <span class="title">Rekap Aplikan</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>

                                 <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Main')?>" class="nav-link ">
                                            <span class="title">Rekap KPI</span>
                                        </a>
                                    </li>   
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Main/inputstandar')?>" class="nav-link ">
                                            <span class="title">Rekap Ews Cabang</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="<?=base_url('index.php/Main/inputstandar')?>" class="nav-link ">
                                            <span class="title">Rekap Laporan Bulanan</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-diamond"></i>
                                    <span class="title">Rekapitulasi Aplikan</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="<?=base_url('index.php/Aplikan/form1')?>" class="nav-link ">
                                            <span class="title">Aplikan per-Program</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="ui_metronic_grid.html" class="nav-link ">
                                            <span class="title">Batal Kuliah</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="ui_metronic_grid.html" class="nav-link ">
                                            <span class="title">Rasio Presenter</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="ui_metronic_grid.html" class="nav-link ">
                                            <span class="title">Rasio Media</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="ui_metronic_grid.html" class="nav-link ">
                                            <span class="title">Registrasi Kelas</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="ui_metronic_grid.html" class="nav-link ">
                                            <span class="title">Potensi Income</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="ui_metronic_grid.html" class="nav-link ">
                                            <span class="title">Penutupan Registrasi</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="ui_metronic_grid.html" class="nav-link ">
                                            <span class="title">Aplikan Online</span>
                                        </a>
                                    </li>
                                </ul>
                            </li> -->
<!-- 
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-puzzle"></i>
                                    <span class="title">Rekap PMB Nasional</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="components_date_time_pickers.html" class="nav-link ">
                                            <span class="title">History PMB Cabang</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="components_date_time_pickers.html" class="nav-link ">
                                            <span class="title">Rekapitulasi PMB Nasional</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="components_date_time_pickers.html" class="nav-link ">
                                            <span class="title">Rekapitulasi Registrasi PMB</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="components_date_time_pickers.html" class="nav-link ">
                                            <span class="title">Rekapitulasi PMB Pertanggal</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="components_date_time_pickers.html" class="nav-link ">
                                            <span class="title">Rekapitulasi PMB Banding</span>
                                        </a>
                                    </li>
                                </ul>
                            </li> -->
                       <!--      <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-settings"></i>
                                    <span class="title">Target Perolehan</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-bulb"></i>
                                    <span class="title">Statistik Data Aplikan</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-briefcase"></i>
                                    <span class="title">Profile Head Of Marketing</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="?p=" class="nav-link nav-toggle">
                                    <i class="icon-wallet"></i>
                                    <span class="title">Rekapitulasi Harga Jual Pusat</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Rekapitulasi Realisasi Kerja</span>
                                </a>
                            </li>
                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-pointer"></i>
                                    <span class="title">Rekapitulasi Peserta BBH 1M</span>
                                </a>
                            </li>
   -->
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>