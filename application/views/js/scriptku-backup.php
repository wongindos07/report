<!-- action dashboard detail -->
<script type="text/javascript">
    $(document).ready(function(){
        $('.load').hide();

        // DETAIL PIUTANG POLTEK
        $("#poltingkat1").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk1").html(data);
                            $(".tk2").html("");
                            $(".tk3").html("");
                        }
                    });

        });

        $("#poltingkat2").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk2").html(data);
                            $(".tk1").html("");
                            $(".tk3").html("");
                        }
                    });

        });

        $("#poltingkat3").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk3").html(data);
                            $(".tk2").html("");
                            $(".tk1").html("");
                        }
                    });

        });

        $("#coltingkat1").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk1col").html(data);
                            $(".tk2col").html("");
                            $(".tk3").html("");
                            $(".tk2").html("");
                        }
                    });

        });

        $("#coltingkat2").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk2col").html(data);
                            $(".tk1col").html("");
                            $(".tk1").html("");
                            $(".tk3").html("");
                        }
                    });

        });

        $("#colpiutangnot").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutangNot')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk2col").html("");
                            $(".tk1col").html("");
                            $(".tk1").html("");
                            $(".tk3").html("");
                            $(".notpol").html("");
                            $(".not").html(data);
                        }
                    });

        });

        $("#polpiutangnot").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutangNot')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".not").html("");
                            $(".tk2col").html("");
                            $(".tk1col").html("");
                            $(".tk1").html("");
                            $(".tk3").html("");
                            $(".notpol").html(data);
                        }
                    });

        });


        $("#pmbu30").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let ket = $(this).attr('ket');
            $(".load").show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Marketing/detailCabU30')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            ket:ket,
                            dashboard:'dashboard'
                        },
                        success : function(data){
                            $('.load').hide();
                            $('.pmbu30col').html(data);
                        }
                    });
        });

       

    });   
</script>

<script type="text/javascript">
            $(document).ready(function(){

                $("#tipe").change(function(){
                    $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getTahun')?>",
                        data : {
                            tipe:$("#tipe").val(),
                            kriteria:$("#kriteria").val()
                        },
                        success : function(data){
                            $("#tahun").html(data);
                        }
                    })
                });
                //ini untuk REKAP pmb PERIODE KALKULASI------------------

                $("#kriteriapmb").change(function(){
                    $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Marketing/getTahunPMB')?>",
                        data : {
                            kriteria:$("#kriteriapmb").val()
                        },
                        success : function(data){
                            $("#TApmb").html(data);
                             //alert(data);
                        }
                    })
                });

                $("#TApmb").change(function(){
                    //alert($("#TApmb").val());
                    $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Marketing/getPeriodeRekapPMB')?>",
                        data : {
                            kriteria:$("#TApmb").val()
                        },
                        success : function(data){
                            $("#periodekalkulasi").html(data);
                            // alert(data);
                        }
                    })
                });


                $("#kalkulasi").click(function(){
                    let kriteria = $("#kriteriapmb").val();
                    let tahun = $("#inisialpmb").val();
                    
                    // if(!kriteria)
                    // {
                    //     alert('Pilih kriteria kalkulasi');
                    //     exit;
                    // }else
                    // {
                        $('.loading').show();
                        $.ajax({
                            type : "POST",
                            url  : "<?=base_url('index.php/Marketing/simpanperolehannas')?>",
                            data : {
                                tahun:tahun,
                                kriteria:kriteria,
                            },
                            success : function(data){
                                $('.loading').hide();
                                // $("#isi").html(data);
                                alert("Kalkulasi Berhasil");
                                exit;
                            }
                        });
                    // }    
                });

                $("#tampilrekapPMB").click(function(){
                    let kriteria = $("#kriteriapmb").val();
                    let tahun = $("#TApmb").val();
                    let tglkalkulasi = $("#periodekalkulasi").val();
                    let milik = $("#skala").val();
                    $('.loading').show();
                    $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Marketing/getrekapPMB')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tglkalkulasi:tglkalkulasi,
                            milik:milik
                        },
                        success : function(data){
                            $('.loading').hide();
                            $("#isi").html(data);
                            //alert(milik);
                        }
                    });
                });

                //START untuk REKAP pendidikan ------------------

                $("#Tampilkan").click(function(){
                    let kriteria = $("#kriteria").val();
                    $('.loading').show();
                    // alert(kriteria);
                    $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Pendidikan/getrekapdosen')?>",
                        data : {
                            kriteria:kriteria
                        },
                        success : function(data){
                            $('.loading').hide();
                            $("#isi").html(data);
                            //alert(data);
                        }
                    });
                });

                

                //END  untuk REKAP pendidikan ------------------


                $("#kriteria").change(function(){
                    let kriteria = $("#kriteria").val();
                    $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getTipePiutang')?>",
                        data : {
                            kriteria:kriteria
                        },
                        success : function(data){
                            $("#tipe").html(data);
                        }
                    });
                });

                
                //END untuk REKAP pmb PERIODE KALKULASI------------------

                $("#caripiutang").click(function(){
                    let kriteria = $("#kriteria").val();
                    let tahun = $("#tahun").val();
                    let tipe = $("#tipe").val();
                    $('.loading').show();
                    $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe
                        },
                        success : function(data){
                            $('.loading').hide();
                            $("#isi").html(data);
                        }
                    });
                });

                $('.datepicker').datepicker({
                    format: 'yyyy/mm/dd',
                });

                $("#cab").change(function(){
                    let cab = $("#cab").val();
                    $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Pendidikan/getTak')?>",
                        data: {'cab': cab},
                        success: function(msg){
                            $("#tak").html(msg);
                        }
                    });
                  });

                $("#proseslaporan").click(function(){
                    let cab = $("#cab").val();
                    let tak = $("#tak").val();
                    let periode = $("#periode").val();
                    $('.loading').show();
                    $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Pendidikan/FormJmlMhs')?>",
                        data: {
                            cab: cab,
                            tak: tak,
                            periode: periode
                        },
                        success: function(msg){
                            $("#isi").html(msg);
                            $('.loading').hide();
                        }
                    });
                });

                $("#prosesdata").click(function(){
                    let kriteria = $("#kriterias").val();
                    let tahun = $("#tahun").val();
                    $('.loading').show();
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url('index.php/Pendidikan/rekappddjur')?>",
                        data: {
                            kriteria: kriteria,
                            tahun: tahun
                        },
                        success: function(msg){
                            $('.loading').hide();
                            $(".isi").html(msg);
                        }
                    })
                });


                $("#lmsstudent").click(function(){
                    let periode = $("#periode").val();
                    let tahunaka = $("#tahunaka").val();
                    $.ajax({
                        type: "POST",
                        url: "<?=base_url('index.php/Pendidikan/lmsstudent')?>",
                        data: {
                            periode: periode,
                            tahunaka: tahunaka
                        },
                        success: function(data){
                            $('.isilmsstudent').html(data);
                        }
                    });
                });

                $("#tipepmb").change(function(){
                    let tipe = $("#tipepmb").val();
                    $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/getTakPmb')?>",
                        data:{
                            tipe:tipe
                        },
                        success: function(data){
                            $('#tahunpmb').html(data);
                        }
                    });
                });

                $("#prosesdatapmb").hide();

                $("#tahunpmb").change(function(){
                    $("#prosesdatapmb").show();
                });

                $("#prosesdatapmb").click(function(){
                    let tahun = $("#tahunpmb").val();
                    let tipe  = $("#tipepmb").val();
                    $('.loading').show();
                    $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/regCabangPmb')?>",
                        data:{
                            tahun:tahun,
                            tipe:tipe
                        },
                        success: function(data){
                            $('.loading').hide();
                            $('.isipmb').html(data);
                        }
                    }); 
                });

                $("#prosestransaksi").click(function(){
                    let jenis = $("#transaksi").val();
                    let from = $("#from").val();
                    let to = $("#to").val();
                    $('.loading').show();
                    if(jenis == 'registrasi'){
                        $.ajax({
                            type : "POST",
                            url: "<?=base_url('index.php/keuangan/transaksiRegistrasi')?>",
                            data:{
                                from:from,
                                to:to
                            },
                            success: function(data){
                                $('.loading').hide();
                                $('.isitransaksi').html(data);
                            }
                        });
                    }else if(jenis == 'daftar'){
                        $.ajax({
                            type : "POST",
                            url: "<?=base_url('index.php/keuangan/transaksiDaftar')?>",
                            data:{
                                from:from,
                                to:to
                            },
                            success: function(data){
                                $('.loading').hide();
                                $('.isitransaksi').html(data);
                            }
                        });
                    }else{
                        $.ajax({
                            type : "POST",
                            url: "<?=base_url('index.php/keuangan/transaksiRealisasi')?>",
                            data:{
                                from:from,
                                to:to
                            },
                            success: function(data){
                                $('.loading').hide();
                                $('.isitransaksi').html(data);
                            }
                        });
                    }

                    
                });

                $('.notereal').hide();
                $("#transaksi").change(function(){
                    let jenis = $("#transaksi").val();
                    if(jenis == 'realisasi'){
                        $('.notereal').show();
                    }else{
                        $('.notereal').hide();
                    }
                });

                $("#kriteriaipk").change(function(){
                    // cari tahun
                    $.ajax({
                            type : "POST",
                            url: "<?=base_url('index.php/Pendidikan/dataipk')?>",
                            data:{
                                kriteria:$("#kriteriaipk").val(),
                                x:1
                            },
                            success: function(data){
                                $("#tahun").html(data);
                            }
                        });
                    // cari tipe
                    $.ajax({
                            type : "POST",
                            url: "<?=base_url('index.php/Pendidikan/dataipk')?>",
                            data:{
                                kriteria:$("#kriteriaipk").val(),
                                x:2
                            },
                            success: function(data){
                                $("#tipeipk").html(data);
                            }
                        });
                });

                $("#tipeipk").change(function(){
                    let data = $("#tipeipk").val();
                    $.ajax({
                            type : "POST",
                            url: "<?=base_url('index.php/Pendidikan/dataipk')?>",
                            data:{
                                kriteria:$("#kriteriaipk").val(),
                                x:3,
                                tak:$("#tahun").val(),
                                tipe:$("#tipeipk").val()
                            },
                            success: function(data){
                                $("#perkalkulasi").html(data);
                            }
                        });
                });

                $("#berhasilkalkulasi").hide();
                $("#kalkulasiipk").click(function(){
                    $('.loading').show();
                    $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Pendidikan/kalkulasiIpk')?>",
                        success: function(data){
                            $('.loading').hide();
                            $("#berhasilkalkulasi").show();
                        }
                    });
                });

                $('#carikalkulasi').click(function(){
                    $('.loading').show();
                    $.ajax({
                            type : "POST",
                            url: "<?=base_url('index.php/Pendidikan/hasildataipk')?>",
                            data:{
                               kriteria:$("#kriteriaipk").val(),
                               tak:$("#tahun").val(),
                               tingkat:$("#tipeipk").val(),
                               tglkalkulasi:$("#perkalkulasi").val()
                            },
                            success: function(data){
                                $('.loading').hide();
                                $("#isiipk").html(data);
                            }
                        });
                });

                

            });
</script>


<script type="text/javascript">
    let waktu = 1800;

    const hitungMundur = setInterval(function() {
    waktu--;
    document.getElementById("text").innerHTML = "Waktu Sesi: "+waktu+" Detik";
    
    // setTimeout("location.href='http://www.dte.web.id/2012/01/membuat-animasi-loading-dengan-jquery.html'", 15000);

    if(waktu==0){
    clearInterval(hitungMundur);
    document.getElementById("text").innerHTML = "<b style='color:red;'>Sesi Anda Habis !</b>";
    // jika waktu habis maka akan pindah halaman
    window.location.href="<?php echo base_url('index.php/Login/timeOut')?>"
    }
}, 1000);
    </script>



<script type="text/javascript">
    // CHART JS

    // chart pmb college begin
    var pmbcol = document.getElementById('chartpmbcol').getContext('2d');
    let tahunpmb = document.getElementById('tahunpmb').value;

    let rasiotarget = document.getElementById('rasiotarget').value;
    let rasiorealisasi = document.getElementById('rasiorealisasi').value;

    var chart = new Chart(pmbcol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Registrasi', 'Target Belum Tercapai'],
            datasets: [{
                label: ['Perolehan PMB College '+tahunpmb],
                backgroundColor: ['#2BB33F', '#D04556'],
                data: [rasiorealisasi, rasiotarget],
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text:['Perolehan PMB College '+tahunpmb],
                fontSize:20,
                position:'top'
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end pmb college


    // chart pmb poltek begin
    var pmbpol = document.getElementById('chartpmbpol').getContext('2d');
    let tahunpmbpol = document.getElementById('tahunpmbpol').value;

    let rasiotargetpol = document.getElementById('rasiotargetpol').value;
    let rasiorealisasipol = document.getElementById('rasiorealisasipol').value;

    var chart = new Chart(pmbpol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Registrasi', 'Target Belum Tercapai'],
            datasets: [{
                label: ['Perolehan PMB Politeknik '+tahunpmbpol],
                backgroundColor: ['#2BB33F', '#D04556'],
                data: [rasiorealisasipol, rasiotargetpol],
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text:['Perolehan PMB Politeknik '+tahunpmbpol],
                fontSize:20,
                position:'top'
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end pmb poltek

    // begin piutang tk 1 dan 2 college
    var piucol      = document.getElementById('chartpiucol').getContext('2d');
    let perpiucol = document.getElementById('periodecol').value;
    let warnatk1 = document.getElementById('warnatk1').value;
    let warnatk2 = document.getElementById('warnatk2').value;
    let piutk1 = document.getElementById('piutk1').value;
    let piutk2 = document.getElementById('piutk2').value;


    var chart = new Chart(piucol, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ['Piutang Tingkat 1', 'Piutang Tingkat 2'],
            datasets: [{
                label: ['Piutang peserta didik college '+perpiucol],
                backgroundColor: [warnatk1, warnatk2],
                data: [piutk1, piutk2]
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text:['Piutang peserta didik college '+perpiucol],
                fontSize:20
            },
            legend:{
                position:'right',
                display:false,
            },
            scales: {
                xAxes: [{
                    // barPercentage: 0.5,
                    // barThickness: 350,
                    minBarLength: 2,
                    gridLines: {
                    offsetGridLines: true
                    }
                }],
                yAxes: [{
                  ticks: {
                    min: 0
                    // max: 100,
                    // stepSize: 10
                  },
                  scaleLabel: {
                    display: true,
                    // labelString: "Satuan dalam rupian ( Rp )",
                    fontColor: "green"
                  }
                }],
            },
            plugins: {
                labels: {
                    render: 'value',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end piutang tk 1 dan 2 college

    // begin piutang tk 1 dan 2 poltek
    var piupol      = document.getElementById('chartpiupol').getContext('2d');
    let perpiupol   = document.getElementById('periodepol').value;
    let warnatk1pol = document.getElementById('warnatk1pol').value;
    let warnatk2pol = document.getElementById('warnatk2pol').value;
    let warnatk3pol = document.getElementById('warnatk3pol').value;
    let piutk1pol   = document.getElementById('piutk1pol').value;
    let piutk2pol   = document.getElementById('piutk2pol').value;
    let piutk3pol   = document.getElementById('piutk3pol').value;

    var chart = new Chart(piupol, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ['Piutang Tingkat 1', 'Piutang Tingkat 2', 'Piutang Tingkat 3'],
            datasets: [{
                label: ['Piutang Mahasiswa politeknik '+perpiupol],
                backgroundColor: [warnatk1pol, warnatk2pol, warnatk3pol],
                data: [piutk1pol, piutk2pol, piutk3pol]
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text: ['Piutang Mahasiswa politeknik '+perpiupol],
                fontSize:20
            },
            legend:{
                position:'right',
                display:false
            },
            scales: {
                xAxes: [{
                    // barPercentage: 0.5,
                    minBarLength: 2,
                    gridLines: {
                    offsetGridLines: true
                    }
                }],
                yAxes: [{
                  ticks: {
                    min: 0
                    // max: 100,
                    // stepSize: 10
                  },
                  scaleLabel: {
                    display: true,
                    // labelString: "Satuan dalam rupian ( Rp )",
                    fontColor: "green"
                  }
                }],
            },
            plugins: {
                labels: {
                    render: 'value',
                    fontColor: ['black', 'black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end piutang tk 1 dan 2 poltek

    // begin penggunaan aplikasi
    var aplikasi      = document.getElementById('aplikasi').getContext('2d');
    let lmsmhs = document.getElementById('lmsmhs').value;
    let lmsweb = document.getElementById('lmsweb').value;
    let rasiodoselec = document.getElementById('rasiodoselec').value;
    let rasiodoslms = document.getElementById('rasiodoslms').value;
    let periodelms = document.getElementById('periode').value;
    let tahunaka = document.getElementById('tahunaka').value;

    let wrn = document.getElementById('wrn').value;
    let wrn1 = document.getElementById('wrn1').value;
    let wrn2 = document.getElementById('wrn2').value;
    let wrn3 = document.getElementById('wrn3').value;

    var chart = new Chart(aplikasi, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ['E-Student', 'LMS Student','E-Lecturer','LMS Lecturer'],
            datasets: [{
                label: ['Implementasi Sistem College '+tahunaka],
                backgroundColor: [wrn, wrn1, wrn2, wrn3,],
                data: [lmsweb, lmsmhs, rasiodoselec, rasiodoslms,'100']
                
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Implementasi Sistem College '+tahunaka],
                fontSize:20
            },
            legend:{
                position:'right',
                display:false
            },
            scales: {
                xAxes: [{
                    // barPercentage: 0.5,
                    // barThickness: 350,
                    // maxBarThickness: 100,
                    minBarLength: 2,
                    gridLines: {
                    offsetGridLines: true
                    }
                }],
                yAxes: [{
                  ticks: {
                    min: 0,
                    max: 100,
                    // stepSize: 10
                  },
                  scaleLabel: {
                    display: true,
                    // labelString: "Satuan dalam Persen ( % )",
                    fontColor: "green"
                  }
                }],
            },
            plugins: {
                labels: {
                    render: 'value',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });
    // end penggunaan aplikasi


    // begin cnp penempatan
    var penempatan = document.getElementById('penempatan').getContext('2d');
    let wajib = document.getElementById('wajib').value;
    let dibantu = document.getElementById('dibantu').value;
    let tahuncnp = document.getElementById('tahuncnp').value;

    let target1 = document.getElementById('target1').value;
    let kerja1 = document.getElementById('kerja1').value;
    let target2 = document.getElementById('target2').value;
    let kerja2 = document.getElementById('kerja2').value;
    let batasmax = document.getElementById('batasmax').value;

    var chart = new Chart(penempatan, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Wajib Kerja', 'Realisasi Wajib Kerja'],
            datasets: [{
                label: ['Rasio Penempatan Wajib Kerja TA '+tahuncnp],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target1, kerja1]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Wajib Kerja TA '+tahuncnp],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });

    var chart = new Chart(penempatann, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Dibantu Kerja', 'Realisasi Dibantu Kerja'],
            datasets: [{
                label: ['Rasio Penempatan dibantu kerja TA '+tahuncnp],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target2, kerja2]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Dibantu Kerja TA '+tahuncnp],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });
   


    var penempatanpol = document.getElementById('penempatanpol').getContext('2d');
    let wajibpol = document.getElementById('wajibpol').value;
    let dibantupol = document.getElementById('dibantupol').value;
    let tahuncnppol = document.getElementById('tahuncnppol').value;

    let target1pol = document.getElementById('target1pol').value;
    let kerja1pol = document.getElementById('kerja1pol').value;
    let target2pol = document.getElementById('target2pol').value;
    let kerja2pol = document.getElementById('kerja2pol').value;

    var chart = new Chart(penempatanpol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Dibantu Kerja', 'Realisasi Dibantu Kerja'],
            datasets: [{
                label: ['Rasio Penempatan Wajib kerja TA '+tahuncnppol],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target1pol, kerja1pol]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Wajib Kerja TA '+tahuncnppol],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });

    var chart = new Chart(penempatannpol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Dibantu Kerja', 'Realisasi Dibantu Kerja'],
            datasets: [{
                label: ['Rasio Penempatan dibantu kerja TA '+tahuncnppol],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target2pol, kerja2pol]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Dibantu Kerja TA '+tahuncnppol],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });
    // end cnp penempatan


</script>