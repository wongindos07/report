
<script type="text/javascript">

	function updatechartpmb(){
		// chart pmb college begin
    var pmbcol = document.getElementById('chartpmbcol').getContext('2d');
    let tahunpmb = document.getElementById('tahunpmb').value;

    let rasiotarget = document.getElementById('rasiotarget').value;
    let rasiorealisasi = document.getElementById('rasiorealisasi').value;

    var chart = new Chart(pmbcol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Registrasi', 'Target Belum Tercapai'],
            datasets: [{
                label: ['Perolehan PMB College '+tahunpmb],
                backgroundColor: ['#2BB33F', '#D04556'],
                data: [rasiorealisasi, rasiotarget],
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text:['Perolehan PMB College '+tahunpmb],
                fontSize:20,
                position:'top'
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end pmb college


    // chart pmb poltek begin
    var pmbpol = document.getElementById('chartpmbpol').getContext('2d');
    let tahunpmbpol = document.getElementById('tahunpmbpol').value;

    let rasiotargetpol = document.getElementById('rasiotargetpol').value;
    let rasiorealisasipol = document.getElementById('rasiorealisasipol').value;

    var chart = new Chart(pmbpol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Registrasi', 'Target Belum Tercapai'],
            datasets: [{
                label: ['Perolehan PMB Politeknik '+tahunpmbpol],
                backgroundColor: ['#2BB33F', '#D04556'],
                data: [rasiorealisasipol, rasiotargetpol],
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text:['Perolehan PMB Politeknik '+tahunpmbpol],
                fontSize:20,
                position:'top'
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end pmb poltek
	}

	function updatechartcnp(){
		 // begin cnp penempatan
    var penempatan = document.getElementById('penempatan').getContext('2d');
    let wajib = document.getElementById('wajib').value;
    let dibantu = document.getElementById('dibantu').value;
    let tahuncnp = document.getElementById('tahuncnp').value;

    let target1 = document.getElementById('target1').value;
    let kerja1 = document.getElementById('kerja1').value;
    let target2 = document.getElementById('target2').value;
    let kerja2 = document.getElementById('kerja2').value;
    let batasmax = document.getElementById('batasmax').value;

    var chart = new Chart(penempatan, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Wajib Kerja', 'Realisasi Wajib Kerja'],
            datasets: [{
                label: ['Rasio Penempatan Wajib Kerja TA '+tahuncnp],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target1, kerja1]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Wajib Kerja TA '+tahuncnp],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });

    var chart = new Chart(penempatann, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Dibantu Kerja', 'Realisasi Dibantu Kerja'],
            datasets: [{
                label: ['Rasio Penempatan dibantu kerja TA '+tahuncnp],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target2, kerja2]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Dibantu Kerja TA '+tahuncnp],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });
   


    var penempatanpol = document.getElementById('penempatanpol').getContext('2d');
    let wajibpol = document.getElementById('wajibpol').value;
    let dibantupol = document.getElementById('dibantupol').value;
    let tahuncnppol = document.getElementById('tahuncnppol').value;

    let target1pol = document.getElementById('target1pol').value;
    let kerja1pol = document.getElementById('kerja1pol').value;
    let target2pol = document.getElementById('target2pol').value;
    let kerja2pol = document.getElementById('kerja2pol').value;

    var chart = new Chart(penempatanpol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Dibantu Kerja', 'Realisasi Dibantu Kerja'],
            datasets: [{
                label: ['Rasio Penempatan Wajib kerja TA '+tahuncnppol],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target1pol, kerja1pol]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Wajib Kerja TA '+tahuncnppol],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });

    var chart = new Chart(penempatannpol, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Target Dibantu Kerja', 'Realisasi Dibantu Kerja'],
            datasets: [{
                label: ['Rasio Penempatan dibantu kerja TA '+tahuncnppol],
                backgroundColor: ['#D04556','#2BB33F'],
                data: [target2pol, kerja2pol]
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Rasio Penempatan Dibantu Kerja TA '+tahuncnppol],
                fontSize:15
            },
            legend:{
                position:'right',
                display:true
            },
            plugins: {
                labels: {
                    render: 'percentage',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });
    // end cnp penempatan
	}

    function updatechartit(){
        // begin penggunaan aplikasi
    var aplikasi      = document.getElementById('aplikasi').getContext('2d');
    let lmsmhs = document.getElementById('lmsmhs').value;
    let lmsweb = document.getElementById('lmsweb').value;
    let rasiodoselec = document.getElementById('rasiodoselec').value;
    let rasiodoslms = document.getElementById('rasiodoslms').value;
    let periodelms = document.getElementById('periode').value;
    let tahunaka = document.getElementById('tahunaka').value;

    let wrn = document.getElementById('wrn').value;
    let wrn1 = document.getElementById('wrn1').value;
    let wrn2 = document.getElementById('wrn2').value;
    let wrn3 = document.getElementById('wrn3').value;

    var chart = new Chart(aplikasi, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ['E-Student', 'LMS Student','E-Lecturer','LMS Lecturer'],
            datasets: [{
                label: ['Implementasi Sistem College '+tahunaka],
                backgroundColor: [wrn, wrn1, wrn2, wrn3,],
                data: [lmsweb, lmsmhs, rasiodoselec, rasiodoslms,'100']
                
            }],
            
        },

        options: {
            title:{
                display:true,
                text: ['Implementasi Sistem College '+tahunaka],
                fontSize:20
            },
            legend:{
                position:'right',
                display:false
            },
            scales: {
                xAxes: [{
                    // barPercentage: 0.5,
                    // barThickness: 350,
                    // maxBarThickness: 100,
                    minBarLength: 2,
                    gridLines: {
                    offsetGridLines: true
                    }
                }],
                yAxes: [{
                  ticks: {
                    min: 0,
                    max: 100,
                    // stepSize: 10
                  },
                  scaleLabel: {
                    display: true,
                    // labelString: "Satuan dalam Persen ( % )",
                    fontColor: "green"
                  }
                }],
            },
            plugins: {
                labels: {
                    render: 'value',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
        
    });
    // end penggunaan aplikasi
    }


    function updatechartkeu(){
         // begin piutang tk 1 dan 2 college
    var piucol      = document.getElementById('chartpiucol').getContext('2d');
    let perpiucol = document.getElementById('periodecol').value;
    let warnatk1 = document.getElementById('warnatk1').value;
    let warnatk2 = document.getElementById('warnatk2').value;
    let piutk1 = document.getElementById('piutk1').value;
    let piutk2 = document.getElementById('piutk2').value;


    var chart = new Chart(piucol, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ['Piutang Tingkat 1', 'Piutang Tingkat 2'],
            datasets: [{
                label: ['Piutang peserta didik college '+perpiucol],
                backgroundColor: [warnatk1, warnatk2],
                data: [piutk1, piutk2]
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text:['Piutang peserta didik college '+perpiucol],
                fontSize:16
            },
            legend:{
                position:'right',
                display:false,
            },
            scales: {
                xAxes: [{
                    // barPercentage: 0.5,
                    // barThickness: 350,
                    minBarLength: 2,
                    gridLines: {
                    offsetGridLines: true
                    }
                }],
                yAxes: [{
                  ticks: {
                    min: 0
                    // max: 100,
                    // stepSize: 10
                  },
                  scaleLabel: {
                    display: true,
                    // labelString: "Satuan dalam rupian ( Rp )",
                    fontColor: "green"
                  }
                }],
            },
            plugins: {
                labels: {
                    render: 'value',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end piutang tk 1 dan 2 college

    // begin piutang tk 1 dan 2 poltek
    var piupol      = document.getElementById('chartpiupol').getContext('2d');
    let perpiupol   = document.getElementById('periodepol').value;
    let warnatk1pol = document.getElementById('warnatk1pol').value;
    let warnatk2pol = document.getElementById('warnatk2pol').value;
    let warnatk3pol = document.getElementById('warnatk3pol').value;
    let piutk1pol   = document.getElementById('piutk1pol').value;
    let piutk2pol   = document.getElementById('piutk2pol').value;
    let piutk3pol   = document.getElementById('piutk3pol').value;

    var chart = new Chart(piupol, {
        // The type of chart we want to create
        type: 'bar',

        // The data for our dataset
        data: {
            labels: ['Piutang Tingkat 1', 'Piutang Tingkat 2', 'Piutang Tingkat 3'],
            datasets: [{
                label: ['Piutang Mahasiswa politeknik '+perpiupol],
                backgroundColor: [warnatk1pol, warnatk2pol, warnatk3pol],
                data: [piutk1pol, piutk2pol, piutk3pol]
            }]
        },

        // Configuration options go here
        options: {
            title:{
                display:true,
                text: ['Piutang Mahasiswa politeknik '+perpiupol],
                fontSize:16
            },
            legend:{
                position:'right',
                display:false
            },
            scales: {
                xAxes: [{
                    // barPercentage: 0.5,
                    minBarLength: 2,
                    gridLines: {
                    offsetGridLines: true
                    }
                }],
                yAxes: [{
                  ticks: {
                    min: 0
                    // max: 100,
                    // stepSize: 10
                  },
                  scaleLabel: {
                    display: true,
                    // labelString: "Satuan dalam rupian ( Rp )",
                    fontColor: "green"
                  }
                }],
            },
            plugins: {
                labels: {
                    render: 'value',
                    fontColor: ['black', 'black'],
                    precision: 2,
                    fontSize: 12
                  }
            }
        }
    });
    // end piutang tk 1 dan 2 college
    }


	$(document).ready(function(){
		$(".pmbmilker").click(function(){
			$('.loading').show();
			$.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/pmbmilker')?>",
                        success : function(data){
                           	$(".pmbnasional").removeClass("red");
                           	$(".pmbnasional").addClass("default");

                           	$(".pmbmilker").addClass("red");
                           	$('.loading').hide();
                           	$(".datapmb").empty();
                           	$(".datapmb").html(data);
                           	updatechartpmb();
                        }
                    });
		});

		$(".pmbnasional").click(function(){
			$('.loading').show();
			$.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/pmbnasional')?>",
                        success : function(data){
                           	$(".pmbmilker").removeClass("red");
                           	$(".pmbmilker").addClass("default");

                           	$(".pmbnasional").addClass("red");
                           	$('.loading').hide();
                           	$(".datapmb").empty();
                           	$(".datapmb").html(data);
                           	updatechartpmb();
                        }
                    });
		});
        // END PMB

		// BEGIN  CNP
		$(".cnpmilker").click(function(){
			$('.loading').show();
			$.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/cnpmilker')?>",
                        success : function(data){
                           	$(".cnpnasional").removeClass("red");
                           	$(".cnpnasional").addClass("default");

                           	$(".cnpmilker").addClass("red");
                           	$('.loading').hide();
                           	$(".datacnp").empty();
                           	$(".datacnp").html(data);
                           	updatechartcnp();
                        }
                    });
		});

		$(".cnpnasional").click(function(){
			$('.loading').show();
			$.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/cnpnasional')?>",
                        success : function(data){
                           	$(".cnpmilker").removeClass("red");
                           	$(".cnpmilker").addClass("default");

                           	$(".cnpnasional").addClass("red");
                           	$('.loading').hide();
                           	$(".datacnp").empty();
                           	$(".datacnp").html(data);
                           	updatechartcnp();
                        }
                    });
		});
        // END CNP

        // BEGIN IT
        $(".itmilker").click(function(){
            $('.loading').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/itmilker')?>",
                        success : function(data){
                            $(".itnasional").removeClass("red");
                            $(".itnasional").addClass("default");

                            $(".itmilker").addClass("red");
                            $('.loading').hide();
                            $(".datait").empty();
                            $(".datait").html(data);
                            updatechartit();
                        }
                    });
        });

        $(".itnasional").click(function(){
            $('.loading').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/itnasional')?>",
                        success : function(data){
                            $(".itmilker").removeClass("red");
                            $(".itmilker").addClass("default");

                            $(".itnasional").addClass("red");
                            $('.loading').hide();
                            $(".datait").empty();
                            $(".datait").html(data);
                            updatechartit();
                        }
                    });
        });
        // END IT

        // BEGIN KEUANGAN
        $(".keuanganmilker").click(function(){
            $('.loading').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/keuanganmilker')?>",
                        success : function(data){
                            $(".keuangannasional").removeClass("red");
                            $(".keuangannasional").addClass("default");

                            $(".keuanganmilker").addClass("red");
                            $('.loading').hide();
                            $(".datakeuangan").empty();
                            $(".datakeuangan").html(data);
                            updatechartkeu();
                        }
                    });
        });

        $(".keuangannasional").click(function(){
            $('.loading').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Dashboard/keuangannasional')?>",
                        success : function(data){
                            $(".keuanganmilker").removeClass("red");
                            $(".keuanganmilker").addClass("default");

                            $(".keuangannasional").addClass("red");
                            $('.loading').hide();
                            $(".datakeuangan").empty();
                            $(".datakeuangan").html(data);
                            updatechartkeu();
                        }
                    });
        });
        // END KEUANGAN
	});
</script>


<script type="text/javascript">

</script>