<!DOCTYPE html>
<?php
$this->load->view('tools/head')
?>
<style type="text/css">
.lp3i{
    margin-top: 5px !important;
    margin: 5px;
    margin-right: -20px;
    width: 105px;
    height: 80px;
}
.esr{
    margin-top: 5px;
    width: 180px;
    height: 80px;
    margin-right: -40px;
}
.logo{
    margin-bottom: -25px !important;
}
</style>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <?php $this->load->view('css/csslogin')?>
 
    <!-- END HEAD -->

     <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <span class="form-title"><img src="<?=base_url()?>assets/img/LP3I.png" alt="logo" class="logo-default lp3i" /></span>
            <span class="form-title"><img src="<?=base_url()?>assets/img/esr-directorate.png" alt="logo" class="logo-default esr" /></span>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="<?=base_url('index.php/Login/ceklogin')?>" method="post">
                <h3 class="form-title font-red">Sign In</h3>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <input  class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input  class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn red uppercase btn-block">Login</button>
  
                </div>
                
              
            </form>
            <!-- END LOGIN FORM -->
        </div>
        <div class="copyright"> 2018 © ICT LP3I</div>
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        
    </body>

</html>