<div class="portlet box blue-hoki">
     <div class="portlet-title">
         <div class="caption">
             <i class="fa fa-gift"></i>Rekapitulasi Perolehan PMB 
             <input type="hidden" value="<?=$inisialPMB?>" name="inisialpmb" id="inisialpmb" >                         
             </div>
     </div>
     <div class="portlet-body" style='padding-top:30px;'>
        <div class="row">
            <div class="col-md-6">
                <form role="form">
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                      <div class="form-group">
                                                            <label class="col-md-4 control-label">Kriteria</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="kriteriapmb">
                                                                <?php
                                                            $krit=array('-- Pilih Kriteria --','Perolehan Jumlah Junior','Perolehan Target PMB');
                                                            $alias=array('','junior','target');
                                                            for($i=0; $i<=2; $i++)
                                                            {
                                                                $kritpmb=$krit[$i];    
                                                        ?>
                                                            <option value="<?=$alias[$i];?>" title='<?=$alias[$i];?>'><?=$kritpmb;?></option>
                                                        <?php }?>
                                                                </select>
                                                            </div>
                                                      </div>
                                                   
                                                    
                                                </div>
                                                <br>
                                                <div class="row">
                                                    
                                                       <div class="form-group">
                                                            <label class="col-md-4 control-label">Tahun PMB</label>
                                                            <div class="col-md-8">
                                                            <select class="form-control" id="TApmb">
                                                                    <option value="">-- Pilih Tahun PMB --</option>
                                                                </select>
                                                            </div>
                                                      </div>
                                                    
                                                </div>
                                                <br>
                                                <div class="row">
                                                    
                                                      <div class="form-group">
                                                            <label class="col-md-4 control-label">Periode Kalkulasi</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="periodekalkulasi">
                                                                    <option value="">-- Pilih Periode --</option>
                                                                </select>
                                                            </div>
                                                      </div>
                                                  
                                                </div>
                                                <br>
                                                <div class="row">
                                                    
                                                      <div class="form-group">
                                                            <label class="col-md-4 control-label">Skala</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="skala">
                                                                <option value="">-- Pilih Skala --</option>
                                                                <?php
                                                                $arai=array('','Milik & Kerjasama','Nasional');
                                                                for($i=1;$i<=2; $i++)
                                                                {
                                                                ?>
                                                                    <option value="<?php echo $i;?>"><?php echo $arai[$i];?></option>
                                                                <?php }?>    
                                                                </select>
                                                            </div>
                                                      </div>
                                                  
                                                </div>
                                                <br>
                                                <br>
                                                <div class="row">
                                              
                                                       <div class="form-group">
                                                            <label class="col-md-4 control-label"></label>
                                                            <div class="col-md-8">
                                                                <input type="button" value="Tampilkan" name="tampilrekapPMB" id="tampilrekapPMB" class="btn red-thunderbird">
                                                                <input type="button" value="Kalkulasi" name="kalkulasi" id="kalkulasi" class="btn red-thunderbird">
                                                            </div>
                                                      </div>

                                                </div>
                                                

                                            </div>
                                        </form>
            </div>
            <div class="col-md-6">
               <div class="note" style="background-color: #EBE832">
                     <ol>
                        <li>R. = Realisasi</li>
                        <li>Rasio TK1 = Diambil dari total registrasi dengan DP (diatas & dibawah 30%)</li>
                        <li>Target TK1 NAS = Target Tingkat 1 Nasional Sesuai Keputusan Rakernas</li>
                        <li>Target TK1 CAB = Target Cabang Per-Presenter</li>
                    </ol>
                </div>
            </div>
        </div>

                                        
        <div id="isi"></div>

     </div>
 </div







                    
                    

