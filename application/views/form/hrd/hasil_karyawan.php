<?php
if($kriteria == "PST"){
            ?>
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"><?=$label?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            
                                        </div>

                                    </div>
                                    <div class="portlet-body flip-scroll">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div style="margin-bottom: 10px;">
                                                        <div class="note note-warning">
                                                            <b>Klik icon <i class='fa fa-check' style='color:green;'></i>  untuk melihat CV</b>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                       
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul> -->

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-bordered table-striped table-condensed flip-content" id="pmb1">
                                            <thead class="flip-content">
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    
                                                    <th>NIK</th>
                                                    <th>NAMA KARYAWAN</th>
                                                    <th>JABATAN</th>
                                                    <th>BAGIAN</th>
                                                    <th>CV</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                               
                                                    foreach($karyawan as $row){
                                                ?>
                                                <tr>
                                                  
                                                    <td><?=$row->nik?></td>
                                                    <td><?=$row->nama?></td>
                                                    <td><?=$row->jabatan?></td>
                                                    <td>
                                                    <?php
                                                    $kodebagian = $row->bagian;
                                                    $jabatan = $this->Mainmodel->getWheres("kodepusat_bagian",array("idbagian"=>$kodebagian),"Coll")->result();
                                                    foreach($jabatan as $rjabatan);
                                                    echo $rjabatan->namabagian;
                                                    ?>
                                                    </td>
                                                    <td>
                                                    <?php
                                                    if($row->filecv == ""){
                                                        echo "<i class='fa fa-close' style='color:red;'></i>";
                                                    }else{
                                                        $filecv = $row->filecv;
                                                        ?>
                                                        <a target="blank_" href="http://dumi.lp3i.ac.id/emanajemen/AdminPendidikan/<?=$filecv?>"><i class='fa fa-check' style='color:green;'></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                    </td>
                                                    <td><button type="button" class="btn red btn-outline">Detail</button></td>
                                                </tr>
                                                    <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>


            <?php
        }elseif($kriteria == "milker"){
            ?>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"><?=$label?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            
                                        </div>

                                    </div>
                                    <div class="portlet-body flip-scroll">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div style="margin-bottom: 10px;">
                                                        <div class="note note-warning">
                                                            <b>Klik icon <i class='fa fa-check' style='color:green;'></i>  untuk melihat CV</b>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                       
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul> -->

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- begin isi -->
                                            <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_1" data-toggle="tab"> PTS </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_2" data-toggle="tab"> College </a>
                                            </li>
                                            
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade active in" id="tab_1_1">
                                        <!-- begin -->
                                        <table class="table table-bordered table-striped table-condensed flip-content" id="pmb3">
                                            <thead class="flip-content">
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NIK</th>
                                                    <th>NAMA KARYAWAN</th>
                                                    <th>CABANG</th>
                                                    <th>JABATAN</th>
                                                    <th>BAGIAN</th>
                                                    <th>CV</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php
                                                foreach($karyawanpts1 as $rowpts){
                                                    ?>
                                                    <tr>
                                                    <td><?=$rowpts->nik?></td>
                                                    <td><?=$rowpts->nama?></td>
                                                    <td>
                                                    <?php 
                                                        $kocab = $rowpts->cabang;
                                                        $cabang = $this->Mainmodel->getWheres("cabang",array("kodecabang"=>$kocab),"Coll")->result();
                                                        foreach($cabang as $rcabang);
                                                        echo $rcabang->namacabang;
                                                    ?>

                                                    </td>
                                                    <td><?=$rowpts->jabatan?></td>
                                                    <td>
                                                        <?php
                                                        $kodebagian = $rowpts->bagian;
                                                        $jabatan = $this->Mainmodel->getWheres("kategori_jabatan",array("kodejabatan"=>$kodebagian),"Coll")->result();
                                                        foreach($jabatan as $rjabatan);
                                                        echo $rjabatan->namajabatan;
                                                        ?>
                                                    </td>
                                                    <td>
                                                    <?php
                                                    if($rowpts->filecv == ""){
                                                        echo "<i class='fa fa-close' style='color:red;'></i>";
                                                    }else{
                                                        $filecv = $rowpts->filecv;
                                                        ?>
                                                        <a target="blank_" href="http://dumi.lp3i.ac.id/emanajemen/AdminPendidikan/<?=$filecv?>"><i class='fa fa-check' style='color:green;'></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                    </td>
                                                    <td><button type="button" class="btn red btn-outline">Detail</button></td>
                                                    </tr>
                                                    <?php
                                                }
                                               ?>
                                              <!-- PLJ -->
                                               <?php
                                               foreach($karyawanpts2 as $rowpts2){
                                                   ?>
                                                    <tr>
                                                    <td><?=$rowpts2->nik?></td>
                                                    <td><?=$rowpts2->nama?></td>
                                                    <td>
                                                        <?php 
                                                        $kocab = $rowpts2->cabang;
                                                        $cabang = $this->Mainmodel->getWheres("cabang",array("kodecabang"=>$kocab),"PLJ")->result();
                                                        foreach($cabang as $rcabang);
                                                        echo $rcabang->namacabang;
                                                        ?>
                                                    </td>
                                                    <td><?=$rowpts2->jabatan?></td>
                                                    <td>
                                                        <?php
                                                        $kodebagian = $rowpts2->bagian;
                                                        $jabatan = $this->Mainmodel->getWheres("kategori_jabatan",array("kodejabatan"=>$kodebagian),"Coll")->result();
                                                        foreach($jabatan as $rjabatan);
                                                        echo $rjabatan->namajabatan;
                                                        ?>
                                                    </td>
                                                    <td>
                                                    <?php
                                                    if($rowpts2->filecv == ""){
                                                        echo "<i class='fa fa-close' style='color:red;'></i>";
                                                    }else{
                                                        $filecv = $rowpts2->filecv;
                                                        ?>
                                                        <a target="blank_" href="http://dumi.lp3i.ac.id/emanajemen/AdminPendidikan/<?=$filecv?>"><i class='fa fa-check' style='color:green;'></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                    </td>
                                                    <td><button type="button" class="btn red btn-outline">Detail</button></td>
                                                    </tr>
                                                    <?php
                                               }
                                               ?>
                                            </tbody>
                                        </table>
                                        <!-- end -->
                                            </div>
                                            <div class="tab-pane fade" id="tab_1_2">
                                        <!-- begin -->
                                        <table class="table table-bordered table-striped table-condensed flip-content" id="pmb2">
                                            <thead class="flip-content">
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NIK</th>
                                                    <th>NAMA KARYAWAN</th>
                                                    <th>CABANG</th>
                                                    <th>JABATAN</th>
                                                    <th>BAGIAN</th>
                                                    <th>CV</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                                foreach($karyawancoll as $rowcol){
                                            ?>
                                               <tr>
                                                 <td><?=$rowcol->nik?></td>
                                                 <td><?=$rowcol->nama?></td>
                                                 <td>
                                                    <?php 
                                                        $kocab = $rowcol->cabang;
                                                        $cabang = $this->Mainmodel->getWheres("cabang",array("kodecabang"=>$kocab),"Coll")->result();
                                                        foreach($cabang as $rcabang);
                                                        echo $rcabang->namacabang;
                                                    ?>
                                                 </td>
                                                 <td><?=$rowcol->jabatan?></td>
                                                 <td>
                                                        <?php
                                                        $kodebagian = $rowcol->bagian;
                                                        $jabatan = $this->Mainmodel->getWheres("kategori_jabatan",array("kodejabatan"=>$kodebagian),"Coll")->result();
                                                        foreach($jabatan as $rjabatan);
                                                        echo $rjabatan->namajabatan;
                                                        ?>
                                                 </td>
                                                 <td>
                                                 <?php
                                                    if($rowcol->filecv == ""){
                                                        echo "<i class='fa fa-close' style='color:red;'></i>";
                                                    }else{
                                                        $filecv = $rowcol->filecv;
                                                        ?>
                                                        <a target="blank_" href="http://dumi.lp3i.ac.id/emanajemen/AdminPendidikan/<?=$filecv?>"><i class='fa fa-check' style='color:green;'></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                    </td>
                                                    <td><button type="button" class="btn red btn-outline">Detail</button></td>
                                               </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                        <!-- end -->
                                            </div>
                                        </div>
                                        <!-- end isi -->
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>
            <?php
        }elseif($kriteria == "Coll"){
            ?>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"><?=$label?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            
                                        </div>

                                    </div>
                                    <div class="portlet-body flip-scroll">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div style="margin-bottom: 10px;">
                                                        <div class="note note-warning">
                                                            <b>Klik icon <i class='fa fa-check' style='color:green;'></i>  untuk melihat CV</b>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                       
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul> -->

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-bordered table-striped table-condensed flip-content" id="pmb1">
                                            <thead class="flip-content">
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    
                                                    <th>NIK</th>
                                                    <th>NAMA KARYAWAN</th>
                                                    <th>CABANG</th>
                                                    <th>JABATAN</th>
                                                    <th>BAGIAN</th>
                                                    <th>CV</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                               
                                                    foreach($karyawan as $row){
                                                ?>
                                                <tr>
                                                  
                                                    <td><?=$row->nik?></td>
                                                    <td><?=$row->nama?></td>
                                                    <td>
                                                        <?php 
                                                        $kocab = $row->cabang;
                                                        $cabang = $this->Mainmodel->getWheres("cabang",array("kodecabang"=>$kocab),$kriteria)->result();
                                                        foreach($cabang as $rcabang);
                                                        echo $rcabang->namacabang;
                                                    ?>
                                                    </td>
                                                    <td><?=$row->jabatan?></td>
                                                    <td>
                                                    <?php
                                                    $kodebagian = $row->bagian;
                                                     $jabatan = $this->Mainmodel->getWheres("kategori_jabatan",array("kodejabatan"=>$kodebagian),"Coll")->result();
                                                    foreach($jabatan as $rjabatan);
                                                    echo $rjabatan->namajabatan;
                                                    ?>
                                                    </td>
                                                    <td>
                                                    <?php
                                                    if($row->filecv == ""){
                                                        echo "<i class='fa fa-close' style='color:red;'></i>";
                                                    }else{
                                                        $filecv = $row->filecv;
                                                        ?>
                                                        <a target="blank_" href="http://dumi.lp3i.ac.id/emanajemen/AdminPendidikan/<?=$filecv?>"><i class='fa fa-check' style='color:green;'></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                    </td>
                                                    <td><button type="button" class="btn red btn-outline">Detail</button></td>
                                                </tr>
                                                    <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>
            <?php
        }else{
            ?>
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"><?=$label?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            
                                        </div>

                                    </div>
                                    <div class="portlet-body flip-scroll">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div style="margin-bottom: 10px;">
                                                        <div class="note note-warning">
                                                            <b>Klik icon <i class='fa fa-check' style='color:green;'></i>  untuk melihat CV</b>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                       
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul> -->

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-bordered table-striped table-condensed flip-content" id="pmb1">
                                            <thead class="flip-content">
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    
                                                    <th>NIK</th>
                                                    <th>NAMA KARYAWAN</th>
                                                    <th>CABANG</th>
                                                    <th>JABATAN</th>
                                                    <th>BAGIAN</th>
                                                    <th>CV</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                               
                                                    foreach($karyawan1 as $row){
                                                ?>
                                                <tr>
                                                  
                                                    <td><?=$row->nik?></td>
                                                    <td><?=$row->nama?></td>
                                                    <td>
                                                        <?php 
                                                        $kocab = $row->cabang;
                                                        $cabang = $this->Mainmodel->getWheres("cabang",array("kodecabang"=>$kocab),$kriteria)->result();
                                                        foreach($cabang as $rcabang);
                                                        echo $rcabang->namacabang;
                                                    ?>
                                                    </td>
                                                    <td><?=$row->jabatan?></td>
                                                    <td>
                                                    <?php
                                                    $kodebagian = $row->bagian;
                                                     $jabatan = $this->Mainmodel->getWheres("kategori_jabatan",array("kodejabatan"=>$kodebagian),$kriteria)->result();
                                                    foreach($jabatan as $rjabatan);
                                                    echo $rjabatan->namajabatan;
                                                    ?>
                                                    </td>
                                                    <td>
                                                    <?php
                                                    if($row->filecv == ""){
                                                        echo "<i class='fa fa-close' style='color:red;'></i>";
                                                    }else{
                                                        $filecv = $row->filecv;
                                                        ?>
                                                        <a target="blank_" href="http://dumi.lp3i.ac.id/emanajemen/AdminPendidikan/<?=$filecv?>"><i class='fa fa-check' style='color:green;'></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                    </td>
                                                    <td><button type="button" class="btn red btn-outline">Detail</button></td>
                                                </tr>
                                                    <?php } ?>
                                                

                                                <?php
                                               
                                                    foreach($karyawan2 as $row2){
                                                        ?>

                                                        <tr>
                                                        <td><?=$row2->nik?></td>
                                                        <td><?=$row2->nama?></td>
                                                        <td>
                                                        <?php 
                                                        $kocab = $row2->cabang;
                                                        $cabang = $this->Mainmodel->getWheres("cabang",array("kodecabang"=>$kocab),"PLJ")->result();
                                                        foreach($cabang as $rcabang);
                                                        echo $rcabang->namacabang;
                                                        ?>
                                                        </td>
                                                        <td><?=$row2->jabatan?></td>
                                                        <td>
                                                        <?php
                                                        $kodebagian = $row2->bagian;
                                                        $jabatan = $this->Mainmodel->getWheres("kategori_jabatan",array("kodejabatan"=>$kodebagian),"PLJ")->result();
                                                        foreach($jabatan as $rjabatan);
                                                        echo $rjabatan->namajabatan;
                                                        ?>
                                                        </td>
                                                        <td>
                                                        <?php
                                                        if($row2->filecv == ""){
                                                            echo "<i class='fa fa-close' style='color:red;'></i>";
                                                        }else{
                                                            $filecv = $row2->filecv;
                                                            ?>
                                                            <a target="blank_" href="http://dumi.lp3i.ac.id/emanajemen/AdminPendidikan/<?=$filecv?>"><i class='fa fa-check' style='color:green;'></i></a>
                                                            <?php
                                                        }
                                                        ?>
                                                        </td>
                                                        <td><button type="button" class="btn red btn-outline">Detail</button></td>
                                                        </tr>

                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>
            <?php
        }
?>

                            <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('#pmb1').DataTable();
                                        $('#pmb2').DataTable();
                                        $('#pmb3').DataTable();
                                    });
                            </script>