<!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-red-sunglo">
                               
                                            <span class="caption-subject bold uppercase"> Input KPI Standar</span>
                                        </div>
                                   
                                    </div>
                                    <div class="notif">
                                     <?=$this->session->flashdata('message')?>
                                    </div>
                                    <div class="portlet-body form">
                                        <form role="form" action="<?=base_url('index.php/Main/simpanstandar')?>" method="post">
                                            <div class="form-body">

                                             <div class="form-group">
											    <label for="exampleFormControlSelect1">Tahun</label>
											    <select class="form-control" id="exampleFormControlSelect1" name="tahun">
											      <option value="2018">2018</option>
											      <option value="2019">2019</option>
											      <option value="2020">2020</option>
											      <option value="2021">2021</option>
												  <option value="2022">2022</option>
											    </select>
											  </div>

											  <div class="form-group">
											    <label>Ukuran Keberhasilan</label>
											    <textarea class="form-control" name="goal" rows="3" required></textarea>
											  </div>
                                            
	                                          <div class="form-group">
											    <label>Standar Minimal</label>
											    <input type="number" name="standar" class="form-control" required>
											  </div>

											  <div class="form-group">
											    <label>Target</label>
											    <input type="number" name="target" class="form-control" required>
											  </div>

											  <div class="form-group">
											    <label for="exampleFormControlSelect1">Satuan</label>
											    <select class="form-control" id="exampleFormControlSelect1" name="satuan" required>
											      <option value="%">%</option>
											      <option value="Buah">Buah</option>
											      <option value="Kali">Kali</option>
											      <option value="Orang">Orang</option>
											      <option value="Unit">Unit</option>
											      <option value="Mbps">Mbps</option>
											      <option value="Poin">Poin</option>
											      <option value="Rp">Rp</option>
											    </select>
											  </div>

											  <div class="form-group">
											    <label>Bobot</label>
											    <input type="text" class="form-control" name="bobot" required>
											  </div>

											  <div class="form-group">
											    <label for="exampleFormControlSelect1">Sumber</label>
											    <select class="form-control" id="exampleFormControlSelect1" name="sumber" required>
											      <option value="">-- Sumber --</option>
											      <option value="Input">Input</option>
											      <option value="Sistem">Sistem</option>
											    </select>
											  </div>

											  <div class="form-group">
											    <label>Keterangan</label>
											    <textarea class="form-control" rows="3" name="keterangan" required></textarea>
											  </div>
                                                
                                               
                                              
                                               
                                               
                                            </div>
                                            <div class="form-actions">
                                                <button type="submit" class="btn blue">Submit</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- END SAMPLE FORM PORTLET-->