<?php
$user = $this->session->userdata('username');
$kocab = $this->session->userdata('kocab');
$grp = $this->session->userdata('grp');
?>
<?php echo $this->session->flashdata('message')?>

<form method="POST" action="<?=base_url();?>index.php/main/dkpi?user=<?=$user?>&kocab=<?=$kocab?>&grp=<?=$grp?>">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <div id="context" data-toggle="context" data-target="#context-menu">
                                           <div class="form-group col-md-10">
                                    <label class="control-label col-md-3">Bidang</label>
                                <div class="col-md-6">
                            <?php
                    $js = 'class="form-control"';
                    echo form_dropdown('kodebidang', $cmbbidang, '', $js);
                    ?>
                                </div>
                            </div>

                            <div class="form-group col-md-10">
                                    <label class="control-label col-md-3">Tahun Periode</label>
                                    <div class="col-md-6">
                                        <?php
                    $js = 'class="form-control" style="width:200px" ';
                    echo form_dropdown('tahun', $cmbtahun, '', $js);
                    ?>
                                    </div>
                            </div>

                            <div class="form-group col-md-10">
                                    <label class="control-label col-md-3">Nama Cabang</label>
                                    <div class="col-md-6">
                                        <?php
                    $js = 'class="form-control" style="width:200px" ';
                    echo form_dropdown('cabang', $cmbcabang, '', $js);
                    ?>
                                        </div>
                            </div>
</form>
                            <div class="form-actions">
                                <div class="row">
                                <div class=" col-md-3">
                                </div>
                                <div class=" col-md-9">
                                    <input type="submit" value="Proses Data" name="cari" id="modal" class="btn btn-danger">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            $n=0;
            $hasput=0;
            $hasilall=0;
            $bobotall=0;  
            ?>


                        <span class="caption-subject font-green sbold uppercase" style="font-size: 25px">
                            <?php foreach($namcab as $nc);?>
                        KEY PERFORMANCE INDEX - KAMPUS 
                        <?php
                        if(empty($nc)){
                            $nc = "A";
                            $namkampus = "B";
                        }else{
                        echo $nc->namkampus; }?> </span><br>
                        <?php foreach($nambid as $nb);?>
                <span style="font-size: 16px">Bidang 
                    <?php
                    if(empty($nb)){
                        $nb = "A";
                    }else{
                        echo $nb->namabidang; } ?> - Tahun <?php echo $vtahun;?></span>
                 <div class="portlet-body flip-scroll" style="margin-top:20px">
                                       <table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr>
                                                    <th style="height: 32px"> KODE </th>
                                                    <th style="height: 32px"> GOAL </th>
                                                    <th style="height: 32px"> MINIMAL </th>
                                                    <th style="height: 32px"> TARGET </th>
                                                    <th style="height: 32px"> HASIL </th>
                                                    <th style="height: 32px"> BOBOT </th>
                                                    <th style="height: 32px"> SKOR </th>
                                                    <th style="height: 32px"> SUMBER </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach($list_kpi as $row)
                                                {
                                                    $kodestandar=$row->kodestandar;
                                                    $kodebidang=$row->kodebidang;
                                                    $kodecapai=$row->kodecapai;
                                                    $tahun=$row->tahun;
                                                    $goal=$row->goal;
                                                ?>
                                                <tr>
                                                    <td style="height: 32px"><?php echo $kodestandar;?></td>
                                                    <td><?php
                                            if ($row->sumber=='Input'){

                                                // echo "<a href=mod_kpi?kodecapai=".$row->kodecapai."&kb=".$row->kodebidang."&st=".$row->satuan."&ks=".$row->kodestandar."&tahun=".$row->tahun."' rel='facebox' title='View Detail'><font color='green'>".$row->goal."</font>";

                                                ?>
                                                <a data-toggle="modal" data-target="#edit<?php echo $kodecapai;?>"><font color='green'><?=$row->goal ?></font></a>
                                                </td>
                                        <?php
                                            }else{
                                                
                                                //AWAL SKRIP BUAT KLIK UNTUK REFRESH NILAI HASIL DARI SISTEM
                                                
                                                echo $row->goal."</td>";
                                            
                                                // AKHIR SKRIP  
                                                
                                            }
                                        ?></td>
                                                    <td style="height: 32px"><?=$row->minimal;?></td>
                                                    <td style="height: 32px"><?=$row->standar;?></td>
                                                    <!-- NILAI HASIL BERDASARKAN INPUT ATAU SISTEM -->                                                                                   
                                            <?php
                                            if ($row->sumber=='Sistem'){    
                                                $sekrip=$row->skrip."'$kodekampus'";                                      
                                                // $hasilsistem=mysqli_query($sekrip);
                                                                                    
                                                // $sekrip1="select @a/@b";   


                                                // $hasilsistem1=mysql_query($sekrip1);                        
                                                // $hasil1=mysql_fetch_array($hasilsistem1);                                               
                                                if ($row->ab>=$row->std2){
                                                    echo "<td align='center' bgcolor='#00FF00'>";
                                                    echo "<font color='#000000'>";                                              
                                                }elseif ($row->ab>=$row->std1){                                                                                         
                                                    echo "<td align='center' bgcolor='#FFA500'>";
                                                    echo "<font color='#000000'>";                                              
                                                }else{                                          
                                                    echo "<td align='center' bgcolor='#FF0000'>";   
                                                    echo "<font color='#FFFFFF'>";                                                  
                                                }                                                                                   
                                                echo number_format($row->ab)." ".$row->satuan."</font></td>";
                                                $hasput=$row->ab;                                             
                                            }else{
                                                echo "<td align='center' bgcolor=".$row->warnabg.">";                                      
                                                echo "<font color=".$row->warnahrf.">";
                                                echo "".$row->hasil." ".$row->satuan."</font></td>";
                                                $hasput=$row->hasil;                                                  
                                            }
                                            ?>  
                                                    <td style="height: 32px">
                                                         <?php
                                            $bobot=number_format($row->bobot,2);
                                            echo $bobot; 
                                            $bobotall=$bobotall+$bobot;
                                            ?>

                                                    </td>
                                                    <td style="height: 32px">
                                                        <!-- NILAI SKOR BERDASARKAN INPUT ATAU SISTEM -->                                   
                                            <?php
                                            if ($hasput>=$row->std2){
                                                $hasil=number_format($row->bobot,2);
                                                echo $hasil;
                                            }elseif ($hasput<$row->std2){
                                                $hasil=number_format($row->bobot*($hasput/$row->std2),2);
                                                echo $hasil;
                                            }else{
                                                echo '0';                                   
                                            }
                                            $hasilall=$hasilall+$hasil;
                                            ?> 
                                                    </td>

                                                    <td style="height: 32px">
                                                        <?php
                                            if ($row->sumber=='Input'){                                       
                                            $adakah=$row->lampiran;
                                            $link='documents/dokumenews/'.$row->kodecapai.".pdf";                                           
                                            if ($adakah != ''){
                                                // echo "<a href='$link' target=_blank>PDF</a></td>";
                                                echo "<a href='".base_url($link)."' target=_blank>PDF</a></td>";
                                            }elseif ($adakah == ''){
                                                echo "Tidak Ada File</td>";
                                            }
                                            }else{
                                                
                                                //AWAL SKRIP BUAT KLIK UNTUK REFRESH NILAI HASIL DARI SISTEM
                                                
                                                //echo "<a href='updatenilaisistem.php?kodecapai=".$row['kodecapai']."' rel='facebox' title='View Detail'><font color='green'>".$row['sumber']."</font></td>";
                                            
                                            echo "<b>".$row->sumber."</b></td>";
                                            
                                                // AKHIR SKRIP  
                                                
                                            }
                                        ?>
                                                    </td>
                                                </tr>
                                                
                                                <?php }

                                                if($hasilall==0){
                                                    $hasilall="0";
                                                    $bobotall="1";
                                                }else{
                                                    $hasilall;
                                                }
                                        $totalskor=number_format(($hasilall/$bobotall)*100,2);
                                        if ($totalskor>=80){
                                            $warna="Blue";
                                        }elseif ($totalskor<60){
                                            $warna="Red";
                                        }else{
                                            $warna="DarkOrange";                                            
                                        }
                                        
                                        echo "<center><font size=5 color='$warna'><b>".$totalskor." %"."</i></font></b></center>";                               
                                        echo "<center><font size=2 color='DarkSlateGray'><i>(Total Skor : Total Bobot) x 100% = (".number_format($hasilall,2)." : ".number_format($bobotall,2).") x 100%</i></i></font><br><br></center>";  
                                        ?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- <div class="row">
                                        <div class="col-md-12">
                                            <input type="button" value="Upload Lampiran" id="uploadlampir" data-toggle="modal" data-target="#exampleModal" class="btn btn-danger">
                                        </div>
                                    </div> -->
                                    <?php
                                                foreach($list_kpi as $row)
                                                {
                                                    $kodestandar=$row->kodestandar;
                                                    $kodebidang=$row->kodebidang;
                                                    $kodecapai=$row->kodecapai;
                                                    $tahun=$row->tahun;
                                                    $goal=$row->goal;
                                                    $satuan=$row->satuan;
                                                ?>

                                    <div class="modal fade" id="edit<?php echo $kodecapai;?>" tabindex="-1" role="basic" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                         <span class="caption-subject font-green sbold uppercase" style="font-size: 25px">UPLOAD LAMPIRAN</span>
                                                         <br>
                                            <b>Goal : <?=$goal?>
                                                    </div>
                                                    <div class="modal-body"> 

            <form method="POST" action="<?php echo base_url('index.php/main/updatekpidata')?>" enctype="multipart/form-data">
                <?php echo form_open_multipart(site_url('documents/dokumenews'));?>

        <table width="597" height="186" border="0" cellpadding="10" cellspacing="10">
              <tr>
                <td valign="top">
                 <input type="hidden" name="kodecapai" value="<?php echo $kodecapai; ?>" id="textfield" >
                 <input type="hidden" name="kodebidang" value="<?php echo $kodebidang; ?>" id="textfield" >
                <input type="hidden" name="tahun" value="<?php echo $tahun; ?>" id="textfield" >
                <table width="600" height="133" border="0">
                  <tr>
                    <td width="100"><label><b>Nilai</b></label></td>
                    <td width="10"><label><b>:</b></label></td>
                    <td width="200"> 
                      <input type="text" class="form-control" style="width: 200px" name="hasil" id="textfield" onKeyUp="digitsOnly(this)" onBlur="digitsOnly(this)"></td>
                      <td>&nbsp;<?php echo $satuan;?></td>
                  </tr>
                  <tr>
                    <td><label><b>Lampiran</b></label></td>
                    <td><label><b>:</b></label></td>
                    <td colspan="2"><input type="file" name="lampiran" id="lampiran" class="form-control" style="width: 300px" name="fuplod" accept="application/pdf"></td>
                  </tr>
                </table>
                </td>
              </tr>
            </table>
        

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                       <input type="submit" class="btn btn-primary" name="button" id="button" value="Submit">
                                                    </div>
                                                    </form>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                        <?php
                                    }
                                    ?>

<!-- modal box -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><b>UPLOAD LAMPIRAN</b></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=base_url('index.php/Main/simpanlampiran')?>" method="post" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleInputEmail1">Kode Standar</label>
            <input type="text" class="form-control">
           
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Nama Lampiran</label>
            <input type="text" class="form-control">
          </div>

          <div class="form-group">
            <label for="exampleFormControlSelect1">Jenis Lampiran</label>
            <select class="form-control" id="pilihjenis">
              <option>--Jenis Lampiran--</option>
              <option value="pdf">PDF</option>
              <option value="link">LINK</option>
            </select>
          </div>

          <div class="form-group" id="link">
            <label for="exampleInputPassword1">Link</label>
            <input type="text" id="link1" class="form-control">
          </div>

          <div class="form-group" id="file">
            <label for="exampleFormControlFile1">File</label>
            <input type="file" id="file1" class="form-control-file" id="exampleFormControlFile1">
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
       </form>
      </div>
    </div>
  </div>
</div>

