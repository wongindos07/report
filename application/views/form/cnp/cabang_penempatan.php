<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                	<div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN ANGKATAN <?=$tahun?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                    	<div class="row">
                                                <div class="col-md-6">
                                                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>
 -->
                                                      
                                                    </div>
                                                </div>
                                            </div>


                                            <table class="table table-bordered table-striped table-condensed flip-content pmbu300" id="pmbu300">
                                            	<thead>
                                            		<tr class='bg-grey-gallery bg-font-grey-gallery'>
                                            			<th>NO</th>
                                            			<th>CABANG </th>
														<th>TARGET REALISASI</th>
                                            			<th>JUMLAH REALISASI</th>
                                            			<th>TANGGAL POSTING</th>
                                            		</tr>
                                            	</thead>
                                            	<tbody>
                                                   <?php
                                                   $n=1;
                                                   $ttl1 = 0;
												   $ttl2 = 0;
												   $ttl3 = 0;
												   $ttl4 = 0;
                                                   foreach($cabangcnp as $cabang){
                                                   ?>
                                                    <tr>
                                                		<td><?=$n++?></td>
                                                		<td><?=substr($cabang->namacabang,7,50)?></td>
														<?php 
                                                            $kocab = $cabang->kodecabang;
															$getwaktuinput = $this->Mainmodel->getWaktuInput($kriteria,$kocab,$tahun)->result();

															foreach($getwaktuinput as $rwaktu);
															$tglinput = $rwaktu->tgl_input;

															$getcnp = $this->Mainmodel->penempatan($kriteria,$kocab,$tahun,$tglinput)->result();
															if($getcnp){
																foreach($getcnp as $rcnp);
																$target1 = $rcnp->terget1;
																$kerja1 = $rcnp->kerja1;
																$target2 = $rcnp->target2;
																$kerja2 = $rcnp->kerja2;
															}else{
																foreach($getcnp as $rcnp);
																$target1 = 0;
																$kerja1 = 0;
																$target2 = 0;
																$kerja2 = 0;
															}

															if($jenis == "wajib"){
																if($kerja1 == 0){
																	$realkerja = "Tidak Ada";
																}else{
																	$realkerja = $kerja1." Orang";
																}

																if($target1 == 0){
																	$realtarget = "Tidak Ada";
																}else{
																	$realtarget = $target1;
																}
																
															}else{
																if($kerja2 == 0){
																	$realkerja = "Tidak Ada";
																}else{
																	$realkerja = $kerja2." Orang";
																}

																if($target2 == 0){
																	$realtarget = "Tidak Ada";
																}else{
																	$realtarget = $target2;
																}
															}
															
															
												
															$ttl1 += $target1;
															$ttl2 += $kerja1;
															$ttl3 += $target2;
															$ttl4 += $kerja2;
                                                            ?>
														<td><?=$realtarget?></td>
                                                		<td><a href="#" class="realkerja" jeniscabang="<?=$jeniscabang?>" kriteria="<?=$kriteria?>" kocab="<?=$kocab?>" jenis="<?=$jenis?>" tahun="<?=$tahun?>"><?=$realkerja?></a></td>
														<td><?=$tglinput?></td>
                                                    </tr>
                                                 	<?php } ?> 
                                            	</tbody>

                                                <tfoot>
                                                    <tr>
                                                        <td class='bg-grey-gallery bg-font-grey-gallery' colspan="2">TOTAL</td>
														<td>
														<?php 
                                                        	if($jenis == "wajib"){
																echo $ttl1;
															}else{
																echo $ttl3;
															}
                                                        	?>
														</td>
                                                        <td>
                                                        	<?php 
                                                        	if($jenis == "wajib"){
																echo $ttl2." Orang";
															}else{
																echo $ttl4." Orang";
															}
                                                        	?>
                                                        </td>
														<td></td>
                                                    </tr>
                                                </tfoot>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        $('.pmbu300').DataTable();
		

		$(".realkerja").click(function(){
			let kriteria = $(this).attr("kriteria");
			let tahun = $(this).attr("tahun");
			let jenis = $(this).attr("jenis");
			let kocab = $(this).attr("kocab");
			let jeniscabang = $(this).attr("jeniscabang");

			$('.load').show();
			$.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Cnp/detailPenempatanMhs')?>",
                        data : {
                            kriteria:kriteria,
							tahun:tahun,
							jenis:jenis,
							kocab:kocab,
							jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            if(kriteria == 'Coll' || kriteria == 'group'){
                                        if(jenis == 'wajib'){
                                                 $('.isicnpcol').html(data);
                                            }else{
                                                $('.isicnpdibantu').html(data);
                                            }
                                        }else{
                                            if(jenis == 'wajib'){
                                                $('.isicnppol').html(data);
                                            }else{
                                                $('.isicnppoldibantu').html(data);
                                            }
                                        }
                        }
                    });
		});
    });
</script>