<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                	<div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"><?=$label?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN ANGKATAN <?=$tahun?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                    	<div class="row">
                                                <div class="col-md-6">
                                                   <div class="note note-warning">
                                                         <b><p>Jika Jumlah total pada cabang tidak sesuai dengan jumlah Mahasiswa dikarenakan cabang belum posting data penempatan terbaru</p></b>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>
 -->
                                                        <a style="margin-left: 20px;" tahun='<?=$tahun?>' kriteria='<?=$kriteria?>' jeniscabang='<?=$jeniscabang?>' jenis="<?=$jenis?>" kocab=<?=$kocab?> class="btn red  btn-outline kembali" href="#">
                                                            Kembali
                                                            <i class="fa fa-mail-reply"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>


                                            <table class='table table-bordered table-striped table-condensed flip-content pmbu300' id="pmbu300">
                                            	<thead>
                                            		<tr class='bg-grey-gallery bg-font-grey-gallery'>
                                            			<th>NO</th>
                                            			<th>NIM</th>
                                                        <th>NAMA</th>
                                                        <th>IPK</th>
                                            			<th>JURUSAN</th>
                                                        <th>NAMA PERUSAHAAN</th>
                                                        <th>POSISI</th>
                                                        <th>GAJI</th>
                                            		</tr>
                                            	</thead>
                                            	<tbody>
                                                <?php
                                                $n = 1;
                                                    foreach($penempatan as $row){
                                                ?>
                                                    <tr>
                                                		<td><?=$n++?></td>
                                                		<td><?=$row->nim?></td>
                                                		<td><?=$row->Nama_Mahasiswa?></td>
                                                        <td><?=round($row->ipk,2)?></td>
                                                        <td>
                                                        <?php
                                                            $kojur = $row->kodejurusan;
                                                            $jurusan = $this->Mainmodel->getWheres('jurusan',array('kodejurusan'=>$row->kodejurusan),$kriteria)->result();
                                                            foreach($jurusan as $cjurusan);
                                                        ?>
                                                        <a href="#" title="Klik untuk detail per jurusan" class="mhsjurusan" kojur="<?=$kojur?>" kriteria="<?=$kriteria?>" tahun="<?=$tahun?>" jenis="<?=$jenis?>" jeniscabang="<?=$jeniscabang?>" kocab="<?=$kocab?>"><?=$cjurusan->namajurusan?></a>
                                                        </td>
                                                        <td>
                                                        <?php
                                                            $perusahaan = $this->Mainmodel->getWheres('perusahaan',array('id_perusahaan'=>$row->id_perusahaan),$kriteria)->result();
                                                            foreach($perusahaan as $cperusahaan);
                                                            echo $cperusahaan->nama;
                                                        ?>
                                                        </td>
                                                        <td><?=$row->posisi?></td>
                                                        <td><?=$row->gaji?></td>
                                                    </tr>
                                                <?php } ?> 
                                            	</tbody>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        $('.pmbu300').DataTable();
        
        $('.kembali').click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            let jenis = $(this).attr("jenis");
            let kocab = $(this).attr("kocab");
            $('.load').show();
                $.ajax({
                                type : "POST",
                                url: "<?=base_url('index.php/Cnp/detailPenempatanMhs')?>",
                                data:{
                                    tahun:tahun,
                                    kriteria:kriteria,
                                    jeniscabang,
                                    jenis:jenis,
                                    kocab:kocab
                                },
                                success: function(data){
                                    $('.load').hide();
                                    if(kriteria == 'Coll' || kriteria == 'group'){
                                        if(jenis == 'wajib'){
                                                 $('.isicnpcol').html(data);
                                            }else{
                                                $('.isicnpdibantu').html(data);
                                            }
                                        }else{
                                            if(jenis == 'wajib'){
                                                $('.isicnppol').html(data);
                                            }else{
                                                $('.isicnppoldibantu').html(data);
                                            }
                                        }
                                }
                            });
            });

            $('.mhsjurusan').click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            let jenis = $(this).attr("jenis");
            let kocab = $(this).attr("kocab");
            let kojur = $(this).attr("kojur");
            $('.load').show();
                $.ajax({
                                type : "POST",
                                url: "<?=base_url('index.php/Cnp/wajibKerjaJurusan')?>",
                                data:{
                                    tahun:tahun,
                                    kriteria:kriteria,
                                    jeniscabang,
                                    jenis:jenis,
                                    kojur:kojur,
                                    kocab:kocab
                                },
                                success: function(data){
                                    $('.load').hide();
                                    if(kriteria == 'Coll' || kriteria == 'group'){
                                        if(jenis == 'wajib'){
                                                 $('.isicnpcol').html(data);
                                            }else{
                                                $('.isicnpdibantu').html(data);
                                            }
                                        }else{
                                            if(jenis == 'wajib'){
                                                $('.isicnppol').html(data);
                                            }else{
                                                $('.isicnppoldibantu').html(data);
                                            }
                                        }
                                }
                            });
            });
    });
</script>