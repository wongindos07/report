<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                	<div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tahun?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>
 -->
                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
				<table class='table table-bordered table-striped table-condensed flip-content piutangnot' id="piutangnot">
					<thead>
                        <tr class='bg-grey-gallery bg-font-grey-gallery'>
                            <th>N0</th>
                            <?php
                            if($kriteria == 'Coll'){
                            	$cabangr = 'CABANG';
                            }else{
                            	$cabangr = 'KAMPUS';
                            }
                            ?>
                            <th><?=$cabangr?></th>
                            <th>JUMLAH PIUTANG</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
					<tbody>
						<?php
							$n = 1;
							$totalnya = '';
							foreach($cabang as $row){
								$kocab = $row->kodecabang;

								$piutangnot = $this->Mainmodel->piutangNotCabang($kriteria,$tahun,$kocab)->result();
								foreach($piutangnot as $row2);
								$jumlahbiaya = $row2->jumlahbiaya;
								$terbayar = $row2->terbayar;
								$total = $jumlahbiaya - $terbayar;
								$totalnya += $total;

								$sepuluh = ($terbayar * 10) / 100;
								$lima = ($terbayar * 5) / 100;
								if($total > $sepuluh){
									$warna = '#D04556';
									$color = '#FDFDFE';
								}elseif($total <= $sepuluh and $total > $lima){
									$warna = '#DFD43B';
									$color = '';
								}else{
									$warna = '#3DDA42';
									$color = '';
								}

								if($kriteria == "Coll" || $kriteria == "group"){
									$link = "<a class='btn red btn-outline bold detailpiutangnot' kriteria='Coll' href='#' tahun='$tahun' kocab='$kocab' jeniscabang='$jeniscabang'  dashboard='$dashboard'>Detail</a>";
									$nama = substr($row->namacabang,7,100);
								}else{
									$link = "<a class='btn red btn-outline bold detailpiutangnot' kriteria='PLJ' href='#' tahun='$tahun' kocab='$kocab' jeniscabang='$jeniscabang'  dashboard='$dashboard'>Detail</a>";
									$nama = substr($row->namacabang,7,100);
								}
								?>
							<tr>
								<td><?=$n++?></td>
								<td><?=$nama?></td>
								<td style='background-color:<?=$warna?>;color:<?=$color?>'><b>Rp<?=number_format($total)?></b></td>
								<td>
									<?php
										if($total == 0){
											echo "";
										}else{
											echo $link;
										}
									?>
								</td>
							</tr>
						<?php
							}
						?>
					</tbody>
					<tfoot>
						<td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
						<td>
						<?php
						if($totalnya == 0){
							echo '';
						}else{
							echo '<b>Rp'.number_format($totalnya).'</b>';
						}
						?>
						</td>
						<td></td>
					</tfoot>		
					</tbody>
				</table>
			</div>
                                </div>
                            </div>
</div>

<script type="text/javascript">
	$(".detailpiutangnot").click(function(){
	let tahun = $(this).attr("tahun");
	let kocab = $(this).attr("kocab");
	let dashboard = $(this).attr("dashboard");
	let kriteria = $(this).attr("kriteria");
	let jeniscabang = $(this).attr('jeniscabang');
	$('.load').show();
	$.ajax({
	                        type : "POST",
	                        url  : "<?=base_url('index.php/Keuangan/detailpiutangnot')?>",
	                        data : {
	                            tahun:tahun,
	                            kocab:kocab,
	                            kriteria:kriteria,
	                            dashboard:dashboard,
                           		jeniscabang:jeniscabang
	                        },
	                        success : function(data){
	                            $('.load').hide();
	                            if(kriteria == 'Coll'){
	                            	$(".notpol").html("");
	                            	$(".not").html("");
	                            	$(".not").html(data);
	                        	}else{
	                        		$(".not").html("");
	                        		$(".notpol").html("");
	                        		$(".notpol").html(data);
	                        	}
	                        }
	                    });
	})
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('.piutangnot').DataTable();
    });
</script>