<div class="portlet box blue-hoki">
     <div class="portlet-title">
         <div class="caption">
             <i class="fa fa-gift"></i>Rekap Transaksi Pembayaran Peserta Didik</div>
     </div>
     <div class="portlet-body" style='padding-top:30px;'>
        <div class="row">
            <div class="col-md-7">
                <form method="POST" action="<?=base_url();?>index.php/Pendidikan/rekappddjurus">
            <div class="form-body">
            <div class="portlet light bordered">
                 <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                            <div class="form-group">
                                <label class="col-md-5 control-label">Pilih Transaksi</label>
                                <div class="col-md-7">
                                    <select name="transaksi" id="transaksi" class="form-control" required="required">
                                            <option>-- Pilih Transaksi --</option>
                                            <option value="registrasi">Registrasi</option>
                                            <option value="daftar">Daftar</option>    
                                            <option value="realisasi">Realisasi Pembayaran</option>                       
                                    </select>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="form-group" style="margin-top: 15px !important;">
                            <label class="control-label col-md-5">Pilih Tanggal</label>
                                <div class="col-md-7">
                                        <div class="input-group input-large date-picker input-daterange" data-date="10/11/2012" data-date-format="mm/dd/yyyy">
                                            <input type="text" class="form-control datepicker" name="from" id="from" data-date-format="yyyy/mm/dd">
                                            <span class="input-group-addon"> to </span>
                                            <input type="text" class="form-control datepicker" name="to" id="to" data-date-format="yyyy/mm/dd"> 
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <div class="form-group" style="margin-top: 10px !important">
                                <label class="col-md-5 control-label"></label>
                                <div class="col-md-7">
                                    <input type="button" value="Proses Data" name="cari" id="prosestransaksi" class="btn red-thunderbird">
                                </div>
                            </div>
                            </div>
                    
                        </div>
                    </div>
                 </div>
            </div>
            </div>
        </form>
            </div>
            <div class="col-md-5">
                <div class="note note-warning notereal">
                     <ol>
                        <li>Kosongkan kedua kolom tanggal untuk menampilkan realisasi hari ini</li>
                    </ol>
                </div>
            </div>
        </div>



     	

	 <div class="isitransaksi"></div>
     </div>
 </div>