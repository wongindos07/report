<style type="text/css">
    .note-warning ol li{
        font-size: 13px;
    }
</style>

<div class="portlet box blue-hoki">
     <div class="portlet-title">
         <div class="caption">
             <i class="fa fa-gift"></i>Rekap Registrasi PMB</div>
     </div>
     <div class="portlet-body" style='padding-top:30px;'>
     	<form method="POST" action="<?=base_url();?>index.php/Pendidikan/rekappddjurus">
            <div class="form-body">
            <div class="portlet light bordered">
                 <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                            <div class="form-group">
                                <label class="col-md-5 control-label">Kriteria</label>
                                <div class="col-md-7">
                                    <select name="kriteria" id="tipepmb" class="form-control" required="required">
                                            <option>-- Kriteria --</option>
                                            <option value="group">Group</option>
                                            <option value="PLJ">Politeknik</option>    
                                            <option value="Coll">LP3I College</option>                       
                                    </select>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="form-group" style="margin-top: 10px !important">
                                <label class="col-md-5 control-label">Tahun Akademik</label>
                                <div class="col-md-7">
                                    <select id="tahunpmb" class="form-control" required="required">
                                        <option value="">-- Tahun Akademik --</option>
                                    </select>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                            <div class="form-group" style="margin-top: 10px !important">
                                <label class="col-md-5 control-label"></label>
                                <div class="col-md-7">
                                    <input type="button" value="Proses Data" name="cari" id="prosesdatapmb" class="btn red-thunderbird">
                                </div>
                            </div>
                            </div>
                    
                        </div>
                        <div class="col-md-6">
                            <div class="note" style="background-color: #EBE832">
                                <ol>
                                    <li>REG <30% = Pembayaran registrasi mahasiswa tingkat 1 dibawah 30% dari biaya kuliah</li>
                                    <li>REG >=30% = Pembayaran registrasi mahasiswa tingkat 1 diatas sama dengan 30% dari biaya kuliah</li>
                                    <li>REG TK2 = Pembayaran registrasi mahasiswa dari tingkat 1 ke tingkat 2</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
            </div>
        </form>

	 <div class="isipmb"></div>
     </div>
 </div>