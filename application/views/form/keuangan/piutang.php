<div class="portlet box blue-hoki">
     <div class="portlet-title">
         <div class="caption">
             <i class="fa fa-gift"></i>Rekap Piutang Peserta Didik</div>
     </div>
     <div class="portlet-body" style='padding-top:30px;'>
        <div class="row">
            <div class="col-md-6">
                <form role="form">
                                            <div class="form-body">
                                                <div class="row">
                                                    
                                                      <div class="form-group">
                                                            <label class="col-md-4 control-label">Kriteria</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="kriteria">
                                                                    <option value="">-- Pilih Kriteria --</option>
                                                                    <option value="Coll">College</option>
                                                                    <option value="PLJ">Politeknik</option>
                                                                    <option value="group">Group</option>
                                                                </select>
                                                            </div>
                                                      </div>
                                                   
                                                    
                                                </div>
                                                <br>
                                                <div class="row">
                                                    
                                                      <div class="form-group">
                                                            <label class="col-md-4 control-label">Tipe</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="tipe">
                                                                    <option>-- Pilih Tipe -- </option>
                                                                </select>
                                                            </div>
                                                      </div>
                                                  
                                                  
                                                </div>
                                                <br>
                                                <div class="row">
                                                    
                                                       <div class="form-group">
                                                            <label class="col-md-4 control-label">Tahun Akademik</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="tahun">
                                                                    <option>-- Pilih Tahun Akademik --</option>
                                                                </select>
                                                            </div>
                                                      </div>
                                                    
                                                    
                                                </div>
                                                <br>
                                                <div class="row">
                                              
                                                       <div class="form-group">
                                                            <label class="col-md-4 control-label"></label>
                                                            <div class="col-md-8">
                                                                <input type="button" value="Proses Data" name="cari" id="caripiutang" class="btn red-thunderbird">
                                                            </div>
                                                      </div>

                                                </div>
                                                

                                            </div>
                                        </form>
            </div>
            <div class="col-md-6">
               <div class="note note-warning">
                     <ol>
                        <li>Total piutang adalah total tagihan mahasiswa yang telah jatuh tempo sampai dengan tanggal berjalan</li>
                        <li>Jika piutang > 10% dari total penerimaan maka akan berwarna merah</li>
                        <li>Jika piutang > 5% dan <=10% dari total penerimaan maka akan berwarna Kuning</li>
                        <li>Jika piutang <= 5% dari total penerimaan maka akan berwarna Hijau</li>
                    </ol>
                </div>
            </div>
        </div>

                                        
        <div id="isi"></div>

     </div>
 </div







                    
                    

