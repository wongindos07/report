<style type="text/css">
    th{
        text-align: center;
        vertical-align: middle !important;
    }
    .subisi{
        background-color: #D3D5D7 !important;
    }
    .u30, .a30{
        text-align: center !important;
    }
    tfoot tr td{
        text-align: center !important;
    }
</style>
<?php
foreach($dp as $dpnilai);
?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tak?></span>
                                        </div>

                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printcabangpmb/').$kriteria.'/'.$tak?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/cabangpmbpdf/').$kriteria.'/'.$tak?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="pmb1">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th rowspan="2">NO</th>
                                                    <th rowspan="2">CABANG/KAMPUS</th>
                                                    <th colspan="3">PAYMENT</th>
                                                </tr>
                                                <tr>
                                                    <!-- <?=$dpnilai->keterangan?> -->
                                                    <th class="subisi">REG <30% </th>
                                                    <th class="subisi">REG >=30% </th>
                                                    <th class="subisi">REG TK 2 </th>
                                                </tr>   
                                            </thead>
                                            <tbody>
                                                <?php
                                                $no = 1;
                                                $ttl1 = '';
                                                $ttl2 = '';
                                                $ttl3 = '';
                                                foreach($cabang as $nilaicabang){
                                                    
                                                    ?>
                                                    <tr>
                                                        <td><?=$no++?></td>
                                                        <td><?=substr($nilaicabang->namacabang,7,30)?></td>
                                                        <?php
                                                        $kocab = $nilaicabang->kodecabang;
                                                        $tak = $nilaicabang->tahunakademik;
                                                        // ambil nilai dibawah 30%
                                                        $u30 = $this->Mainmodel->getU30($kocab,$kriteria,$tak)->result();
                                                        foreach($u30 as $n30);
                                                        if($n30->total == 0){
                                                            $regu30 = '';
                                                            $warna = '#B2B4B2';
                                                            $jenis1 = '';
                                                        }else{
                                                            $regu30 = number_format($n30->total);
                                                            $reu30 = $n30->total;
                                                            $warna = '';
                                                            $jenis1 = '<30%';
                                                            $status1 = 'U-30';
                                                            $ket1 = 'Registrasi Mahasiswa Baru';
                                                            $ttl1 += $reu30; 
                                                        }


                                                        $a30 = $this->Mainmodel->getA30($kocab,$kriteria,$tak)->result();
                                                        foreach($a30 as $na30);
                                                        if($na30->total == 0){
                                                            $rega30 = '';
                                                            $warnaa = '#B2B4B2';
                                                            $jenis2 = '';
                                                        }else{
                                                            $rega30 = number_format($na30->total);
                                                            $warnaa = '';
                                                            $rea30 = $na30->total;
                                                            $jenis2 = '>=30%';
                                                            $status2 = 'Aktif';
                                                            $ket2 = 'Registrasi Mahasiswa Baru';
                                                            $ttl2 += $rea30;
                                                        }

                                                        $tk2 = $this->Mainmodel->ambiltk2($kocab,$kriteria,$tak)->result();
                                                        foreach($tk2 as $ntk2);
                                                        if($ntk2->total == 0){
                                                            $regtk2 = '';
                                                            $warnaaa = '#B2B4B2';
                                                            $jenis3 = '';
                                                         
                                                        }else{
                                                            $regtk2 = number_format($ntk2->total);
                                                            $warnaaa = '';
                                                            $tk = $ntk2->total;
                                                            $jenis3 = 'TK 2';
                                                            $status3 = 'Aktif';
                                                            $ket3 = 'Registrasi Mahasiswa Senior';
                                                            $ttl3 += $tk;
                                                        }
                                                        ?>
                                                        <td class="u30" style="background-color: <?=$warna?>"><a class="isiu30" href="#" kodecabang='<?=$kocab?>' tak='<?=$tak?>' tipe='<?=$kriteria?>' jenis='<?=$jenis1?>' status='<?=$status1?>' ket='<?=$ket1?>'><?=$regu30?></a></td>
                                                        <td class="a30" style="background-color: <?=$warnaa?>"><a class="isiu30" href="#" kodecabang='<?=$kocab?>' tak='<?=$tak?>' tipe='<?=$kriteria?>' jenis='<?=$jenis2?>' status='<?=$status2?>' ket='<?=$ket2?>'><?=$rega30?></a></td>
                                                        <td class="a30" style="background-color: <?=$warnaaa?>"><a class="isiu30" href="#" kodecabang='<?=$kocab?>' tak='<?=$tak?>' tipe='<?=$kriteria?>' jenis='<?=$jenis3?>' status='<?=$status3?>' ket='<?=$ket3?>'><?=$regtk2?></a></td>
                                                    </tr>
                                                    <?php

                                                }
                                                // tutup pengulangan
                                                ?>
                                                <tfoot>
                                                <tr>   
                                                    <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>       <td>
                                                    <?php
                                                    if($ttl1 == ''){
                                                        
                                                    }else{
                                                        echo number_format($ttl1);
                                                    }
                                                    ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                    if($ttl2 == ''){
                                                        
                                                    }else{
                                                        echo number_format($ttl2);
                                                    }
                                                    ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                    if($ttl3 == ''){
                                                        
                                                    }else{
                                                        echo number_format($ttl3);
                                                    }
                                                    ?>
                                                    </td>
                                                </tr>
                                                </tfoot> 
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>


<!-- detail perjurusan -->
<script type="text/javascript">

             $(".isiu30").click(function(){
                    let tahun = $(this).attr('tak');
                    let tipe  = $(this).attr('tipe');
                    let kocab = $(this).attr('kodecabang');
                    let jenis = $(this).attr('jenis');
                    let status = $(this).attr('status');
                    let ket = $(this).attr('ket');
                    $('.loading').show();
                 $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/regJurusanPmb')?>",
                        data:{
                            tahun:tahun,
                            tipe:tipe,
                            kocab:kocab,
                            jenis:jenis,
                            status:status,
                            ket:ket
                        },
                        success: function(data){
                            $('.loading').hide();
                            $('.isipmb').html(data);
                        }
                    });
                     
                });
</script>

                            <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('#pmb1').DataTable();
                                    });
                            </script>

                           
           
                        


                            