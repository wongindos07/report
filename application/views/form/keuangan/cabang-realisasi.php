
<!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
                                            <span class="caption-subject bold uppercase"> <?=strtoupper($label2)?></span>
                                        </div>

                                    </div>
                                    <div class="tabbable tabbable-tabdrop">
                                            <ul class="nav nav-pills">
                                                <li class="active">
                                                    <a href="#tab11" data-toggle="tab">College</a>
                                                </li>
                                                <li>
                                                    <a href="#tab12" data-toggle="tab">Politeknik</a>
                                                </li>
                                                <li>
                                                    <a href="#tab13" data-toggle="tab">Group</a>
                                                </li>
                                                
                                            </ul>
                                            <div class="tab-content">
                                    
                                   <div class="tab-pane active" id="tab11" style="overflow-x: auto;">
                                      <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="note" style="background-color: #F77598">
                                                         <ol>
                                                          
                                                            <li>Realisasi Bayar adalah pembayaran mahasiswa/peserta didik sampai dengan tanggal yang dipilih</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="btn-group pull-right">
                                                        

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printdaftar/').$from.'/'.$to.'/Coll'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printdaftarpdf/').$from.'/'.$to.'/Coll'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                    <!-- isi -->
                                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="coldaf1">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>CABANG</th>
                                                             
                                                                <th>REALISASI BAYAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $no = 1;
                                                       
                                                            $totalreal = '';
                                                            foreach($college as $row1){
                                                                ?>
                                                                <tr>
                                                                    <td><?=$no++?></td>
                                                                    <td><?=substr($row1->namacabang,7,30)?></td>
                                                                    <?php

                                                                    $d = $this->Mainmodel->tampildatacollege("select ta from inisialperiode where tgl_awal <= '$dates' and tgl_akhir >= '$dates'");
                                                                        foreach($d as $rperiode);
                                                                    $periode = $rperiode->ta;
                                                                    $kocab = $row1->kodecabang;

                                                                    if($from == '' && $to == ''){

                                                                     
                        
                                                                        $realisasi = $this->Mainmodel->tampildatacollege("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab'and tahunakademik='$periode' and ket='Pembayaran angsuran' and tanggalbayar = '$dates'");
                                                                        foreach($realisasi as $real);
                                                                        $real = $real->realisasi;

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                        }
  

                                                                    

                                                                    }elseif($from <> '' && $to <> ''){

                                                                        
                        
                                                                        $realisasi = $this->Mainmodel->tampildatacollege("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab'and tahunakademik='$periode' and ket='Pembayaran angsuran' and tanggalbayar between '$from' and '$to'");
                                                                        foreach($realisasi as $real);
                                                                        $real = $real->realisasi;

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                        }

                                                                     


                                                                    }else{
                                                                       
                                                                        $real = '0';

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                        }

                                                                     
                                                                    }

                                                                   
                                                                    $totalreal += $real;
                                                                    
                                                                    ?>
                                                                  
                                                                    <td style="background-color: <?=$warna?>">
                                                                        <?php
                                                                        if($real == 0){

                                                                        }else{
                                                                        ?>
                                                                        <a href="#" class="detailreal" ta='<?=$periode?>' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='Coll'>Rp <?=number_format($real)?></a>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                               
                                                                <td>Rp <?=number_format($totalreal)?></td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                 <div class="tab-pane" id="tab12" style="overflow-x: auto;">
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="note" style="background-color: #F77598">
                                                         <ol>
                                                           
                                                            <li>Realisasi Bayar adalah pembayaran mahasiswa/peserta didik sampai dengan tanggal berjalan</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="btn-group pull-right">
                                                        

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasi/').$from.'/'.$to.'/PLJ'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasipdf/').$from.'/'.$to.'/PLJ'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                <!-- isi -->
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="coldaf1">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>CABANG</th>
                                                               
                                                                <th>REALISASI BAYAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $no = 1;
                                                        
                                                            $totalreal = '';
                                                            foreach($poltek as $row1){
                                                                ?>
                                                                <tr>
                                                                    <td><?=$no++?></td>
                                                                    <td><?=substr($row1->namacabang,7,30)?></td>
                                                                    <?php

                                                                    $d = $this->Mainmodel->tampildatapoltek("select ta from inisialperiode where tgl_awal <= '$dates' and tgl_akhir >= '$dates'");
                                                                        foreach($d as $rperiode);
                                                                    $periode = $rperiode->ta;
                                                                    $kocab = $row1->kodecabang;

                                                                    if($from == '' && $to == ''){

                                                                      
                        
                                                                        $realisasi = $this->Mainmodel->tampildatapoltek("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab'and tahunakademik='$periode' and ket='Pembayaran angsuran' and tanggalbayar = '$dates'");
                                                                        foreach($realisasi as $real);
                                                                        $real = $real->realisasi;

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                       
                                                                        }

                                                                    }elseif($from <> '' && $to <> ''){

                                                                      
                        
                                                                        $realisasi = $this->Mainmodel->tampildatapoltek("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab'and tahunakademik='$periode' and ket='Pembayaran angsuran' and tanggalbayar between '$from' and '$to'");
                                                                        foreach($realisasi as $real);
                                                                        $real = $real->realisasi;

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                          
                                                                        }
                                                                    }else{
                                                                      
                                                                        $real = '0';

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                        
                                                                        }
                                                                    }

                                                                  
                                                                    $totalreal += $real;
                                                                    
                                                                    ?>
                                                              
                                                                    <td style="background-color: <?=$warna?>">
                                                                        <?php
                                                                        if($real == 0){

                                                                        }else{
                                                                        ?>
                                                                        <a href="#" class="detailreal" ta='<?=$periode?>' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='PLJ'>Rp <?=number_format($real)?></a>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                            
                                                                <td>Rp <?=number_format($totalreal)?></td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                <div class="tab-pane" id="tab13" style="overflow-x: auto;">
                                     <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="note" style="background-color: #F77598">
                                                         <ol>
                                               
                                                            <li>Realisasi Bayar adalah pembayaran mahasiswa/peserta didik sampai dengan tanggal berjalan</li>
                                                        </ol>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                                  <!-- isi -->
                                                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="coldaf1">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>CABANG</th>
                                                              
                                                                <th>REALISASI BAYAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $no = 1;
                                                           
                                                            $totalreal = '';
                                                            foreach($grup as $row1){
                                                                ?>
                                                                <tr>
                                                                    <td><?=$no++?></td>
                                                                    <td><?=substr($row1->namacabang,7,30)?></td>
                                                                    <?php

                                                                    $d = $this->Mainmodel->tampildatacollege("select ta from inisialperiode where tgl_awal <= '$dates' and tgl_akhir >= '$dates'");
                                                                        foreach($d as $rperiode);
                                                                    $periode = $rperiode->ta;
                                                                    $kocab = $row1->kodecabang;

                                                                    if($from == '' && $to == ''){

                                                                 
                                                                        $realisasi = $this->Mainmodel->tampildatacollege("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab'and tahunakademik='$periode' and ket='Pembayaran angsuran' and tanggalbayar = '$dates'");
                                                                        foreach($realisasi as $real);
                                                                        $real = $real->realisasi;

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                           
                                                                        }

                                                                    }elseif($from <> '' && $to <> ''){

                                                                    
                        
                                                                        $realisasi = $this->Mainmodel->tampildatacollege("SELECT  sum(jumlahbayar) as realisasi from bayarkuliah inner join biodata on bayarkuliah.nim=biodata.nim where bayarkuliah.kodecabang='$kocab'and tahunakademik='$periode' and ket='Pembayaran angsuran' and tanggalbayar between '$from' and '$to'");
                                                                        foreach($realisasi as $real);
                                                                        $real = $real->realisasi;

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                            
                                                                        }

                                                                    }else{
                                                                        
                                                                        $real = '0';

                                                                        if($real == 0){
                                                                            $warna = '#B2B4B2';
                                                                        }else{
                                                                            $warna = '';
                                                                            
                                                                        }
                                                                    }

                                                                    $totalreal += $real;
                                                                    
                                                                    ?>
                                                                  
                                                                    <td style="background-color: <?=$warna?>">
                                                                        <?php
                                                                        if($real == 0){

                                                                        }else{
                                                                        ?>
                                                                        <a href="#" class="detailreal" ta='<?=$periode?>' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='grup'>Rp <?=number_format($real)?></a>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                                <?php
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                              
                                                                <td>Rp <?=number_format($totalreal)?></td>

                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                                


                                            </div>
                                        </div>


                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>

<script type="text/javascript">
    $('.detailreal').click(function(){
        let ta = $(this).attr('ta');
        let from = $(this).attr('from');
        let to = $(this).attr('to');
        let kocab = $(this).attr('kocab');
        let kriteria = $(this).attr('kriteria');
        $('.loading').show();
        $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/trxMhsRealisasi')?>",
                        data:{
                            ta:ta,
                            from:from,
                            kocab:kocab,
                            to:to,
                            kriteria:kriteria
                        },
                        success: function(data){
                            $('.loading').hide();
                            $('.isitransaksi').html(data);
                        }
                    });
    });
</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#coldaf1').DataTable();
        $('#coldaf2').DataTable();
        $('#coldaf3').DataTable();
     });
</script>