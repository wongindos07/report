<!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
                                            <span class="caption-subject bold uppercase"> <?=strtoupper($label2)?></span>
                                        </div>

                                    </div>
                                    <div class="tabbable tabbable-tabdrop">
                                            <ul class="nav nav-pills">
                                                <li class="active">
                                                    <a href="#tab11" data-toggle="tab">College</a>
                                                </li>
                                                <li>
                                                    <a href="#tab12" data-toggle="tab">Politeknik</a>
                                                </li>
                                                <li>
                                                    <a href="#tab13" data-toggle="tab">Group</a>
                                                </li>
                                                
                                            </ul>
                                            <div class="tab-content">
                                                <div class="tab-pane active" id="tab11">
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasi/').$from.'/'.$to.'/Coll'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasipdf/').$from.'/'.$to.'/Coll'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="colreg1">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NO</th>
                                                    <th>CABANG</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 1</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 2</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                                 $n = 1;
                                                 $totalcollege1 = '';
                                                 $totalcollege2 = '';
                                                 $ttlreg1 = '';
                                                 $ttlreg2 = '';
                                                 foreach($college as $row){
                                                    ?>
                                                    <tr>
                                                        <td><?=$n++?></td>
                                                        <td><?=$row->namacabang?></td>
                                                        <?php
                                                        $kocab = $row->kodecabang;
                                                        // cari yg registrasi tingkat 1
                                                        $ambil1 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'Coll',1)->result();
                                                        foreach($ambil1 as $row1);
                                                        $total1 = $row1->total1;
                                                        $ttl1 = $row1->total2;
                                                        if($total1 == null || $total1 == 0){
                                                            $warna = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna = '';  
                                                        }

                                                        // cari yg registrasi tingkat 2
                                                        $ambil2 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'Coll',2)->result();
                                                        foreach($ambil2 as $row2);
                                                        $total2 = $row2->total1;
                                                        $ttl2 = $row2->total2;
                                                        if($total2 == null || $total2 == 0){
                                                            $warna2 = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna2 = '';  
                                                        }

                                                        $totalcollege1 += $total1;
                                                        $totalcollege2 += $total2;
                                                        $ttlreg1 += $ttl1;
                                                        $ttlreg2 += $ttl2;
                                                        ?>
                                                        <td><?=$ttl1?></td>
                                                        <td style="<?=$warna?>"><a href="#" class="colreg" tingkat='1' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='Coll'>Rp <?=number_format($total1)?></a></td>
                                                        <td><?=$ttl2?></td>
                                                        <td style="<?=$warna2?>"><a href="#" class="colreg" tingkat='2' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='Coll'>Rp <?=number_format($total2)?></a></td>
                                                      
                                                    </tr>
                                                    <?php
                                                 }
                                                 ?>
                                                <tfoot>
                                                 <tr>
                                                     <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                     <td><?=$ttlreg1?></td>
                                                     <td>Rp<?=number_format($totalcollege1)?></td>
                                                     <td><?=$ttlreg2?></td>
                                                     <td>Rp<?=number_format($totalcollege2)?></td>
                                                  
                                                 </tr>
                                                 </tfoot>

                                            </tbody>
                                        </table>
                                    </div>
                                                </div>
                                                <div class="tab-pane" id="tab12">
                                   <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasi/').$from.'/'.$to.'/PLJ'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasipdf/').$from.'/'.$to.'/PLJ'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="colreg2">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NO</th>
                                                    <th>CABANG</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 1</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 2</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 3</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                                 $n = 1;
                                                 $ttlpol1 = '';
                                                 $ttlpol2 = '';
                                                 $ttlpol3 = '';

                                                 $ttlreg1 = '';
                                                 $ttlreg2 = '';
                                                 $ttlreg3 = '';
                                                 
                                                 foreach($poltek as $row){
                                                    ?>
                                                    <tr>
                                                        <td><?=$n++?></td>
                                                        <td><?=$row->namacabang?></td>
                                                        <?php
                                                        $kocab = $row->kodecabang;
                                                        // cari yg registrasi tingkat 1
                                                        $ambil1 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'PLJ',1)->result();
                                                        foreach($ambil1 as $row1);
                                                        $total1 = $row1->total1;
                                                        $ttl1 = $row1->total2;
                                                        if($total1 == null || $total1 == 0){
                                                            $warna = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna = '';  
                                                        }

                                                        // cari yg registrasi tingkat 2
                                                        $ambil2 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'PLJ',2)->result();
                                                        foreach($ambil2 as $row2);
                                                        $total2 = $row2->total1;
                                                        $ttl2 = $row2->total2;
                                                        if($total2 == null || $total2 == 0){
                                                            $warna2 = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna2 = '';  
                                                        }

                                                        // cari yg registrasi tingkat 2
                                                        $ambil3 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'PLJ',3)->result();
                                                        foreach($ambil3 as $row3);
                                                        $total3 = $row3->total1;
                                                        $ttl3 = $row3->total2;
                                                        if($total3 == null || $total3 == 0){
                                                            $warna3 = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna3 = '';  
                                                        }

                                                         $ttlpol1 += $total1;
                                                         $ttlpol2 += $total2;
                                                         $ttlpol3 += $total3;
                                                         $ttlreg1 += $ttl1;
                                                         $ttlreg2 += $ttl2;
                                                         $ttlreg3 += $ttl3;
                                                        ?>
                                                        <td><?=$ttl1?></td>
                                                        <td style="<?=$warna?>"><a href="#" class="colreg" tingkat='1' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='PLJ'>Rp <?=number_format($total1)?></a></td>
                                                        <td><?=$ttl2?></td>
                                                        <td style="<?=$warna2?>"><a href="#" class="colreg" tingkat='2' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='PLJ'>Rp <?=number_format($total2)?></a></td>
                                                        <td><?=$ttl3?></td>
                                                        <td style="<?=$warna3?>"><a href="#" class="colreg" tingkat='3' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='PLJ'>Rp <?=number_format($total3)?></a></td>
                                                    </tr>
                                                    <?php
                                                 }
                                                 ?>
                                                <tfoot>
                                                 <tr>
                                                     <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                     <td><?=$ttlreg1?></td>
                                                     <td>Rp<?=number_format($ttlpol1)?></td>
                                                     <td><?=$ttlreg2?></td>
                                                     <td>Rp<?=number_format($ttlpol2)?></td>
                                                     <td><?=$ttlreg3?></td>
                                                     <td>Rp<?=number_format($ttlpol3)?></td>
                                                 </tr>
                                                 </tfoot>

                                            </tbody>
                                        </table>
                                    </div>
                                                </div>
                              <div class="tab-pane" id="tab13">
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasi/').$from.'/'.$to.'/group'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasipdf/').$from.'/'.$to.'/group'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="colreg3">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NO</th>
                                                    <th>CABANG</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 1</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 2</th>
                                                    <th>REGISTRAN</th>
                                                    <th>TINGKAT 3</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                                 $n = 1;
                                                 $ttlgr1 = '';
                                                 $ttlgr2 = '';
                                                 $ttlgr3 = '';

                                                 $ttlreg1 = '';
                                                 $ttlreg2 = '';
                                                 $ttlreg3 = '';
                                                 foreach($grup as $row){
                                                    ?>
                                                    <tr>
                                                        <td><?=$n++?></td>
                                                        <td><?=$row->namacabang?></td>
                                                        <?php
                                                        $kocab = $row->kodecabang;
                                                        // cari yg registrasi tingkat 1
                                                        $ambil1 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'grup',1)->result();
                                                        foreach($ambil1 as $row1);
                                                        $total1 = $row1->total1;
                                                        $ttl1 = $row1->total2;
                                                        if($total1 == null || $total1 == 0){
                                                            $warna = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna = '';  
                                                        }

                                                        // cari yg registrasi tingkat 2
                                                        $ambil2 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'grup',2)->result();
                                                        foreach($ambil2 as $row2);
                                                        $total2 = $row2->total1;
                                                        $ttl2 = $row2->total2;
                                                        if($total2 == null || $total2 == 0){
                                                            $warna2 = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna2 = '';  
                                                        }

                                                        // cari yg registrasi tingkat 2
                                                        $ambil3 = $this->Mainmodel->getRegistrasi($kocab,$from,$to,'grup',3)->result();
                                                        foreach($ambil3 as $row3);
                                                        $total3 = $row3->total1;
                                                        $ttl3 = $row3->total2;
                                                        if($total3 == null || $total3 == 0){
                                                            $warna3 = "background-color:#B2B4B2";
                                                        }else{
                                                            $warna3 = '';  
                                                        }

                                                         $ttlgr1 += $total1;
                                                         $ttlgr2 += $total2;
                                                         $ttlgr3 += $total3;
                                                        ?>
                                                        <td><?=$ttl1?></td>
                                                        <td style="<?=$warna?>"><a href="#" class="colreg" tingkat='1' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='group'>Rp <?=number_format($total1)?></a></td>
                                                        <td><?=$ttl2?></td>
                                                        <td style="<?=$warna2?>"><a href="#" class="colreg" tingkat='2' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='group'>Rp <?=number_format($total2)?></a></td>
                                                        <td><?=$ttl3?></td>
                                                        <td style="<?=$warna3?>"><a href="#" class="colreg" tingkat='3' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='group'>Rp <?=number_format($total3)?></a></td>
                                                    </tr>
                                                    <?php
                                                 }
                                                 ?>
                                                <tfoot>
                                                 <tr>
                                                     <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                     <td></td>
                                                     <td>Rp<?=number_format($ttlgr1)?></td>
                                                     <td></td>
                                                     <td>Rp<?=number_format($ttlgr2)?></td>
                                                     <td></td>
                                                     <td>Rp<?=number_format($ttlgr3)?></td>
                                                 </tr>
                                                 </tfoot>

                                            </tbody>
                                        </table>
                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>


                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>

                            <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('#colreg1').DataTable();
                                                $('#colreg2').DataTable();
                                                $('#colreg3').DataTable();
                                            });
                            </script>

                            <script type="text/javascript">
                                $(".colreg").click(function(){
                                    let tingkat = $(this).attr('tingkat');
                                    let kriteria  = $(this).attr('kriteria');
                                    let kocab = $(this).attr('kocab');
                                    let from = $(this).attr('from');
                                    let to = $(this).attr('to');
                                    $('.loading').show();
                                 $.ajax({
                                        type : "POST",
                                        url: "<?=base_url('index.php/Keuangan/detailRegistrasi')?>",
                                        data:{
                                            tingkat:tingkat,
                                            kriteria:kriteria,
                                            kocab:kocab,
                                            from:from,
                                            to:to
                                        },
                                        success: function(data){
                                            $('.loading').hide();
                                            $('.isitransaksi').html(data);
                                        }
                                    });
                                     
                                });
                            </script>