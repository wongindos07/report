 <!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                	<div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> DETAIL PIUTANG <?=substr($namacabang,7,30)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tahun?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div style="margin-bottom: 10px;float: left">
                                                      <b style="color: red;">TOTAL PIUTANG : Rp <?=number_format($total)?></b> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                          
                                                        </ul> -->

                                                        <a style="margin-left: 20px;" kriteria='<?=$kriteria?>' tahun='<?=$tahun?>' dashboard='<?=$dashboard?>' jeniscabang='<?=$jeniscabang?>' class="btn red  btn-outline" href="#" id="kembali">
                                                            Kembali
                                                            <i class="fa fa-mail-reply"></i>
                                                        </a>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column piutang1" id="piutang1">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th> NO </th>
                                                    <th> NIM </th>
                                                    <th> NAMA PESERTA DIDIK / MAHASISWA </th>
                                                    <th> JURUSAN </th>
                                                    <th> TINGKAT </th>
                                                    <th> TOTAL BIAYA </th>
                                                    <th> DISKON </th>
                                                    <th> TERBAYAR </th>
                                                    <th> JUMLAH PIUTANG</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php
                                            	$n = 1;
                                            	$detail = $this->Mainmodel->piutangNotMhs($kriteria,$tahun,$kocab)->result();
                                            	foreach($detail as $row){
                                            		$nim = $row->nim;
                                            		$kode = $row->kode;
                                            	$jurusan = $this->Mainmodel->getWheres('jurusan',array('kodejurusan'=>$kode),$kriteria)->result();
                                            	foreach($jurusan as $row2);
                                            	$namajurusan = $row2->namajurusan;

                                            		$jumlahbiaya = $row->jumlahbiaya;
                                            		$terbayar = $row->terbayar;
                                            		$total = $jumlahbiaya-$terbayar;
                                                    $disk = $row->diskon;

                                            		if($jumlahbiaya != $terbayar){
                                            	?>
                                            	<tr class="odd gradeX">
                                            		<td><?=$n++?></td>
                                            		<td><?=$nim?></td>
                                            		<td><?=$row->Nama_Mahasiswa?></td>
                                            		<td><?=$namajurusan?></td>
                                            		<td>
                                            			<?php
                                            			if($row->ket == 'Registrasi Mahasiswa Baru'){
                                            				echo '1';
                                            			}elseif($row->ket == 'Registrasi Mahasiswa Senior'){
                                            				echo '2';
                                            			}else{
                                            				echo '3';
                                            			}
                                            			?>
                                            		</td>
                                            		<td>Rp<?=number_format($jumlahbiaya)?></td>
                                                    <td>Rp<?=number_format($disk)?> </td>
                                            		<td>Rp<?=number_format($terbayar)?></td>
                                            		<td><b>Rp<?=number_format($total)?></b></td>
                                            	</tr>
                                            	<?php } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- END PORTLET -->

<script type="text/javascript">
	$("#kembali").click(function(){
		let tahun = $(this).attr('tahun');
		let dashboard = $(this).attr('dashboard');
		let kriteria = $(this).attr('kriteria');
        let jeniscabang = $(this).attr('jeniscabang');
		$('.load').show();
		$.ajax({
             type : "POST",
             url: "<?=base_url('index.php/Keuangan/getPiutangNot')?>",
             data:{
                 tahun:tahun,
                 dashboard:dashboard,
                 kriteria:kriteria,
                 jeniscabang:jeniscabang
             },
             success: function(data){
                 $('.loading').hide();
                 $('.load').hide();
                 // alert(data);
                 $('#isi').html(data);
                 if(kriteria == 'Coll'){
	                  $(".not").html(data);
	              }else{
	                  $(".notpol").html(data);
	              }
             }
         });
	})
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.piutang1').DataTable();
    });
</script>