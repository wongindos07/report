 <!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> DETAIL PIUTANG <?=substr($namacabang,7,30)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tahun?></span>
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div style="margin-bottom: 10px;float: left">
                                                      <b style="color: red;">TOTAL PIUTANG : Rp <?=number_format($total)?></b> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=$linkprint?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=$links?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                          
                                                        </ul>

                                                        <a style="margin-left: 20px;" tahun='<?=$tahun?>' tipe='<?=$tipe?>' kriteria='<?=$kriteria?>' jeniscabang='<?=$jeniscabang?>' class="btn red  btn-outline" href="#" id="kembali">
                                                            Kembali
                                                            <i class="fa fa-mail-reply"></i>
                                                        </a>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column poltek">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th> NO </th>
                                                    <th> NIM </th>
                                                    <th> NAMA PESERTA DIDIK </th>
                                                    <th> JUMLAH PIUTANG</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            	<?php
							                    	$no = 1;
							                    	foreach($detailPiutang as $rowpiutang){
                                                        $sebesar = $rowpiutang->ttlsebesar;
                                                        $terbayar = $rowpiutang->ttlterbayar;
                                                        $piutang = $sebesar - $terbayar;
                                                        $nim = $rowpiutang->nim;

                                                        
                                                        $val = array('nim' => $nim);
                                                        $getNama = $this->Mainmodel->getWheres('biodata',$val,$kriteria)->result();
                                                        foreach($getNama as $rownama);
                                                        
                                                        if($piutang == 0){

                                                        }elseif($piutang < 0){

                                                        }else{
							                    ?>
                                                <tr class="odd gradeX">
                                                    <td><?=$no++?></td>
						                    		<td><?=$nim?></td>

						                    		<td><?=$rownama->Nama_Mahasiswa?></td>
						                    		<td><a class="ambilmodal" href="#" data-toggle="modal" ta='<?=$rowpiutang->ta?>' data-target="#<?=$rowpiutang->nim?>"><b>Rp <?=number_format($piutang)?></b></a>
                                                        </td>
                                                </tr>
                                               <?php }  }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>

                            <?php
                            if($dashboard == 'dashboard'){
                            ?>
                            <!-- modal box -->
                            <?php
                              foreach($detailPiutang as $rowpiutang2){
                                $nim2 = $rowpiutang2->nim;
                                $ta2 = $rowpiutang2->ta;
                            ?>
                            <div style="position: absolute;z-index: 2;" class="modal fade modaldetail" id="<?=$rowpiutang2->nim?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <?php 

                                    $val2 = array('nim' => $nim2);
                                    $getNama2 = $this->Mainmodel->getWheres('biodata',$val2,$kriteria)->result();
                                    foreach($getNama2 as $rownama2);
                                    ?>
                                    <h5 class="modal-title" id="exampleModalLabel">Rencana Pembayaran <b><?=$rownama2->Nama_Mahasiswa?></b></h5>
                                    <button type="button" class="close" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body isimodal">
                                    
                                    <div class='portlet-body flip-scroll' style='margin-top:20px'>
                                        <table class='table table-bordered table-striped table-condensed flip-content'>
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>TANGGAL RENCANA</th>
                                                    <th>SEBESAR</th>
                                                    <th>TERBAYAR</th>
                                                    <th>CICIL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                // $detailMhs = $this->db->query("SELECT tglrencana, sebesar, terbayar, cicil FROM biaya WHERE nim='$nim2' and ta='$ta2'")->result();
                                                $detailMhs = $this->Mainmodel->detPiutang($nim2,$ta2,$kriteria)->result();
                                                foreach($detailMhs as $rowMhs){
                                                    ?>
                                                    <tr>
                                                        <td><?=$rowMhs->tglrencana?></td>
                                                        <td><?=$rowMhs->sebesar?></td>
                                                        <td><b>Rp <?=$rowMhs->terbayar?></b></td>
                                                        <td><?=$rowMhs->cicil?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>

                                  </div>
                                 
                                </div>
                              </div>
                            </div>
                            <? } ?>
                            <?php
                            }else{
                            ?>
                            <!-- modal box -->
                            <?php
                              foreach($detailPiutang as $rowpiutang2){
                                $nim2 = $rowpiutang2->nim;
                                $ta2 = $rowpiutang2->ta;
                            ?>
                            <div class="modal fade modaldetail" id="<?=$rowpiutang2->nim?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <?php 

                                    $val2 = array('nim' => $nim2);
                                    $getNama2 = $this->Mainmodel->getWheres('biodata',$val2,$kriteria)->result();
                                    foreach($getNama2 as $rownama2);
                                    ?>
                                    <h5 class="modal-title" id="exampleModalLabel">Rencana Pembayaran <b><?=$rownama2->Nama_Mahasiswa?></b></h5>
                                    <button type="button" class="close" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body isimodal">
                                    
                                    <div class='portlet-body flip-scroll' style='margin-top:20px'>
                                        <table class='table table-bordered table-striped table-condensed flip-content'>
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>TANGGAL RENCANA</th>
                                                    <th>SEBESAR</th>
                                                    <th>TERBAYAR</th>
                                                    <th>CICIL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php

                                                // $detailMhs = $this->db->query("SELECT tglrencana, sebesar, terbayar, cicil FROM biaya WHERE nim='$nim2' and ta='$ta2'")->result();
                                                $detailMhs = $this->Mainmodel->detPiutang($nim2,$ta2,$kriteria)->result();
                                                foreach($detailMhs as $rowMhs){
                                                    ?>
                                                    <tr>
                                                        <td><?=$rowMhs->tglrencana?></td>
                                                        <td><?=$rowMhs->sebesar?></td>
                                                        <td><b>Rp <?=$rowMhs->terbayar?></b></td>
                                                        <td><?=$rowMhs->cicil?></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>

                                  </div>
                                 
                                </div>
                              </div>
                            </div>
                            <? } ?>
                            <?php
                            }
                            ?>

                            

                            <input type="hidden" id="tipemhs" name="" value="<?=$tipe?>">


                            <script type="text/javascript">
                                 $(".close").click(function(){
                                        $(".modaldetail").modal('hide');
                                 });

                                $("#kembali").click(function(){
                                    let tahun = $(this).attr('tahun');
                                    let tipe  = $('#tipemhs').val();
                                    let kriteria = $(this).attr('kriteria');
                                    let jeniscabang = $(this).attr('jeniscabang');
                                 
                                    if(tipe == 'junior'){
                                        let isi = "Registrasi Mahasiswa Baru";
                                        $('.loading').show();
                                        $('.load').show();
                                         $.ajax({
                                                type : "POST",
                                                url: "<?=base_url('index.php/Keuangan/getPiutang')?>",
                                                data:{
                                                    tahun:tahun,
                                                    tipe:isi,
                                                    kriteria:kriteria,
                                                    jeniscabang:jeniscabang
                                                },
                                                success: function(data){
                                                    $('.loading').hide();
                                                    $('.load').hide();
                                                    // alert(data);
                                                    $('#isi').html(data);
                                                    $(".tk1").html(data);
                                               
                                                }
                                            });
                                    }else if(tipe == 'senior'){
                                        let isi = "Registrasi Mahasiswa Senior";
                                        $('.loading').show();
                                        $('.load').show();
                                        $.ajax({
                                                type : "POST",
                                                url: "<?=base_url('index.php/Keuangan/getPiutang')?>",
                                                data:{
                                                    tahun:tahun,
                                                    tipe:isi,
                                                    kriteria:kriteria,
                                                    jeniscabang:jeniscabang
                                                },
                                                success: function(data){
                                                    $('.loading').hide();
                                                    $('.load').hide();
                                                    // alert(data);
                                                    $('#isi').html(data);
                                                    $(".tk2").html(data);
                                                }
                                         });
                                    }else{
                                        let isi = "Registrasi Mahasiswa Tingkat 3";
                                        $('.loading').show();
                                        $('.load').show();
                                        $.ajax({
                                                type : "POST",
                                                url: "<?=base_url('index.php/Keuangan/getPiutang')?>",
                                                data:{
                                                    tahun:tahun,
                                                    tipe:isi,
                                                    kriteria:kriteria,
                                                    jeniscabang:jeniscabang
                                                },
                                                success: function(data){
                                                    $('.loading').hide();
                                                    $('.load').hide();
                                                    // alert(data);
                                                    $('#isi').html(data);    
                                                    $(".tk3").html(data);
                                                }
                                         });
                                    }
                                });
                            </script>

                            <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('.poltek').DataTable();
                                    });
                            </script>