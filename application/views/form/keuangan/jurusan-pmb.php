<style type="text/css">
    th{
        text-align: center;
        vertical-align: middle !important;
    }
    .subisi{
        background-color: #D3D5D7 !important;
    }
    .u30{
        text-align: center;
    }
</style>
<?php
foreach($dp as $dpnilai);
?>
<!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tak?></span>
                                        </div>

                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div style="margin-bottom: 10px;">
                                                      <b style="color: red;"></b> 
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                    	<input kodecabang='<?=$kocab?>' tak='<?=$tak?>' tipe='<?=$kriteria?>' jenis='<?=$jenis?>' status='<?=$status?>' ket='<?=$ket?>' type="button" value="kembali" id="kembali" class="btn red" style="float: right;margin-left: 10px;">
                                                        <?php
                                                        if($jenis == 'TK 2'){
                                                            $j = 'tk2';
                                                        }elseif($jenis == '>=30%'){
                                                            $j = 'up';
                                                        }else{
                                                            $j = 'under';
                                                        }

                                                        if($ket == 'Registrasi Mahasiswa Baru'){
                                                            $r = 'baru';
                                                        }else{
                                                            $r = 'senior';
                                                        }
                                                        ?>
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printjurusanpmb/').$kriteria.'/'.$tak.'/'.$kocab.'/'.$j.'/'.$status.'/'.$r?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/jurusanpmbpdf/').$kriteria.'/'.$tak.'/'.$kocab.'/'.$j.'/'.$status.'/'.$r?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="pmb1">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NO</th>
                                                    <th>JURUSAN </th>
                                                    <th>TOTAL REG <?=$jenis?></th>
                                                </tr>  
                                            </thead>
                                            <tbody>
                                            	<?php
                                            	$no = 1;
                                                $totalnya = '';
												foreach($jurusan as $njurusan){

													$data = array("kodejurusan"=>$njurusan->kode);
													$namajurusan = $this->Mainmodel->getWheres('jurusan',$data,$kriteria)->result();
													foreach($namajurusan as $cetnama);
													$kojur = $cetnama->kodejurusan;
													$hitungjurusan = $this->Mainmodel->jurusanPmb($kocab,$tak,$kriteria,$kojur,$status,$ket)->result();
													foreach($hitungjurusan as $cethitung);
                                                    $totalnya += $cethitung->total;
                                            	?>
                                                <tr>
                                                	<td><?=$no++?></td>
                                                	<td><?=$cetnama->namajurusan;?></td>
                                                	<td style="text-align: center"><a href="#" class="isimhsu30" tak='<?=$tak?>' tipe='<?=$kriteria?>' kodecabang='<?=$kocab?>' jenis='<?=$jenis?>' ket='<?=$ket?>' status='<?=$status?>' kojur='<?=$kojur?>'><?=number_format($cethitung->total)?></a></td>
                                                </tr>

                                                
                                                <?php } ?>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                    <td align="center"><?=number_format($totalnya)?></td>
                                                </tr>
                                                </tfoot>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>


            <script type="text/javascript">
                 $(".isimhsu30").click(function(){
                    let tahun = $(this).attr('tak');
                    let tipe  = $(this).attr('tipe');
                    let kocab = $(this).attr('kodecabang');
                    let jenis = $(this).attr('jenis');
                    let status = $(this).attr('status');
                    let ket = $(this).attr('ket');
                    let kojur = $(this).attr('kojur');
                    $('.loading').show();
                 $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/regMhsPmb')?>",
                        data:{
                            tahun:tahun,
                            tipe:tipe,
                            kocab:kocab,
                            jenis:jenis,
                            status:status,
                            ket:ket,
                            kojur:kojur
                        },
                        success: function(data){
                            $('.loading').hide();
                            $('.isipmb').html(data);
                        }
                    });
                     
                });

                 $("#kembali").click(function(){
                    let tahun = $(this).attr('tak');
                    let tipe  = $(this).attr('tipe');
                    $('.loading').show();
                 $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/regCabangPmb')?>",
                        data:{
                            tahun:tahun,
                            tipe:tipe
                        },
                        success: function(data){
                            $('.loading').hide();
                            $('.isipmb').html(data);
                        }
                    });
                     
                });

            </script>

                            <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('#pmb1').DataTable();
                                    });
                            </script>