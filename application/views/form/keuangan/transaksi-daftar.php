
<!-- BEGIN EXAMPLE TABLE PORTLET-->
 						<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
                                            <span class="caption-subject bold uppercase"> <?=strtoupper($label2)?></span>
                                        </div>

                                    </div>
                                    <div class="tabbable tabbable-tabdrop">
                                            <ul class="nav nav-pills">
                                                <li class="active">
                                                    <a href="#tab11" data-toggle="tab">College</a>
                                                </li>
                                                <li>
                                                    <a href="#tab12" data-toggle="tab">Politeknik</a>
                                                </li>
                                                <li>
                                                    <a href="#tab13" data-toggle="tab">Group</a>
                                                </li>
                                                
                                            </ul>
                                            <div class="tab-content">
                                    
                                   <div class="tab-pane active" id="tab11" style="overflow-x: auto;">
                                      <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printdaftar/').$from.'/'.$to.'/Coll'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printdaftarpdf/').$from.'/'.$to.'/Coll'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="coldaf1">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>CABANG</th>
                                                                <th>TOTAL PENDAFTAR</th>
                                                                <th>TOTAL BIAYA DAFTAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                    <?php
                                                    $n = 1;
                                                    $totaldaf1 = '';
                                                    $totalapl1 = '';
                                                    foreach($college as $row){
                                                        ?>
                                                        <tr>
                                                            <td><?=$n++?></td>
                                                            <td><?=$row->namacabang?></td>
                                                            
                                                            <?php
                                                            $kocab = $row->kodecabang;

                                                            // cari yg registrasi tingkat 1
                                                            $ambil1 = $this->Mainmodel->getDaftar($kocab,$from,$to,'Coll')->result();
                                                            foreach($ambil1 as $row1);
                                                            $total1 = $row1->totaldaftar;
                                                            $ttlaplikan = $row1->ttlaplikan;
                                                            if($total1 == null || $total1 == 0){
                                                                $warna = "background-color:#B2B4B2";
                                                            }else{
                                                                $warna = '';  
                                                            }
                                                            $totaldaf1 += $total1;
                                                            $totalapl1 += $ttlaplikan;
                                                            ?>
                                                            <td><?=$ttlaplikan?></td>
                                                            <td style="<?=$warna?>"><a href="#" class="dafcol" from='<?=$from?>' to='<?=$to?>' kocab='<?=$kocab?>' kriteria='Coll'>Rp <?=number_format($total1)?></a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                                <td><?=$totalapl1?></td>
                                                                <td>Rp <?=number_format($totaldaf1)?></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                 <div class="tab-pane" id="tab12" style="overflow-x: auto;">
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasi/').$from.'/'.$to.'/PLJ'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasipdf/').$from.'/'.$to.'/PLJ'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="coldaf2">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>CABANG</th>
                                                                <th>TOTAL PENDAFTAR</th>
                                                                <th>TOTAL BIAYA DAFTAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                    <?php
                                                    $n = 1;
                                                    $totaldaf1 = '';
                                                    $totalapl1 = '';
                                                    foreach($poltek as $row){
                                                        ?>
                                                        <tr>
                                                            <td><?=$n++?></td>
                                                            <td><?=$row->namacabang?></td>
                                                            
                                                            <?php
                                                            $kocab = $row->kodecabang;

                                                            // cari yg registrasi tingkat 1
                                                            $ambil1 = $this->Mainmodel->getDaftar($kocab,$from,$to,'PLJ')->result();
                                                            foreach($ambil1 as $row1);
                                                            $total1 = $row1->totaldaftar;
                                                            $ttlaplikan = $row1->ttlaplikan;
                                                            if($total1 == null || $total1 == 0){
                                                                $warna = "background-color:#B2B4B2";
                                                            }else{
                                                                $warna = '';  
                                                            }
                                                            $totaldaf1 += $total1;
                                                            $totalapl1 += $ttlaplikan;
                                                            ?>
                                                            <td><?=$ttlaplikan?></td>
                                                            <td style="<?=$warna?>"><a href="#" class="dafcol" from='<?=$from?>' to='<?=$to?>' kocab='<?=$kocab?>' kriteria='PLJ'>Rp <?=number_format($total1)?></a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                                <td><?=$totalapl1?></td>
                                                                <td>Rp <?=number_format($totaldaf1)?></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                <div class="tab-pane" id="tab13" style="overflow-x: auto;">
                                     <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasi/').$from.'/'.$to.'/group'?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printregistrasipdf/').$from.'/'.$to.'/group'?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                   <table class="table table-striped table-bordered table-hover table-checkable order-column" id="coldaf3">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>CABANG</th>
                                                                <th>TOTAL PENDAFTAR</th>
                                                                <th>TOTAL BIAYA DAFTAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                    <?php
                                                    $n = 1;
                                                    $totaldaf1 = '';
                                                    $totalapl1 = '';
                                                    foreach($grup as $row){
                                                        ?>
                                                        <tr>
                                                            <td><?=$n++?></td>
                                                            <td><?=$row->namacabang?></td>
                                                            
                                                            <?php
                                                            $kocab = $row->kodecabang;

                                                            // cari yg registrasi tingkat 1
                                                            $ambil1 = $this->Mainmodel->getDaftar($kocab,$from,$to,'grup')->result();
                                                            foreach($ambil1 as $row1);
                                                            $total1 = $row1->totaldaftar;
                                                            $ttlaplikan = $row1->ttlaplikan;
                                                            if($total1 == null || $total1 == 0){
                                                                $warna = "background-color:#B2B4B2";
                                                            }else{
                                                                $warna = '';  
                                                            }
                                                            $totaldaf1 += $total1;
                                                            $totalapl1 += $ttlaplikan;
                                                            ?>
                                                            <td><?=$ttlaplikan?></td>
                                                            <td style="<?=$warna?>"><a href="#" class="dafcol" from='<?=$from?>' to='<?=$to?>' kocab='<?=$kocab?>' kriteria='group'>Rp <?=number_format($total1)?></a></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="2" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                                <td><?=$totalapl1?></td>
                                                                <td>Rp <?=number_format($totaldaf1)?></td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                                


                                            </div>
                                        </div>


                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>

<script type="text/javascript">

             $(".dafcol").click(function(){
                    let from = $(this).attr('from');
                    let to  = $(this).attr('to');
                    let kocab = $(this).attr('kocab');
                    let kriteria = $(this).attr('kriteria');
                    $('.loading').show();
                 $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/daftarAplikan')?>",
                        data:{
                            from:from,
                            to:to,
                            kocab:kocab,
                            kriteria:kriteria
                        },
                        success: function(data){
                            $('.loading').hide();
                            $('.isitransaksi').html(data);
                        }
                    });
                     
                });
</script>

                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('#coldaf1').DataTable();
                                                $('#coldaf2').DataTable();
                                                $('#coldaf3').DataTable();
                                            });
                                        </script>