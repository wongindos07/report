                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
                                            <span class="caption-subject bold uppercase"> <?=strtoupper($label2)?></span>
                                        </div>

                                    </div>

                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <input from='<?=$from?>' to='<?=$to?>' type="button" value="kembali" id="kembali" class="btn red" style="float: right;margin-left: 10px;">

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printDetailRegistrasi/').$from.'/'.$to.'/'.$kriteria.'/'.$kodecabang.'/'.$tingkat?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printDetailRegistrasiPdf/').$from.'/'.$to.'/'.$kriteria.'/'.$kodecabang.'/'.$tingkat?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF</a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="myTable">
                                        <thead>
                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                <th>NO</th>
                                                <th>NIM</th>
                                                <th>NAMA</th>
                                                <th>TGL BAYAR</th>
                                                <th>JUMLAH BAYAR</th>
                                                <th>POTONGAN</th>
                                                <th>DISKON</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <?php
                                                $n = 1;
                                                $totalterbayar = '';
                                                $ttlpotongan = '';
                                                $ttldiskon = '';
                                                foreach($registrasi as $row){
                                                ?>
                                                <td><?=$n++?></td>
                                                <td><?=$row->nim?></td>
                                                <td><?=$row->Nama_Mahasiswa?></td>
                                                <td><?=$row->deo_tg?></td>
                                                <td><?=$row->terbayar?></td>
                                                <td><?=$row->potongan?></td>
                                                <td><?=$row->diskon?></td>
                                            </tr>
                                            <?php 
                                                $terbayar = $row->terbayar;
                                                $potongan = $row->potongan;
                                                $diskon = $row->diskon;
                                                $totalterbayar += $terbayar;
                                                $ttlpotongan += $potongan;
                                                $ttldiskon += $diskon;
                                            } 

                                            ?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td colspan="4" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                <td><?php
                                                if($totalterbayar == null){
                                                    echo 0;
                                                }else{
                                                    echo number_format($totalterbayar);
                                                }
                                                
                                                ?>
                                                </td>
                                                <td><?php
                                                if($ttlpotongan == null){
                                                    echo 0;
                                                }else{
                                                    echo number_format($ttlpotongan);
                                                }
                                                
                                                ?>
                                                </td>
                                                <td><?php
                                                if($ttldiskon == null){
                                                    echo 0;
                                                }else{
                                                    echo number_format($ttldiskon);
                                                }
                                                
                                                ?>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <script type="text/javascript">
                                $("#kembali").click(function(){
                                    let from = $(this).attr('from');
                                    let to = $(this).attr('to');
                                    $('.loading').show();
                                 $.ajax({
                                        type : "POST",
                                        url: "<?=base_url('index.php/Keuangan/transaksiRegistrasi')?>",
                                        data:{
                                            from:from,
                                            to:to
                                        },
                                        success: function(data){
                                            $('.loading').hide();
                                            $('.isitransaksi').html(data);
                                        }
                                    });
                                     
                                });
                            </script>

                        <script type="text/javascript">
                            $(document).ready(function(){
                                 $('#myTable').DataTable();
                            });
                        </script>

                        