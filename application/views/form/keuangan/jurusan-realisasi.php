 <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="caption font-dark">
                                                     <span class="caption-subject bold uppercase"> DETAIL TRANSAKSI PEMBAYARAN <?=substr($namacabang,7,30)?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="caption font-dark" style="float: right">
                                                    <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tahun?></span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-6">
                                                <div class="caption font-dark">
                                                     <span class="caption-subject bold uppercase"> JURUSAN <?=$jurusan?></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="caption font-dark" style="float: right">
                                                    
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">

                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="#>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                          
                                                        </ul> -->

                                                        <a style="margin-left: 20px;" ta='<?=$ta?>' from='<?=$from?>' kriteria='<?=$kriteria?>' to='<?=$to?>' kocab='<?=$kocab?>' class="btn red  btn-outline" href="#" id="kembali2">
                                                            Kembali
                                                            <i class="fa fa-mail-reply"></i>
                                                        </a>
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="piutang1">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th> NO </th>
                                                    <th> NIM </th>
                                                    <th> NAMA </th>
                                                    <th> JURUSAN </th>
                                                    <th> TINGKAT </th>
                                                    <th> TOTAL TERBAYAR </th>
                                                    <th> SUMBER PENERIMAAN</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $no=1;
                                                $totalnya = '';
                                                foreach ($mahasiswa as $row){
                                                    $nim = $row->nim;
                                                    $kojur = $row->kode;
                                                    $realisasi = $row->total;

                                                    $totalnya += $realisasi;
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td><?=$no++?></td>
                                                    <td><?=$nim?></td>
                                                    <td><?=$row->Nama_Mahasiswa?></td>
                                                    <td>
                                                        <?php
                                                        $val = array("kodejurusan"=>$kojur);
                                                        $ambiljurusan = $this->Mainmodel->getWheres('jurusan',$val,$kriteria)->result();
                                                        foreach($ambiljurusan as $najur);
                                                        ?>
                                                        <a href="#" ta='<?=$tahun?>' kocab='<?=$kocab?>' from='<?=$from?>' to='<?=$to?>' kriteria='<?=$kriteria?>' kojur='<?=$kojur?>' class='jurusan' title="Klik Untuk Detail Per Jurusan">
                                                            <?=$najur->namajurusan?>
                                                        </a>
                                                    </td>
                                                    <td><?=$row->tingkat?></td>
                                                    <td><a class="ambilmodal" href="#" data-toggle="modal" data-target="#<?=$row->nim?>"><b>Rp <?=number_format($realisasi)?></b></a>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $detailbayar = $this->Mainmodel->detBayar($nim,$tahun,$kriteria,$kocab,$from,$to)->result();
                                                        foreach($detailbayar as $rowdetail){
                                                        $kdakun = $rowdetail->akun;
                                                        if($kdakun == '' || $kdakun == 0){
                                                            echo "<code>Undefined</code>,";
                                                        }else{
                                                        $dataakun1 = $this->Mainmodel->getWheres('perkiraan',array('kdakun'=>$kdakun,'kodecabang'=>$kocab),$kriteria)->result();
                                                        foreach($dataakun1 as $rowakun1){
                                                        if($dataakun1 == false){
                                                               $dat = "Undefined";
                                                            }else{
                                                                if($kdakun == '' || $kdakun == 0){
                                                                    $dat = "Undefined";
                                                                }else{
                                                                    $dat = $rowakun1->perkiraan;
                                                                }
                                                                
                                                            }
                                                        ?>
                                                        <code><?=$dat?></code>,
                                                        <?php } } } ?>

                                                    </td>
                                                </tr>
                                                <?php
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                            <tr>
                                                              <tr>
                                                                  <td colspan="5" class='bg-grey-gallery bg-font-grey-gallery'>
                                                                      TOTAL
                                                                  </td>
                                                                  <td>
                                                                    <?php
                                                                        if($totalnya == ''){

                                                                        }else{
                                                                            echo 'Rp '.number_format($totalnya);
                                                                        }
                                                                    ?>
                                                                  </td>
                                                              </tr>
                                                            </tr>
                                                        </tfoot>
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>


                             <!-- modal box -->
                            <?php
                              foreach($mahasiswa as $row2){
                                $nim2 = $row2->nim;
                                $realisasi2 = $row2->total;
                                $tak = $row2->tahunakademik;
                            ?>
                            <div class="modal fade" id="<?=$row2->nim?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <?php 

                                    $val2 = array('nim' => $nim2);
                                    $getNama2 = $this->Mainmodel->getWheres('biodata',$val2,$kriteria)->result();
                                    foreach($getNama2 as $rownama2);
                                    ?>
                                    <h5 class="modal-title" id="exampleModalLabel">Rencana Pembayaran <b><?=$rownama2->Nama_Mahasiswa?></b></h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      <span aria-hidden="true">&times;</span>
                                    </button>
                                  </div>
                                  <div class="modal-body isimodal">
                                    
                                    <div class='portlet-body flip-scroll' style='margin-top:20px'>
                                        <table class='table table-bordered table-striped table-condensed flip-content'>
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NO BUKTI</th>
                                                    <th>TANGGAL BAYAR</th>
                                                    <th>CICIL</th>
                                                    <th>JENIS TRANSAKSI</th>
                                                    <th>JUMLAH BAYAR</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                               <?php

                                                $detailMhs = $this->Mainmodel->detBayar($nim2,$tak,$kriteria,$kocab,$from,$to)->result();
                                                $ttlbayar = '';
                                                foreach($detailMhs as $rowMhs){
                                                    $bayar = $rowMhs->jumlahbayar;
                                                    $ttlbayar += $bayar;
                                                    $akun = $rowMhs->akun
                                                    ?>
                                                    <tr>
                                                        <td><?=$rowMhs->nobukti?></td>
                                                        <td><?=$rowMhs->tanggalbayar?></td>
                                                        <td><?=$rowMhs->cicil?></td>
                                                        <td>
                                                            <?php
                                                            $dataakun = $this->Mainmodel->getWheres('perkiraan',array('kdakun'=>$akun,'kodecabang'=>$kocab),$kriteria)->result();
                                                            foreach($dataakun as $rowakun);
                                                            if($dataakun == false){
                                                                $dat = "Undefined";
                                                            }else{
                                                                if($kdakun == '' || $kdakun == '0'){
                                                                    $dat = "Undefined";
                                                                }else{
                                                                    $dat = $rowakun->perkiraan;
                                                                }
                                                                
                                                            }

                                                            echo $dat;
                                                            
                                                            ?>
                                                        </td>
                                                        <td><b>Rp <?=number_format($rowMhs->jumlahbayar)?></b></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td colspan="4">Total</td>
                                                    <td>Rp<?=number_format($ttlbayar)?></td>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                  </div>
                                 
                                </div>
                              </div>
                            </div>
                            <? } ?>

                       

                            <script type="text/javascript">
                                $('.jurusan').click(function(){
                                    let ta = $(this).attr('ta');
                                    let from = $(this).attr('from');
                                    let to = $(this).attr('to');
                                    let kocab = $(this).attr('kocab');
                                    let kriteria = $(this).attr('kriteria');
                                    let kojur = $(this).attr('kojur');
                                    $('.loading').show();
                                    $.ajax({
                                                    type : "POST",
                                                    url: "<?=base_url('index.php/Keuangan/trxJurusanRealisasi')?>",
                                                    data:{
                                                        ta:ta,
                                                        from:from,
                                                        kocab:kocab,
                                                        to:to,
                                                        kriteria:kriteria,
                                                        kojur:kojur
                                                    },
                                                    success: function(data){
                                                        $('.loading').hide();
                                                        $('.isitransaksi').html(data);
                                                    }
                                                });
                                });

                                $('#kembali2').click(function(){
                                    let ta = $(this).attr('ta');
                                    let from = $(this).attr('from');
                                    let to = $(this).attr('to');
                                    let kocab = $(this).attr('kocab');
                                    let kriteria = $(this).attr('kriteria');
                                    $('.loading').show();
                                    $.ajax({
                                                    type : "POST",
                                                    url: "<?=base_url('index.php/Keuangan/transaksiRealisasi')?>",
                                                    data:{
                                                        ta:ta,
                                                        from:from,
                                                        kocab:kocab,
                                                        to:to,
                                                        kriteria:kriteria
                                                    },
                                                    success: function(data){
                                                        $('.loading').hide();
                                                        $('.isitransaksi').html(data);
                                                    }
                                                });
                                });
                            </script>
                           

                            <script type="text/javascript">
                                    $(document).ready(function(){
                                        $('#piutang1').DataTable();
                                    });
                            </script>