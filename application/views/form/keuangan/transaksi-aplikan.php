<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
                                            <span class="caption-subject bold uppercase"> <?=strtoupper($label2)?></span>
                                        </div>

                                    </div>

<div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <input from='<?=$from?>' to='<?=$to?>' type="button" value="kembali" id="kembali1" class="btn red" style="float: right;margin-left: 10px;">

                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printdaftaraplikan/').$from.'/'.$to.'/'.$kriteria.'/'.$kodecabang?>">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="<?=base_url('index.php/Keuangan/printdaftaraplikanpdf/').$from.'/'.$to.'/'.$kriteria.'/'.$kodecabang?>">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF</a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

<table class="table table-striped table-bordered table-hover table-checkable order-column" id="myTable">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>NAMA APLIKAN</th>
                                                                <th>JURUSAN DIAMBIL</th>
                                                                <th>TANGGAL DAFTAR</th>
                                                                <th>GELOMBANG DAFTAR</th>
                                                                <th>BIAYA DAFTAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $n = 1;
                                                            $total = '';
                                                            foreach($aplikan as $row){
                                                                $kojur = $row->KdJurusan;
                                                                ?>  
                                                                <tr>
                                                                    <td><?=$n++?></td>
                                                                    <td><?=$row->Nama?></td>
                                                                    <?php
                                                                    $jurusan = $this->Mainmodel->getWheres('jurusan',array('kodejurusan'=>$kojur),$kriteria)->result();
                                                                    foreach($jurusan as $rjur);
                                                                    $namajurusan = $rjur->namajurusan;
                                                                    ?>
                                                                    <td><?=$namajurusan?></td>
                                                                    <td><?=$row->TglDaftar?></td>
                                                                    <td><?=$row->Gelombang?></td>
                                                                    <td>Rp <?=number_format($row->total)?></td>
                                                                </tr>
                                                                <?php
                                                                $ttl = $row->total;
                                                                $total += $ttl;
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                                <td>
                                                                    <!-- Rp <?=number_format($total)?> -->
                                                                <?php
                                                                if($total == null){
                                                                    echo 0;
                                                                }else{
                                                                    echo 'Rp '.number_format($total);
                                                                }
                                                                ?>    
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

<script type="text/javascript">

             $("#kembali1").click(function(){
                    let from = $(this).attr('from');
                    let to  = $(this).attr('to');
                    $('.loading').show();
                 $.ajax({
                        type : "POST",
                        url: "<?=base_url('index.php/Keuangan/transaksiDaftar')?>",
                        data:{
                            from:from,
                            to:to
                        },
                        success: function(data){
                            $('.loading').hide();
                            $('.isitransaksi').html(data);
                        }
                    });
                     
                });
</script>

                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                                $('#myTable').DataTable();
                                            });
                                        </script>