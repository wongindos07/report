<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tahun?></span>
                                        </div>

                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>

                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
				<table class='table table-bordered table-striped table-condensed flip-content piutang1' id="piutang1">
					<thead>
                        <tr class='bg-grey-gallery bg-font-grey-gallery'>
                            <th>N0</th>
                            <?php
                            if($kriteria == 'Coll'){
                            	$cabangr = 'CABANG';
                            }else{
                            	$cabangr = 'KAMPUS';
                            }
                            ?>
                            <th><?=$cabangr?></th>
                            <th>JUMLAH PIUTANG</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
					<tbody>
<?php
$totalpiutang = 0;
$no = 1;
foreach($cabang as $rcabang){
	$kocab = $rcabang->kodecabang;

	if($kriteria == 'Coll'){
		$ambilpiutang = $this->Mainmodel->ambilpiutangcol($tipe,$tahun,$tahunangkatan,$kocab,$waktu)->result();
		if($ambilpiutang == null){

		}else{
			foreach($ambilpiutang as $rowpiutang);
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;

			$sepuluh = ($totterbayar * 10) / 100;
			$lima = ($totterbayar * 5) / 100;
			if($totpiutang > $sepuluh){
				$warna = '#D04556';
				$color = '#FDFDFE';
			}elseif($totpiutang <= $sepuluh and $totpiutang > $lima){
				$warna = '#DFD43B';
				$color = '';
			}else{
				$warna = '#3DDA42';
				$color = '';
			}

			$totalpiutang = $totalpiutang += $totpiutang;
			if($tipe == "Registrasi Mahasiswa Baru"){
				$link = "<a class='btn red btn-outline bold detailpiutang' kriteria='Coll' href='#' tahun='$tahun' kocab='$kocab' tipe='junior' dashboard='$dashboard' jeniscabang='$jeniscabang'>Detail</a>";
			}else{
				$link = "<a class='btn red btn-outline bold detailpiutang' kriteria='Coll' href='#' tahun='$tahun' kocab='$kocab' tipe='senior' dashboard='$dashboard' jeniscabang='$jeniscabang'>Detail</a>";
			}
			

			echo "			
						<tr>
							<td>".$no++."</td>
							<td>".substr($rcabang->namacabang,7,100)."</td>
							<td style='background-color:$warna;color:$color'><b>Rp ".number_format($totpiutang)."</b></td>
							<td><center>$link</center></td>
						</tr>
			";	
		}
		
	}elseif($kriteria == 'PLJ'){
		$ambilpiutang = $this->Mainmodel->ambilpiutangpol($tipe,$tahun,$tahunangkatan,$kocab,$waktu)->result();
		if($ambilpiutang == null){

		}else{
			foreach($ambilpiutang as $rowpiutang);
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;

			$sepuluh = ($totterbayar * 10) / 100;
			$lima = ($totterbayar * 5) / 100;
			if($totpiutang > $sepuluh){
				$warna = '#D04556';
				$color = '#FDFDFE';
			}elseif($totpiutang <= $sepuluh and $totpiutang > $lima){
				$warna = '#DFD43B';
				$color = '';
			}else{
				$warna = '#3DDA42';
				$color = '';
			}

			$totalpiutang = $totalpiutang += $totpiutang;
			if($tipe == "Registrasi Mahasiswa Baru"){
				$link = "<a class='btn red btn-outline bold detailpiutang' kriteria='PLJ' href='#' tahun='$tahun' kocab='$kocab' tipe='junior' dashboard='$dashboard' jeniscabang='$jeniscabang'>Detail</a>";
			}elseif($tipe == "Registrasi Mahasiswa Senior"){
				$link = "<a class='btn red btn-outline bold detailpiutang' kriteria='PLJ' href='#' tahun='$tahun' kocab='$kocab' tipe='senior' dashboard='$dashboard' jeniscabang='$jeniscabang'>Detail</a>";
			}else{
				$link = "<a class='btn red btn-outline bold detailpiutang' kriteria='PLJ' href='#' tahun='$tahun' kocab='$kocab' tipe='poltek' dashboard='$dashboard' jeniscabang='$jeniscabang'>Detail</a>";
			}
			

			echo "			
						<tr>
							<td>".$no++."</td>
							<td>".substr($rcabang->namacabang,7,50)."</td>
							<td style='background-color:$warna;color:$color'><b>Rp ".number_format($totpiutang)."</b></td>
							<td><center>$link</center></td>
						</tr>
			";	
		}
	}else{
		$ambilpiutang = $this->Mainmodel->ambilpiutangcol($tipe,$tahun,$tahunangkatan,$kocab,$waktu)->result();
		if($ambilpiutang == null){

		}else{
			foreach($ambilpiutang as $rowpiutang);
			$totsebesar = $rowpiutang->ttlsebesar;
			$totterbayar = $rowpiutang->ttlterbayar;
			$totpiutang = $totsebesar - $totterbayar;

			$sepuluh = ($totterbayar * 10) / 100;
			$lima = ($totterbayar * 5) / 100;
			if($totpiutang > $sepuluh){
				$warna = '#D04556';
				$color = '#FDFDFE';
			}elseif($totpiutang <= $sepuluh and $totpiutang > $lima){
				$warna = '#DFD43B';
				$color = '';
			}else{
				$warna = '#3DDA42';
				$color = '';
			}

			$totalpiutang = $totalpiutang += $totpiutang;
			if($tipe == "Registrasi Mahasiswa Baru"){
				$link = "<a class='btn red btn-outline bold detailpiutang' kriteria='group' href='#' tahun='$tahun' kocab='$kocab' tipe='junior' dashboard='$dashboard' jeniscabang='$jeniscabang'>Detail</a>";
			}else{
				$link = "<a class='btn red btn-outline bold detailpiutang' kriteria='group' href='#' tahun='$tahun' kocab='$kocab' tipe='senior' dashboard='$dashboard' jeniscabang='$jeniscabang'>Detail</a>";
			}
			

			echo "			
						<tr>
							<td>".$no++."</td>
							<td>".substr($rcabang->namacabang,7,50)."</td>
							<td style='background-color:$warna;color:$color'><b>Rp ".number_format($totpiutang)."</b></td>
							<td><center>$link</center></td>
						</tr>
			";	
		}
	}
	
}
?>
					<tfoot>
						<tr>
							<td colspan='2' class='bg-grey-gallery bg-font-grey-gallery'><b>TOTAL</b></td>
							<td><b>Rp <?=number_format($totalpiutang)?></b></td>
							<td></td>
						</tr>
					</tfoot>		
					</tbody>
				</table>
			</div>
		</div>
                                <!-- END EXAMPLE TABLE PORTLET-->
	</div>
</div>

<script type="text/javascript">
	$(".detailpiutang").click(function(){
                    let tahun = $(this).attr('tahun');
                    let kocab = $(this).attr('kocab');
                    let tipe = $(this).attr('tipe');
                    let kriteria = $(this).attr('kriteria');
                    let dashboard = $(this).attr('dashboard');
                    let jeniscabang = $(this).attr('jeniscabang');

                    $('.loading').show();
                    $('.load').show();
                    if(tipe == "junior"){
	                    $.ajax({
	                        type : "POST",
	                        url  : "<?=base_url('index.php/Keuangan/getDetailPiutangJunior')?>",
	                        data : {
	                            tahun:tahun,
	                            kocab:kocab,
	                            kriteria:kriteria,
	                            tipe:tipe,
	                            dashboard:dashboard,
                                jeniscabang:jeniscabang
	                        },
	                        success : function(data){
	                            $('.loading').hide();
	                            $('.load').hide();
	                            $("#isi").html(data);

	                            if(kriteria == 'Coll'){
	                            	$(".tk1col").html(data);
	                        	}else{
	                        		$(".tk1").html(data);
	                        	}
	                        }
	                    });
                    }else if(tipe == "senior"){
                    	
                    	$.ajax({
	                        type : "POST",
	                        url  : "<?=base_url('index.php/Keuangan/getDetailPiutangSenior')?>",
	                        data : {
	                            tahun:tahun,
	                            kocab:kocab,
	                            kriteria:kriteria,
	                            tipe:tipe,
	                            dashboard:dashboard,
                                jeniscabang:jeniscabang
	                        },
	                        success : function(data){
	                            $('.loading').hide();
	                            $('.load').hide();
	                            $("#isi").html(data);
	                      
	                            if(kriteria == 'Coll'){
	                            	$(".tk2col").html(data);
	                        	}else{
	                        		$(".tk2").html(data);
	                        	}
	                          
	                        }
	                    });
                    }else{

                    	$.ajax({
	                        type : "POST",
	                        url  : "<?=base_url('index.php/Keuangan/getDetailPiutangPoltek')?>",
	                        data : {
	                            tahun:tahun,
	                            kocab:kocab,
	                            kriteria:kriteria,
	                            tipe:tipe,
	                            dashboard:dashboard,
                                jeniscabang:jeniscabang
	                        },
	                        success : function(data){
	                            $('.loading').hide();
	                            $('.load').hide();
	                            $("#isi").html(data);
	                            $(".tk3").html(data);
	                        }
	                    });
                    }
                    

                });
</script>

										<script type="text/javascript">
                                            $(document).ready(function(){
                                                $('.piutang1').DataTable();
                                            });
                                        </script>