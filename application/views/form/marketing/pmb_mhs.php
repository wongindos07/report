<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                	<div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tahun?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                    	<div class="row">
                                                <div class="col-md-6">
                                                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>
 -->
                                                        <a style="margin-left: 20px;" tahun='<?=$tahun?>' kriteria='<?=$kriteria?>' jeniscabang='<?=$jeniscabang?>' ket='<?=$ket?>' keterangan="<?=$keterangan?>" class="btn red  btn-outline kembali" href="#">
                                                            Kembali
                                                            <i class="fa fa-mail-reply"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>


                                            <table class='table table-bordered table-striped table-condensed flip-content pmbu300' id="pmbu300">
                                            	<thead>
                                            		<tr class='bg-grey-gallery bg-font-grey-gallery'>
                                            			<th>NO</th>
                                            			<th>NAMA</th>
                                            			<th>JURUSAN</th>
                                                        <th>BIAYA KULIAH</th>
                                                        <th>POTONGAN</th>
                                                        <th>TOTAL BIAYA</th>
                                                        <th>TERBAYAR</th>
                                                        <th>DISKON</th>
                                                        <th>PRESENTER</th>
                                                        <th>KETERANGAN BAYAR</th>
                                            		</tr>
                                            	</thead>
                                            	<tbody>
                                                   <?php
                                                   $n=1;
                                                   $ttlbiaya = "";
                                                   $ttlpotongan = "";
                                                   $ttl = "";
                                                   $ttlbayar = "";
                                                   $ttldiskon = "";
                                                   foreach($aplikan as $row){
                                                    $kojur = $row->KdJurusan;
                                                    $nim = $row->nim;
                                                    $kodepres = $row->KdPresenter;
                                                   ?>
                                                    <tr>
                                                		<td><?=$n++?></td>
                                                		<td><?=$row->Nama_Mahasiswa?></td>
                                                		<td>
                                                            <a href="#" class="pmbjurusan" tahun="<?=$tahun?>" ket="<?=$ket?>" keterangan="<?=$keterangan?>" jeniscabang="<?=$jeniscabang?>" kriteria="<?=$kriteria?>" kojur="<?=$kojur?>" kocab="<?=$kocab?>" title="Klik untuk cari berdasarkan jurusan">
                                                            <?php
                                                            $datajur = $this->Mainmodel->getWheres('jurusan',array('kodejurusan'=>$kojur),$kriteria)->result();
                                                            foreach($datajur as $rowjur);
                                                            echo $rowjur->namajurusan;
                                                            ?> 
                                                            </a>     
                                                        </td>
                                                        <?php
                                                        $databiaya = $this->Mainmodel->getWheres('biayakuliah',array('nim'=>$nim,'tahunakademik'=>$tahun),$kriteria)->result();
                                                        foreach($databiaya as $rowbiaya);
                                                        $uangkuliah = $rowbiaya->uangkuliah;
                                                        $potongan = $rowbiaya->potongan;
                                                        $totalbiaya = $rowbiaya->jumlahbiaya;
                                                        $terbayar = $rowbiaya->terbayar;
                                                        $diskon = $rowbiaya->diskon;
                                                        $bayar2 = $terbayar + $diskon;

                                                        $ttlbiaya += $uangkuliah;
                                                        $ttlpotongan += $potongan;
                                                        $ttl += $totalbiaya;
                                                        $ttlbayar += $terbayar;
                                                        $ttldiskon += $diskon;

                                                        if($terbayar == 0){
                                                            $keteranganx = "BELUM BAYAR";
                                                        }else{
                                                           if($totalbiaya == $bayar2){
                                                            $keteranganx = "CASH";
                                                            }else{
                                                                $keteranganx = "CICIL";
                                                            } 
                                                        }
                                                        
                                                        ?>
                                                        <td><?=number_format($rowbiaya->uangkuliah)?></td>
                                                        <td><?=number_format($rowbiaya->potongan)?></td>
                                                        <td><?=number_format($totalbiaya)?></td>
                                                        <td><?=number_format($terbayar)?></td>
                                                        <td><?=number_format($rowbiaya->diskon)?></td>
                                                        <td>
                                                            <?php
                                                            $datapres = $this->Mainmodel->getWheres("presenter",array("KdPresenter"=>$kodepres),$kriteria)->result();
                                                            if($datapres == false){
                                                                echo "undefined";
                                                            }else{
                                                                foreach($datapres as $rowpres);
                                                                 echo $rowpres->Presenter;
                                                            }
                                                            
                                                            ?>
                                                        </td>
                                                        <td><?=$keteranganx?></td>
                                                    </tr>
                                                    <?php } ?>
                                            	</tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td class='bg-grey-gallery bg-font-grey-gallery' colspan="3"></td>
                                                        <td><?=number_format($ttlbiaya)?></td>
                                                        <td><?=number_format($ttlpotongan)?></td>
                                                        <td><?=number_format($ttl)?></td>
                                                        <td><?=number_format($ttlbayar)?></td>
                                                        <td><?=number_format($ttldiskon)?></td>
                                                        <td colspan="2"></td>
                                                    </tr>
                                                </tfoot>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        $('.pmbu300').DataTable();

        $(".pmbjurusan").click(function(){
            let keterangan = $(this).attr("keterangan");
            let tahun = $(this).attr("tahun");
            let ket = $(this).attr("ket");
            let jeniscabang = $(this).attr("jeniscabang");
            let kriteria = $(this).attr("kriteria");
            let kocab = $(this).attr("kocab");
            let kojur = $(this).attr("kojur");
            $(".load").show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Marketing/detailMhsPmbJurusan')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            ket:ket,
                            jeniscabang:jeniscabang,
                            keterangan:keterangan,
                            kocab : kocab,
                            kojur:kojur

                        },
                        success : function(data){
                            $('.load').hide();

                            if(kriteria == 'Coll'){
                                if(keterangan == 'U-30'){
                                    $('.isiu30').html(data);
                                }else{
                                    $('.isia30').html(data);
                                }
                            }else{
                                if(keterangan == 'U-30'){
                                    $('.isiu30pol').html(data);
                                }else{
                                    $('.isia30pol').html(data);
                                }
                            }
                            
                        }
                    });
        });

        $('.kembali').click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let ket = $(this).attr('ket');
            let jeniscabang = $(this).attr('jeniscabang');
            let keterangan = $(this).attr("keterangan");
            $('.load').show();
                $.ajax({
                                type : "POST",
                                url: "<?=base_url('index.php/Marketing/detailCab')?>",
                                data:{
                                    tahun:tahun,
                                    kriteria:kriteria,
                                    ket:ket,
                                    jeniscabang,
                                    keterangan:keterangan
                                },
                                success: function(data){
                                    $('.load').hide();
                                    if(kriteria == 'Coll'){
                                        if(keterangan == 'U-30'){
                                                 $('.isiu30').html(data);
                                            }else{
                                                $('.isia30').html(data);
                                            }
                                        }else{
                                            if(keterangan == 'U-30'){
                                                $('.isiu30pol').html(data);
                                            }else{
                                                $('.isia30pol').html(data);
                                            }
                                        }
                                }
                            });
            });
    });
</script>