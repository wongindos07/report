<div class="row">
                            <div class="col-md-12">
                                <div class="portlet light bordered">
                                	<div class="portlet-title">
                                        <div class="caption font-dark">
                                             <span class="caption-subject bold uppercase"> <?=strtoupper($label)?></span>
                                        </div>
                                        <div class="caption font-dark" style="float: right">
								            <span class="caption-subject bold uppercase"> TAHUN AKADEMIK <?=$tahun?></span>
                                        </div>
                                    </div>
                                    <div class="portlet-body" style="overflow-x: auto;">
                                    	<div class="row">
                                                <div class="col-md-6">
                                                   
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="btn-group pull-right">
                                                        <!-- <button class="btn green  btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                            <i class="fa fa-angle-down"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right">
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-print"></i> Print </a>
                                                            </li>
                                                            <li>
                                                                <a target="_blank" href="#">
                                                                    <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                            </li>
                                                        </ul>
 -->
                                                      
                                                    </div>
                                                </div>
                                            </div>


                                            <table class='table table-bordered table-striped table-condensed flip-content pmbu300' id="pmbu300">
                                            	<thead>
                                            		<tr class='bg-grey-gallery bg-font-grey-gallery'>
                                            			<th>NO</th>
                                            			<th>CABANG </th>
                                            			<th>JUMLAH</th>
                                            			
                                            		</tr>
                                            	</thead>
                                            	<tbody>
                                                    <?php
                                                    $n = 1;
                                                    $totalx = "";
                                                    foreach($u30 as $row1){
                                                        $regsah = $row1->registrasisah;
                                                        $kocab = $row1->kodecabang;
                                                        $totalx += $regsah;
                                                        if($regsah == 0){
                                                            $warna = '#D04556';
                                                            $color = '#FDFDFE';
                                                            $none = 'none';
                                                        }else{
                                                            $warna = '';
                                                            $color = '';
                                                            $none = '';
                                                        }
                                                    ?>
                                                    <tr>
                                                		<td><?=$n++?></td>
                                                		<td><?=substr($row1->namacabang,7,40)?></td>
                                                		<td style="background-color: <?=$warna?>;">
                                                            <a href="#" class="pmbmhs" keterangan='<?=$keterangan?>' tahun='<?=$tahun?>' ket='<?=$ket?>' jeniscabang='<?=$jeniscabang?>' kriteria='<?=$kriteria?>' kocab='<?=$kocab?>' style="text-decoration: <?=$none?>;color: <?=$color?>"><?=$regsah?></a>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                            	</tbody>

                                                <tfoot>
                                                    <tr>
                                                        <td class='bg-grey-gallery bg-font-grey-gallery' colspan="2">TOTAL</td>
                                                        <td><?=$totalx?></td>
                                                    </tr>
                                                </tfoot>
                                            </table>


                                        </div>
                                    </div>
                                </div>
                            </div>
</div>



<script type="text/javascript">
    $(document).ready(function(){
        $('.pmbu300').DataTable();


        $(".pmbmhs").click(function(){
            let keterangan = $(this).attr("keterangan");
            let tahun = $(this).attr("tahun");
            let ket = $(this).attr("ket");
            let jeniscabang = $(this).attr("jeniscabang");
            let kriteria = $(this).attr("kriteria");
            let kocab = $(this).attr("kocab");
            $(".load").show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Marketing/detailMhsPmb')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            ket:ket,
                            jeniscabang:jeniscabang,
                            keterangan:keterangan,
                            kocab : kocab
                        },
                        success : function(data){
                            $('.load').hide();
                            if(kriteria == 'Coll'){
                                        if(keterangan == 'U-30'){
                                                $('.isiu30').html(data);
                                            }else{
                                                $('.isia30').html(data);
                                            }
                                        }else{
                                            if(keterangan == 'U-30'){
                                                $('.isiu30pol').html(data);
                                            }else{
                                                $('.isia30pol').html(data);
                                            }
                                        }
                        }
                    });
        });
    });
</script>