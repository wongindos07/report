<div class="portlet light bordered">
	<div class="portlet-title">
		<div class="caption">
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">JUMLAH MAHASISWA PROFESI</span>
				</div>	
			</div>
            <div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">Laporan Bulanan :</span>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">FR - PDK - 079</span>
				</div>	
			</div>  
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default" target="_blank" href="#">
                <i class="icon-printer"></i>
            </a>
            <a class="btn btn-circle btn-icon-only btn-default" href="#" target="_blank" >
                <i class="fa fa-file-pdf-o"></i>
            </a>
        </div>
	</div>
	<div class="portlet-body" style="overflow-x: auto;">
		<?=$kontenku?>
	</div>
</div>
