        <div class="col-md-6">
    					<div class="row">
    						<div class="col-md-12">
    							<div class="tab-pane" id="portlet_comments_2">
                                               <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            Detail Penggunaan LMS Student <?=$namacabang?> Tingkat 1 & 2 <img style="width: 25px;height: 25px;" id="imgloading2" src="<?=base_url()?>assets/img/Reload-1s-200px.gif"></div>
                                                    </div>
                                                    <div class="portlet-body">
                                <table class="table table-striped table-bordered table-hover">
    								<thead>
    									<tr class='bg-grey-gallery bg-font-grey-gallery'>
	    									<td>Tingkat</td>
											<td>Rasio</td>
										</tr>
    								</thead>
    								<tbody>
    									<tr>
	    									<td>Tingkat 1</td>
											<td><a tingkat='1' class="lihatjurusan" href="#"><?=number_format($tk1)?>%
                                            </a></td>
										</tr>
										<tr>
	    									<td>Tingkat 2</td>
											<td><a tingkat='2' class="lihatjurusan" href="#"><?=number_format($tk2)?>%
                                            </a></td>
										</tr>
    								</tbody>
    							</table>
                                                    </div>
                                                </div>
    						</div>
    					</div>
                    </div>
    					<br>
    					<div class="row">
    						<div class="col-md-12 ">
                                
    						<div class="isijurusan"></div>	
                           
    						</div>
    					</div>
    			 </div>

<script type="text/javascript">
    $("#imgloading2").hide();
    $(".lihatjurusan").click(function(){
        let periode = $(this).attr("periode");
        let tak = $(this).attr("tak");
        let kocab = $(this).attr("kocab");
        let tingkat = $(this).attr("tingkat");
        $("#imgloading2").show();
        $.ajax({
            type : "POST",
            url: "<?=base_url('index.php/Pendidikan/GetJurusanTk')?>",
            data: {
                periode:periode,
                tak:tak,
                kocab:kocab,
                tingkat:tingkat
            },
            success: function(msg){
                $("#imgloading2").hide();
                $(".isijurusan").html(msg);
            }
        });
    });
</script>