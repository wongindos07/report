<?php
foreach($biodata as $rbiodata);
$nim = $rbiodata->nim;#
$nama = $rbiodata->Nama_Mahasiswa;
$kelas = $rbiodata->kelas;
$kode = $rbiodata->kode;
$kocab = $kocab;
$angkatan = $rbiodata->TahunAngkatan;

$data = array('kodejurusan'=>$kode);
$getjurusan = $this->Mainmodel->getWheres('jurusan',$data,$kriteria)->result();
foreach($getjurusan as $rjurusan);

$najur = $rjurusan->namajurusan;

$cekPA = $this->Mainmodel->cekPA($kode,$kelas,$kocab,$kriteria);
$jumlah = $cekPA->num_rows();
if($jumlah == 0){
    $namapa = "-";
}else{
    $getPA = $this->Mainmodel->getPA($kode,$kelas,$kocab,$kriteria)->result();
    foreach($getPA as $rgetpa);
    $namapa = $rgetpa->PA;
}


$m=date("m");
$t=date("Y");
$tg=date("d");

$nilaicab='nilai'.$kocab;


?>
<div>
    <div class="portlet box blue-hoki" style="margin-top: 20px;">
    <div class="portlet-title">
        <div class="caption">DETAIL MAHASISWA/PESERTA DIDIK</div>
    </div>
    <div class="portlet-body">
        <div class="detailmhs">
            <div class="row">
                <div class="col-md-12">
                    <div style="overflow-x: auto;">
                        <table>
                            <tr>
                                <td>Nomor Induk Mahasiswa/Peserta Didik</td>
                                <td style="padding-left: 40px;">: <b><?=$nim?></b></td>
                            </tr>
                            <tr>
                                <td>Nama Mahasiswa/Peserta Didik</td>
                                <td style="padding-left: 40px;">: <b><?=$nama?></b></td>
                            </tr>
                            <tr>
                                <td>Konsentrasi</td>
                                <td style="padding-left: 40px;">: <b><?=$najur?></b></td>
                            </tr>
                            <tr>
                                <td>Kelas</td>
                                <td style="padding-left: 40px;">: <b><?=$kelas?></b></td>
                            </tr>
                            <tr>
                                <td>Pembimbing Akademik</td>
                                <td style="padding-left: 40px;">: <b><?=$namapa?></b></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="detailnilai" style="margin-top: 20px;">
  <div class="row">
        <div class="col-md-12">
            <div class="isinilai">
                <table class="table table-striped table-bordered table-hover">
                    <thead style="background: #031727; color: white">
                        <tr>
                            <td>NO</td>
                            <td>KODE</td>
                            <td>NAMA MATAKULIAH</td>
                            <td>SKS</td>
                            <td>HDR</td>
                            <td>PRK</td>
                            <td>TGS</td>
                            <td>FRM</td>
                            <td>UTS</td>
                            <td>UAS</td>
                            <td>SP</td>
                            <td>AHR</td>
                            <td>HRF</td>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $getsmtCabang = $this->Mainmodel->getsmtCabang($kriteria,$nim,$angkatan,$nilaicab)->result();
                        $jumsks = 0;
                        $jumkum = 0;
                        foreach($getsmtCabang as $nilaicabang){
                            $smt =  $nilaicabang->semester;

                           
                        ?>
                        <tr>
                            <td colspan="13" style="background-color: #30495A !important; color: white">Semester <?=$smt?></td>
                        </tr>
                        <?php


                        $getNilaiCabang = $this->Mainmodel->getNilaiCabang($nilaicab,$nim,$angkatan,$smt,$kriteria)->result();

                            $jsks=0;
                            $jkum=0;
                          
                            // $jsksall = 0;
                            // $jkumall = 0;
                            $n=0;
                            foreach($getNilaiCabang as $nilaicabang2){
                                $n++;
                                $kojur = $nilaicabang2->kodejurusan;
                                $kodemtk = $nilaicabang2->kodemtk;

                                $getMatkul = $this->Mainmodel->getMatkul($kodemtk,$kojur,$smt,$kriteria)->result();
                                foreach($getMatkul as $rmatkul);
                                $namamtk = $rmatkul->mk;
                                $sks = $nilaicabang2->sks;

                                if($nilaicabang2->N_hadir == 0){
                                    $nilaihdr = "";
                                    $warnakosong1 = "#DADBDC";
                                }else{
                                    $nilaihdr = $nilaicabang2->N_hadir;
                                    $warnakosong1 = "white";
                                }

                                if($nilaicabang2->N_perilaku == 0){
                                    $nilaiprl = "";
                                    $warnakosong2 = "#DADBDC";
                                }else{
                                    $nilaiprl = $nilaicabang2->N_perilaku;
                                    $warnakosong2 = "white";
                                }

                                if($nilaicabang2->N_tugas == 0){
                                    $nilaitgs = "";
                                    $warnakosong3 = "#DADBDC";
                                }else{
                                    $nilaitgs = $nilaicabang2->N_tugas;
                                    $warnakosong3 = "white";
                                }

                                if($nilaicabang2->N_formatif == 0){
                                    $nilaifrm = "";
                                    $warnakosong4 = "#DADBDC";
                                }else{
                                    $nilaifrm = $nilaicabang2->N_formatif;
                                    $warnakosong4 = "white";
                                }

                                if($nilaicabang2->N_uts == 0){
                                    $nilaiuts = "";
                                    $warnakosong5 = "#DADBDC";
                                }else{
                                    $nilaiuts = $nilaicabang2->N_uts;
                                    $warnakosong5 = "white";
                                }

                                if($nilaicabang2->N_Her == 0){
                                    $nilaiher = "";
                                    $warnakosong6 = "#DADBDC";
                                }else{
                                    $nilaiher = $nilaicabang2->N_Her;
                                    $warnakosong6 = "white";
                                }

                                if($nilaicabang2->N_uas == 0){
                                    $nilaiuas = "";
                                    $warnakosong7 = "#DADBDC";
                                }else{
                                    $nilaiuas = $nilaicabang2->N_uas;
                                    $warnakosong7 = "white";
                                }

                                if($nilaicabang2->N_Akhir2 == 0){
                                    $nilaiakhir = "";
                                    $warnakosong8 = "#DADBDC";
                                }else{
                                    $nilaiakhir = $nilaicabang2->N_Akhir2;
                                    $warnakosong8 = "white";
                                }

                                echo "
                                    <tr>
                                        <td>$n</td>
                                        <td>".$kojur."</td>
                                        <td>".$namamtk."</td>
                                        <td>".$sks."</td>
                                        <td style='background-color:".$warnakosong1 ."'>".$nilaihdr."</td>
                                        <td style='background-color:".$warnakosong2 ."'>".$nilaiprl."</td>
                                        <td style='background-color:".$warnakosong3 ."'>".$nilaitgs."</td>
                                        <td style='background-color:".$warnakosong4 ."'>".$nilaifrm."</td>
                                        <td style='background-color:".$warnakosong5 ."'>".$nilaiuts."</td>
                                        <td style='background-color:".$warnakosong6 ."'>".$nilaiher."</td>
                                        <td style='background-color:".$warnakosong7 ."'>".$nilaiuas."</td>
                                        <td style='background-color:".$warnakosong8 ."'>".$nilaiakhir."</td>
                                        <td>".$nilaicabang2->Huruf_Mutu."</td>
                                    </tr>
                                    ";

                                $jsks += $sks;

                                $jkum += $nilaicabang2->Kumulatif;

                               
                           
                               
                            }


                            $jumsks += $jsks;

                            $jumkum += $jkum; 
                            
                            $jsksall = $jumsks;
                                
                            $jkumall = $jumkum;

                            $ips=$jkum/$jsks;

                            $ipk=$jumkum/$jumsks;

                            



                             if($ips <= 2.00){
                                                
                                                $predikat="Kurang";
                                                $warni="#009933";

                                            }elseif($ips <= 2.50){
                                            
                                                $predikat="Cukup";
                                                $warni="#009933";

                                            } elseif($ips <= 2.90){
                                                
                                                $predikat="Baik";
                                                $warni="#009933";

                                            }elseif($ips <= 3.50){
                                                
                                                 $predikat="Memuaskan";
                                                 $warni="#009933";

                                            }elseif($ips <= 4.00){

                                                $predikat="Sangat Memuaskan";
                                                $warni="#009933";
                                            
                                            }
                            ?>

                             

                                <tr>
                                    <td colspan="3" style="text-align: right;font-weight: bold;">Beban SKS </td>
                                    <td><?=$jsks?></td>
                                    <td rowspan="4" colspan="9" style="background-color: <?=$warni?> !important;">
                                        <h4 style="color: white;font-weight: bold;"><?=strtoupper($predikat)?></h4>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right;font-weight: bold;">Jumlah Kumulatif</td>
                                    <td><?=$jkum?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right;font-weight: bold;">Index Prestasi Sementara</td>
                                    <td><?=number_format($ips,2)?></td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="text-align: right;font-weight: bold;">Index Prestasi Kumulatif</td>
                                    <td><?=number_format($ipk,2)?></td>
                                </tr>


                           
                        <?php

                        if($jumkum == 0 && $jumsks == 0){
                                
                            }else{
                                
                            

                                                                                    if($ipk <= 2.75){


                                                                                    $predikatakhir="Memuaskan";


                                                                                    $warn="#009933";


                                                                                    }


                                                                                    elseif($ipk <= 3.50){


                                                                                    $predikatakhir="Sangat Memuaskan";


                                                                                    $warn="#009933";


                                                                                    }


                                                                                    elseif($ipk <= 4.00){


                                                                                    $predikatakhir="Cum Laude";


                                                                                    $warn="#009933";


                                                                                    }

                        }


                        ?>
                        <tr>
                            <td colspan='13' style="font-weight: "><font style="font-weight: bold;">INDEX PRESTASI KUMULATIF </font></td>
                        </tr>
                        <tfoot>
                        <tr>
                            <td colspan="3">Total Beban SKS</td>
                            <td><?=$jumsks?></td>
                            <td rowspan="2" colspan="9" style="background-color: <?=$warn?>">
                                <h4 style="color: white;font-weight: bold;"><?=strtoupper($predikatakhir)?></h4>
                            </td>

                            <!-- <td colspan="3">Total Beban SKS </td>
                            <td><?=$jumsks?></td>
                            <td rowspan="2" colspan="9" style="background-color: <?=$warn?>">
                                <h4 style="color: white;font-weight: bold;"><?=strtoupper($predikatakhir)?></h4>
                            </td> -->
                        </tr>
                        <tr>
                            <td colspan="3">Index Prestasi kumulatif (IPK)</td>
                            <td><?=number_format($ipk,2)?></td>
                        </tr>
                        </tfoot>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>  
</div>
</div>

