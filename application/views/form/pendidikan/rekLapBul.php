<!-- BEGIN SAMPLE FORM PORTLET-->
                                <div class="portlet light bordered">
                                  
                                    <div class="portlet-body form">
                                        <form role="form">
                                            <div class="form-body">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                      <div class="form-group">
                                                            <label class="col-md-4 control-label">Cabang</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="cab">
                                                                    <option value="">-- Pilih Cabang --</option>
                                                                    <?php
                                                                    foreach($cabangc as $rcabang){
                                                                    	echo "<option value=".$rcabang->kodecabang.">".$rcabang->namacabang."</option>";
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                       
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label class="col-md-4 control-label">Tahun Akademik</label>
                                                            <div class="col-md-8">
                                                                <select class="form-control" id="tak">
                                                                    <option>-- Pilih Tahun Akademik --</option>
                                                                </select>
                                                            </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label class="col-md-4 control-label">Periode Laporan</label>
                                                            <div class="col-md-8">
                                                                <input id="periode" style="margin-bottom: 13px;" class="form-control datepicker" data-date-format="yyyy/mm/dd">
                                                            </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    
                                                    </div>
                                                </div>
                                                
                                                <div class="row">
                                                    <div class="col-md-6">
                                                       <div class="form-group">
                                                            <label class="col-md-4 control-label"></label>
                                                            <div class="col-md-8">
                                                                <button type="button" id="proseslaporan" class="btn red-thunderbird">Proses Data</button>
                                                            </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                    
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                 </div>

                                 <div id="isi"></div>