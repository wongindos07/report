	<div class="row">
    		<div class="col-md-12">
    			<div class="row">
    				<div class="col-md-12" id="lmscabang" style="overflow: auto !important;">
    					<div class="tab-pane" id="portlet_comments_2">
                            <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            Detail Penggunaan LMS Student per Cabang / Kampus
                                <img style="width: 25px;height: 25px;" id="imgloading" src="<?=base_url()?>assets/img/Reload-1s-200px.gif">
                                                        </div>
                                                    </div>
                         	<div class="portlet-body">
                         		<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr class='bg-grey-gallery bg-font-grey-gallery'>
											<td>Kode Cabang</td>
											<td>Nama Cabang</td>
											<td>Rasio</td>
										</tr>
									</thead>
									<tbody>
										<?php
										foreach($getLmsCabang as $rlms){
										?>
										<tr>
											<td><?=$rlms->kodecabang?></td>
											<td><?=$rlms->namacabang?></td>
											<?php
											$kocab = $rlms->kodecabang;
											?>
											<td><a href="#" per="<?=$periode?>" tak="<?=$tahunaka?>" kocab='<?=$kocab?>' class='rasiolmscab'><?=number_format($rlms->lmsmhs)?>%</a>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
                         	</div>
                     		</div>
                     	</div>
    					
    				</div>
    				<div class="isi2"></div>
    		</div>
    	</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" id="modaljurusan" class="close"  aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
	$("#imgloading").hide();
	$(".rasiolmscab").click(function(){
		let periode = $(this).attr('per');
		let tak = $(this).attr('tak');
		let kocab = $(this).attr('kocab');
		$("#imgloading").show();
		$.ajax({
			type : "POST",
			url: "<?=base_url('index.php/Pendidikan/Gettk')?>",
			data: {
				periode : periode,
				tak: tak,
				kocab: kocab
			},
			success: function(msg){
				$("#imgloading").hide();
				$("#lmscabang").removeClass("col-md-12");
				$("#lmscabang").removeClass("col-md-6");
				$("#lmscabang").addClass("col-md-6");

				$(".isi2").html(msg);
				$(".lihatjurusan").attr("periode",periode).attr("tak",tak).attr("kocab",kocab);
			}
		});
	});


	$("#modaljurusan").click(function(){
		$("#exampleModal1").modal('hide');
	});
</script>

