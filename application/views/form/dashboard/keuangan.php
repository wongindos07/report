<?php
foreach($piutangsemua as $row1);
$piusemua = $row1->sebesar;

foreach($piutangsemua2 as $row2);
$piusemua2 = $row2->sebesar;

// rasio piutang tk1
$persentase1 = number_format(($sebesar / $piusemua) * 100);
// rasio piutang tk2
$persentase2 = number_format(($sebesar2 / $piusemua2) * 100);

foreach($piutangnotbiaya as $rowpiutangnot){
	$jumlahbiaya = $rowpiutangnot->jumlahbiaya;
	$jumlahterbayar = $rowpiutangnot->terbayar;
	$totalpiutangnot = $jumlahbiaya - $jumlahterbayar;
}

if($sebesar > $sepuluhpersen){
	$warnatk1 = '#D04556';
	$war1 = 'red-mint';
}elseif($sebesar <= $sepuluhpersen and $sebesar > $limapersen){
	$warnatk1 = '#DFD43B';
	$war1 = 'yellow-soft';
}else{
	$warnatk1 = '#3DDA42';
	$war1 = 'green-jungle';
}

if($sebesar2 > $sepuluhpersen2){
	$warnatk2 = '#D04556';
	$war2 = 'red-mint';
}elseif($sebesar2 <= $sepuluhpersen2 and $sebesar2 > $limapersen2){
	$warnatk2 = '#DFD43B';
	$war2 = 'yellow-soft';
}else{
	$warnatk2 = '#3DDA42';
	$war2 = 'green-jungle';
}

        
?>

<input type="hidden" id="warnatk1" value="<?=$warnatk1?>">
<input type="hidden" id="warnatk2" value="<?=$warnatk2?>">
<input type="hidden" id="piutk1" value="<?=$sebesar?>">
<input type="hidden" id="piutk2" value="<?=$sebesar2?>">
<input type="hidden" id="periodecol" value="<?=$periodecol?>">


<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Piutang Peserta Didik College</b>
        </div>
     </div>
	   <div class="portlet-body">
	   	<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war1?>" title="Klik untuk Detail" href="#" tipe='Registrasi Mahasiswa Baru' kriteria='Coll' tahun='<?=$periodes?>' id="coltingkat1" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".coltingkat1">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesar)?>"><?=number_format($sebesar)?></span>
		                                        </div>
		                                        <div class="desc"><b><p style="font-size: 20px;">Piutang Tingkat 1</p></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war2?>" title="Klik untuk Detail" href="#" tipe='Registrasi Mahasiswa Senior' kriteria='Coll' tahun='<?=$periodes?>' id="coltingkat2" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".coltingkat2">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesar2)?>"><?=number_format($sebesar2)?></span></div>
		                                        <div class="desc"> <b><p style="font-size: 20px;">Piutang Tingkat 2</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <?php if($totalpiutangnot <> 0){?>
		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 red-mint" title="Klik untuk Detail" href="#" kriteria='Coll' tahun='<?=$periodes?>' id="colpiutangnot" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".colpiutangnot">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($totalpiutangnot)?>"><?=number_format($totalpiutangnot)?></span></div>
		                                        <div class="desc"> <b><p style="font-size: 15px;">Piutang PD Belum Dibuatkan Rencana</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>
		                            <?php } ?>
                        		</div>
                        	</div>
                        </div>

                        <br>
                        <div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-6">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpiucol"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-6">
										<div class="note note-warning">
			                                <ol>
			                                    
			                                    <li>Jika piutang > 10% dari total penerimaan maka akan berwarna merah</li>
			                                    <li>Jika piutang > 5% dan <=10% dari total penerimaan maka akan berwarna Kuning</li>
			                                    <li>Jika piutang <= 5% dari total penerimaan maka akan berwarna Hijau</li>
			                                </ol>
			                            </div>
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
	   </div>
</div>

<!-- menampilkan modal detail -->
<div class="modal fade coltingkat1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 1</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk1col"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade coltingkat2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 2</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk2col"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade colpiutangnot" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
        <h4><b class="detail">DETAIL PIUTANG MHS BELUM DIBUATKAN RENCANA COLLEGE</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="not"></div>
    	
   		
    </div>
  </div>
</div>


<?php
foreach($piutangsemuapol as $row1);
$piusemuapol = $row1->sebesar;

foreach($piutangsemua2pol as $row2);
$piusemua2pol = $row2->sebesar;

// rasio piutang tk1
$persentase1 = number_format(($sebesarpol / $piusemuapol) * 100);
// rasio piutang tk2
$persentase2 = number_format(($sebesar2pol / $piusemua2pol) * 100);

foreach($piutangnotbiayapol as $rowpiutangnot){
	$jumlahbiaya = $rowpiutangnot->jumlahbiaya;
	$jumlahterbayar = $rowpiutangnot->terbayar;
	$totalpiutangnot = $jumlahbiaya - $jumlahterbayar;
}

$ceilsepuluh = ceil($sepuluhpersen);
$ceillima = ceil($limapersen);
$ceilsepuluh2pol = ceil($sepuluhpersen2pol);
$ceillima2pol = ceil($limapersen2pol);
$ceilsepuluh3 = ceil($sepuluhpersenpol3);
$ceillima3 = ceil($limapersenpol3);

if($sebesarpol > $ceilsepuluh){
	$warnatk1 = '#D04556';
	$war1 = 'red-mint';
}elseif($sebesarpol <= $ceilsepuluh and $sebesarpol > $ceillima){
	$warnatk1 = '#DFD43B';
	$war1 = 'yellow-soft';
}else{
	$warnatk1 = '#3DDA42';
	$war1 = 'green-jungle';
}

if($sebesar2pol > $ceilsepuluh2pol){
	$warnatk2 = '#D04556';
	$war2 = 'red-mint';
}elseif($sebesarpol <= $ceilsepuluh2pol and $sebesarpol > $ceillima2pol){
	$warnatk2 = '#DFD43B';
	$war2 = 'yellow-soft';
}else{
	$warnatk2 = '#3DDA42';
	$war2 = 'green-jungle';
}


if($sebesarpoltk3 > $ceilsepuluh3){
	$warnatk3 = '#D04556';
	$war3 = 'red-mint';
}elseif($sebesarpoltk3 <= $ceilsepuluh3 and $sebesarpoltk3 > $ceillima3){
	$warnatk3 = '#DFD43B';
	$war3 = 'yellow-soft';
}else{
	$warnatk3 = '#3DDA42';
	$war3 = 'green-jungle';
}
        
?>

<input type="hidden" id="warnatk1pol" value="<?=$warnatk1?>">
<input type="hidden" id="warnatk2pol" value="<?=$warnatk2?>">
<input type="hidden" id="warnatk3pol" value="<?=$warnatk3?>">
<input type="hidden" id="piutk1pol" value="<?=$sebesarpol?>">
<input type="hidden" id="piutk2pol" value="<?=$sebesar2pol?>">
<input type="hidden" id="piutk3pol" value="<?=$sebesarpoltk3?>">
<input type="hidden" id="periodepol" value="<?=$periodespol?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Piutang Mahasiswa Politeknik</b>
        </div>
     </div>
	   <div class="portlet-body">
	   	<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war1?>" title="Klik untuk Detail" href="#" tipe='Registrasi Mahasiswa Baru' kriteria='PLJ' tahun='<?=$periodespol?>' id="poltingkat1" jeniscabang='<?=$jeniscabang?>'  data-toggle="modal" data-target=".poltingkat1">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesarpol)?>"><?=number_format($sebesarpol)?></span>
		                                        </div>
		                                        <div class="desc"><b><p style="font-size: 20px;">Piutang Tingkat 1</p></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war2?>" title="Klik untuk Detail" href="#" tipe='Registrasi Mahasiswa Senior' kriteria='PLJ' tahun='<?=$periodespol?>' id="poltingkat2" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".poltingkat2">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesar2pol)?>"><?=number_format($sebesar2pol)?></span></div>
		                                        <div class="desc"> <b><p style="font-size: 20px;">Piutang Tingkat 2</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war3?>" title="Klik untuk Detail" href="#" tipe='Registrasi Mahasiswa Tingkat 3' kriteria='PLJ' tahun='<?=$periodespol?>' id="poltingkat3" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".poltingkat3">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesarpoltk3)?>"><?=number_format($sebesarpoltk3)?></span></div>
		                                        <div class="desc"> <b><p style="font-size: 20px;">Piutang Tingkat 3</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <?php if($totalpiutangnot <> 0){?>
		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 red-mint" title="Klik untuk Detail" href="#" kriteria='PLJ' tahun='<?=$periodes?>' id="polpiutangnot" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".polpiutangnot">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($totalpiutangnot)?>"><?=number_format($totalpiutangnot)?></span></div>
		                                        <div class="desc"> <b><p style="font-size: 15px;">Piutang MHS Belum Dibuatkan Rencana</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>
		                            <?php } ?>
                        		</div>
                       	</div>

                       	<br>
                        <div class="row">
                        	</div>
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-6">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpiupol"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-6">
										<div class="note note-warning">
			                                <ol>
			                                    
			                                    <li>Jika piutang > 10% dari total penerimaan maka akan berwarna merah</li>
			                                    <li>Jika piutang > 5% dan <=10% dari total penerimaan maka akan berwarna Kuning</li>
			                                    <li>Jika piutang <= 5% dari total penerimaan maka akan berwarna Hijau</li>
			                                </ol>
			                            </div>
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
	   </div>
</div>



<!-- menampilkan modal detail -->
<div class="modal fade poltingkat1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 1</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk1"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade poltingkat2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 2</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk2"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade poltingkat3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 3</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk3"></div>
    	
   		
    </div>
  </div>
</div>


<div class="modal fade polpiutangnot" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG MHS BELUM DIBUATKAN RENCANA POLITEKNIK</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="notpol"></div>
    	
   		
    </div>
  </div>
</div>

<!-- action dashboard detail -->
<script type="text/javascript">
    $(document).ready(function(){
        $('.load').hide();

        // DETAIL PIUTANG POLTEK
        $("#poltingkat1").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');

            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard',
                            jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk1").html(data);
                            $(".tk2").html("");
                            $(".tk3").html("");
                        }
                    });

        });

        $("#poltingkat2").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard',
                            jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk2").html(data);
                            $(".tk1").html("");
                            $(".tk3").html("");
                        }
                    });

        });

        $("#poltingkat3").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard',
                            jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk3").html(data);
                            $(".tk2").html("");
                            $(".tk1").html("");
                        }
                    });

        });

        $("#coltingkat1").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard',
                            jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk1col").html(data);
                            $(".tk2col").html("");
                            $(".tk3").html("");
                            $(".tk2").html("");
                        }
                    });

        });

        $("#coltingkat2").click(function(){
            let tipe = $(this).attr('tipe');
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            tipe:tipe,
                            dashboard:'dashboard',
                            jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk2col").html(data);
                            $(".tk1col").html("");
                            $(".tk1").html("");
                            $(".tk3").html("");
                        }
                    });

        });

        $("#colpiutangnot").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutangNot')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            dashboard:'dashboard',
                            jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".tk2col").html("");
                            $(".tk1col").html("");
                            $(".tk1").html("");
                            $(".tk3").html("");
                            $(".notpol").html("");
                            $(".not").html(data);
                        }
                    });

        });

        $("#polpiutangnot").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jeniscabang = $(this).attr('jeniscabang');
            $('.load').show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Keuangan/getPiutangNot')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            dashboard:'dashboard',
                            jeniscabang:jeniscabang
                        },
                        success : function(data){
                            $('.load').hide();
                            $(".not").html("");
                            $(".tk2col").html("");
                            $(".tk1col").html("");
                            $(".tk1").html("");
                            $(".tk3").html("");
                            $(".notpol").html(data);
                        }
                    });

        });
       

    });   
</script>