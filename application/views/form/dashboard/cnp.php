<?php
					$ttl1 = 0;
					$ttl2 = 0;
					$ttl3 = 0;
					$ttl4 = 0;
					
		foreach($cabangcnp as $cabang){
				$kocab = $cabang->kodecabang;
					$getwaktuinput = $this->Mainmodel->getWaktuInput('Coll',$kocab,$tahun)->result();
					foreach($getwaktuinput as $rwaktu);
					$tglinput = $rwaktu->tgl_input;

					$getcnp = $this->Mainmodel->penempatan('Coll',$kocab,$tahun,$tglinput)->result();
					if($getcnp){
						foreach($getcnp as $rcnp);
						$target1 = $rcnp->terget1;
						$kerja1 = $rcnp->kerja1;
						$target2 = $rcnp->target2;
						$kerja2 = $rcnp->kerja2;
					}else{
						foreach($getcnp as $rcnp);
						$target1 = 0;
						$kerja1 = 0;
						$target2 = 0;
						$kerja2 = 0;
					}
		
					$ttl1 += $target1;
					$ttl2 += $kerja1;
					$ttl3 += $target2;
					$ttl4 += $kerja2;
			}

					$totaltarget = $ttl1 + $ttl3;
					$batas = number_format(($totaltarget * 25) / 100);
					$batasmax = $totaltarget + $batas;



			$rasio1 = number_format(($ttl2/$ttl1)*100);
			$rasio2 = number_format(($ttl4/$ttl3)*100);

			if($rasio1 >= 0 and $rasio1 < 50){
				$wajib = '#D04556';
				$warnanya1 = 'red-mint';
			}elseif($rasio1 >= 50 and $rasio1 < 65){
				$wajib = '#DFD43B';
				$warnanya1 = 'yellow-soft';
			}else{
				$wajib = '#3DDA42';
				$warnanya1 = 'green-jungle';
			}

			if($rasio2 >= 0 and $rasio2 < 50){
				$dibantu = '#D04556';
				$warnanya2 = 'red-mint';
			}elseif($rasio2 >= 50 and $rasio2 < 65){
				$dibantu = '#DFD43B';
				$warnanya2 = 'yellow-soft';
			}else{
				$dibantu = '#3DDA42';
				$warnanya2 = 'green-jungle';
			}

?>		
			<input type="hidden" id="wajib" value="<?=$wajib?>">
			<input type="hidden" id="dibantu" value="<?=$dibantu?>">

			<input type="hidden" id="target1" value='<?=$ttl1?>'>
			<input type="hidden" id="kerja1" value='<?=$ttl2?>'>
			<input type="hidden" id="target2" value='<?=$ttl3?>'>
			<input type="hidden" id="kerja2" value='<?=$ttl4?>'>
			<input type="hidden" id="tahuncnp" value="<?=$tahun?>">
			<input type="hidden" id="batasmax" value="<?=$batasmax?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Penempatan Kerja College </b>
        </div>
	  </div>
<div class="portlet-body">
<div class="row">
<div class="col-lg-12">
<div class="row">					

									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl1?>"><?=$ttl1?></span> Orang</div>
		                                        <div class="desc"> <b>Target Wajib Kerja</b> </div>
		                                    </div>
		                                </a>
		                            </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="cnpcol" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 <?=$warnanya1?>" href="#" tahun="<?=$tahun?>" kriteria="Coll" jeniscabang="<?=$jeniscabang?>" jenis="wajib" data-toggle="modal" data-target=".cnpcol">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl2?>"><?=$ttl2?></span> Orang
		                                        </div>
		                                        <div class="desc"><b> Realisasi Wajib Kerja </b></div>
		                                    </div>
		                                </a>
		                            </div>

                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl3?>"><?=$ttl3?></span> Orang
		                                        </div>
		                                        <div class="desc"><b> Target Dibantu Kerja </b></div>
		                                    </div>
		                                </a>
		                            </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="cnpdibantu" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 <?=$warnanya2?>" href="#"  tahun="<?=$tahun?>" kriteria="Coll" jeniscabang="<?=$jeniscabang?>" jenis="dibantu" data-toggle="modal" data-target=".cnpdibantu">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl4?>"><?=$ttl4?></span> Orang</div>
		                                        <div class="desc"> <b>Realisasi Dibantu Kerja</b></div>
		                                    </div>
		                                </a>
		                            </div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          	<div class="row">
                                          		<div class="col-md-6">
                                          			<div class="portlet-body">
                                            	<!-- tampilan chart -->
                                        		<canvas id="penempatan"></canvas>
                                            	</div>
                                          		</div>
                                          		<div class="col-md-6">
                                          			<div class="portlet-body">
                                            	<!-- tampilan chart -->
                                        		<canvas id="penempatann"></canvas>
                                            	</div>
                                          		</div>
                                          	</div>
                                        </div>
									</div>
									<div class="col-lg-12">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
</div>
</div>
</div>
</div>

<?php
					$ttl1 = 0;
					$ttl2 = 0;
					$ttl3 = 0;
					$ttl4 = 0;
		foreach($cabangcnppol as $cabang){
				$kocab = $cabang->kodecabang;
					$getwaktuinput = $this->Mainmodel->getWaktuInput('PLJ',$kocab,$tahunpol)->result();
					foreach($getwaktuinput as $rwaktu);
					$tglinput = $rwaktu->tgl_input;

					$getcnp = $this->Mainmodel->penempatan('PLJ',$kocab,$tahunpol,$tglinput)->result();
					if($getcnp){
						foreach($getcnp as $rcnp);
						$target1 = $rcnp->terget1;
						$kerja1 = $rcnp->kerja1;
						$target2 = $rcnp->target2;
						$kerja2 = $rcnp->kerja2;
					}else{
						foreach($getcnp as $rcnp);
						$target1 = 0;
						$kerja1 = 0;
						$target2 = 0;
						$kerja2 = 0;
					}
		
					$ttl1 += $target1;
					$ttl2 += $kerja1;
					$ttl3 += $target2;
					$ttl4 += $kerja2;
			}

					$totaltarget = $ttl1 + $ttl3;
					$batas = number_format(($totaltarget * 25) / 100);
					$batasmax = $totaltarget + $batas;


			$rasio1 = number_format(($ttl2/$ttl1)*100);
			$rasio2 = number_format(($ttl4/$ttl3)*100);

			if($rasio1 >= 0 and $rasio1 < 50){
				$wajib = '#D04556';
				$warnanya1 = 'red-mint';
			}elseif($rasio1 >= 50 and $rasio1 < 65){
				$wajib = '#DFD43B';
				$warnanya1 = 'yellow-soft';
			}else{
				$wajib = '#3DDA42';
				$warnanya1 = 'green-jungle';
			}

			if($rasio2 >= 0 and $rasio2 < 50){
				$dibantu = '#D04556';
				$warnanya2 = 'red-mint';
			}elseif($rasio2 >= 50 and $rasio2 < 65){
				$dibantu = '#DFD43B';
				$warnanya2 = 'yellow-soft';
			}else{
				$dibantu = '#3DDA42';
				$warnanya2 = 'green-jungle';
			}

?>		
			<input type="hidden" id="wajibpol" value="<?=$wajib?>">
			<input type="hidden" id="dibantupol" value="<?=$dibantu?>">

			<input type="hidden" id="target1pol" value='<?=$ttl1?>'>
			<input type="hidden" id="kerja1pol" value='<?=$ttl2?>'>
			<input type="hidden" id="target2pol" value='<?=$ttl3?>'>
			<input type="hidden" id="kerja2pol" value='<?=$ttl4?>'>
			<input type="hidden" id="tahuncnppol" value="<?=$tahunpol?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Penempatan Kerja Politeknik </b>
        </div>
	  </div>
<div class="portlet-body">
<div class="row">
<div class="col-lg-12">
<div class="row">					

									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl1?>"><?=$ttl1?></span> Orang</div>
		                                        <div class="desc"> <b>Target Wajib Kerja</b> </div>
		                                    </div>
		                                </a>
		                            </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="cnppol" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 <?=$warnanya1?>" href="#"  tahun="<?=$tahunpol?>" kriteria="PLJ" jeniscabang="<?=$jeniscabang?>" jenis="wajib" data-toggle="modal" data-target=".cnppol">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl2?>"><?=$ttl2?></span> Orang
		                                        </div>
		                                        <div class="desc"><b> Realisasi Wajib Kerja </b></div>
		                                    </div>
		                                </a>
		                            </div>

                                    
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl3?>"><?=$ttl3?></span> Orang
		                                        </div>
		                                        <div class="desc"><b> Target Dibantu Kerja </b></div>
		                                    </div>
		                                </a>
		                            </div>

                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="cnpdibantupol" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 <?=$warnanya2?>" href="#" tahun="<?=$tahunpol?>" kriteria="PLJ" jeniscabang="<?=$jeniscabang?>" jenis="dibantu" data-toggle="modal" data-target=".cnppoldibantu">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$ttl4?>"><?=$ttl4?></span> Orang</div>
		                                        <div class="desc"> <b>Realisasi Dibantu Kerja</b></div>
		                                    </div>
		                                </a>
		                            </div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<div class="row">
                                          		<div class="col-md-6">
                                          			<div class="portlet-body">
                                            	<!-- tampilan chart -->
                                        		<canvas id="penempatanpol"></canvas>
                                            	</div>
                                          		</div>
                                          		<div class="col-md-6">
                                          			<div class="portlet-body">
                                            	<!-- tampilan chart -->
                                        		<canvas id="penempatannpol"></canvas>
                                            	</div>
                                          		</div>
                                          	</div>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-12">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
</div>
</div>
</div>
</div>


<!-- MODAL -->
<!-- menampilkan modal detail -->
<div class="modal fade cnpcol" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">REALISASI WAJIB KERJA COLLEGE</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isicnpcol"></div>
    	
   		
    </div>
  </div>
</div>
<!-- END MODAL -->

<!-- menampilkan modal detail -->
<div class="modal fade cnpdibantu" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">REALISASI DIBANTU KERJA COLLEGE</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isicnpdibantu"></div>
    	
   		
    </div>
  </div>
</div>
<!-- END MODAL -->

<!-- MODAL -->
<!-- menampilkan modal detail -->
<div class="modal fade cnppol" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">REALISASI WAJIB KERJA POLTEK</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isicnppol"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade cnppoldibantu" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">REALISASI DIBANTU KERJA POLTEK</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isicnppoldibantu"></div>
    	
   		
    </div> title="Klik untuk Detail"
  </div>
</div>
<!-- END MODAL -->


<script>
$(document).ready(function(){
	$("#cnpcol").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jenis = $(this).attr('jenis');
            let jeniscabang = $(this).attr('jeniscabang');
            $(".load").show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Cnp/wajibKerjaCabang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            jeniscabang:jeniscabang,
                            jenis:jenis
                        },
                        success : function(data){
                            $('.load').hide();
                            $('.isicnpcol').html(data);
                        }
                    });
        });

        $("#cnpdibantu").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jenis = $(this).attr('jenis');
            let jeniscabang = $(this).attr('jeniscabang');
            $(".load").show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Cnp/wajibKerjaCabang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            jeniscabang:jeniscabang,
                            jenis:jenis
                        },
                        success : function(data){
                            $('.load').hide();
                            $('.isicnpdibantu').html(data);
                        }
                    });
        });

        $("#cnppol").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jenis = $(this).attr('jenis');
            let jeniscabang = $(this).attr('jeniscabang');
            $(".load").show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Cnp/wajibKerjaCabang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            jeniscabang:jeniscabang,
                            jenis:jenis
                        },
                        success : function(data){
                            $('.load').hide();
                            $('.isicnppol').html(data);
                        }
                    });
        });

        $("#cnpdibantupol").click(function(){
            let tahun = $(this).attr('tahun');
            let kriteria = $(this).attr('kriteria');
            let jenis = $(this).attr('jenis');
            let jeniscabang = $(this).attr('jeniscabang');
            $(".load").show();
            $.ajax({
                        type : "POST",
                        url  : "<?=base_url('index.php/Cnp/wajibKerjaCabang')?>",
                        data : {
                            tahun:tahun,
                            kriteria:kriteria,
                            jeniscabang:jeniscabang,
                            jenis:jenis
                        },
                        success : function(data){
                            $('.load').hide();
                            $('.isicnppoldibantu').html(data);
                        }
                    });
        });
});
</script>	