                <form method="POST" action="<?=base_url();?>index.php/Main/recap">
                                <div class="portlet light bordered">
                                    <div class="portlet-body">
                                        <div id="context" data-toggle="context" data-target="#context-menu">
                                <div class="form-group col-md-10">
                                    <label class="control-label col-md-3">Bidang</label>
                                <div class="col-md-6">
                            <?php
                    $js = 'class="form-control"';
                    echo form_dropdown('kodebidang', $cmbbidang, '', $js);
                    ?>
                                </div>
                            </div>

                            <div class="form-group col-md-10">
                                    <label class="control-label col-md-3">Tahun Periode</label>
                                    <div class="col-md-6">
                                        <?php
                    $js = 'class="form-control" style="width:200px" ';
                    echo form_dropdown('tahun', $cmbtahun, '', $js);
                    ?>
                                        </div>
                            </div>
                            </form>
                            <div class="form-actions col">
                                <div class="row">
                                <div class="col-md-offset-1 col-md-9">
                                    <input type="submit" value="Proses Data" name="cari" id="modal" class="btn btn-danger">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php foreach($nambidang as $nambid);
                        ?>
                <span style="font-size: 16px">Bidang <?php
                        if(empty($nambid)){
                            $nambid = "A";
                            $namkampus = "B";
                        }else{
                        echo $nambid->namabidang; }?> - Tahun <?php echo $vtahun;?></span>

            <div class="portlet-body flip-scroll" style="margin-top:20px">
                                       <table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr>
                                                    <th>Nama Kampus</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                foreach($list_kpi as $row)
                                                {
                                                    $namakampus         = $row->namakampus;
                                                    $totalrasio         = $row->rasio;

                                                    if($totalrasio >= 80){
                                                        $tcolor="Green";
                                                    }elseif($totalrasio >= 60){
                                                        $tcolor="Yellow";
                                                    }else{
                                                        $tcolor="Red";
                                                    }
                                                ?>
                                                <tr>
                                                    <td><?php echo $namakampus;?></td>
                                                    <td>
                                                        <a href="<?php echo base_url('index.php/main/detailkpi') .'?kodebidang=' .$row->kodebidang .'&tahun=' .$row->tahun .'&kdcabang=' .$row->kodecabang;?>">
                                                        <font color="<?php echo $tcolor ?>">
                                                        <?php echo number_format($row->skor, 2) ." %";?>
                                                        </font>
                                                    </a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
   