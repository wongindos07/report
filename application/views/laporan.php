<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <?php
    $this->load->view('tools/head');
    ?>
    <!-- END HEAD -->
    <style type="text/css">
        .jarak{
            margin-bottom: 20px !important;
        }
        .desc{
            font-size: 18px !important;
        }
        .logo-default{
            margin-top: -0px !important;
            width: 70px !important;
            height: 50px !important;
        }
        body{
            background-color: white !important;
        }
        .judul{
            margin-bottom: 50px;
        }
        .jud{
            margin-bottom: 25px;
        }
        table {
            border:solid #000 !important;
            border-width:1px 0 0 1px !important;
            }
        th, td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }

    </style>

    <body>
        <?php
        $this->load->view($konten);
        ?>

        <?php
        $this->load->view('js/script');
        $this->load->view('js/scriptku');
        ?>
    </body>


</html>