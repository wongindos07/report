
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Aplikan</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Range Umur</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tjumlahumrap =0;
											$trasioumr =0;
											$rasioumr=0;
											$numr=0;
											foreach ($umuraplikan as $umur)
											{
												$numr++;
												$range_umur=$umur->range_umur;
												$jumlahumrap=$umur->jumlahapumur;
											
												$rasioumr = (($jumlahumrap/$totalap)*100);
											?>
												<tr >
													<td ><?=$range_umur?></td>
													<td ><?=$jumlahumrap?></td>
													<td ><?=number_format($rasioumr)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$range_umur?>" id="nama<?=$numr?>" name="nama">
												<input type="hidden" value="<?=$jumlahumrap?>" id="jumlah<?=$numr?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioumr)?>" id="rasio<?=$numr?>" name="rasio">

											<?php 
											$tjumlahumrap += $jumlahumrap;
											$trasioumrap = (($tjumlahumrap/$totalap)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$numr?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlahumrap?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="charumurapl" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Peserta Didik Baru</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Range Umur</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tjumlahumrbio =0;
											$trasioumrbio =0;
											$rasioumrbio=0;
                                            $b=0;
                                            // echo $umbio;
											foreach ($umurbiodata as $umurbio)
											{
												$b++;
												$range_umurbio=$umurbio->range_umurbio;
												$jumlahumrbio=$umurbio->jumlahbioumur;
											
												$rasioumrbio = (($jumlahumrbio/$totalbiodata)*100);
											?>
												<tr >
													<td ><?=$range_umurbio?></td>
													<td ><?=$jumlahumrbio?></td>
													<td ><?=number_format($rasioumrbio)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$range_umurbio?>" id="nama<?=$b?>" name="nama">
												<input type="hidden" value="<?=$jumlahumrbio?>" id="jumlah<?=$b?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioumrbio)?>" id="rasio<?=$b?>" name="rasio">

											<?php 
											$tjumlahumrbio += $jumlahumrbio;
											$trasioumrbio = (($tjumlahumrbio/$totalbiodata)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$b?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlahumrbio?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="charumurbio" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>



					
