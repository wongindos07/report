
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Aplikan</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Kategori Informasi</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tinfopoltek =0;
											$trasioinfpoltek =0;
											$rasioinfpoltek=0;
											$fpoltek=0;
											foreach ($infomasterpoltek as $infpoltek)
											{
												$fpoltek++;
												$infopoltek=$infpoltek->info;
                                                $jumlahinfopoltek=$infpoltek->jumlahinfo;
                                                       
                                                
                                                $jumlahinfoaplpoltek=$jumlahinfopoltek;
                                                $rasioinfpoltek = (($jumlahinfoaplpoltek/$totalappoltek)*100);
											?>
												<tr >
													<td ><?=$infopoltek?></td>
													<td ><?=$jumlahinfoaplpoltek?></td>
													<td ><?=number_format($rasioinfpoltek,2)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$infopoltek?>" id="nama<?=$fpoltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahinfoaplpoltek?>" id="jumlah<?=$fpoltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioinfpoltek)?>" id="rasio<?=$fpoltek?>" name="rasio">

											<?php 
											$tinfopoltek += $jumlahinfopoltek;
											$rasioinfpoltek = (($tinfopoltek/$totalappoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$fpoltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tinfopoltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartinfoaplpoltek" style="height: 420px; max-width: 980; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Mahasiswa Baru</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Kategori Informasi</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tinfo2poltek =0;
											$trasioinf2poltek =0;
											$rasioinf2poltek=0;
											$f2poltek=0;
											foreach ($infomaster2poltek as $inf2poltek)
											{
												$f2poltek++;
												$info2poltek=$inf2poltek->info2;
                                                $jumlahinfo2poltek=$inf2poltek->jumlahinfo2;
                                                       
                                                
                                                $jumlahinfoapl2poltek=$jumlahinfo2poltek;
                                                $rasioinf2poltek = (($jumlahinfoapl2poltek/$totalbiodatapoltek)*100);
                                                
											?>
												<tr >
                                                <td ><?=$info2poltek?></td>
													<td ><?=$jumlahinfoapl2poltek?></td>
													<td ><?=number_format($rasioinf2poltek,2)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$info2poltek?>" id="nama<?=$f2poltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahinfoapl2poltek?>" id="jumlah<?=$f2poltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioinf2poltek)?>" id="rasio<?=$f2poltek?>" name="rasio">

											<?php 
											$tinfo2poltek += $jumlahinfo2poltek;
											$rasioinf2poltek = (($tinfo2poltek/$totalbiodatapoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$f2poltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tinfo2poltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartinfoapl2poltek" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>



					
