<?php
foreach($piutangsemuapol as $row1);
$piusemuapol = $row1->sebesar;

foreach($piutangsemua2pol as $row2);
$piusemua2pol = $row2->sebesar;

// rasio piutang tk1
$persentase1 = number_format(($sebesarpol / $piusemuapol) * 100);
// rasio piutang tk2
$persentase2 = number_format(($sebesar2pol / $piusemua2pol) * 100);

// if($persentase1 >= 0 and $persentase1 < 50){
// 	$warnatk1 = '#3DDA42';
// 	// $warnatk1 = '#D04556';
// }elseif($persentase1 >= 50 and $persentase1 < 65){
// 	$warnatk1 = '#DFD43B';
// }else{
// 	// $warnatk1 = '#3DDA42';
// 	$warnatk1 = '#D04556';
// }

// if($persentase2 >= 0 and $persentase2 < 50){
// 	// $warnatk2 = '#D04556';
// 	$warnatk2 = '#3DDA42';
// }elseif($persentase2 >= 50 and $persentase2 < 65){
// 	$warnatk2 = '#DFD43B';
// }else{
// 	$warnatk2 = '#D04556';
// 	// $warnatk2 = '#3DDA42';
// }
$ceilsepuluh = ceil($sepuluhpersen);
$ceillima = ceil($limapersen);
$ceilsepuluh2pol = ceil($sepuluhpersen2pol);
$ceillima2pol = ceil($limapersen2pol);
$ceilsepuluh3 = ceil($sepuluhpersenpol3);
$ceillima3 = ceil($limapersenpol3);

if($sebesarpol > $ceilsepuluh){
	$warnatk1 = '#D04556';
	$war1 = 'red-mint';
}elseif($sebesarpol <= $ceilsepuluh and $sebesarpol > $ceillima){
	$warnatk1 = '#DFD43B';
	$war1 = 'yellow-soft';
}else{
	$warnatk1 = '#3DDA42';
	$war1 = 'green-jungle';
}

if($sebesar2pol > $ceilsepuluh2pol){
	$warnatk2 = '#D04556';
	$war2 = 'red-mint';
}elseif($sebesarpol <= $ceilsepuluh2pol and $sebesarpol > $ceillima2pol){
	$warnatk2 = '#DFD43B';
	$war2 = 'yellow-soft';
}else{
	$warnatk2 = '#3DDA42';
	$war2 = 'green-jungle';
}


if($sebesarpoltk3 > $ceilsepuluh3){
	$warnatk3 = '#D04556';
	$war3 = 'red-mint';
}elseif($sebesarpoltk3 <= $ceilsepuluh3 and $sebesarpoltk3 > $ceillima3){
	$warnatk3 = '#DFD43B';
	$war3 = 'yellow-soft';
}else{
	$warnatk3 = '#3DDA42';
	$war3 = 'green-jungle';
}
        
?>

<input type="hidden" id="warnatk1pol" value="<?=$warnatk1?>">
<input type="hidden" id="warnatk2pol" value="<?=$warnatk2?>">
<input type="hidden" id="warnatk3pol" value="<?=$warnatk3?>">
<input type="hidden" id="piutk1pol" value="<?=$sebesarpol?>">
<input type="hidden" id="piutk2pol" value="<?=$sebesar2pol?>">
<input type="hidden" id="piutk3pol" value="<?=$sebesarpoltk3?>">
<input type="hidden" id="periodepol" value="<?=$periodespol?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Piutang Mahasiswa Politeknik</b>
        </div>
     </div>
	   <div class="portlet-body">
	   	<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war1?>" href="#" tipe='Registrasi Mahasiswa Baru' kriteria='PLJ' tahun='<?=$periodespol?>' id="poltingkat1" data-toggle="modal" data-target=".poltingkat1">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesarpol)?>">0</span>
		                                        </div>
		                                        <div class="desc"><b><p style="font-size: 20px;">Piutang Tingkat 1</p></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war2?>" href="#" tipe='Registrasi Mahasiswa Senior' kriteria='PLJ' tahun='<?=$periodespol?>' id="poltingkat2" data-toggle="modal" data-target=".poltingkat2">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesar2pol)?>">0</span></div>
		                                        <div class="desc"> <b><p style="font-size: 20px;">Piutang Tingkat 2</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war3?>" href="#" tipe='Registrasi Mahasiswa Tingkat 3' kriteria='PLJ' tahun='<?=$periodespol?>' id="poltingkat3" data-toggle="modal" data-target=".poltingkat3">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesarpoltk3)?>">0</span></div>
		                                        <div class="desc"> <b><p style="font-size: 20px;">Piutang Tingkat 3</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>
                       	</div>

                       	<br>
                        <div class="row">
                        	</div>
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-6">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpiupol"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-6">
										<div class="note note-warning">
			                                <ol>
			                                    
			                                    <li>Jika piutang > 10% dari total penerimaan maka akan berwarna merah</li>
			                                    <li>Jika piutang > 5% dan <=10% dari total penerimaan maka akan berwarna Kuning</li>
			                                    <li>Jika piutang <= 5% dari total penerimaan maka akan berwarna Hijau</li>
			                                </ol>
			                            </div>
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
	   </div>
</div>



<!-- menampilkan modal detail -->
<div class="modal fade poltingkat1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 1</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk1"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade poltingkat2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 2</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk2"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade poltingkat3" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 3</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk3"></div>
    	
   		
    </div>
  </div>
</div>


