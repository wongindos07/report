<?php
foreach($piutangsemua as $row1);
$piusemua = $row1->sebesar;

foreach($piutangsemua2 as $row2);
$piusemua2 = $row2->sebesar;

// rasio piutang tk1
$persentase1 = number_format(($sebesar / $piusemua) * 100);
// rasio piutang tk2
$persentase2 = number_format(($sebesar2 / $piusemua2) * 100);

foreach($piutangnotbiaya as $rowpiutangnot){
	$jumlahbiaya = $rowpiutangnot->jumlahbiaya;
	$jumlahterbayar = $rowpiutangnot->terbayar;
	$totalpiutangnot = $jumlahbiaya - $jumlahterbayar;
}

if($sebesar > $sepuluhpersen){
	$warnatk1 = '#D04556';
	$war1 = 'red-mint';
}elseif($sebesar <= $sepuluhpersen and $sebesar > $limapersen){
	$warnatk1 = '#DFD43B';
	$war1 = 'yellow-soft';
}else{
	$warnatk1 = '#3DDA42';
	$war1 = 'green-jungle';
}

if($sebesar2 > $sepuluhpersen2){
	$warnatk2 = '#D04556';
	$war2 = 'red-mint';
}elseif($sebesar2 <= $sepuluhpersen2 and $sebesar2 > $limapersen2){
	$warnatk2 = '#DFD43B';
	$war2 = 'yellow-soft';
}else{
	$warnatk2 = '#3DDA42';
	$war2 = 'green-jungle';
}

        
?>

<input type="hidden" id="warnatk1" value="<?=$warnatk1?>">
<input type="hidden" id="warnatk2" value="<?=$warnatk2?>">
<input type="hidden" id="piutk1" value="<?=$sebesar?>">
<input type="hidden" id="piutk2" value="<?=$sebesar2?>">
<input type="hidden" id="periodecol" value="<?=$periodecol?>">


<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Piutang Peserta Didik College</b>
        </div>
     </div>
	   <div class="portlet-body">
	   	<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war1?>" title="Klik untuk Detail" href="#" tipe='Registrasi Mahasiswa Baru' kriteria='Coll' tahun='<?=$periodes?>' id="coltingkat1" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".coltingkat1">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesar)?>">0</span>
		                                        </div>
		                                        <div class="desc"><b><p style="font-size: 20px;">Piutang Tingkat 1</p></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$war2?>" title="Klik untuk Detail" href="#" tipe='Registrasi Mahasiswa Senior' kriteria='Coll' tahun='<?=$periodes?>' id="coltingkat2" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".coltingkat2">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesar2)?>">0</span></div>
		                                        <div class="desc"> <b><p style="font-size: 20px;">Piutang Tingkat 2</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <?php if($totalpiutangnot <> 0){?>
		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 red-mint" href="#" title="Klik untuk Detail" kriteria='Coll' tahun='<?=$periodes?>' id="colpiutangnot" jeniscabang='<?=$jeniscabang?>' data-toggle="modal" data-target=".colpiutangnot">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($totalpiutangnot)?>">0</span></div>
		                                        <div class="desc"> <b><p style="font-size: 15px;">Piutang PD Belum Dibuatkan Rencana</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>
		                            <?php } ?>
                        		</div>
                        	</div>
                        </div>

                        <br>
                        <div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-6">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpiucol"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-6">
										<div class="note note-warning">
			                                <ol>
			                                    
			                                    <li>Jika piutang > 10% dari total penerimaan maka akan berwarna merah</li>
			                                    <li>Jika piutang > 5% dan <=10% dari total penerimaan maka akan berwarna Kuning</li>
			                                    <li>Jika piutang <= 5% dari total penerimaan maka akan berwarna Hijau</li>
			                                </ol>
			                            </div>
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
	   </div>
</div>

<!-- menampilkan modal detail -->
<div class="modal fade coltingkat1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 1</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk1col"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade coltingkat2" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL PIUTANG TINGKAT 2</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="tk2col"></div>
    	
   		
    </div>
  </div>
</div>

<div class="modal fade colpiutangnot" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> 
        <h4><b class="detail">DETAIL PIUTANG MHS BELUM DIBUATKAN RENCANA COLLEGE</b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="not"></div>
    	
   		
    </div>
  </div>
</div>