
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Aplikan</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Kategori Informasi</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tinfo =0;
											$trasioinf =0;
											$rasioinf=0;
											$f=0;
											foreach ($infomaster as $inf)
											{
												$f++;
												$info=$inf->info;
                                                $jumlahinfo=$inf->jumlahinfo;
                                                       
                                                
                                                $jumlahinfoapl=$jumlahinfo;
                                                $rasioinf = (($jumlahinfoapl/$totalap)*100);
											?>
												<tr >
													<td ><?=$info?></td>
													<td ><?=$jumlahinfoapl?></td>
													<td ><?=number_format($rasioinf,2)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$info?>" id="nama<?=$f?>" name="nama">
												<input type="hidden" value="<?=$jumlahinfoapl?>" id="jumlah<?=$f?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioinf)?>" id="rasio<?=$f?>" name="rasio">

											<?php 
											$tinfo += $jumlahinfo;
											$rasioinf = (($tinfo/$totalap)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$f?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tinfo?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartinfoapl" style="height: 420px; max-width: 980; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Peserta Didik Baru</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Kategori Informasi</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tinfo2 =0;
											$trasioinf2 =0;
											$rasioinf2=0;
											$f2=0;
											foreach ($infomaster2 as $inf2)
											{
												$f2++;
												$info2=$inf2->info2;
                                                $jumlahinfo2=$inf2->jumlahinfo2;
                                                       
                                                
                                                $jumlahinfoapl2=$jumlahinfo2;
                                                $rasioinf2 = (($jumlahinfoapl2/$totalbiodata)*100);
                                                
											?>
												<tr >
                                                <td ><?=$info2?></td>
													<td ><?=$jumlahinfoapl2?></td>
													<td ><?=number_format($rasioinf2,2)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$info2?>" id="nama<?=$f2?>" name="nama">
												<input type="hidden" value="<?=$jumlahinfoapl2?>" id="jumlah<?=$f2?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioinf2)?>" id="rasio<?=$f2?>" name="rasio">

											<?php 
											$tinfo2 += $jumlahinfo2;
											$rasioinf2 = (($tinfo2/$totalbiodata)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$f2?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tinfo2?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartinfoapl2" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>



					
