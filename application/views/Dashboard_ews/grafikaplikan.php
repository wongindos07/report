
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Aplikan</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Jenis Sekolah</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											<?php
											$tjumlah =0;
											$trasio =0;
											$rasio=0;
											$n=0;
											foreach ($sekolah as $sek)
											{
												$n++;
												$idsekolah=$sek->PddkAkhir;
												$jumlah=$sek->jumlah;
											
												//panggil nama sekolah
												$nm="select * from jenissekolah where PddkAkhir='$idsekolah'";
												$nmsek = $this->Mainmodel->tampildatacollege($nm);
												foreach($nmsek as $cek1)
												{
													$nama=$cek1->Pendidikan;
												}
												$rasio = (($jumlah/$totalap)*100);
											?>
												<tr >
													<td ><?=$nama?></td>
													<td ><?=$jumlah?></td>
													<td ><?=number_format($rasio)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$nama?>" id="nama<?=$n?>" name="nama">
												<input type="hidden" value="<?=$jumlah?>" id="jumlah<?=$n?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasio)?>" id="rasio<?=$n?>" name="rasio">

											<?php 
											$tjumlah += $jumlah;
											$trasio = (($tjumlah/$totalap)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$n?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlah?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartsekolah" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Peserta Didik baru</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Jenis Sekolah</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											<?php
											$tjumlahbio =0;
											$trasiobio =0;
											$rasiobio=0;
											$nbio=0;
											foreach ($sekolahbio as $sek2)
											{
												$nbio++;
												$jenissekolah=$sek2->js;
												$jumlahbio=$sek2->totalbio;
											
												//panggil nama sekolah
												//$nm2="select * from jenissekolah where PddkAkhir='$idsekolahbio'";
												//$nmsek2 = $this->Mainmodel->tampildatacollege($nm2);
												// foreach($nmsek2 as $cek2)
												// {
												// 	$namabio=$cek2>Pendidikan;
												// }
												$rasiobio = (($jumlahbio/$totalbiodata)*100);
												
											?>
												<tr >
													<td ><?=$jenissekolah?></td>
													<td ><?=$jumlahbio?></td>
													<td ><?=number_format($rasiobio)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$jenissekolah?>" id="nama<?=$n?>" name="nama">
												<input type="hidden" value="<?=$jumlahbio?>" id="jumlah<?=$n?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasiobio)?>" id="rasio<?=$n?>" name="rasio">

											<?php 
											$tjumlahbio += $jumlahbio;
											$trasiobio = (($tjumlahbio/$totalbiodata)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$n?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlahbio?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13 ">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartsekolahbio" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>


					
