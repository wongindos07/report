
<?php
if($rasio2 >=80){
	$warnanya = 'green-jungle';
}elseif($rasio2 <= 79 and $rasio2 >= 45){
	$warnanya = 'yellow-soft';
}else{
	$warnanya = 'red-mint';
}

if($satupol == 0){
$persentargetpol = 0;
$aplikandaftar = 0;
$persenu30pol = 0;
$persena30pol = 0;
$persen2pol = 0;
}else{
$persentargetpol = $satupol-($tigapol + $duapol);
$aplikandaftar = $duapol + $tigapol;
$persenu30pol = number_format(($tigapol/$satupol)*100);
$persena30pol = number_format(($duapol/$satupol)*100);
$persen2pol = number_format(($persentargetpol / $satupol)*100);
}


if($persenu30pol >= 0 and $persenu30pol < 50){
	$warnau30 = '#D04556';
}elseif($persenu30pol >= 50 and $persenu30pol < 65){
	$warnau30 = '#DFD43B';
}else{
	$warnau30 = '#3DDA42';
}

if($persena30pol >= 0 and $persena30pol < 50){
	$warnaa30 = '#D04556';
}elseif($persena30pol >= 50 and $persena30pol < 65){
	$warnaa30 = '#DFD43B';
}else{
	$warnaa30 = '#3DDA42';
}
?>

<input type="hidden" id="warnau30pol" value="<?=$warnau30?>">
<input type="hidden" id="warnaa30pol" value="<?=$warnaa30?>">

<input type="hidden" id="ketpol" value='<?=$ketpol?>'>
<input type="hidden" id="tahunpmbpol" value='<?=$tes?>'>
<input type="hidden" id="lebih30pol" value="<?=$duapol?>">
<input type="hidden" id="kurang30pol" value='<?=$tigapol?>'>

<input type="hidden" id="rasiotargetpol" value="<?=$persentargetpol?>">
<input type="hidden" id="rasiorealisasipol" value="<?=$aplikandaftar?>">



<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Perolehan PMB Politeknik</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="u30pol" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 blue-madison" href="#" tahun="<?=$tes?>" ket="<?=$ket?>" keterangan="U-30" jeniscabang="<?=$jeniscabang?>" kriteria="PLJ" data-toggle="modal" data-target=".u30pol">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$tigapol?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"><b> Reg &lt <?=$ketpol?></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="aktifpol" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 blue-madison" href="#" tahun="<?=$tes?>" ket="<?=$ket?>" keterangan="Aktif" jeniscabang="<?=$jeniscabang?>" kriteria="PLJ" data-toggle="modal" data-target=".a30pol">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$duapol?>">0</span> Orang
													</div>
		                                        <div class="desc"> <b>Reg &gt= <?=$ketpol?></b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		

                        		
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=$satupol?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"> <b>Target Junior</b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=ceil($rasio2)?>">0</span>%
		                                        </div>
		                                        <div class="desc"> <b>Rasio Penerimaan</b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        	</div>
                        	</div>
                        	</div>

                        	<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-2">
										
									</div>
									<div class="col-lg-8">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpmbpol"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-2">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>	
</div>
</div>



<!-- MODAL -->
<!-- menampilkan modal detail -->
<div class="modal fade u30pol" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL REGISTRASI &lt <?=$ket?></b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isiu30pol"></div>
    	
   		
    </div>
  </div>
</div>
<!--  -->

<!-- menampilkan modal detail -->
<div class="modal fade a30pol" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL REGISTRASI &gt= <?=$ket?></b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isia30pol"></div>
    	
   		
    </div>
  </div>
</div>
<!--  -->