

<?php
if($rasio >=80){
	$warnanya = 'green-jungle';
}elseif($rasio <= 79 and $rasio >= 45){
	$warnanya = 'yellow-soft';
}else{
	$warnanya = 'red-mint';
}

if($satu == 0){
	$persentarget 	= 0;
	$persenu30 		= 0;
	$persena30 		= 0;
}else{
	$persentarget 	= $satu -($tiga + $dua);
	$persenu30 		= number_format(($tiga/$satu)*100);
	$persena30 		= number_format(($dua/$satu)*100);
}



$aplikandaftar = $dua + $tiga;


if($persenu30 >= 0 and $persenu30 < 50){
	$warnau30 = '#D04556';
}elseif($persenu30 >= 50 and $persenu30 < 65){
	$warnau30 = '#DFD43B';
}else{
	$warnau30 = '#3DDA42';
}

if($persena30 >= 0 and $persena30 < 50){
	$warnaa30 = '#D04556';
}elseif($persena30 >= 50 and $persena30 < 65){
	$warnaa30 = '#DFD43B';
}else{
	$warnaa30 = '#3DDA42';
}

?>


<input type="hidden" id="ket" value='<?=$ket?>'>
<input type="hidden" id="tahunpmb" value='<?=$tes1?>'>

<input type="hidden" id="rasiotarget" value="<?=$persentarget?>">
<input type="hidden" id="rasiorealisasi" value="<?=$aplikandaftar?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Perolehan PMB College</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="u30" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 blue-madison" href="#" tahun="<?=$tes1?>" ket="<?=$ket?>" keterangan="U-30" jeniscabang="<?=$jeniscabang?>" kriteria="Coll" data-toggle="modal" data-target=".u30">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$tiga?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"><b>Registrasi &lt <?=$ket?></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a id="aktif" title="Klik untuk Detail" class="dashboard-stat dashboard-stat-v2 blue-madison" href="#" tahun="<?=$tes1?>" ket="<?=$ket?>" keterangan="Aktif" jeniscabang="<?=$jeniscabang?>" kriteria="Coll" data-toggle="modal" data-target=".a30">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$dua?>">0</span> Orang</div>
		                                        <div class="desc"> <b>Registrasi &gt= <?=$ket?></b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		
                        			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=$satu?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"> <b>Target Junior</b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=ceil($rasio)?>">0</span>%
		                                        </div>
		                                        <div class="desc"> <b>Rasio Penerimaan</b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>
                        	</div>
                        </div>

                        <div class="row">
                        	<div class="col-lg-12 col-md-12 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-2">
										
									</div>
									<div class="col-lg-8">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpmbcol"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-2">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>
                        </div>
                       
</div>
</div>


<!-- MODAL -->
<!-- menampilkan modal detail -->
<div class="modal fade u30" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL REGISTRASI &lt <?=$ket?></b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isiu30"></div>
    	
   		
    </div>
  </div>
</div>
<!--  -->

<!-- menampilkan modal detail -->
<div class="modal fade a30" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4><b class="detail">DETAIL REGISTRASI &gt= <?=$ket?></b><span class="load"><img src="<?=base_url()?>assets/img/load5.gif"></span></h4>
      </div>

      <div class="isia30"></div>
    	
   		
    </div>
  </div>
</div>
<!--  -->

					
