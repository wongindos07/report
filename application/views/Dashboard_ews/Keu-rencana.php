<?php
foreach($dpang as $rpang);
$rata1 = $rpang->rata1;
$rata2 = $rpang->rata2;

$persen1 = ($rata1/$jmlpercab)*100;
$persen2 = ($rata2/$jmlpercab)*100;
$tingkat1 = number_format($persen1);
$tingkat2 = number_format($persen2); 

// warna 1
if($tingkat1 >= 96){
	$warnanya = 'green-jungle';
}elseif($tingkat1 >= 71 && $tingkat1 <= 95){
	$warnanya = 'yellow-crusta';
}else{
	$warnanya = 'red-mint';
}
        
?>

<input type="hidden" id="tingkat1" value='<?=$tingkat1?>'>
<input type="hidden" id="tingkat2" value='<?=$tingkat2?>'>
<input type="hidden" id="taa" value='<?=$taren?>'>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Rencana Pembayaran College</b>
        </div>
     </div>
	   <div class="portlet-body">
	   		<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$tingkat1?>">0</span>%
		                                        </div>
		                                        <div class="desc" ><b>Rasio Input Rencana </b></div>
		                                        <div class="desc"><b>Tingkat 1 </b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$tingkat2?>">0</span>%</div>
		                                        <div class="desc"><b>Rasio Input Rencana </b></div>
		                                        <div class="desc"><b>Tingkat 2 </b></div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>

                        		
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartContainer5" style="height: 300px; width: 100%;"></div>
												<!-- <div id="chartContainer" style="height: 300px; width: 100%;"></div> -->
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-12">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
	   </div>
</div>
						