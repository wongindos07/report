
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Peserta Didik baru</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Jurusan</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											<?php
											$tjurus =0;
											$trasiojur =0;
											$rasiojur=0;
											$j=0;
											foreach ($infojurusan as $juru)
											{
												$j++;
												$jurusan=$juru->namajurusan;
												$jumlahjur=$juru->jumlahjur;
											
												//panggil nama sekolah
												//$nm2="select * from jenissekolah where PddkAkhir='$idsekolahbio'";
												//$nmsek2 = $this->Mainmodel->tampildatacollege($nm2);
												// foreach($nmsek2 as $cek2)
												// {
												// 	$namabio=$cek2>Pendidikan;
												// }
												$rasiobiojur = (($jumlahjur/$totalbiodata)*100);
												
											?>
												<tr >
													<td ><?=$jurusan?></td>
													<td ><?=$jumlahjur?></td>
													<td ><?=number_format($rasiobiojur)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$jurusan?>" id="nama<?=$j?>" name="nama">
												<input type="hidden" value="<?=$jumlahjur?>" id="jumlah<?=$j?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasiobiojur)?>" id="rasio<?=$j?>" name="rasio">

											<?php 
											$tjurus += $jumlahjur;
											$trasiojur = (($tjurus/$totalbiodata)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$j?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjurus?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13 ">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartjurus" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>


					
