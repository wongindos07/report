<style type="text/css">
    td,th{
        padding: 5px;
        color: black;
    }
    a{
    	color: black !important;
    }
    #judul{
    	color: black !important;
    }
</style>
<div class="row">
    <div class="col-md-12" id="judul">
        <div class="continer jud" style="text-align: center;">
            <h3><b><?=strtoupper($label)?></b></h3>
            <h3 style="margin-top: -5px;"><b><?=strtoupper($tipekampus)?></b></h3>
            <h4><b><?=$from.' s/d '.$to?></b></h4>
        </div>
    </div>
</div>
<table class="table table-striped table-bordered table-hover table-checkable order-column" id="myTable">
                                                        <thead>
                                                            <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                                <th>NO</th>
                                                                <th>NAMA APLIKAN</th>
                                                                <th>JURUSAN DIAMBIL</th>
                                                                <th>TANGGAL DAFTAR</th>
                                                                <th>GELOMBANG DAFTAR</th>
                                                                <th>BIAYA DAFTAR</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            $n = 1;
                                                            $total = '';
                                                            foreach($aplikan as $row){
                                                                $kojur = $row->KdJurusan;
                                                                ?>  
                                                                <tr>
                                                                    <td><?=$n++?></td>
                                                                    <td><?=$row->Nama?></td>
                                                                    <?php
                                                                    $jurusan = $this->Mainmodel->getWheres('jurusan',array('kodejurusan'=>$kojur),$kriteria)->result();
                                                                    foreach($jurusan as $rjur);
                                                                    $namajurusan = $rjur->namajurusan;
                                                                    ?>
                                                                    <td><?=$namajurusan?></td>
                                                                    <td><?=$row->TglDaftar?></td>
                                                                    <td><?=$row->Gelombang?></td>
                                                                    <td>Rp <?=number_format($row->total)?></td>
                                                                </tr>
                                                                <?php
                                                                $ttl = $row->total;
                                                                $total += $ttl;
                                                            }
                                                            ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5" class='bg-grey-gallery bg-font-grey-gallery' style="color: black !important;text-align: center !important;">TOTAL</td>
                                                                <td>
                                                                    <!-- Rp <?=number_format($total)?> -->
                                                                <?php
                                                                if($total == null){
                                                                    echo 0;
                                                                }else{
                                                                    echo 'Rp '.number_format($total);
                                                                }
                                                                ?>    
                                                                </td>
                                                            </tr>
                                                        </tfoot>
                                                    </table>