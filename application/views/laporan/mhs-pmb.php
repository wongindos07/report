<div class="row">
    <div class="col-md-12">
        <div class="continer jud" style="text-align: center;">
            <h3><b><?=strtoupper($label)?></b></h3>
            <h4><b>TAHUN AKADEMIK <?=$tak?></b></h4>
        </div>
    </div>
</div>

<table border="1" class="table table-condensed flip-content" style="border-collapse: collapse;">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>NO</th>
                                                    <th>NIM</th>
                                                    <th>NAMA MAHASISWA / PESERTA DIDIK</th>
                                                    <th>TANGGAL REGISTRASI</th>
                                                    <th>TOTAL DP</th>
                                                    <th>UANG KULIAH</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $no = 1;
                                                $totalnya1 = '';
                                                $totalnya2 = '';
                                                foreach($mahasiswa as $nmahasiswa){
                                                    $nim = $nmahasiswa->nim;
                                                    ?>
                                                    <tr>
                                                        <td><?=$no++?></td>
                                                        <td><?=$nmahasiswa->nim?></td>
                                                        <td><?=$nmahasiswa->Nama_Mahasiswa?></td>
                                                        <?php
                                                        $data1 = array(
                                                            'ket'=>$ket,
                                                            'nim'=>$nim,
                                                        );

                                                        $data2 = array(
                                                            'ket'=>$ket,
                                                            'nim'=>$nim,
                                                        );

                                                        $jumlahdp = $this->Mainmodel->getWheres('bayarkuliah',$data1,$kriteria)->result();
                                                        if($jumlahdp == null){
                                                            $jumlahdp = 0;
                                                           
                                                        }else{
                                                            foreach($jumlahdp as $cetakdp);
                                                            $jumlahdp = $cetakdp->jumlahbayar;
                                                            $tglregis = $cetakdp->tanggalbayar;
                                                            $totalnya1 += $cetakdp->jumlahbayar;
                                                        }
                                                        

                                                        $jumlahbiaya = $this->Mainmodel->getWheres('biayakuliah',$data2,$kriteria)->result();
                                                        if($jumlahbiaya == null){
                                                            $jumlahbiaya = 0;
                                                          
                                                        }else{
                                                            foreach($jumlahbiaya as $cetakbiaya);
                                                            $jumlahbiaya = $cetakbiaya->jumlahbiaya;
                                                            $totalnya2 += $cetakbiaya->jumlahbiaya;
                                                        }
                                                        
                                                        ?>
                                                        <td><?=$tglregis?></td>
                                                        <td>Rp <?=number_format($jumlahdp)?></td>
                                                        <td>Rp <?=number_format($jumlahbiaya)?></td>
                                                        
                                                    </tr>
                                                <?php
                                                }
                                                ?>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="4" class='bg-grey-gallery bg-font-grey-gallery'>TOTAL</td>
                                                    <td>Rp<?=number_format($totalnya1)?></td>
                                                    <td>Rp<?=number_format($totalnya2)?></td>
                                                    
                                                </tr>
                                                </tfoot>
                                            </tbody>
                                        </table>


<script type="text/javascript">
    var css = '@page { size: landscape; }',
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');

style.type = 'text/css';
style.media = 'print';

if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

head.appendChild(style);
    window.print();
</script>