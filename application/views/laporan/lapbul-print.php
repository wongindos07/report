<style type="text/css">
   table {
    border:solid #000 !important;
    border-width:1px 0 0 1px !important;
    }
    th, td {
        border:solid #000 !important;
        border-width:0 1px 1px 0 !important;
    }
    
</style>
<script type="text/javascript">
    var css = '@page { size: landscape; }',
    head = document.head || document.getElementsByTagName('head')[0],
    style = document.createElement('style');

style.type = 'text/css';
style.media = 'print';

if (style.styleSheet){
  style.styleSheet.cssText = css;
} else {
  style.appendChild(document.createTextNode(css));
}

head.appendChild(style);
    window.print();
</script>
