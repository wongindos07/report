<style>
.fin{
    margin-bottom: 20px;
}
</style>
                    <div class="col-lg-12 col-xs-12 col-sm-12" style="margin-top: 10px;">
                        <div class="portlet light bordered">


                                    <div class="portlet-title tabbable-line">
                                        <div class="caption">
                                            <i class="icon-bubbles font-dark hide"></i>
                                            <!-- <span class="caption-subject font-dark bold uppercase">Dashboard College</span> -->
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#portlet_comments_1" data-toggle="tab"> <b>College </b></a>
                                            </li>
                                            <li>
                                                <a href="#portlet_comments_2" data-toggle="tab"> <b>PLJ </b></a>
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="portlet-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="portlet_comments_1">
                                               <!-- Awal dashboard marketing -->
                                               <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan Jurusan Yang Dipilih </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikjurusan')?>
                                                    </div>
                                                </div>
                                               <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan JENIS SEKOLAH </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                        <?php $this->load->view('Dashboard_ews/grafikaplikan')?>
                                                    </div>
                                                </div>
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan UMUR </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikaplikanumur')?>
                                                    </div>
                                                </div>
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan GENDER </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikpemetaanjk')?>
                                                    </div>
                                                </div>
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan Sumber Informasi </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikSumberInfo')?>
                                                    </div>
                                                </div>
                                               
                                            </div>

                                            <div class="tab-pane" id="portlet_comments_2">
                                              <!-- Awal dashboard marketing -->
                                              <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan Jurusan Yang Dipilih </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikjurusanpoltek')?>
                                                    </div>
                                                </div>
                                               <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan JENIS SEKOLAH </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                        <?php $this->load->view('Dashboard_ews/grafikaplikanpoltek')?>
                                                    </div>
                                                </div>
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan UMUR </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikaplikanumurpoltek')?>
                                                    </div>
                                                </div>
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan GENDER </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikpemetaanjkpoltek')?>
                                                    </div>
                                                </div>
                                                <div class="portlet box blue-hoki">
                                                    <div class="portlet-title">
                                                        <div class="caption">
                                                            <i class="fa fa-gift"></i><b>Grafik Pemetaan Berdasarkan Sumber Informasi </b></div>
                                                    </div>
                                                    <div class="portlet-body" style='padding-top:30px;'>
                                                    <?php $this->load->view('Dashboard_ews/grafikSumberInfopoltek')?>
                                                    </div>
                                                </div>
                                               
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

