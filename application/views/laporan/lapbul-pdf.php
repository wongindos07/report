<style type="text/css">
    td,th{
        padding: 5px;
    }
    table{
    	margin-bottom: 50px !important;
    }
    .portlet-title{
    	margin-bottom: 30px !important;
    }
</style>
<?php
$ta = substr($tak,0,4);

$bull=array("", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "Nopember", "Desember");
		$thnx = substr($periode,0,4)*1;
		$blnx = substr($periode,5,2)*1;
		$tglx = substr($periode,8,2)*1;

		$gabb = $tglx." ".$bull[$blnx*1]." ".$thnx;
		$gabb2 = $bull[$blnx*1]." ".$thnx;

if($blnx == "01"){
			$bln_mundur = '12';
		}else{
			$bln_mundur=($blnx*1)-1;
		}
?>

	<div class="portlet light">
	<div class="portlet-title">
		<div class="caption">
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">JUMLAH MAHASISWA PROFESI</span>
				</div>	
			</div>
            <div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">Laporan Bulanan : <?=$gabb2?></span>
				</div>	
			</div>
			<div class="row">
				<div class="col-md-12">
					<span style="font-weight: bold;font-size: 15px;">FR - PDK - 079</span>
				</div>	
			</div>  
        </div>
        
	</div>
	<div class="portlet-body">
			
	<?php
	for($x=1;$x<=2;$x++){
		?>
			<div>
			  <table border="1">
				<b>Peserta Didik Tingkat</b>
                <thead>
                	<tr class="bg-grey-gallery bg-font-grey-gallery">
                	<td>No</td>
                	<td>Bidang Keahlian</td>
                	<td>Kelas</td>
                	<td>Jml Awal Bulan</td>
                	<td>Jml Bulan Januari</td>
                	<td>Jml Bulan Februari</td>
                	<td>Jml Out Bulan Ini</td>
                	<td>Total Out</td>
                	<td>%Out Bln Ini</td>
                	<td>%Total Out</td>
                	<td>Nama PA</td>
                	<td>Wkt Kuliah</td>
                	</tr>
                </thead>
                <tbody>
                
<?php
if($x == 1){
				$tot1x=0;
				$tot2x=0;
				$tot3x=0;
				$tot4x=0;
				$tot5x=0;

				$tot1=0;
				$tot2=0;
				$tot3=0;
				$tot4=0;
				$tot5=0;
				$no = 0;
				$persen_skrxx = 0;
				$persen_allxx = 0;
				$jurcab = $this->db->query("select biodata.kode from biodata where TahunAngkatan='$ta' and kodecabang='$cab' group by biodata.kode")->result();
				foreach($jurcab as $rjurcab){
					$kojur = $rjurcab->kode;

				$klscab = $this->db->query("select kelas from mhs_serahterima where kode='$kojur' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak' group by kelas")->result();
				foreach($klscab as $rklscab){
					$klscab = $rklscab->kelas;

				$getjur = $this->db->query("select namajurusan from jurusan where kodejurusan='$kojur'")->result();
				foreach($getjur as $rgetjur);
				$najur = $rgetjur->namajurusan;

				//JUMLAH AWAL BULAN
				$cekjml = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak'");
				$jumlah1 = $cekjml->num_rows();
				if($jumlah1 == 0){
					$hasil1 = 0;
				}else{
					$jmledu = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='1' and tahunakademik='$tak'")->result();
					foreach($jmledu as $rjmledu);
					if($rjmledu->jml_edu == ""){
						$hasil1 = 0;
					}else{
						$hasil1 = $rjmledu->jml_edu;
					}

				}


			//JUMLAH AWAL BULAN MUNDUR
			$cekjmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'");
				$jumlah2 = $cekjmlbln->num_rows();

				if($jumlah2 == 0){
					$hasil2 = 0;
				}else{

				$jmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'")->result();
				foreach($jmlbln as $rjmlbln);
				if($rjmlbln->jml_edu == ""){
						$hasil2 = 0;
					}else{
						$hasil2 = $rjmlbln->jml_edu;
					}

				}

				
				//JUMLAH AWAL BULAN SAAT INI
				$cekblnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'");
				$jumlah3 = $cekblnnow->num_rows();
				if($jumlah3 == 0){
					$hasil3 = 0;
				}else{
					$blnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'")->result();
					foreach($blnnow as $rblnnow);
					if($rblnnow->jml_edu == ""){
						$hasil3 = 0;
					}else{
						$hasil3 = $rblnnow->jml_edu;
					}
				}


				//JUMLAH OUT SEKARANG
				$cekout = $this->db->query("select * from mhs_statusdo where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' and month(tgl_pengajuan)='$blnx' and year(tgl_pengajuan)='$thnx'");
				$jumlah4 = $cekout->num_rows();
				if($jumlah4 == ""){
					$hasil4 = 0;
				}else{
					$hasil4 = $jumlah4;
				}


				//JUMLAH OUT HINGGA SEKARANG
				$cekoutnow = $this->db->query("select * from mhs_statusdo where tingkat='1' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' or tgl_pengajuan  ='$periode'");
				$jumlah5 = $cekoutnow->num_rows();
				if($jumlah5 == ""){
					$hasil5 = 0;
				}else{
					$hasil5 = $jumlah5;
				}


				//CARI KELAS, PA, WAKTU
				$cekjmlpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'");
				$jumlah6 = $cekjmlpa->num_rows();
				if($jumlah6 == ""){
					$pa = "-";
					$waktu = "-";
				}else{

				$cekpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'")->result();
				foreach($cekpa as $rcekpa);
				// validasi pa
				if($rcekpa->PA == ""){
					$pa = "-";
				}else{
					$pa = $rcekpa->PA;
				}

				// validasi waktu
				if($rcekpa->Waktu == ""){
					$waktu = "-";
				}else{
					$waktu = $rcekpa->Waktu;
				}

				}

				// MENCARI PERSENTASE
				if($hasil1 == 0){
					//%OUT BULAN INI
					$persen_skr = 0;
					
					//% TOTAL OUT
					$persen_all = 0;
				}else{
					//%OUT BULAN INI
					$persen_skr = ($hasil4/$hasil1)*100;
					if($persen_skr >= 100)
					{$persen_skr="100%";}else{$persen_skr=number_format($persen_skr,0)."%";}
					//% TOTAL OUT
					
					$persen_all = ($hasil5/$hasil1)*100;
					if($persen_all >= 100)
					{$persen_all="100%";}else{$persen_all=number_format($persen_all,0)."%";}
				}
				
				$no++;
		
				echo '
				    <tr>
				    	<td>'.$no.'</td>
				    	<td>'.$najur.'</td>
				    	<td>'.$klscab.'</td>
				    	<td>'.$hasil1.'</td>
				    	<td>'.$hasil2.'</td>
				    	<td>'.$hasil3.'</td>
				    	<td>'.$hasil4.'</td>
				    	<td>'.$hasil5.'</td>
				    	<td>'.$persen_skr.'</td>
				    	<td>'.$persen_all.'</td>
				    	<td>'.$pa.'</td>
				    	<td>'.$waktu.'</td>
				    </tr>
				    ';

				  $tot1+=$hasil1;
				  $tot2+=$hasil2;
				  $tot3+=$hasil3;
				  $tot4+=$hasil4;
				  $tot5+=$hasil5;
				

				}
				// tutup foreach klscab

				}
				// tutup foreach jurcab

				if($tot1==0)
					{
					//%OUT BULAN INI
					$persen_skrx = 0;
					//% TOTAL OUT
					$persen_allx = 0;
					}
					else
					{
					//%OUT BULAN INI
					$persen_skrx = ($tot4/$tot1)*100;
					//% TOTAL OUT
					$persen_allx = ($tot5/$tot1)*100;
					}

					echo '
					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL</td>
					<td>'.$tot1.'</td>
					<td>'.$tot2.'</td>
					<td>'.$tot3.'</td>
					<td>'.$tot4.'</td>
					<td>'.$tot5.'</td>
					<td>'.number_format($persen_skrx,2).'</td>
					<td>'.number_format($persen_allx,2).'</td>
					<td colspan="2"></td>
			
					</tr>';

			}elseif($x == 2){
			

			$no = 0;
			$jurcab = $this->db->query("select regissenior.kode from regissenior inner join biodata on regissenior.kode=regissenior.kode where regissenior.ta='$tak' and regissenior.kodecabang='$cab' group by regissenior.kode")->result();
			foreach($jurcab as $rjurcab){
				$kojur = $rjurcab->kode;
				$klscab = $this->db->query("select kelas from mhs_serahterima where kode='$kojur' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak' group by kelas")->result();
				foreach($klscab as $rklscab){
					$klscab = $rklscab->kelas;

				$getjur = $this->db->query("select namajurusan from jurusan where kodejurusan='$kojur'")->result();
				foreach($getjur as $rgetjur);
				$najur = $rgetjur->namajurusan;

				//JUMLAH AWAL BULAN
				$cekjml = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak'");
				$jumlah1 = $cekjml->num_rows();
				if($jumlah1 == 0){
					$hasil1 = 0;
				}else{
					$jmledu = $this->db->query("select jml_edu from mhs_serahterima where kode='$kojur' and kelas='$klscab' and kodecabang='$cab' and tingkat='2' and tahunakademik='$tak'")->result();
					foreach($jmledu as $rjmledu);
					if($rjmledu->jml_edu == ""){
						$hasil1 = 0;
					}else{
						$hasil1 = $rjmledu->jml_edu;
					}

				}


			//JUMLAH AWAL BULAN MUNDUR
			$cekjmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'");
				$jumlah2 = $cekjmlbln->num_rows();

				if($jumlah2 == 0){
					$hasil2 = 0;
				}else{

				$jmlbln = $this->db->query("select jml_edu from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$bln_mundur' and year(tanggalinput)='$thnx'")->result();
				foreach($jmlbln as $rjmlbln);
				if($rjmlbln->jml_edu == ""){
						$hasil2 = 0;
					}else{
						$hasil2 = $rjmlbln->jml_edu;
					}

				}

				
				//JUMLAH AWAL BULAN SAAT INI
				$cekblnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'");
				$jumlah3 = $cekblnnow->num_rows();
				if($jumlah3 == 0){
					$hasil3 = 0;
				}else{
					$blnnow = $this->db->query("select jml_edu,tanggalinput from mhs_jml_bln where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunakademik='$tak' and month(tanggalinput)='$blnx' and year(tanggalinput)='$thnx'")->result();
					foreach($blnnow as $rblnnow);
					if($rblnnow->jml_edu == ""){
						$hasil3 = 0;
					}else{
						$hasil3 = $rblnnow->jml_edu;
					}
				}


				//JUMLAH OUT SEKARANG
				$cekout = $this->db->query("select * from mhs_statusdo where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' and month(tgl_pengajuan)='$blnx' and year(tgl_pengajuan)='$thnx'");
				$jumlah4 = $cekout->num_rows();
				if($jumlah4 == ""){
					$hasil4 = 0;
				}else{
					$hasil4 = $jumlah4;
				}


				//JUMLAH OUT HINGGA SEKARANG
				$cekoutnow = $this->db->query("select * from mhs_statusdo where tingkat='2' and kodecabang='$cab' and kelas='$klscab' and kode='$kojur' and tahunangkatan='$ta' or tgl_pengajuan  ='$periode'");
				$jumlah5 = $cekoutnow->num_rows();
				if($jumlah5 == ""){
					$hasil5 = 0;
				}else{
					$hasil5 = $jumlah5;
				}


				//CARI KELAS, PA, WAKTU
				$cekjmlpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'");
				$jumlah6 = $cekjmlpa->num_rows();
				if($jumlah6 == ""){
					$pa = "-";
					$waktu = "-";
				}else{

				$cekpa = $this->db->query("select * from kelas where kodecabang='$cab' and kelas='$klscab' and kodejurusan='$kojur'")->result();
				foreach($cekpa as $rcekpa);
				// validasi pa
				if($rcekpa->PA == ""){
					$pa = "-";
				}else{
					$pa = $rcekpa->PA;
				}

				// validasi waktu
				if($rcekpa->Waktu == ""){
					$waktu = "-";
				}else{
					$waktu = $rcekpa->Waktu;
				}

				}

				// MENCARI PERSENTASE
				if($hasil1 == 0){
					//%OUT BULAN INI
					$persen_skr = 0;
					
					//% TOTAL OUT
					$persen_all = 0;
				}else{
					//%OUT BULAN INI
					$persen_skr = ($hasil4/$hasil1)*100;
					if($persen_skr >= 100)
					{$persen_skr="100%";}else{$persen_skr=number_format($persen_skr,0)."%";}
					//% TOTAL OUT
					
					$persen_all = ($hasil5/$hasil1)*100;
					if($persen_all >= 100)
					{$persen_all="100%";}else{$persen_all=number_format($persen_all,0)."%";}
				}
				
				$no++;
		
				echo '
				    <tr>
				    	<td>'.$no.'</td>
				    	<td>'.$najur.'</td>
				    	<td>'.$klscab.'</td>
				    	<td>'.$hasil1.'</td>
				    	<td>'.$hasil2.'</td>
				    	<td>'.$hasil3.'</td>
				    	<td>'.$hasil4.'</td>
				    	<td>'.$hasil5.'</td>
				    	<td>'.$persen_skr.'</td>
				    	<td>'.$persen_all.'</td>
				    	<td>'.$pa.'</td>
				    	<td>'.$waktu.'</td>
				    </tr>
				    ';

				  $tot1x+=$hasil1;
				  $tot2x+=$hasil2;
				  $tot3x+=$hasil3;
				  $tot4x+=$hasil4;
				  $tot5x+=$hasil5;
				

				}
				// tutup foreach klscab

			}
			// tutup foreach jurcab
			if($tot1x==0)
					{
					//%OUT BULAN INI
					$persen_skrxx = 0;
					//% TOTAL OUT
					$persen_allxx = 0;
					}
					else
					{
					//%OUT BULAN INI
					$persen_skrxx = ($tot4x/$tot1x)*100;
					//% TOTAL OUT
					$persen_allxx = ($tot5x/$tot1x)*100;
					}

					echo '
					<tr class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL</td>
					<td>'.$tot1x.'</td>
					<td>'.$tot2x.'</td>
					<td>'.$tot3x.'</td>
					<td>'.$tot4x.'</td>
					<td>'.$tot5x.'</td>
					<td>'.number_format($persen_skrxx,2).'</td>
					<td>'.number_format($persen_allxx,2).'</td>
					<td colspan="2"></td>
		
					</tr>';

				

			}
			// tutup if $x
			$tott1 = $tot1 + $tot1x;
			$tott2 = $tot2 + $tot2x;
			$tott3 = $tot3 + $tot3x;
			$tott4 = $tot4 + $tot4x;
			$tott5 = $tot5 + $tot5x;
			$persen1 = $persen_skrx + $persen_skrxx;
			$persen2 = $persen_allx + $persen_allxx;

	if($x == 2){
	?>			
				
				<tr style="margin-top: 50px !important;" class="bg-grey-gallery bg-font-grey-gallery">
					<td colspan="3">TOTAL PROFESI</td>
					<td><?=$tott1?></td>
					<td><?=$tott2?></td>
					<td><?=$tott3?></td>
					<td><?=$tott4?></td>
					<td><?=$tott5?></td>
					<td><?=number_format($persen1,2)?></td>
					<td><?=number_format($persen2,2)?></td>
					<td colspan="2"></td>
					
				</tr>
	<?php } ?>
				</tbody>
            </table>
          </div>
<?php

	}
	// tutup for
?>	
	
			
			
				
	
				