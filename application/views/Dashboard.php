<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <?php
    $this->load->view('tools/head');
    ?>
    <!-- END HEAD -->
    <style type="text/css">
        .jarak{
            margin-bottom: 20px !important;
        }
        .desc{
            font-size: 18px !important;
        }
        .logo-default{
            margin-top: -0px !important;
            width: 100px !important;
            height: 50px !important;
        }
        .loading{
        position: fixed;
        top: 40%;
        left: 50%;
        width: 130px;
        height: 65px;
        z-index: 9999;
        display: none;
      }
      .loading img{
        width: 70px;
        height: 70px;
      }
      .load img{
        width: 30px;
        height: 30px;
        margin-left: 12px;
      }
      .kotak{
        margin-bottom: 10px;
      }
      .isilmsstudent{
        padding: 30px 20px;
      }
      .img-circle{
        margin-right: 10px !important;
      }
    </style>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?=base_url()?>/Dashboard">
                            <img src="<?=base_url()?>assets/img/esr-directorate.png" alt="logo" class="logo-default" /> </a>
                            <br>
                            <div class="menu-toggler sidebar-toggler">
                            <span></span>
                            </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            
                           
                           
                            <!-- BEGIN TODO DROPDOWN -->
                           
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <?php
                            $nik = $this->session->userdata('nik');
                            $getfoto = $this->db->query("select * from profilkaryawan where nik='$nik'")->result();
                            if($getfoto == null){
                                $foto = "";
                                $img = "<img alt='' class='img-circle' src='../assets/img/noimg.jpg'>";
                            }else{
                            foreach($getfoto as $fotonya);
                                $foto = $fotonya->imagenya;
                                $img = "<img alt='' class='img-circle' src='https://direktorat.lp3i.ac.id/$foto'>";
                            }
                            ?>
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="color: white">

                                    <?=$img?>
                                    <span class="username username-hide-on-mobile"> 
                                    <?php
                                    
                                    $getNama = $this->db->query("select * from profil_struktur where nik='$nik'")->result();
                                    foreach($getNama as $ambilnama);
                                    echo $ambilnama->jabatan;
                                    ?> 
                                    </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!-- <li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li> -->
                                    <li>
                                        <a href="<?=base_url('index.php/Main/logout')?>">
                                            <i class="icon-logout"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <?php
                $this->load->view('tools/sidebar');
                ?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            
                            <div class="page-toolbar">
                                <div class="pull-right tooltips btn btn-sm">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <?=date('d F Y')?>&nbsp;
                                </div>
                                <div class="pull-right tooltips btn btn-sm">
                                    <i class="icon-clock"></i>&nbsp;
                                    <span id="text"></span>&nbsp;
                                </div>
                            </div>
                        </div>
                        <!-- END PAGE BAR -->
                        <!-- BEGIN PAGE TITLE-->
                        
                            <?php
                            $this->load->view($konten);
                            ?>
                        
                        <div class="clearfix"></div>
                        <!-- END DASHBOARD STATS 1-->


                        <!-- membuat loading gif -->
                          <div class="loading">
                            <div class="row">
                              <div class="col-lg-12">
                                <img src="<?=base_url()?>assets/img/Pacman-1s-200px.gif">
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-lg-12"><b><font style="margin-left: -5px;font-size: 16px;">Sabar ya...</font></b></div>
                            </div>
                          </div>
                       
                       
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?php
            $this->load->view('tools/footer');
            ?>
            <!-- END FOOTER -->
        </div>
        
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script> 
<script src="../assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <?php
        $this->load->view('js/script');
        $this->load->view('js/scriptku');
        $this->load->view('js/jenis-cabang');
        ?>
        
        <!-- END THEME LAYOUT SCRIPTS -->
        
        
    </body>

</html>