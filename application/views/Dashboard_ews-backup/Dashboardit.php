<?php
$lmsmhs = number_format($lmsmhs);
$lmsweb = number_format($lmsweb);
$rasiodoselec = number_format($rasiodoselec);
$rasiodoslms = number_format($rasiodoslms);

if($lmsmhs >=90){
	$warnanya = 'green-jungle';
	$wrn = '#3DDA42';
}elseif($lmsmhs <= 30 and $lmsmhs >= 60){
	$warnanya = 'yellow-soft';
	$wrn = '#DFD43B';
}else{
	$warnanya = 'red-mint';
	$wrn = '#D04556';
}


if($lmsweb >=100){
	$warnanya1 = 'green-jungle';
	$wrn1 = '#3DDA42';
}elseif($lmsmhs <= 99 and $lmsmhs >= 75){
	$warnanya1 = 'yellow-soft';
	$wrn1 = '#DFD43B';
}else{
	$warnanya1 = 'red-mint';
	$wrn1 = '#D04556';
}


if($rasiodoselec >=99){
	$warnanya2 = 'green-jungle';
	$wrn2 = '#3DDA42';
}elseif($rasiodoselec < 99 and $rasiodoselec >= 75){
	$warnanya2 = 'yellow-soft';
	$wrn2 = '#DFD43B';
}else{
	$warnanya2 = 'red-mint';
	$wrn2 = '#D04556';
}

if($rasiodoslms >=90){
	$warnanya3 = 'green-jungle';
	$wrn3 = '#3DDA42';
}elseif($rasiodoslms <= 89 and $rasiodoslms >= 75){
	$warnanya3 = 'yellow-soft';
	$wrn3 = '#DFD43B';
}else{
	$warnanya3 = 'red-mint';
	$wrn3 = '#D04556';
}

?>

<input type="hidden" id="lmsmhs" value="<?=$lmsmhs?>">
<input type="hidden" id="lmsweb" value="<?=$lmsweb?>">
<input type="hidden" id="rasiodoselec" value="<?=$rasiodoselec?>">
<input type="hidden" id="rasiodoslms" value="<?=$rasiodoslms?>">
<input type="hidden" id="periode" value="<?=$periode?>">
<input type="hidden" id="tahunaka" name="" value="<?=$tahunaka?>">

<input type="hidden" id="wrn" value="<?=$wrn?>">
<input type="hidden" id="wrn1" value="<?=$wrn1?>">
<input type="hidden" id="wrn2" value="<?=$wrn2?>">
<input type="hidden" id="wrn3" value="<?=$wrn3?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Penggunaan E-Student dan E-Lecturer College</b>
        </div>
	  </div>
<div class="portlet-body">
	<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12">
<div class="row">					

									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya1?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$lmsweb?>">0</span>%</div>
		                                        <div class="desc"> <b>E - Student</b> </div>
		                                    </div>
		                                </a>
		                            </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a id="lmsstudent" class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$lmsmhs?>">0</span>%
		                                        </div>
		                                        <div class="desc"><b> LMS Student </b></div>
		                                    </div>
		                                </a>
		                            </div>

                                    
</div>

<div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya2?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$rasiodoselec?>">0</span>%
		                                        </div>
		                                        <div class="desc"><b> E - Lecturer </b></div>
		                                    </div>
		                                </a>
		                            </div>

                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya3?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$rasiodoslms?>">0</span>%</div>
		                                        <div class="desc"> <b>LMS Lecturer</b> </div>
		                                    </div>
		                                </a>
		                            </div>
</div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12">
<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="aplikasi"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-12">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
</div>
</div>
</div>
</div>




<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 98%;height: 40%;">
    <div class="modal-content">

      <div class="modal-header">
        <h4><b>Detail LMS STUDENT</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="isilmsstudent"></div>
    	
   		
    </div>
  </div>
</div>

