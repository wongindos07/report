

<?php
if($rasio >=80){
	$warnanya = 'green-jungle';
}elseif($rasio <= 79 and $rasio >= 45){
	$warnanya = 'yellow-soft';
}else{
	$warnanya = 'red-mint';
}

$persentarget 	= $satu-($tiga + $dua);
$persenu30 		= number_format(($tiga/$satu)*100);
$persena30 		= number_format(($dua/$satu)*100);
$persen2 		= number_format(($persentarget / $satu)*100);

if($persenu30 >= 0 and $persenu30 < 50){
	$warnau30 = '#D04556';
}elseif($persenu30 >= 50 and $persenu30 < 65){
	$warnau30 = '#DFD43B';
}else{
	$warnau30 = '#3DDA42';
}

if($persena30 >= 0 and $persena30 < 50){
	$warnaa30 = '#D04556';
}elseif($persena30 >= 50 and $persena30 < 65){
	$warnaa30 = '#DFD43B';
}else{
	$warnaa30 = '#3DDA42';
}

?>

<input type="hidden" id="warnau30" value="<?=$warnau30?>">
<input type="hidden" id="warnaa30" value="<?=$warnaa30?>">

<input type="hidden" id="ket" value='<?=$ket?>'>
<input type="hidden" id="tahunpmb" value='<?=$tes1?>'>
<input type="hidden" id="lebih30" value="<?=$dua?>">
<input type="hidden" id="kurang30" value='<?=$tiga?>'>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Perolehan PMB College</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$tiga?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"><b>Registrasi &lt <?=$ket?></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$dua?>">0</span> Orang</div>
		                                        <div class="desc"> <b>Registrasi &gt= <?=$ket?></b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>

                        		<div class="row">
                        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=$satu?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"> <b>Target Junior</b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=floor($rasio)?>">0</span>%
		                                        </div>
		                                        <div class="desc"> <b>Rasio Penerimaan</b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpmbcol"></canvas>
												
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-12">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>



					
