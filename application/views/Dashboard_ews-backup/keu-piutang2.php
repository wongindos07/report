<?php
foreach($piutangsemuapol as $row1);
$piusemuapol = $row1->sebesar;

foreach($piutangsemua2pol as $row2);
$piusemua2pol = $row2->sebesar;

// rasio piutang tk1
$persentase1 = number_format(($sebesarpol / $piusemuapol) * 100);
// rasio piutang tk2
$persentase2 = number_format(($sebesar2pol / $piusemua2pol) * 100);

if($persentase1 >= 0 and $persentase1 < 50){
	$warnatk1 = '#D04556';
}elseif($persentase1 >= 50 and $persentase1 < 65){
	$warnatk1 = '#DFD43B';
}else{
	$warnatk1 = '#3DDA42';
}

if($persentase2 >= 0 and $persentase2 < 50){
	$warnatk2 = '#D04556';
}elseif($persentase2 >= 50 and $persentase2 < 65){
	$warnatk2 = '#DFD43B';
}else{
	$warnatk2 = '#3DDA42';
}
        
?>

<input type="hidden" id="warnatk1pol" value="<?=$warnatk1?>">
<input type="hidden" id="warnatk2pol" value="<?=$warnatk2?>">
<input type="hidden" id="piutk1pol" value="<?=$sebesarpol?>">
<input type="hidden" id="piutk2pol" value="<?=$sebesar2pol?>">
<input type="hidden" id="periodepol" value="<?=$periodespol?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Piutang Mahasiswa Politeknik</b>
        </div>
     </div>
	   <div class="portlet-body">
	   	<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesarpol)?>">0</span>
		                                        </div>
		                                        <div class="desc"><b><p style="font-size: 20px;">Piutang Tingkat 1</p></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number"  style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            Rp <span data-counter="counterup" data-value="<?=number_format($sebesar2pol)?>">0</span></div>
		                                        <div class="desc"> <b><p style="font-size: 20px;">Piutang Tingkat 2</p></b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>

                        		
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpiupol"></canvas>
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-12">
									
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
	   </div>
</div>


						