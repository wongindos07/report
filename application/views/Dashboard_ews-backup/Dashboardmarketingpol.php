
<?php
if($rasio2 >=80){
	$warnanya = 'green-jungle';
}elseif($rasio2 <= 79 and $rasio2 >= 45){
	$warnanya = 'yellow-soft';
}else{
	$warnanya = 'red-mint';
}

$persentargetpol = $satupol-($tigapol + $duapol);
$persenu30pol = number_format(($tigapol/$satupol)*100);
$persena30pol = number_format(($duapol/$satupol)*100);
$persen2pol = number_format(($persentargetpol / $satupol)*100);

if($persenu30pol >= 0 and $persenu30pol < 50){
	$warnau30 = '#D04556';
}elseif($persenu30pol >= 50 and $persenu30pol < 65){
	$warnau30 = '#DFD43B';
}else{
	$warnau30 = '#3DDA42';
}

if($persena30pol >= 0 and $persena30pol < 50){
	$warnaa30 = '#D04556';
}elseif($persena30pol >= 50 and $persena30pol < 65){
	$warnaa30 = '#DFD43B';
}else{
	$warnaa30 = '#3DDA42';
}
?>

<input type="hidden" id="warnau30pol" value="<?=$warnau30?>">
<input type="hidden" id="warnaa30pol" value="<?=$warnaa30?>">

<input type="hidden" id="ketpol" value='<?=$ketpol?>'>
<input type="hidden" id="tahunpmbpol" value='<?=$tes?>'>
<input type="hidden" id="lebih30pol" value="<?=$duapol?>">
<input type="hidden" id="kurang30pol" value='<?=$tigapol?>'>



<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Perolehan PMB Politeknik</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
                        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$tigapol?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"><b> Reg &lt <?=$ketpol?></b></div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="<?=$duapol?>">0</span> Orang
													</div>
		                                        <div class="desc"> <b>Reg &gt <?=$ketpol?></b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>

                        		<div class="row">
                        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue-madison" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=$satupol?>">0</span> Orang
		                                        </div>
		                                        <div class="desc"> <b>Target Junior</b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 28px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="<?=ceil($rasio2)?>">0</span>%
		                                        </div>
		                                        <div class="desc"> <b>Rasio Penerimaan</b> </div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<canvas id="chartpmbpol"></canvas>
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-12">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>	
</div>
</div>



