<?php
$ttlmhs2 = $subbyr;
$ttlpiu = $blmterbayar;
$ttlsummhs = $ttltarget;

if($ttlsummhs <> 0){

$persentasesebelum = ($ttlpiu/$ttlsummhs)*100;
$persentasebayar = ($ttlmhs2/$ttlsummhs)*100;

$satu = number_format($persentasesebelum);
$dua = number_format($persentasebayar);

if($satu < 71 && $dua >= 0){
	$warnanya = 'red-mint';
}elseif($satu >= 71 && $dua < 96){
	$warnanya = 'yellow-crusta';
}elseif($satu >= 96){
	$warnanya = 'green-jungle';
}

if($dua < 71 && $dua >= 0){
	$warnanya2 = 'red-mint';
}elseif($dua >= 71 && $dua < 96){
	$warnanya2 = 'yellow-crusta';
}elseif($dua >= 96){
	$warnanya2 = 'green-jungle';
}

}else{
	$satu = 0;
	$dua = 0;

	if($satu < 71 && $dua >= 0){
	$warnanya = 'red-mint';
}elseif($satu >= 71 && $dua < 96){
	$warnanya = 'yellow-crusta';
}elseif($satu >= 96){
	$warnanya = 'green-jungle';
}

if($dua < 71 && $dua >= 0){
	$warnanya2 = 'red-mint';
}elseif($dua >= 71 && $dua < 96){
	$warnanya2 = 'yellow-crusta';
}elseif($dua >= 96){
	$warnanya2 = 'green-jungle';
}

$notif = '<div class="alert alert-danger" role="alert">
										<center><b>Data Tidak Ditemukan !!!</b></center>
										</div>';
}


?>

<input type="hidden" id="rencana" value="<?=$satu?>">
<input type="hidden" id="bayar" value="<?=$dua?>">
<input type="hidden" id="target" value="<?=$ttlsummhs?>">
<input type="hidden" id="tahunreal" value="<?=$tahunpmb?>">

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>EWS Realisasi Pembayaran College</b>
        </div>
     </div>
	   <div class="portlet-body">
	   		<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya2?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-bar-chart-o"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="Rp <?=number_format($ttlpiu)?>">0</span></div>
		                                        <div class="desc"> <b>Realisasi Yang Belum Terbayar</b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 <?=$warnanya?>" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 22px;font-weight:bold;">
		                                        	
		                                            <span data-counter="counterup" data-value="Rp <?=number_format($ttlmhs2)?>">0</span>
		                                        </div>
		                                        <div class="desc"><b>Realisasi Yang Sudah Terbayar</b></div>
		                                    </div>
		                                </a>
		                            </div>
                        		</div>

                        		<div class="row">
                        			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 kotak">
		                                <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
		                                    <div class="visual">
		                                        <i class="fa fa-comments"></i>
		                                    </div>
		                                    <div class="details">
		                                        <div class="number" style="font-size: 22px;font-weight:bold;">
		                                            <span data-counter="counterup" data-value="Rp <?=number_format($ttlsummhs)?>">0</span>
		                                        </div>
		                                        <div class="desc"> <b>Target Rencana Bayar</b> </div>
		                                    </div>
		                                </a>
		                            </div>

		                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-12">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
												<?php
												if($ttlsummhs == 0){
													echo $notif;
												}else{
												?>
                                            	<div id="chartContainer6" style="height: 300px; width: 100%;"></div>
												<?php } ?>
												<!-- <div id="chartContainer" style="height: 300px; width: 100%;"></div> -->
                                            </div>
                                        </div>
									</div>
									<div class="col-lg-12">
										
									</div>
								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
	   </div>
</div>



        