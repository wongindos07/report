
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Aplikan</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Jenis Sekolah</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											<?php
											$tjumlahpoltek =0;
											$trasiopoltek =0;
											$rasiopoltek=0;
											$npoltek=0;
											foreach ($sekolahpoltek as $sekpoltek)
											{
												$npoltek++;
												$idsekolahpoltek=$sekpoltek->PddkAkhir;
												$jumlahpoltek=$sekpoltek->jumlah;
											
												//panggil nama sekolah
												$nmpoltek="select * from jenissekolah where PddkAkhir='$idsekolahpoltek'";
												$nmsekpoltek = $this->Mainmodel->tampildatapoltek($nmpoltek);
												foreach($nmsekpoltek as $cek1poltek)
												{
													$namapoltek=$cek1poltek->Pendidikan;
												}
												$rasiopoltek = (($jumlahpoltek/$totalappoltek)*100);
											?>
												<tr >
													<td ><?=$namapoltek?></td>
													<td ><?=$jumlahpoltek?></td>
													<td ><?=number_format($rasiopoltek)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$namapoltek?>" id="nama<?=$npoltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahpoltek?>" id="jumlah<?=$npoltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasiopoltek)?>" id="rasio<?=$npoltek?>" name="rasio">

											<?php 
											$tjumlahpoltek += $jumlahpoltek;
											$trasiopoltek = (($tjumlahpoltek/$totalappoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$npoltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlahpoltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartsekolahpoltek" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Mahasiswa Baru</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Jenis Sekolah</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											<?php
											$tjumlahbiopoltek =0;
											$trasiobiopoltek =0;
											$rasiobiopoltek=0;
											$nbiopoltek=0;
											foreach ($sekolahbiopoltek as $sek2poltek)
											{
												$nbiopoltek++;
												$jenissekolahpoltek=$sek2poltek->js;
												$jumlahbiopoltek=$sek2poltek->totalbio;
											
												//panggil nama sekolah
												//$nm2="select * from jenissekolah where PddkAkhir='$idsekolahbio'";
												//$nmsek2 = $this->Mainmodel->tampildatacollege($nm2);
												// foreach($nmsek2 as $cek2)
												// {
												// 	$namabio=$cek2>Pendidikan;
												// }
												$rasiobiopoltek = (($jumlahbiopoltek/$totalbiodatapoltek)*100);
												
											?>
												<tr >
													<td ><?=$jenissekolahpoltek?></td>
													<td ><?=$jumlahbiopoltek?></td>
													<td ><?=number_format($rasiobiopoltek)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$jenissekolahpoltek?>" id="nama<?=$npoltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahbiopoltek?>" id="jumlah<?=$npoltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasiobiopoltek)?>" id="rasio<?=$npoltek?>" name="rasio">

											<?php 
											$tjumlahbiopoltek += $jumlahbiopoltek;
											$trasiobiopoltek = (($tjumlahbiopoltek/$totalbiodatapoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$npoltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlahbiopoltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13 ">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartsekolahbiopoltek" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>


					
