
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Aplikan</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Gender</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tgenpoltek =0;
											$trasiogenpoltek =0;
											$rasiogenpoltek=0;
											$gpoltek=0;
											foreach ($sexaplikanpoltek as $sex1poltek)
											{
												$gpoltek++;
												$genderaplpoltek=$sex1poltek->sexapli;
												$jumlahsexpoltek=$sex1poltek->jumlahsex;
											
                                                $rasiogenpoltek = (($jumlahsexpoltek/$totalappoltek)*100);
											?>
												<tr >
													<td ><?=$genderaplpoltek?></td>
													<td ><?=$jumlahsexpoltek?></td>
													<td ><?=number_format($rasiogenpoltek)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$genderaplpoltek?>" id="nama<?=$gpoltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahsexpoltek?>" id="jumlah<?=$gpoltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasiogenpoltek)?>" id="rasio<?=$gpoltek?>" name="rasio">

											<?php 
											$tgenpoltek += $jumlahsexpoltek;
											$trasiogenpoltek = (($tgenpoltek/$totalappoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$gpoltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tgenpoltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartgenderpoltek" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Mahasiswa Baru</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Gender</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tgen2poltek =0;
											$trasiogen2poltek =0;
											$rasiogen2poltek=0;
											$g2poltek=0;
											foreach ($sexbiodatapoltek as $sex2poltek)
											{
												$g2poltek++;
												$genderapl2poltek=$sex2poltek->sexbiod;
												$jumlahsex2poltek=$sex2poltek->jumlahsexbio;
											
                                                $rasiogen2poltek = (($jumlahsex2poltek/$totalbiodatapoltek)*100);
                                                
											?>
												<tr >
													<td ><?=$genderapl2poltek?></td>
													<td ><?=$jumlahsex2poltek?></td>
													<td ><?=number_format($rasiogen2poltek)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$genderapl2poltek?>" id="nama<?=$g2poltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahsex2poltek?>" id="jumlah<?=$g2poltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasiogen2poltek)?>" id="rasio<?=$g2poltek?>" name="rasio">

											<?php 
											$tgen2poltek += $jumlahsex2poltek;
											$trasiogen2poltek = (($tgen2poltek/$totalbiodatapoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$g2poltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tgen2poltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="chartgender2poltek" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>



					
