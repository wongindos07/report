
<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Aplikan</b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Range Umur</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tjumlahumrappoltek =0;
											$trasioumrpoltek =0;
											$rasioumrpoltek=0;
											$numrpoltek=0;
											foreach ($umuraplikanpoltek as $umurpoltek)
											{
												$numrpoltek++;
												$range_umurpoltek=$umurpoltek->range_umur;
												$jumlahumrappoltek=$umurpoltek->jumlahapumur;
											
												$rasioumrpoltek = (($jumlahumrappoltek/$totalappoltek)*100);
											?>
												<tr >
													<td ><?=$range_umurpoltek?></td>
													<td ><?=$jumlahumrappoltek?></td>
													<td ><?=number_format($rasioumrpoltek)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$range_umurpoltek?>" id="nama<?=$numrpoltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahumrappoltek?>" id="jumlah<?=$numrpoltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioumrpoltek)?>" id="rasio<?=$numrpoltek?>" name="rasio">

											<?php 
											$tjumlahumrappoltek += $jumlahumrappoltek;
											$trasioumrappoltek = (($tjumlahumrappoltek/$totalappoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$numrpoltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlahumrappoltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="charumuraplpoltek" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>

<div class="portlet">
      <div class="portlet-title">
        <div class="caption">
            <b>Mahasiswa Baru/b>
        </div>
</div>
<div class="portlet-body">
<div class="row">
                        	<div class="col-lg-6 col-md-6 col-sm-12">
                        		<div class="row">
									<!-- ini untuk tiap segmen perjudul -->
		                            <div class="col-lg-8 col-md-6 col-sm-6 col-xs-12">
                                        <a class="dashboard-stat dashboard-stat-v2 <?PHP ?>" href="#">
		                                </a>
										<table class="table table-bordered table-striped table-condensed flip-content">
                                            <thead>
                                                <tr class='bg-grey-gallery bg-font-grey-gallery'>
                                                    <th>Range Umur</th>
                                                    <th>Total</th>
                                                    <th>Rasio</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
											$tjumlahumrbiopoltek =0;
											$trasioumrbiopoltek =0;
											$rasioumrbiopoltek=0;
                                            $bpoltek=0;
                                            // echo $umbio;
											foreach ($umurbiodatapoltek as $umurbiopoltek)
											{
												$bpoltek++;
												$range_umurbiopoltek=$umurbiopoltek->range_umurbio;
												$jumlahumrbiopoltek=$umurbiopoltek->jumlahbioumur;
											
												$rasioumrbiopoltek = (($jumlahumrbiopoltek/$totalbiodatapoltek)*100);
											?>
												<tr >
													<td ><?=$range_umurbiopoltek?></td>
													<td ><?=$jumlahumrbiopoltek?></td>
													<td ><?=number_format($rasioumrbiopoltek)."%"?></td>
												</tr>
												<input type="hidden" value="<?=$range_umurbiopoltek?>" id="nama<?=$bpoltek?>" name="nama">
												<input type="hidden" value="<?=$jumlahumrbiopoltek?>" id="jumlah<?=$bpoltek?>" name="jumlah">
												<input type="hidden" value="<?=number_format($rasioumrbiopoltek)?>" id="rasio<?=$bpoltek?>" name="rasio">

											<?php 
											$tjumlahumrbiopoltek += $jumlahumrbiopoltek;
											$trasioumrbiopoltek = (($tjumlahumrbiopoltek/$totalbiodatapoltek)*100);
											}
											
											?>	
											<input type="hidden" value="<?=$bpoltek?>" id="totalbaris" name="totalbaris">
											<tr class='bg-grey-gallery bg-font-grey-gallery' >
													<td><b>Total</b></td>
													<td><b><?=$tjumlahumrbiopoltek?></b></td>
													<td></td>
												</tr>
											</tbody>
										</table>	
		                            </div>
                        		</div>
                        	</div>
                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        		<!-- BEGIN CHART PORTLET-->
								<div class="row">
									<div class="col-lg-13">
										<div class="portlet light bordered">
                                          
                                            <div class="portlet-body">
                                            	<!-- tampilan chart -->
                                            	<div id="charumurbiopoltek" style="height: 400px; max-width: 980px; margin: 0px auto;"></div>
                                            </div>
                                        </div>
									</div>

								</div>
                                        <!-- END CHART PORTLET-->
                        	</div>

                        </div>
</div>
</div>



					
