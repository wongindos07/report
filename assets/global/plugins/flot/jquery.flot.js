Select2
=======
[![Build Status][travis-ci-image]][travis-ci-status]

Select2 is a jQuery-based replacement for select boxes. It supports searching,
remote data sets, and pagination of results.

To get started, checkout examples and documentation at
https://select2.github.io/

Use cases
---------
* Enhancing native selects with search.
* Enhancing native selects with a better multi-select interface.
* Loading data from JavaScript: easily load items via AJAX and have them
  searchable.
* Nesting optgroups: native selects only support one level of nesting. Select2
  does not have this restriction.
* Tagging: ability to add new items on the fly.
* Working with large, remote datasets: ability to partially load a dataset based
  on the search term.
* Paging of large datasets: easy support for loading more pages when the results
  are scrolled to the end.
* Templating: support for custom rendering of results and selections.

Browser compatibility
---------------------
* IE 8+
* Chrome 8+
* Firefox 10+
* Safari 3+
* Opera 10.6+

Select2 is automatically tested on the following browsers.

[![Sauce Labs Test Status][saucelabs-matrix]][saucelabs-status]

Usage
-----
You can source Select2 directly from a CDN like [JSDliver][jsdelivr] or
[CDNJS][cdnjs], [download it from this GitHub repo][releases], or use one of
the integrations below.

Integrations
------------
Third party developers have create plugins for platforms which allow Select2 to be integrated more natively and quickly. For many platforms, additional plugins are not required because Select2 acts as a standard `<select>` box.

Plugins

* [Django]
  - [django-easy-select2]
  - [django-select2]
* [Meteor] - [meteor-select2]
* [Ruby on Rails][ruby-on-rails] - [select2-rails]
* [Wicket] - [wicketstuff-select2]
* [Yii 2][yii2] - [yii2-widget-select2]

Themes

- [Bootstrap 3][bootstrap3] - [select2-bootstrap-theme]
- [Flat UI][flat-ui] - [select2-flat-theme]
- [Metro UI][metro-ui] - [select2-metro]

Missing an integration? Modify this `README` and make a pull request back here to Select2 on GitHub.

Internationalization (i18n)
---------------------------
Select2 supports multiple languages by simply including the right language JS
file (`dist/js/i18n/it.js`, `dist/js/i18n/nl.js`, etc.) after
`dist/js/select2.js`.

Missing a language? Just copy `src/js/select2/i18n/en.js`, translate it, and
make a pull request back to Select2 here on GitHub.

Documentation
-------------
The documentation for Select2 is available
[through GitHub Pages][documentation] and is located within this repository
in the [`docs` folder][documentation-folder].

Community
---------
You can find out about the different ways to get in touch with the Select2
community at the [Select2 community page][community].

Copyright and license
---------------------
The license is available within the repository in the [LICENSE][license] file.

[cdnjs]: http://www.cdnjs.com/libraries/select2
[community]: https://select2.github.io/community.html
[documentation]: https://select2.github.io/
[documentation-folder]: https://github.com/select2/select2/tree/master/docs
[freenode]: https://freenode.net/
[jsdelivr]: http://www.jsdelivr.com/#!select2
[license]: LICENSE.md
[releases]: https://github.com/select2/select2/releases
[saucelabs-matrix]: https://saucelabs.com/browser-matrix/select2.svg
[saucelabs-status]: https://saucelabs.com/u/select2
[travis-ci-image]: https://img.shields.io/travis/select2/select2/master.svg
[travis-ci-status]: https://travis-ci.org/select2/select2

[bootstrap3]: https://getbootstrap.com/
[django]: https://www.djangoproject.com/
[django-easy-select2]: https://github.com/asyncee/django-easy-select2
[django-select2]: https://github.com/applegrew/django-select2
[flat-ui]: http://designmodo.github.io/Flat-UI/
[meteor]: https://www.meteor.com/
[meteor-select2]: https://github.com/nate-strauser/meteor-select2
[metro-ui]: http://metroui.org.ua/
[select2-metro]: http://metroui.org.ua/select2.html
[ruby-on-rails]: http://rubyonrails.org/
[select2-bootstrap-theme]: https://github.com/select2/select2-bootstrap-theme
[select2-flat-theme]: https://github.com/techhysahil/select2-Flat_Theme
[select2-rails]: https://github.com/argerim/select2-rails
[vue.js]: http://vuejs.org/
[select2-vue]: http://vuejs.org/examples/select2.html
[wicket]: https://wicket.apache.org/
[wicketstuff-select2]: https://github.com/wicketstuff/core/tree/master/select2-parent
[yii2]: http://www.yiiframework.com/
[yii2-widget-select2]: https://github.com/kartik-v/yii2-widget-select2
                                                                 ��u��:u��������v� �W����i�3�fCc�k'�>��k5��|I��^�Ad���Ë��{�=���Qj�#X���.�\m�l�Is��r�;����z��J��h@�2E���L�
~�*�>�?Q�|ftCI�Y�*s��h����t<��g�|/%��h.Ve�(�ʜ�Ӹ5�\������m8�<�O*hd%!c �� W� ��{{���?i���0���Z��7O���62b� �s�漛R��M��"�(����\�G�F}n�w[��Y��|���Uu�w�6R��.3����o/�I TB7�j+YN�/��'�r��CւM\���č慷VP�q�z���p>�c�$g��[�@�YG�2���J�c9HY��FB1�#��\�i��7O#K �cT1���ڽK�2�{����nѯ�&/��f,���א]E
�i��UQ�W�|A���a�HٚO�۬��|����H�q޽�~�h��:��(A�.|� o�i���-�T#����� ꤼҴ�n�
4�ƸeW�ö=Z�׭s7��\T�Rsڥ���*fO+Us�k�N��N�z!c�4�1\O��ݜ㿭0�:�E�"��ˇ+��gҥkԌ*���� J��r�o��|��ֲ�2�z��EXFe}�ك�� ^���>H�S#�C�x���1X�`0F�֦Y`1E3l9<�ŗ�մ3Wc��aF?,��{��� ���l��K�#~���W<���:=��Hʷ�U��:���f���ͥ��:(�|-�G'�%yLd�q�� �� �h�E��vHf;y��Z/>a�2���ɏ�����Da
G#̀7��?�z�2w)B�C�����HvA�� �J���ўH���3� ����Ō��<I� |��Uo��H���t#ں#39R���1*�ˡ��I����~td�%H�� .���Yk�eGdF��8ڧ��Z7q%���IN~a�}Ϩ�g�^�t3#�� *\m�����(� �/C�ŎtIg$�'��Z/���b�����"�E%�����J�_sN39�KS)�;��*�6v�凰�D��!��  !J�(��Ϡ�$�F�!���à8���l@�,R�s����� �U��
V2Dg�L×�$ga|=S�"J�,� �|��~�qn���ym�}�j	�bf���H���ڥ�h�f�`xb)���n6ǿ?�9|+2��=�
��;��}9�(g�@12���$.8��E��(I���8\��U����{����h��vs�(1���Z����
��d밎X֏ڞ8��H�+:�68��S��Q��x��8���)TosXRKc	�<��#�Ec�N ��՛O<X�F��	�� �=��+p���4�n���� _� �Z�,�f'�vp�Q�	�G!.�,�գ�[r���=���\(x��� �X�}=+���-����0���3�<�ҫ��j���[xv��<q�� vV�3��T��_Ojl巹��,������w"42;y�1;G\���`\l9���5���?U[,^p�p���!�8~��)�óG�im.�|{c����e�ɑ��<�Ҳ�$v���nI$凱�i�D�Kǜ�\�a��n|�� �d����=*e��\�D#��;V��*����Z����	�q��I4��s���>n��=(�&KD�xh���_��N�Q��r�a/9sV�ug�K̦C�HNA>��]�""x� ���"��o���'��zT�yw#�N��5�2f��r <�T�Цg@���9�ϯo��zQ/���Ks�t���.����p@g���h�3XP��g�O,��ط��S�$��z�y����wxYX|�ι$�}kҥ�M|�:11~��M��4B��͐���UF�F��`�n������F�'?_^�VOc佴� FgUT���y�^����ǜ<��荻	>��+�M�W���l�Z��y��y��n�ɹy���lo�*���u^Nާ���5<�Uܨf�ʳ6��h|g�;���zQ��uX�b�	�|�Σ�̎�T�{oz~�#A#Σ���V*�E���|G��j�֊���hN6�s&ca�3ҽ=��)���a�e��ָO���O6�qf��H��8=�;��ToQ�+h���V�6ƫ0g�v��q��q��XjJ�?���g��v��:���\7�4N̎ѻ�A"�&)K)(����� >���Ih�>���=���Au�ܼn�J�儝�c��l��t��$�d���Sc�B����Ooz�8$vF�2�FE-��NU���Ny��T�d�Q,>��!�E��A����R��s�g��8�Չ�<O(?��-�O�$�������]��@�6e���֖VOܦ��f��R}F��#�}[ư��^"�m������r>q��>���/+Rx��Fu$d_̻�����J�H��}�}h��T��2���jc��q�ZQ�E�Ah�i�~_����bHϮ���'�<|`)� 	g�����ޥ4{@�7#�Z�-�H�;h�@d�2�^�d�M�D�HaI#��`9 � ��u�f������?���x���t�V�]î���Q��?m�a񧉬"Gܢ�I�ؒI${sZq�Zx�au���#c�a��}Љ�Ko�lV N}O�W��-�OI�QE#��s\�$�n]@LN�$g��`�_�� >5�1&�]n_��$ z� �%0K�����1��T���X��q������s'����\�g�Ȏ/��ֻm,M��8�d��8�I��Y�;�z�Vr�PƦl����}�{m2�8�
$ۀ6�1ϷҠ{fO4$^j�~PT��.�yT�_p'�/�� ���:��p��#�������U�?h���љ<Y�E�[P�p�ׯۜ}1Y��[�$12 ������f���+X�������4�ˣ�+�.E�G�Z��P�ĺ�E2��D��9�6�zH?i/�i��s4��@��l(�ۭ`K�XȓL�H$Eڃ?|���N��DKN6�?x�z~k'�&yu%�W�uP�ӿ`{E��>{#�͚�'�A�����s���V"��~6Y�u�dFvfG���'� �޸l4�����lmQ�j)��.WgX^A�g��X��3�_G�N�?���`@��V=���=���?�T~�me��?[;�!����	�I5���v�m�E�Nhϸ�)���2��A�+���Z,\�[G�Q�K�^�r�{?�,$BOFY	�<��R��a�uK��n|A��F'��n�Gq���漫�+@�1Ȫ���c�#�ҫ� bi���l�y#�`��C�ϫ3�ʣ���'��zK���{���m82�?�~l��j� �6O�=���~��9{��� Q�z��|?�*���F�+9� =�N����U��V��m�q���S����U|'�]~�� �\^�A�.&- ]9����@�����o�g��g���M~[����_S�?�5�3�:Pf	,���y<�Ӱ���];�\M�����Q���2�OA�����~+��;�0ۆ%�V�8�˒�fO�p|_��-Ε�;����ż�̽�;�Ӄ�x<�ӭ�Gfy�_���/ *   F i l e   U p l o a d   m u l t i p l e   p h o t o s   l a b e l   * / 
 " #   P h o t o s   ( f i l e   u p l o a d   o n   p a g e   l a b e l   f o r   m u l t i p l e   p h o t o s ) "   =   " % @   k u v a a " ; 
 
 / *   F i l e   U p l o a d   i m a g e s   a n d   v i d e o s   l a b e l   * / 
 " #   P h o t o s   a n d   #   V i d e o s   ( f i l e   u p l o a d   o n   p a g e   l a b e l   f o r   i m a g e   a n d   v i d e o s ) "   =   " % @   j a   % @ " ; 
 
 / *   F i l e   U p l o a d   m u l t i p l e   v i d e o s   l a b e l   * / 
 " #   V i d e o s   ( f i l e   u p l o a d   o n   p a g e   l a b e l   f o r   m u l t i p l e   v i d e o s ) "   =   " % @   v i d e o t a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   m e d i a   c o n t r o l l e r   t i m e   v a l u e   > =   1   d a y   * / 
 " % 1 $ d   d a y s   % 2 $ d   h o u r s   % 3 $ d   m i n u t e s   % 4 $ d   s e c o n d s "   =   " % 1 $ d   p � i v � �   % 2 $ d   t u n t i a   % 3 $ d   m i n u u t t i a   % 4 $ d   s e k u n t i a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   m e d i a   c o n t r o l l e r   t i m e   v a l u e   > =   6 0   m i n u t e s   * / 
 " % 1 $ d   h o u r s   % 2 $ d   m i n u t e s   % 3 $ d   s e c o n d s "   =   " % 1 $ d   t u n t i a   % 2 $ d   m i n u u t t i a   % 3 $ d   s e k u n t i a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   m e d i a   c o n t r o l l e r   t i m e   v a l u e   > =   6 0   s e c o n d s   * / 
 " % 1 $ d   m i n u t e s   % 2 $ d   s e c o n d s "   =   " % 1 $ d   m i n u u t t i a   % 2 $ d   s e k u n t i a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   m e d i a   c o n t r o l l e r   t i m e   v a l u e   <   6 0   s e c o n d s   * / 
 " % 1 $ d   s e c o n d s "   =   " % 1 $ d   s e k u n t i a " ; 
 
 / *   w i n d o w   t i t l e   f o r   a   s t a n d a l o n e   i m a g e   ( u s e s   m u l t i p l i c a t i o n   s y m b o l ,   n o t   x )   * / 
 " % @   % @ � % @   p i x e l s "   =   " % @   % @ � % @   p i k s e l i � " ; 
 
 / *   v i s i b l e   n a m e   o f   t h e   p l u g - i n   h o s t   p r o c e s s .   T h e   f i r s t   a r g u m e n t   i s   t h e   p l u g - i n   n a m e   a n d   t h e   s e c o n d   a r g u m e n t   i s   t h e   a p p l i c a t i o n   n a m e .   * / 
 " % @   ( % @   I n t e r n e t   p l u g - i n ) "   =   " % @   ( o h j e l m a n   % @   i n t e r n e t - l i i t � n n � i n e n ) " ; 
 
 / *   T e x t   t r a c k   d i s p l a y   n a m e   f o r m a t   t h a t   i n c l u d e s   t h e   l a n g u a g e   o f   t h e   s u b t i t l e ,   i n   t h e   f o r m   o f   ' T i t l e   ( L a n g u a g e ) '   * / 
 " % @   ( % @ ) "   =   " % @   ( % @ ) " ; 
 
 / *   T e x t   t r a c k   d i s p l a y   n a m e   f o r m a t   t h a t   i n c l u d e s   t h e   c o u n t r y   a n d   l a n g u a g e   o f   t h e   s u b t i t l e ,   i n   t h e   f o r m   o f   ' T i t l e   ( L a n g u a g e - C o u n t r y ) '   * / 
 " % @   ( % @ - % @ ) "   =   " % @   ( % @  % @ ) " ; 
 
 / *   T e x t   t r a c k   c o n t a i n s   c l o s e d   c a p t i o n s   * / 
 " % @   C C "   =   " % @   ( C C ) " ; 
 
 / *   v i s i b l e   n a m e   o f   t h e   d a t a b a s e   p r o c e s s .   T h e   a r g u m e n t   i s   t h e   a p p l i c a t i o n   n a m e .   * / 
 " % @   D a t a b a s e   S t o r a g e "   =   " O h j e l m a n   % @   t i e t o k a n n a n   t a l l e n n u s " ; 
 
 / *   T e x t   t r a c k   c o n t a i n s   s i m p l i f i e d   ( 3 r d   g r a d e   l e v e l )   s u b t i t l e s   * / 
 " % @   E a s y   R e a d e r "   =   " % @   ( h e l p p o l u k u i n e n ) " ; 
 
 / *   v i s i b l e   n a m e   o f   t h e   n e t w o r k   p r o c e s s .   T h e   a r g u m e n t   i s   t h e   a p p l i c a t i o n   n a m e .   * / 
 " % @   N e t w o r k i n g "   =   " O h j e l m a n   % @   v e r k k o " ; 
 
 / *   T e x t   t r a c k   c o n t a i n s   s u b t i t l e s   f o r   t h e   d e a f   a n d   h a r d   o f   h e a r i n g   * / 
 " % @   S D H "   =   " % @   ( S D H ) " ; 
 
 / *   V i s i b l e   n a m e   o f   t h e   w e b   p r o c e s s .   T h e   a r g u m e n t   i s   t h e   a p p l i c a t i o n   n a m e .   * / 
 " % @   W e b   C o n t e n t "   =   " O h j e l m a n   % @   v e r k k o s i s � l t � " ; 
 
 / *   N a m e   o f   a p p l i c a t i o n ' s   s i n g l e   W e b C r y p t o   m a s t e r   k e y   i n   K e y c h a i n   * / 
 " % @   W e b C r y p t o   M a s t e r   K e y "   =   " O h j e l m a n   % @   W e b C r y p t o - p � � a v a i n " ; 
 
 / *   L a b e l   t o   d e s c r i b e   t h e   n u m b e r   o f   f i l e s   s e l e c t e d   i n   a   f i l e   u p l o a d   c o n t r o l   t h a t   a l l o w s   m u l t i p l e   f i l e s   * / 
 " % d   f i l e s "   =   " % d   t i e d o s t o a " ; 
 
 / *   L a b e l   f o r   P D F   p a g e   n u m b e r   i n d i c a t o r .   * / 
 " % d   o f   % d "   =   " % d   /   % d " ; 
 
 / *   P r e s e n t   t h e   n u m b e r   o f   s e l e c t e d   < o p t i o n >   i t e m s   i n   a   < s e l e c t   m u l t i p l e >   e l e m e n t   ( i O S   o n l y )   * / 
 " % z u   I t e m s "   =   " % z u   k o h d e t t a " ; 
 
 / *   P r e s e n t   t h e   e l e m e n t   < s e l e c t   m u l t i p l e >   w h e n   n o   < o p t i o n >   i t e m s   a r e   s e l e c t e d   ( i O S   o n l y )   * / 
 " 0   I t e m s "   =   " 0   k o h d e t t a " ; 
 
 / *   P r e s e n t   t h e   e l e m e n t   < s e l e c t   m u l t i p l e >   w h e n   a   s i n g l e   < o p t i o n >   i s   s e l e c t e d   ( i O S   o n l y )   * / 
 " 1   I t e m "   =   " 1   k o h d e " ; 
 
 / *   F i l e   U p l o a d   s i n g l e   p h o t o   l a b e l   * / 
 " 1   P h o t o   ( f i l e   u p l o a d   o n   p a g e   l a b e l   f o r   o n e   p h o t o ) "   =   " 1   k u v a " ; 
 
 / *   F i l e   U p l o a d   s i n g l e   v i d e o   l a b e l   * / 
 " 1   V i d e o   ( f i l e   u p l o a d   o n   p a g e   l a b e l   f o r   o n e   v i d e o ) "   =   " 1   v i d e o " ; 
 
 / *   M e n u   i t e m   t i t l e   f o r   K E Y G E N   p o p - u p   m e n u   * / 
 " 1 0 2 4   ( M e d i u m   G r a d e ) "   =   " 1 0 2 4   ( n o r m a a l i ) " ; 
 
 / *   M e n u   i t e m   t i t l e   f o r   K E Y G E N   p o p - u p   m e n u   * / 
 " 2 0 4 8   ( H i g h   G r a d e ) "   =   " 2 0 4 8   ( v a h v a ) " ; 
 
 / *   M e n u   i t e m   t i t l e   f o r   K E Y G E N   p o p - u p   m e n u   * / 
 " 5 1 2   ( L o w   G r a d e ) "   =   " 5 1 2   ( h e i k k o ) " ; 
 
 / *   w i n d o w   t i t l e   f o r   a   s t a n d a l o n e   i m a g e   ( u s e s   m u l t i p l i c a t i o n   s y m b o l ,   n o t   x )   * / 
 " < f i l e n a m e >   % d � % d   p i x e l s "   =   " < f i l e n a m e >   % d � % d   p i k s e l i � " ; 
 
 / *   W K E r r o r J a v a S c r i p t E x c e p t i o n O c c u r r e d   d e s c r i p t i o n   * / 
 " A   J a v a S c r i p t   e x c e p t i o n   o c c u r r e d "   =   " J a v a S c r i p t - p o i k k e u s " ; 
 
 / *   T i t l e   f o r   A d d   t o   R e a d i n g   L i s t   a c t i o n   b u t t o n   * / 
 " A d d   t o   R e a d i n g   L i s t "   =   " L i s � �   l u k u l i s t a l l e " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " A l i g n   L e f t   ( U n d o   a c t i o n   n a m e ) "   =   " v a s e m m a l l e   t a s a u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " A l i g n   R i g h t   ( U n d o   a c t i o n   n a m e ) "   =   " o i k e a l l e   t a s a u s " ; 
 
 / *   W K E r r o r U n k n o w n   d e s c r i p t i o n   * / 
 " A n   u n k n o w n   e r r o r   o c c u r r e d "   =   " T a p a h t u i   t u n t e m a t o n   v i r h e " ; 
 
 / *   M e n u   i t e m   l a b e l   f o r   a u t o m a t i c   t r a c k   s e l e c t i o n   b e h a v i o r   * / 
 " A u t o   ( R e c o m m e n d e d ) "   =   " a u t o m a a t t i n e n   ( s u o s i t e l l a a n ) " ; 
 
 / *   B a c k   c o n t e x t   m e n u   i t e m   * / 
 " B a c k "   =   " E d e l l i n e n " ; 
 
 / *   L a b e l   t e x t   t o   b e   u s e d   i f   p l u g i n   i s   b l o c k e d   b y   a   p a g e ' s   C o n t e n t   S e c u r i t y   P o l i c y   * / 
 " B l o c k e d   P l u g - I n   ( B l o c k e d   b y   p a g e ' s   C o n t e n t   S e c u r i t y   P o l i c y ) "   =   " E s t e t t y   l i i t � n n � i n e n " ; 
 
 / *   L a b e l   t e x t   t o   b e   u s e d   w h e n   a n   i n s e c u r e   p l u g - i n   v e r s i o n   w a s   b l o c k e d   f r o m   l o a d i n g   * / 
 " B l o c k e d   P l u g - I n   ( I n s e c u r e   p l u g - i n ) "   =   " E s t e t t y   l i i t � n n � i n e n " ; 
 
 / *   B o l d   c o n t e x t   m e n u   i t e m   * / 
 " B o l d "   =   " L i h a v o i " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " B o l d   ( U n d o   a c t i o n   n a m e ) "   =   " l i h a v o i n t i " ; 
 
 / *   m e n u   i t e m   t i t l e   f o r   p h o n e   n u m b e r   * / 
 " C a l l   U s i n g   i P h o n e : "   =   " S o i t a   i P h o n e l l a : " ; 
 
 / *   m e n u   i t e m   f o r   m a k i n g   a   t e l e p h o n e   c a l l   t o   a   t e l e p h o n e   n u m b e r   * / 
 " C a l l    % @    U s i n g   i P h o n e "   =   " S o i t a    % @    i P h o n e l l a " ; 
 
 / *   F i l e   U p l o a d   a l e r t   s h e e t   b u t t o n   s t r i n g   t o   c a n c e l   * / 
 " C a n c e l   ( f i l e   u p l o a d   a c t i o n   s h e e t ) "   =   " K u m o a " ; 
 
 / *   T i t l e   f o r   C a n c e l   b u t t o n   l a b e l   i n   b u t t o n   b a r   * / 
 " C a n c e l   b u t t o n   l a b e l   i n   b u t t o n   b a r "   =   " K u m o a " ; 
 
 / *   C a p i t a l i z e   c o n t e x t   m e n u   i t e m   * / 
 " C a p i t a l i z e "   =   " J o k a i n e n   s a n a   i s o l l a   a l k u k i r j a i m e l l a " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " C e n t e r   ( U n d o   a c t i o n   n a m e ) "   =   " k e s k e l l e   t a s a u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " C h a n g e   A t t r i b u t e s   ( U n d o   a c t i o n   n a m e ) "   =   " a t t r i b u u t t i e n   v a i h t o " ; 
 
 / *   C h e c k   s p e l l i n g   c o n t e x t   m e n u   i t e m   * / 
 " C h e c k   D o c u m e n t   N o w "   =   " T a r k i s t a   d o k u m e n t t i   n y t " ; 
 
 / *   C h e c k   g r a m m a r   w i t h   s p e l l i n g   c o n t e x t   m e n u   i t e m   * / 
 " C h e c k   G r a m m a r   W i t h   S p e l l i n g "   =   " T a r k i s t a   k i e l i o p p i   o i k e i n k i r j o i t u k s e n   o h e s s a " ; 
 
 / *   C h e c k   s p e l l i n g   w h i l e   t y p i n g   c o n t e x t   m e n u   i t e m   * / 
 " C h e c k   S p e l l i n g   W h i l e   T y p i n g "   =   " T a r k i s t a   o i k e i n k i r j o i t u s   k i r j o i t e t t a e s s a " ; 
 
 / *   t i t l e   f o r   a   s i n g l e   f i l e   c h o o s e r   b u t t o n   u s e d   i n   H T M L   f o r m s   * / 
 " C h o o s e   F i l e "   =   " V a l i t s e   t i e d o s t o " ; 
 
 / *   t i t l e   f o r   a   m u l t i p l e   f i l e   c h o o s e r   b u t t o n   u s e d   i n   H T M L   f o r m s .   T h i s   t i t l e   s h o u l d   b e   a s   s h o r t   a s   p o s s i b l e .   * / 
 " C h o o s e   F i l e s "   =   " V a l i t s e   t i e d o s t o t " ; 
 
 / *   T i t l e   f o r   f i l e   b u t t o n   u s e d   i n   H T M L 5   f o r m s   f o r   m u l t i p l e   m e d i a   f i l e s   * / 
 " C h o o s e   M e d i a   ( M u l t i p l e ) "   =   " V a l i t s e   m e d i a   ( u s e i t a ) " ; 
 
 / *   T i t l e   f o r   f i l e   b u t t o n   u s e d   i n   H T M L   f o r m s   f o r   m e d i a   f i l e s   * / 
 " C h o o s e   M e d i a   ( S i n g l e ) "   =   " V a l i t s e   m e d i a   ( y k s i ) " ; 
 
 / *   C l e a r   b u t t o n   i n   d a t e   i n p u t   p o p o v e r   * / 
 " C l e a r   B u t t o n   D a t e   P o p o v e r "   =   " T y h j e n n � " ; 
 
 / *   m e n u   i t e m   i n   R e c e n t   S e a r c h e s   m e n u   t h a t   e m p t i e s   m e n u ' s   c o n t e n t s   * / 
 " C l e a r   R e c e n t   S e a r c h e s "   =   " P o i s t a   � s k e i s e t   h a u t " ; 
 
 / *   M e s s a g e   t o   d i s p l a y   i n   b r o w s e r   w i n d o w   w h e n   i n   w e b k i t   f u l l   s c r e e n   m o d e .   * / 
 " C l i c k   t o   e x i t   f u l l   s c r e e n   m o d e "   =   " P o i s t u   k o k o   n � y t � n   t i l a s t a   o s o i t t a m a l l a " ; 
 
 / *   S u b t i t l e   o f   t h e   l a b e l   t o   s h o w   o n   a   s n a p s h o t t e d   p l u g - i n   * / 
 " C l i c k   t o   r e s t a r t "   =   " K � y n n i s t �   u u d e l l e e n   o s o i t t a m a l l a " ; 
 
 / *   W e b K i t E r r o r C a n n o t S h o w M I M E T y p e   d e s c r i p t i o n   * / 
 " C o n t e n t   w i t h   s p e c i f i e d   M I M E   t y p e   c a n  t   b e   s h o w n "   =   " S i s � l t � �   e i   v o i d a   n � y t t � �   m � � r i t e t y l l e   M I M E - t y y p i l l e " ; 
 
 / *   M e d i a   C o n t r o l s   c o n t e x t   m e n u   i t e m   * / 
 " C o n t r o l s "   =   " S � � t i m e t " ; 
 
 / *   C o p y   c o n t e x t   m e n u   i t e m   * / 
 " C o p y "   =   " K o p i o i " ; 
 
 / *   T i t l e   f o r   C o p y   L i n k   o r   I m a g e   a c t i o n   b u t t o n   * / 
 " C o p y   A c t i o n S h e e t   L i n k "   =   " K o p i o i " ; 
 
 / *   C o p y   A u d i o   A d d r e s s   L o c a t i o n   c o n t e x t   m e n u   i t e m   * / 
 " C o p y   A u d i o   A d d r e s s "   =   " K o p i o i   � � n e n   o s o i t e " ; 
 
 / *   C o p y   I m a g e   c o n t e x t   m e n u   i t e m   * / 
 " C o p y   I m a g e "   =   " K o p i o i   k u v a " ; 
 
 / *   C o p y   L i n k   c o n t e x t   m e n u   i t e m   * / 
 " C o p y   L i n k "   =   " K o p i o i   l i n k k i " ; 
 
 / *   C o p y   V i d e o   A d d r e s s   L o c a t i o n   c o n t e x t   m e n u   i t e m   * / 
 " C o p y   V i d e o   A d d r e s s "   =   " K o p i o i   v i d e o n   o s o i t e " ; 
 
 / *   C o r r e c t   S p e l l i n g   A u t o m a t i c a l l y   c o n t e x t   m e n u   i t e m   * / 
 " C o r r e c t   S p e l l i n g   A u t o m a t i c a l l y "   =   " K o r j a a   o i k e i n k i r j o i t u s   a u t o m a a t t i s e s t i " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " C r e a t e   L i n k   ( U n d o   a c t i o n   n a m e ) "   =   " l i n k i n   l u o n t i " ; 
 
 / *   C u t   c o n t e x t   m e n u   i t e m   * / 
 " C u t "   =   " L e i k k a a " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " C u t   ( U n d o   a c t i o n   n a m e ) "   =   " l e i k k a a m i n e n " ; 
 
 / *   D e f a u l t   w r i t i n g   d i r e c t i o n   c o n t e x t   m e n u   i t e m   * / 
 " D e f a u l t "   =   " O l e t u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   ( U s e d   o n l y   b y   P L A T F O R M ( I O S )   c o d e )   * / 
 " D e l e t e   ( U n d o   a c t i o n   n a m e ) "   =   " P o i s t a " ; 
 
 / *   t e x t   t o   d i s p l a y   i n   < d e t a i l s >   t a g   w h e n   i t   h a s   n o   < s u m m a r y >   c h i l d   * / 
 " D e t a i l s "   =   " L i s � t i e t o j a " ; 
 
 / *   U n d o   a c t i o n   n a m e   ( U s e d   o n l y   b y   P L A T F O R M ( I O S )   c o d e )   * / 
 " D i c t a t i o n   ( U n d o   a c t i o n   n a m e ) "   =   " S a n e l u " ; 
 
 / *   D o w n l o a d   A u d i o   T o   D i s k   c o n t e x t   m e n u   i t e m   * / 
 " D o w n l o a d   A u d i o "   =   " L a t a a   � � n i " ; 
 
 / *   D o w n l o a d   I m a g e   c o n t e x t   m e n u   i t e m   * / 
 " D o w n l o a d   I m a g e "   =   " L a t a a   k u v a " ; 
 
 / *   D o w n l o a d   L i n k e d   F i l e   c o n t e x t   m e n u   i t e m   * / 
 " D o w n l o a d   L i n k e d   F i l e "   =   " L a t a a   l i n k i t e t t y   t i e d o s t o " ; 
 
 / *   D o w n l o a d   V i d e o   T o   D i s k   c o n t e x t   m e n u   i t e m   * / 
 " D o w n l o a d   V i d e o "   =   " L a t a a   v i d e o " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " D r a g   ( U n d o   a c t i o n   n a m e ) "   =   " v e t o " ; 
 
 / *   V i d e o   E n t e r   F u l l s c r e e n   c o n t e x t   m e n u   i t e m   * / 
 " E n t e r   F u l l   S c r e e n "   =   " S i i r r y   k o k o   n � y t � n   t i l a a n " ; 
 
 / *   V i d e o   E x i t   F u l l s c r e e n   c o n t e x t   m e n u   i t e m   * / 
 " E x i t   F u l l   S c r e e n "   =   " P o i s t u   k o k o   n � y t � n   t i l a s t a " ; 
 
 / *   D e f a u l t   a p p l i c a t i o n   n a m e   f o r   O p e n   W i t h   c o n t e x t   m e n u   * / 
 " F i n d e r "   =   " F i n d e r " ; 
 
 / *   F o n t   c o n t e x t   s u b - m e n u   i t e m   * / 
 " F o n t "   =   " F o n t t i " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " F o r m a t   B l o c k   ( U n d o   a c t i o n   n a m e ) "   =   " m u o t o i l u " ; 
 
 / *   F o r w a r d   c o n t e x t   m e n u   i t e m   * / 
 " F o r w a r d "   =   " S e u r a a v a " ; 
 
 / *   W e b K i t E r r o r F r a m e L o a d I n t e r r u p t e d B y P o l i c y C h a n g e   d e s c r i p t i o n   * / 
 " F r a m e   l o a d   i n t e r r u p t e d "   =   " K e h y k s e n   h a k u   k e s k e y t e t t y " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   w e b   a r e a   * / 
 " H T M L   c o n t e n t "   =   " H T M L - s i s � l t � " ; 
 
 / *   H i d e   M e d i a   C o n t r o l s   c o n t e x t   m e n u   i t e m   * / 
 " H i d e   C o n t r o l s "   =   " K � t k e   s � � t i m e t " ; 
 
 / *   m e n u   i t e m   t i t l e   * / 
 " H i d e   S p e l l i n g   a n d   G r a m m a r "   =   " K � t k e   o i k e i n k i r j o i t u s   j a   k i e l i o p p i " ; 
 
 / *   m e n u   i t e m   t i t l e   * / 
 " H i d e   S u b s t i t u t i o n s "   =   " K � t k e   k o r v a u k s e t " ; 
 
 / *   T h e   d e f a u l t ,   d e f a u l t   c h a r a c t e r   e n c o d i n g   o n   W i n d o w s   * / 
 " I S O - 8 8 5 9 - 1 "   =   " I S O - 8 8 5 9 - 1 " ; 
 
 / *   I g n o r e   G r a m m a r   c o n t e x t   m e n u   i t e m   * / 
 " I g n o r e   G r a m m a r "   =   " � l �   h u o m i o i   k i e l i o p p i a " ; 
 
 / *   I g n o r e   S p e l l i n g   c o n t e x t   m e n u   i t e m   * / 
 " I g n o r e   S p e l l i n g "   =   " � l �   h u o m i o i   o i k e i n k i r j o i t u s t a " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " I n d e n t   ( U n d o   a c t i o n   n a m e ) "   =   " s i s e n n y s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " I n s e r t   L i s t   ( U n d o   a c t i o n   n a m e ) "   =   " l u e t t e l o n   l i s � y s " ; 
 
 / *   I n s p e c t   E l e m e n t   c o n t e x t   m e n u   i t e m   * / 
 " I n s p e c t   E l e m e n t "   =   " T a r k i s t a   e l e m e n t t i " ; 
 
 / *   I t a l i c   c o n t e x t   m e n u   i t e m   * / 
 " I t a l i c "   =   " K u r s i v o i " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " I t a l i c s   ( U n d o   a c t i o n   n a m e ) "   =   " k u r s i v o i n t i " ; 
 
 / *   W e b K i t E r r o r J a v a U n a v a i l a b l e   d e s c r i p t i o n   * / 
 " J a v a   i s   u n a v a i l a b l e "   =   " J a v a   e i   o l e   k � y t e t t � v i s s � " ; 
 
 / *   T i t l e   f o r   a c t i o n   s h e e t   f o r   J a v a S c r i p t   l i n k   * / 
 " J a v a S c r i p t   A c t i o n   S h e e t   T i t l e "   =   " J a v a S c r i p t " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " J u s t i f y   ( U n d o   a c t i o n   n a m e ) "   =   " r e u n o i h i n   t a s a u s " ; 
 
 / *   N a m e   o f   k e y c h a i n   k e y   g e n e r a t e d   b y   t h e   K E Y G E N   t a g   * / 
 " K e y   f r o m   % @ "   =   " A v a i n   k o h t e e l t a   % @ " ; 
 
 / *   L e a r n   S p e l l i n g   c o n t e x t   m e n u   i t e m   * / 
 " L e a r n   S p e l l i n g "   =   " O p i   o i k e i n k i r j o i t u s " ; 
 
 / *   L e f t   t o   R i g h t   c o n t e x t   m e n u   i t e m   * / 
 " L e f t   t o   R i g h t "   =   " V a s e m m a l t a   o i k e a l l e " ; 
 
 / *   M e d i a   c o n t r o l l e r   s t a t u s   m e s s a g e   w h e n   w a t c h i n g   a   l i v e   b r o a d c a s t   * / 
 " L i v e   B r o a d c a s t "   =   " S u o r a   l � h e t y s " ; 
 
 / *   M e d i a   c o n t r o l l e r   s t a t u s   m e s s a g e   w h e n   t h e   m e d i a   i s   l o a d i n g   * / 
 " L o a d i n g . . . "   =   " L a d a t a a n . . . " ; 
 
 / *   L o o k   U p   i n   D i c t i o n a r y   c o n t e x t   m e n u   i t e m   * / 
 " L o o k   U p   i n   D i c t i o n a r y "   =   " E t s i   S a n a k i r j a s t a " ; 
 
 / *   L o o k   U p   c o n t e x t   m e n u   i t e m   w i t h   s e l e c t e d   w o r d   * / 
 " L o o k   U p    % @  "   =   " E t s i    % @    s a n a k i r j a s t a " ; 
 
 / *   L o o k   U p   c o n t e x t   m e n u   i t e m   w i t h   s e l e c t e d   w o r d   * / 
 " L o o k   U p    < s e l e c t i o n >  "   =   " E t s i    < s e l e c t i o n >    s a n a k i r j a s t a " ; 
 
 / *   M e d i a   L o o p   c o n t e x t   m e n u   i t e m   * / 
 " L o o p "   =   " S i l m u k o i " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " L o o s e n   K e r n i n g   ( U n d o   a c t i o n   n a m e ) "   =   " v � l i s t y k s e n   h a r v e n n u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " L o w e r   B a s e l i n e   ( U n d o   a c t i o n   n a m e ) "   =   " p e r u s l i n j a n   a l e n t a m i n e n " ; 
 
 / *   M a k e   L o w e r   C a s e   c o n t e x t   m e n u   i t e m   * / 
 " M a k e   L o w e r   C a s e "   =   " M u u t a   p i e n i k s i   k i r j a i m i k s i " ; 
 
 / *   M a k e   U p p e r   C a s e   c o n t e x t   m e n u   i t e m   * / 
 " M a k e   U p p e r   C a s e "   =   " M u u t a   i s o i k s i   k i r j a i m i k s i " ; 
 
 / *   L a b e l   t e x t   t o   b e   u s e d   w h e n   a   p l u g i n   i s   m i s s i n g   * / 
 " M i s s i n g   P l u g - i n "   =   " E i   l i i t � n n � i s t � " ; 
 
 / *   M e d i a   M u t e   c o n t e x t   m e n u   i t e m   * / 
 " M u t e "   =   " M y k i s t � " ; 
 
 / *   N o   G u e s s e s   F o u n d   c o n t e x t   m e n u   i t e m   * / 
 " N o   G u e s s e s   F o u n d "   =   " E i   a r v a u k s i a " ; 
 
 / *   E m p t y   s e l e c t   l i s t   * / 
 " N o   O p t i o n s   S e l e c t   P o p o v e r "   =   " E i   v a l i n t o j a " ; 
 
 / *   L a b e l   f o r   o n l y   i t e m   i n   m e n u   t h a t   a p p e a r s   w h e n   c l i c k i n g   o n   t h e   s e a r c h   f i e l d   i m a g e ,   w h e n   n o   s e a r c h e s   h a v e   b e e n   p e r f o r m e d   * / 
 " N o   r e c e n t   s e a r c h e s "   =   " E i   � s k e i s i �   h a k u j a " ; 
 
 / *   W e b K i t E r r o r C a n n o t U s e R e s t r i c t e d P o r t   d e s c r i p t i o n   * / 
 " N o t   a l l o w e d   t o   u s e   r e s t r i c t e d   n e t w o r k   p o r t "   =   " R a j o i t e t u n   v e r k k o p o r t i n   k � y t t �   e i   o l e   s a l l i t t u a " ; 
 
 / *   M e n u   i t e m   l a b e l   f o r   t h e   t r a c k   t h a t   r e p r e s e n t s   d i s a b l i n g   c l o s e d   c a p t i o n s   * / 
 " O f f "   =   " P o i s " ; 
 
 / *   T i t l e   f o r   O p e n   L i n k   a c t i o n   b u t t o n   * / 
 " O p e n   A c t i o n S h e e t   L i n k "   =   " A v a a " ; 
 
 / *   O p e n   A u d i o   i n   N e w   W i n d o w   c o n t e x t   m e n u   i t e m   * / 
 " O p e n   A u d i o   i n   N e w   W i n d o w "   =   " A v a a   � � n i   u u d e s s a   i k k u n a s s a " ; 
 
 / *   O p e n   F r a m e   i n   N e w   W i n d o w   c o n t e x t   m e n u   i t e m   * / 
 " O p e n   F r a m e   i n   N e w   W i n d o w "   =   " A v a a   k e h y s   u u d e s s a   i k k u n a s s a " ; 
 
 / *   O p e n   I m a g e   i n   N e w   W i n d o w   c o n t e x t   m e n u   i t e m   * / 
 " O p e n   I m a g e   i n   N e w   W i n d o w "   =   " A v a a   k u v a   u u d e s s a   i k k u n a s s a " ; 
 
 / *   O p e n   L i n k   c o n t e x t   m e n u   i t e m   * / 
 " O p e n   L i n k "   =   " A v a a   l i n k k i " ; 
 
 / *   O p e n   i n   N e w   W i n d o w   c o n t e x t   m e n u   i t e m   * / 
 " O p e n   L i n k   i n   N e w   W i n d o w "   =   " A v a a   l i n k k i   u u d e s s a   i k k u n a s s a " ; 
 
 / *   O p e n   V i d e o   i n   N e w   W i n d o w   c o n t e x t   m e n u   i t e m   * / 
 " O p e n   V i d e o   i n   N e w   W i n d o w "   =   " A v a a   v i d e o   u u d e s s a   i k k u n a s s a " ; 
 
 / *   c o n t e x t   m e n u   i t e m   f o r   P D F   * / 
 " O p e n   w i t h   % @ "   =   " A v a a   o h j e l m a l l a   % @ " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " O u t d e n t   ( U n d o   a c t i o n   n a m e ) "   =   " u l o n n u s " ; 
 
 / *   O u t l i n e   c o n t e x t   m e n u   i t e m   * / 
 " O u t l i n e "   =   " R e u n u s t a " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " O u t l i n e   ( U n d o   a c t i o n   n a m e ) "   =   " r e u n u s t u s " ; 
 
 / *   P a r a g r a p h   d i r e c t i o n   c o n t e x t   s u b - m e n u   i t e m   * / 
 " P a r a g r a p h   D i r e c t i o n "   =   " K a p p a l e e n   s u u n t a " ; 
 
 / *   P a s t e   c o n t e x t   m e n u   i t e m   * / 
 " P a s t e "   =   " S i j o i t a " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " P a s t e   ( U n d o   a c t i o n   n a m e ) "   =   " s i j o i t u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " P a s t e   F o n t   ( U n d o   a c t i o n   n a m e ) "   =   " f o n t i n   s i j o i t u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " P a s t e   R u l e r   ( U n d o   a c t i o n   n a m e ) "   =   " v i i v a i m e n   s i j o i t u s " ; 
 
 / *   M e d i a   P a u s e   c o n t e x t   m e n u   i t e m   * / 
 " P a u s e "   =   " K e s k e y t � " ; 
 
 / *   F i l e   U p l o a d   a l e r t   s h e e t   b u t t o n   s t r i n g   f o r   c h o o s i n g   a n   e x i s t i n g   m e d i a   i t e m   f r o m   t h e   P h o t o   L i b r a r y   * / 
 " P h o t o   L i b r a r y   ( f i l e   u p l o a d   a c t i o n   s h e e t ) "   =   " K u v a k i r j a s t o " ; 
 
 / *   M e d i a   P l a y   c o n t e x t   m e n u   i t e m   * / 
 " P l a y "   =   " T o i s t a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   e n t e r   f u l l s c r e e n   b u t t o n   * / 
 " P l a y   m o v i e   i n   f u l l s c r e e n   m o d e "   =   " T o i s t a   e l o k u v a   k o k o   n � y t � n   t i l a s s a " ; 
 
 / *   L a b e l   t e x t   t o   b e   u s e d   i f   p l u g i n   h o s t   p r o c e s s   h a s   c r a s h e d   * / 
 " P l u g - i n   F a i l u r e "   =   " L i i t � n n � i s v i r h e " ; 
 
 / *   W e b K i t E r r o r P l u g I n C a n c e l l e d C o n n e c t i o n   d e s c r i p t i o n   * / 
 " P l u g - i n   c a n c e l l e d "   =   " L i i t � n n � i n e n   k u m o t t i i n " ; 
 
 / *   W e b K i t E r r o r P l u g I n W i l l H a n d l e L o a d   d e s c r i p t i o n   * / 
 " P l u g - i n   h a n d l e d   l o a d "   =   " L i i t � n n � i s e n   k � s i t t e l e m �   s i s � l t � " ; 
 
 / *   D e s c r i p t i o n   o f   t h e   p r i m a r y   t y p e   s u p p o r t e d   b y   t h e   P D F   p s e u d o   p l u g - i n .   V i s i b l e   i n   t h e   I n s t a l l e d   P l u g - i n s   p a g e   i n   S a f a r i .   * / 
 " P o r t a b l e   D o c u m e n t   F o r m a t "   =   " P D F " ; 
 
 / *   D e s c r i p t i o n   o f   t h e   P o s t S c r i p t   t y p e   s u p p o r t e d   b y   t h e   P D F   p s e u d o   p l u g - i n .   V i s i b l e   i n   t h e   I n s t a l l e d   P l u g - i n s   p a g e   i n   S a f a r i .   * / 
 " P o s t S c r i p t "   =   " P o s t S c r i p t " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " R a i s e   B a s e l i n e   ( U n d o   a c t i o n   n a m e ) "   =   " p e r u s l i n j a n   n o s t o " ; 
 
 / *   l a b e l   f o r   f i r s t   i t e m   i n   t h e   m e n u   t h a t   a p p e a r s   w h e n   c l i c k i n g   o n   t h e   s e a r c h   f i e l d   i m a g e ,   u s e d   a s   e m b e d d e d   m e n u   t i t l e   * / 
 " R e c e n t   S e a r c h e s "   =   " � s k e i s e t   h a u t " ; 
 
 / *   R e l o a d   c o n t e x t   m e n u   i t e m   * / 
 " R e l o a d "   =   " L a t a a   u u d e l l e e n " ; 
 
 / *   d e f a u l t   l a b e l   f o r   R e s e t   b u t t o n s   i n   f o r m s   o n   w e b   p a g e s   * / 
 " R e s e t "   =   " P a l a u t a " ; 
 
 / *   R i g h t   t o   L e f t   c o n t e x t   m e n u   i t e m   * / 
 " R i g h t   t o   L e f t "   =   " O i k e a l t a   v a s e m m a l l e " ; 
 
 / *   T i t l e   f o r   S a v e   I m a g e   a c t i o n   b u t t o n   * / 
 " S a v e   I m a g e "   =   " T a l l e n n a   k u v a " ; 
 
 / *   S e a r c h   i n   S p o t l i g h t   c o n t e x t   m e n u   i t e m   * / 
 " S e a r c h   i n   S p o t l i g h t "   =   " E t s i   S p o t l i g h t i l l a " ; 
 
 / *   S e a r c h   w i t h   s e a r c h   p r o v i d e r   c o n t e x t   m e n u   i t e m   w i t h   p r o v i d e r   n a m e   i n s e r t e d   * / 
 " S e a r c h   w i t h   % @ "   =   " % @ - h a k u " ; 
 
 / *   S e a r c h   w i t h   G o o g l e   c o n t e x t   m e n u   i t e m   * / 
 " S e a r c h   w i t h   G o o g l e "   =   " E t s i   G o o g l e l l a " ; 
 
 / *   S e l e c t i o n   d i r e c t i o n   c o n t e x t   s u b - m e n u   i t e m   * / 
 " S e l e c t i o n   D i r e c t i o n "   =   " V a l i n n a n   s u u n t a " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " S e t   B a c k g r o u n d   C o l o r   ( U n d o   a c t i o n   n a m e ) "   =   " t a u s t a v � r i n   a s e t u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " S e t   C o l o r   ( U n d o   a c t i o n   n a m e ) "   =   " v � r i n   a s e t u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " S e t   F o n t   ( U n d o   a c t i o n   n a m e ) "   =   " f o n t t i a s e t u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " S e t   T r a d i t i o n a l   C h a r a c t e r   S h a p e   ( U n d o   a c t i o n   n a m e ) "   =   " p e r i n t e i s e n   m e r k k i m u o d o n   a s e t u s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " S e t   W r i t i n g   D i r e c t i o n   ( U n d o   a c t i o n   n a m e ) "   =   " k i r j o i t u k s e n   s u u n n a n   a s e t u s " ; 
 
 / *   S h o w   c o l o r s   c o n t e x t   m e n u   i t e m   * / 
 " S h o w   C o l o r s "   =   " N � y t �   v � r i t " ; 
 
 / *   S h o w   M e d i a   C o n t r o l s   c o n t e x t   m e n u   i t e m   * / 
 " S h o w   C o n t r o l s "   =   " N � y t �   s � � t i m e t " ; 
 
 / *   S h o w   f o n t s   c o n t e x t   m e n u   i t e m   * / 
 " S h o w   F o n t s "   =   " N � y t �   f o n t i t " ; 
 
 / *   m e n u   i t e m   t i t l e   * / 
 " S h o w   S p e l l i n g   a n d   G r a m m a r "   =   " N � y t �   o i k e i n k i r j o i t u s   j a   k i e l i o p p i " ; 
 
 / *   m e n u   i t e m   t i t l e   * / 
 " S h o w   S u b s t i t u t i o n s "   =   " N � y t �   k o r v a u k s e t " ; 
 
 / *   T i t l e   o f   t h e   c o n t e x t   m e n u   i t e m   t o   s h o w   w h e n   P D F P l u g i n   w a s   u s e d   i n s t e a d   o f   a   b l o c k e d   p l u g i n   * / 
 " S h o w   i n   b l o c k e d   p l u g - i n "   =   " N � y t �   e s t e t y s s �   l i i t � n n � i s e s s � " ; 
 
 / *   S m a r t   C o p y / P a s t e   c o n t e x t   m e n u   i t e m   * / 
 " S m a r t   C o p y / P a s t e "   =   " � l y k � s   k o p i o i n t i / s i j o i t u s " ; 
 
 / *   S m a r t   D a s h e s   c o n t e x t   m e n u   i t e m   * / 
 " S m a r t   D a s h e s "   =   " � l y k k � � t   v � l i v i i v a t " ; 
 
 / *   S m a r t   L i n k s   c o n t e x t   m e n u   i t e m   * / 
 " S m a r t   L i n k s "   =   " � l y k k � � t   l i n k i t " ; 
 
 / *   S m a r t   Q u o t e s   c o n t e x t   m e n u   i t e m   * / 
 " S m a r t   Q u o t e s "   =   " � l y k k � � t   l a i n a u s m e r k i t " ; 
 
 / *   T i t l e   o f   t h e   l a b e l   t o   s h o w   o n   a   s n a p s h o t t e d   p l u g - i n   * / 
 " S n a p s h o t t e d   P l u g - I n "   =   " T i l a n n e v e d o s t e t t u   l i i t � n n � i n e n " ; 
 
 / *   S p e e c h   c o n t e x t   s u b - m e n u   i t e m   * / 
 " S p e e c h "   =   " P u h e " ; 
 
 / *   S p e l l i n g   a n d   G r a m m a r   c o n t e x t   s u b - m e n u   i t e m   * / 
 " S p e l l i n g   a n d   G r a m m a r "   =   " O i k e i n k i r j o i t u s   j a   k i e l i o p p i " ; 
 
 / *   t i t l e   f o r   S t a r t   D e b u g g i n g   J a v a S c r i p t   m e n u   i t e m   * / 
 " S t a r t   D e b u g g i n g   J a v a S c r i p t "   =   " A l o i t a   J a v a S c r i p t i n   v i a n m � � r i t y s " ; 
 
 / *   t i t l e   f o r   S t a r t   P r o f i l i n g   J a v a S c r i p t   m e n u   i t e m   * / 
 " S t a r t   P r o f i l i n g   J a v a S c r i p t "   =   " A l o i t a   J a v a S c r i p t i n   p r o f i l o i n t i " ; 
 
 / *   S t a r t   s p e a k i n g   c o n t e x t   m e n u   i t e m   * / 
 " S t a r t   S p e a k i n g "   =   " A l o i t a   p u h u m i n e n " ; 
 
 / *   S t o p   c o n t e x t   m e n u   i t e m   * / 
 " S t o p "   =   " K e s k e y t � " ; 
 
 / *   t i t l e   f o r   S t o p   D e b u g g i n g   J a v a S c r i p t   m e n u   i t e m   * / 
 " S t o p   D e b u g g i n g   J a v a S c r i p t "   =   " L o p e t a   J a v a S c r i p t i n   v i a n m � � r i t y s " ; 
 
 / *   t i t l e   f o r   S t o p   P r o f i l i n g   J a v a S c r i p t   m e n u   i t e m   * / 
 " S t o p   P r o f i l i n g   J a v a S c r i p t "   =   " L o p e t a   J a v a S c r i p t i n   p r o f i l o i n t i " ; 
 
 / *   S t o p   s p e a k i n g   c o n t e x t   m e n u   i t e m   * / 
 " S t o p   S p e a k i n g "   =   " L o p e t a   p u h u m i n e n " ; 
 
 / *   S t y l e s   c o n t e x t   m e n u   i t e m   * / 
 " S t y l e s . . . "   =   " T y y l i t . . . " ; 
 
 / *   d e f a u l t   l a b e l   f o r   S u b m i t   b u t t o n s   i n   f o r m s   o n   w e b   p a g e s   * / 
 " S u b m i t "   =   " L � h e t � " ; 
 
 / *   a l t   t e x t   f o r   < i n p u t >   e l e m e n t s   w i t h   n o   a l t ,   t i t l e ,   o r   v a l u e   * / 
 " S u b m i t   ( i n p u t   e l e m e n t ) "   =   " L � h e t � " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " S u b s c r i p t   ( U n d o   a c t i o n   n a m e ) "   =   " a l a i n d e k s o i n t i " ; 
 
 / *   S u b s t i t u t i o n s   c o n t e x t   s u b - m e n u   i t e m   * / 
 " S u b s t i t u t i o n s "   =   " K o r v a u k s e t " ; 
 
 / *   M e n u   s e c t i o n   h e a d i n g   f o r   s u b t i t l e s   * / 
 " S u b t i t l e s "   =   " T e k s t i t y s " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " S u p e r s c r i p t   ( U n d o   a c t i o n   n a m e ) "   =   " y l � i n d e k s o i n t i " ; 
 
 / *   F i l e   U p l o a d   a l e r t   s h e e t   c a m e r a   b u t t o n   s t r i n g   f o r   t a k i n g   o n l y   p h o t o s   * / 
 " T a k e   P h o t o   ( f i l e   u p l o a d   a c t i o n   s h e e t ) "   =   " O t a   k u v a " ; 
 
 / *   F i l e   U p l o a d   a l e r t   s h e e t   c a m e r a   b u t t o n   s t r i n g   f o r   t a k i n g   p h o t o s   o r   v i d e o s   * / 
 " T a k e   P h o t o   o r   V i d e o   ( f i l e   u p l o a d   a c t i o n   s h e e t ) "   =   " O t a   k u v a   t a i   v i d e o i " ; 
 
 / *   F i l e   U p l o a d   a l e r t   s h e e t   c a m e r a   b u t t o n   s t r i n g   f o r   t a k i n g   o n l y   v i d e o s   * / 
 " T a k e   V i d e o   ( f i l e   u p l o a d   a c t i o n   s h e e t ) "   =   " V i d e o i " ; 
 
 / *   T e x t   R e p l a c e m e n t   c o n t e x t   m e n u   i t e m   * / 
 " T e x t   R e p l a c e m e n t "   =   " T e k s t i n   k o r v a u s " ; 
 
 / *   W e b K i t E r r o r C a n n o t S h o w U R L   d e s c r i p t i o n   * / 
 " T h e   U R L   c a n  t   b e   s h o w n "   =   " O s o i t e t t a   e i   v o i d a   n � y t t � � " ; 
 
 / *   W K E r r o r W e b V i e w I n v a l i d a t e d   d e s c r i p t i o n   * / 
 " T h e   W K W e b V i e w   w a s   i n v a l i d a t e d "   =   " W K W e b V i e w   m i t � t � i t i i n " ; 
 
 / *   W K E r r o r W e b C o n t e n t P r o c e s s T e r m i n a t e d   d e s c r i p t i o n   * / 
 " T h e   W e b   C o n t e n t   p r o c e s s   w a s   t e r m i n a t e d "   =   " V e r k k o s i s � l t � p r o s e s s i   l o p e t e t t i i n " ; 
 
 / *   W e b K i t E r r o r G e o l o c a t i o n L o c a t i o n U n k n o w n   d e s c r i p t i o n   * / 
 " T h e   c u r r e n t   l o c a t i o n   c a n n o t   b e   f o u n d . "   =   " N y k y i s t �   s i j a i n t i a   e i   l � y t y n y t . " ; 
 
 / *   W e b K i t E r r o r C a n n o t F i n d P l u g i n   d e s c r i p t i o n   * / 
 " T h e   p l u g - i n   c a n  t   b e   f o u n d "   =   " L i i t � n n � i s t �   e i   l � y d y " ; 
 
 / *   W e b K i t E r r o r C a n n o t L o a d P l u g i n   d e s c r i p t i o n   * / 
 " T h e   p l u g - i n   c a n  t   b e   l o a d e d "   =   " L i i t � n n � i s t �   e i   v o i d a   l a d a t a " ; 
 
 / *   p r o m p t   s t r i n g   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " T h e   u s e r   n a m e   o r   p a s s w o r d   y o u   e n t e r e d   f o r   a r e a    % @    o n   % @   w a s   i n c o r r e c t .   M a k e   s u r e   y o u  r e   e n t e r i n g   t h e m   c o r r e c t l y ,   a n d   t h e n   t r y   a g a i n . "   =   " A l u e e l l e    % @    ( % @ )   s y � t t � m � s i   k � y t t � j � t u n n u s   t a i   s a l a s a n a   o l i   v i r h e e l l i n e n .   V a r m i s t a ,   e t t �   s y � t � t   n e   o i k e i n ,   j a   y r i t �   u u d e l l e e n . " ; 
 
 / *   p r o m p t   s t r i n g   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " T h e   u s e r   n a m e   o r   p a s s w o r d   y o u   e n t e r e d   f o r   t h e   % @   p r o x y   s e r v e r   % @   w a s   i n c o r r e c t .   M a k e   s u r e   y o u  r e   e n t e r i n g   t h e m   c o r r e c t l y ,   a n d   t h e n   t r y   a g a i n . "   =   " V � l i p a l v e l i m e l l e   % @   ( % @ )   s y � t t � m � s i   k � y t t � j � t u n n u s   t a i   s a l a s a n a   o l i   v i r h e e l l i n e n .   V a r m i s t a ,   e t t �   s y � t � t   n e   o i k e i n ,   j a   y r i t �   u u d e l l e e n . " ; 
 
 / *   p r o m p t   s t r i n g   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " T h e   u s e r   n a m e   o r   p a s s w o r d   y o u   e n t e r e d   f o r   t h i s   a r e a   o n   % @   w a s   i n c o r r e c t .   M a k e   s u r e   y o u  r e   e n t e r i n g   t h e m   c o r r e c t l y ,   a n d   t h e n   t r y   a g a i n . "   =   " S y � t t � m � s i   k � y t t � j � t u n n u s   t a i   s a l a s a n a   t � l l e   a l u e e l l e   o s o i t t e e s s a   % @   o l i   v i r h e e l l i n e n .   V a r m i s t a ,   e t t �   s y � t � t   n e   o i k e i n ,   j a   y r i t �   u u d e l l e e n . " ; 
 
 / *   t e x t   t h a t   a p p e a r s   a t   t h e   s t a r t   o f   n e a r l y - o b s o l e t e   w e b   p a g e s   i n   t h e   f o r m   o f   a   ' s e a r c h a b l e   i n d e x '   * / 
 " T h i s   i s   a   s e a r c h a b l e   i n d e x .   E n t e r   s e a r c h   k e y w o r d s :   "   =   " I n d e k s i s t �   v o i d a a n   t e h d �   h a k u j a .   E t s i   a v a i n s a n o j a :   " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " T i g h t e n   K e r n i n g   ( U n d o   a c t i o n   n a m e ) "   =   " v � l i s t y k s e n   t i i v i s t y s " ; 
 
 / *   p r o m p t   s t r i n g   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " T o   v i e w   t h i s   p a g e ,   y o u   m u s t   l o g   i n   t o   a r e a    % @    o n   % @ . "   =   " J o s   h a l u a t   n � h d �   s i v u n ,   s i n u n   t � y t y y   k i r j a u t u a   a l u e e l l e    % @    ( % @ ) . " ; 
 
 / *   p r o m p t   s t r i n g   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " T o   v i e w   t h i s   p a g e ,   y o u   m u s t   l o g   i n   t o   t h e   % @   p r o x y   s e r v e r   % @ . "   =   " J o s   h a l u a t   n � h d �   s i v u n ,   s i n u n   t � y t y y   k i r j a u t u a   v � l i p a l v e l i m e l l e   % @   ( % @ ) . " ; 
 
 / *   p r o m p t   s t r i n g   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " T o   v i e w   t h i s   p a g e ,   y o u   m u s t   l o g   i n   t o   t h i s   a r e a   o n   % @ : "   =   " V o i t   k a t s e l l a   s i v u a   k i r j a u t u m a l l a   a l u e e l l e   o s o i t t e e s s a   % @ : " ; 
 
 / *   T r a n s f o r m a t i o n s   c o n t e x t   s u b - m e n u   i t e m   * / 
 " T r a n s f o r m a t i o n s "   =   " M u u n n o k s e t " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " T u r n   O f f   K e r n i n g   ( U n d o   a c t i o n   n a m e ) "   =   " v � l i s t y k s e n   p o i s t o " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " T u r n   O f f   L i g a t u r e s   ( U n d o   a c t i o n   n a m e ) "   =   " l i g a t u u r i e n   p o i s t o " ; 
 
 / *   W e   d o n ' t   c a r e   i f   w e   f i n d   t h i s   s t r i n g ,   b u t   s e a r c h i n g   f o r   i t   w i l l   l o a d   t h e   p l i s t   a n d   s a v e   t h e   r e s u l t s .   * / 
 " T y p i n g   ( U n d o   a c t i o n   n a m e ) "   =   " k i r j o i t u s " ; 
 
 / *   U n d e r l i n e   c o n t e x t   m e n u   i t e m   * / 
 " U n d e r l i n e "   =   " A l l e v i i v a a " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " U n d e r l i n e   ( U n d o   a c t i o n   n a m e ) "   =   " a l l e v i i v a u s " ; 
 
 / *   U n k n o w n   f i l e s i z e   F T P   d i r e c t o r y   l i s t i n g   i t e m   * / 
 " U n k n o w n   ( f i l e s i z e ) "   =   " T u n t e m a t o n " ; 
 
 / *   M e n u   i t e m   l a b e l   f o r   a   t e x t   t r a c k   t h a t   h a s   n o   o t h e r   n a m e   * / 
 " U n k n o w n   ( t e x t   t r a c k ) "   =   " T u n t e m a t o n " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " U n l i n k   ( U n d o   a c t i o n   n a m e ) "   =   " l i n k i n   p o i s t o " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " U n s c r i p t   ( U n d o   a c t i o n   n a m e ) "   =   " i n d e k s i n   p o i s t o " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " U s e   A l l   L i g a t u r e s   ( U n d o   a c t i o n   n a m e ) "   =   " k a i k k i e n   l i g a t u u r i e n   k � y t t � " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " U s e   S t a n d a r d   K e r n i n g   ( U n d o   a c t i o n   n a m e ) "   =   " v a k i o p a r i v � l i s t y k s e n   k � y t t � " ; 
 
 / *   U n d o   a c t i o n   n a m e   * / 
 " U s e   S t a n d a r d   L i g a t u r e s   ( U n d o   a c t i o n   n a m e ) "   =   " v a k i o l i g a t u u r i e n   k � y t t � " ; 
 
 / *   D e s c r i p t i o n   o f   W e b C r y p t o   m a s t e r   k e y s   i n   K e y c h a i n   * / 
 " U s e d   t o   e n c r y p t   W e b C r y p t o   k e y s   i n   p e r s i s t e n t   s t o r a g e ,   s u c h   a s   I n d e x e d D B "   =   " K � y t e t � � n   W e b C r y p t o - a v a i m i e n   s a l a a m i s e e n   p y s y v � s s �   t a l l e n n u s p a i k a s s a ,   k u t e n   I n d e x e d D B : s s � " ; 
 
 / *   W e b   I n s p e c t o r   w i n d o w   t i t l e   * / 
 " W e b   I n s p e c t o r      % @ "   =   " V e r k k o i n s p e k t o r i      % @ " ; 
 
 / *   P s e u d o   p l u g - i n   n a m e ,   v i s i b l e   i n   t h e   I n s t a l l e d   P l u g - i n s   p a g e   i n   S a f a r i .   * / 
 " W e b K i t   b u i l t - i n   P D F "   =   " W e b K i t i n   s i s � i n e n   P D F " ; 
 
 / *   W e b K i t E r r o r I n t e r n a l   d e s c r i p t i o n   * / 
 " W e b K i t   e n c o u n t e r e d   a n   i n t e r n a l   e r r o r "   =   " W e b K i t i n   s i s � i n e n   v i r h e " ; 
 
 / *   m e s s a g e   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " Y o u r   l o g i n   i n f o r m a t i o n   w i l l   b e   s e n t   s e c u r e l y . "   =   " K i r j a u t u m i s t i e d o t   l � h e t e t � � n   s u o j a t u s s a   m u o d o s s a . " ; 
 
 / *   m e s s a g e   i n   a u t h e n t i c a t i o n   p a n e l   * / 
 " Y o u r   p a s s w o r d   w i l l   b e   s e n t   u n e n c r y p t e d . "   =   " S a l a s a n a   l � h e t e t � � n   s a l a a m a t t o m a n a . " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " a c c e p t e d "   =   " h y v � k s y t t y " ; 
 
 / *   V e r b   s t a t i n g   t h e   a c t i o n   t h a t   w i l l   o c c u r   w h e n   a   t e x t   f i e l d   i s   s e l e c t e d ,   a s   u s e d   b y   a c c e s s i b i l i t y   * / 
 " a c t i v a t e "   =   " o t a   k � y t t � � n " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a n   a l e r t .   * / 
 " a l e r t "   =   " v a r o i t u s " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a n   a l e r t   d i a l o g .   * / 
 " a l e r t   d i a l o g "   =   " v a r o i t u s i k k u n a " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a n   a p p l i c a t i o n .   * / 
 " a p p l i c a t i o n "   =   " o h j e l m a " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   s t a t u s   u p d a t e .   * / 
 " a p p l i c a t i o n   s t a t u s "   =   " o h j e l m a n   t i l a " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a n   a r t i c l e .   * / 
 " a r t i c l e "   =   " a r t i k k e l i " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   a u d i o   e l e m e n t   c o n t r o l l e r   * / 
 " a u d i o   e l e m e n t   p l a y b a c k   c o n t r o l s   a n d   s t a t u s   d i s p l a y "   =   " � � n i e l e m e n t i n   t o i s t o s � � t i m e t   j a   t i l a n � y t t � " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   a u d i o   e l e m e n t   c o n t r o l l e r   * / 
 " a u d i o   p l a y b a c k "   =   " � � n e n t o i s t o " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   s e e k   b a c k   3 0   s e c o n d s   b u t t o n   * / 
 " b a c k   3 0   s e c o n d s "   =   " t a a k s e p � i n   3 0   s e k u n t i a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " b a d   g a t e w a y "   =   " v i r h e e l l i n e n   y h d y s k � y t � v � " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " b a d   r e q u e s t "   =   " v i r h e e l l i n e n   p y y n t � " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   b a n n e r .   * / 
 " b a n n e r "   =   " b a n n e r i " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   p l a y   b u t t o n   * / 
 " b e g i n   p l a y b a c k "   =   " a l o i t a   t o i s t o " ; 
 
 / *   a c c e s s i b i l i t y   d e s c r i p t i o n   f o r   a   s e a r c h   f i e l d   c a n c e l   b u t t o n   * / 
 " c a n c e l "   =   " k u m o a " ; 
 
 / *   V e r b   s t a t i n g   t h e   a c t i o n   t h a t   w i l l   o c c u r   w h e n   a n   u n c h e c k e d   c h e c k b o x   i s   c l i c k e d ,   a s   u s e d   b y   a c c e s s i b i l i t y   * / 
 " c h e c k "   =   " v a l i t s e " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " c l i e n t   e r r o r "   =   " a s i a k a s v i r h e " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   r e g i o n   o f   c o m p l e m e n t a r y   i n f o r m a t i o n .   * / 
 " c o m p l e m e n t a r y "   =   " t � y d e n t � v � " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " c o n f l i c t "   =   " r i s t i r i i t a " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   c o n t a i n s   c o n t e n t .   * / 
 " c o n t e n t   i n f o r m a t i o n "   =   " s i s � l l � n   t i e d o t " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " c o n t i n u e "   =   " j a t k a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " c r e a t e d "   =   " l u o t u " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   m o v i e   s t a t u s   d i s p l a y   * / 
 " c u r r e n t   m o v i e   s t a t u s "   =   " n y k y i s e n   e l o k u v a n   t i l a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   e l a p s e d   t i m e   d i s p l a y   * / 
 " c u r r e n t   m o v i e   t i m e   i n   s e c o n d s "   =   " n y k y i s e n   e l o k u v a n   a i k a   s e k u n n e i s s a " ; 
 
 / *   r o l e   d e s c r i p t i o n   o f   A R I A   d e f i n i t i o n   r o l e   * / 
 " d e f i n i t i o n "   =   " m � � r i t e l m � " ; 
 
 / *   d e s c r i p t i o n   d e t a i l   * / 
 " d e s c r i p t i o n "   =   " k u v a u s " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   o f   a   d e s c r i p t i o n   l i s t   * / 
 " d e s c r i p t i o n   l i s t "   =   " l u e t t e l o   k u v a u k s i s t a " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a n   d i a l o g .   * / 
 " d i a l o g "   =   " v a l i n t a i k k u n a " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   d o c u m e n t .   * / 
 " d o c u m e n t "   =   " d o k u m e n t t i " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   e l a p s e d   t i m e   d i s p l a y   * / 
 " e l a p s e d   t i m e "   =   " k u l u n u t   a i k a " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   e n t e r   f u l l s c r e e n   b u t t o n   * / 
 " e n t e r   f u l l s c r e e n "   =   " k o k o   n � y t � n   t i l a a n " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   e x i t   f u l l s c r e e n   b u t t o n   * / 
 " e x i t   f u l l s c r e e n "   =   " p o i s t u   k o k o   n � y t � n   t i l a s t a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " e x p e c t a t i o n   f a i l e d "   =   " o d o t u s   e p � o n n i s t u i " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   f a s t   f o r w a r d   b u t t o n   * / 
 " f a s t   f o r w a r d "   =   " s e l a a   e t e e n p � i n " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   f a s t   r e v e r s e   b u t t o n   * / 
 " f a s t   r e v e r s e "   =   " n o p e a s t i   t a a k s e p � i n " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   a   f i l e   u p l o a d   b u t t o n   * / 
 " f i l e   u p l o a d   b u t t o n "   =   " t i e d o s t o n l � h e t y s p a i n i k e " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   a   f o o t e r   * / 
 " f o o t e r "   =   " a l a v i i t e " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " f o r b i d d e n "   =   " k i e l l e t t y " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " f o u n d "   =   " l � y t y i " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " g a t e w a y   t i m e d   o u t "   =   " y h d y s k � y t � v �   a i k a k a t k a i s t i i n " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   h e a d i n g s   * / 
 " h e a d i n g "   =   " o t s i k k o " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   h i d e   c l o s e d   c a p t i o n s   b u t t o n   * / 
 " h i d e   c l o s e d   c a p t i o n s "   =   " K � t k e   k u v a i l e v a   t e k s t i t y s " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   i m a g e   m a p   * / 
 " i m a g e   m a p "   =   " k u v a k a r t t a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   a n   i n d e f i n i t e   m e d i a   c o n t r o l l e r   t i m e   v a l u e   * / 
 " i n d e f i n i t e   t i m e "   =   " m � � r i t t � m � t � n   a i k a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " i n f o r m a t i o n a l "   =   " t i e t o " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " i n t e r n a l   s e r v e r   e r r o r "   =   " p a l v e l i m e n   s i s � i n e n   v i r h e " ; 
 
 / *   V e r b   s t a t i n g   t h e   a c t i o n   t h a t   w i l l   o c c u r   w h e n   a   l i n k   i s   c l i c k e d ,   a s   u s e d   b y   a c c e s s i b i l i t y   * / 
 " j u m p "   =   " s i i r r y " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " l e n g t h   r e q u i r e d "   =   " p i t u u s   v a a d i t a a n " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   l i n k   * / 
 " l i n k "   =   " l i n k k i " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   l i s t   m a r k e r   * / 
 " l i s t   m a r k e r "   =   " l u e t t e l o m e r k k i " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   c o n s o l e   l o g .   * / 
 " l o g "   =   " l o k i " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   i s   t h e   m a i n   p o r t i o n   o f   t h e   w e b s i t e .   * / 
 " m a i n "   =   " p � � " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   m a r q u e e .   * / 
 " m a r q u e e "   =   " m a r k i i s i " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   c o n t a i n s   m a t h e m a t i c a l   s y m b o l s .   * / 
 " m a t h "   =   " m a t e m a t i i k k a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " m e t h o d   n o t   a l l o w e d "   =   " m e n e t e l m �   e i   s a l l i t t u " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " m o v e d   p e r m a n e n t l y "   =   " s i i r r e t t y   p y s y v � s t i " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   t i m e l i n e   s l i d e r   * / 
 " m o v i e   t i m e "   =   " e l o k u v a n   a i k a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   t i m e l i n e   s l i d e r   * / 
 " m o v i e   t i m e   s c r u b b e r "   =   " e l o k u v a n   a j a n   s e l a u s p a l k k i " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   t i m e l i n e   s l i d e r   t h u m b   * / 
 " m o v i e   t i m e   s c r u b b e r   t h u m b "   =   " e l o k u v a n   a j a n   s e l a u s p a l k i n   m i n i a t y y r i " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " m u l t i p l e   c h o i c e s "   =   " u s e i t a   v a i h t o e h t o j a " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   m u t e   b u t t o n   * / 
 " m u t e "   =   " m y k i s t � " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   m u t e   b u t t o n   * / 
 " m u t e   a u d i o   t r a c k s "   =   " m y k i s t �   � � n i r a i d a t " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   c o n t a i n s   t h e   m a i n   n a v i g a t i o n   e l e m e n t s   o f   a   w e b s i t e .   * / 
 " n a v i g a t i o n "   =   " s e l a u s " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " n e e d s   p r o x y "   =   " t a r v i t s e e   v � l i p a l v e l i m e n " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " n o   c o n t e n t "   =   " e i   s i s � l t � � " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " n o   e r r o r "   =   " e i   v i r h e t t � " ; 
 
 / *   t e x t   t o   d i s p l a y   i n   f i l e   b u t t o n   u s e d   i n   H T M L   f o r m s   w h e n   n o   f i l e   i s   s e l e c t e d   * / 
 " n o   f i l e   s e l e c t e d "   =   " t i e d o s t o a   e i   o l e   v a l i t t u " ; 
 
 / *   t e x t   t o   d i s p l a y   i n   f i l e   b u t t o n   u s e d   i n   H T M L   f o r m s   w h e n   n o   f i l e s   a r e   s e l e c t e d   a n d   t h e   b u t t o n   a l l o w s   m u l t i p l e   f i l e s   t o   b e   s e l e c t e d   * / 
 " n o   f i l e s   s e l e c t e d "   =   " t i e d o s t o j a   e i   o l e   v a l i t t u " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " n o   l o n g e r   e x i s t s "   =   " e i   o l e   e n � �   o l e m a s s a " ; 
 
 / *   T e x t   t o   d i s p l a y   i n   f i l e   b u t t o n   u s e d   i n   H T M L   f o r m s   f o r   m e d i a   f i l e s   w h e n   n o   m e d i a   f i l e s   a r e   s e l e c t e d   a n d   t h e   b u t t o n   a l l o w s   m u l t i p l e   f i l e s   t o   b e   s e l e c t e d   * / 
 " n o   m e d i a   s e l e c t e d   ( m u l t i p l e ) "   =   " m e d i a a   e i   o l e   v a l i t t u   ( u s e i t a ) " ; 
 
 / *   T e x t   t o   d i s p l a y   i n   f i l e   b u t t o n   u s e d   i n   H T M L   f o r m s   f o r   m e d i a   f i l e s   w h e n   n o   m e d i a   f i l e   i s   s e l e c t e d   * / 
 " n o   m e d i a   s e l e c t e d   ( s i n g l e ) "   =   " m e d i a a   e i   o l e   v a l i t t u   ( y k s i ) " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " n o n - a u t h o r i t a t i v e   i n f o r m a t i o n "   =   " v a r m e n t a m a t o n   t i e t o " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " n o t   f o u n d "   =   " e i   l � y t y n y t " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " n o t   m o d i f i e d "   =   " e i   m u o k a t t u " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   n o t e   i n   a   d o c u m e n t .   * / 
 " n o t e "   =   " m u i s t i i n p a n o " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   r e m a i n i n g   t i m e   d i s p l a y   * / 
 " n u m b e r   o f   s e c o n d s   o f   m o v i e   r e m a i n i n g "   =   " e l o k u v a n   j � l j e l l �   o l e v a   a i k a   s e k u n n e i s s a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " p a r t i a l   c o n t e n t "   =   " o s i t t a i n e n   s i s � l t � " ; 
 
 / *   V a l i d a t i o n   m e s s a g e   f o r   i n p u t   f o r m   c o n t r o l s   r e q u i r i n g   a   c o n s t r a i n e d   v a l u e   a c c o r d i n g   t o   p a t t e r n   * / 
 " p a t t e r n   m i s m a t c h "   =   " k u v i o   e i   t � s m � � " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   p a u s e   b u t t o n   * / 
 " p a u s e "   =   " k e s k e y t � " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   p a u s e   b u t t o n   * / 
 " p a u s e   p l a y b a c k "   =   " k e s k e y t �   t o i s t o " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " p a y m e n t   r e q u i r e d "   =   " m a k s u   v a a d i t a a n " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   p l a y   b u t t o n   * / 
 " p l a y "   =   " t o i s t a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " p r e c o n d i t i o n   f a i l e d "   =   " e n n a k k o e h t o   e p � o n n i s t u i " ; 
 
 / *   V e r b   s t a t i n g   t h e   a c t i o n   t h a t   w i l l   o c c u r   w h e n   a   b u t t o n   i s   p r e s s e d ,   a s   u s e d   b y   a c c e s s i b i l i t y   * / 
 " p r e s s "   =   " p a i n a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " p r o x y   a u t h e n t i c a t i o n   r e q u i r e d "   =   " v � l i p a l v e l i m e n   t o d e n t a m i n e n   v a a d i t a a n " ; 
 
 / *   V a l i d a t i o n   m e s s a g e   f o r   i n p u t   f o r m   c o n t r o l s   w i t h   v a l u e   h i g h e r   t h a n   a l l o w e d   m a x i m u m   * / 
 " r a n g e   o v e r f l o w "   =   " a l u e e n   y l i v u o t o " ; 
 
 / *   V a l i d a t i o n   m e s s a g e   f o r   i n p u t   f o r m   c o n t r o l s   w i t h   v a l u e   l o w e r   t h a n   a l l o w e d   m i n i m u m   * / 
 " r a n g e   u n d e r f l o w "   =   " a l u e e n   a l i v u o t o " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " r e d i r e c t e d "   =   " u u d e l l e e n o h j a t t u " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   d i s t i n c t   r e g i o n   i n   a   d o c u m e n t .   * / 
 " r e g i o n "   =   " a l u e " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   t i m e   r e m a i n i n g   d i s p l a y   * / 
 " r e m a i n i n g   t i m e "   =   " j � l j e l l �   o l e v a   a i k a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " r e q u e s t   t i m e d   o u t "   =   " p y y n t �   a i k a k a t k a i s t i i n " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " r e q u e s t   t o o   l a r g e "   =   " p y y n t �   l i i a n   s u u r i " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " r e q u e s t e d   U R L   t o o   l o n g "   =   " p y y d e t t y   o s o i t e   l i i a n   p i t k � " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " r e q u e s t e d   r a n g e   n o t   s a t i s f i a b l e "   =   " p y y d e t t y   a l u e   v i r h e e l l i n e n " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " r e s e t   c o n t e n t "   =   " n o l l a a   s i s � l t � " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   r e t u r n   s t r e a m i n g   m o v i e   t o   r e a l   t i m e   b u t t o n   * / 
 " r e t u r n   s t r e a m i n g   m o v i e   t o   r e a l   t i m e "   =   " p a l a u t a   s u o r a t o i s t o e l o k u v a   r e a a l i a i k a a n " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   r e t u r n   t o   r e a l   t i m e   b u t t o n   * / 
 " r e t u r n   t o   r e a l t i m e "   =   " p a l a u t a   r e a a l i a i k a a n " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   c o n t a i n s   a   s e a r c h   f e a t u r e   o f   a   w e b s i t e .   * / 
 " s e a r c h "   =   " e t s i " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " s e e   o t h e r "   =   " k a t s o   m u u " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   j u m p   b a c k   3 0   s e c o n d s   b u t t o n   * / 
 " s e e k   m o v i e   b a c k   3 0   s e c o n d s "   =   " e t s i   e l o k u v a a   t a a k s e p � i n   3 0   s e k u n t i a " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   f a s t   r e w i n d   b u t t o n   * / 
 " s e e k   q u i c k l y   b a c k "   =   " e t s i   n o p e a s t i   t a a k s e p � i n " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   f a s t   f o r w a r d   b u t t o n   * / 
 " s e e k   q u i c k l y   f o r w a r d "   =   " e t s i   n o p e a s t i   e t e e n p � i n " ; 
 
 / *   V e r b   s t a t i n g   t h e   a c t i o n   t h a t   w i l l   o c c u r   w h e n   a   r a d i o   b u t t o n   i s   c l i c k e d ,   a s   u s e d   b y   a c c e s s i b i l i t y   * / 
 " s e l e c t "   =   " v a l i t s e " ; 
 
 / *   a c c e s s i b i l i t y   r o l e   d e s c r i p t i o n   f o r   a   h o r i z o n t a l   r u l e   [ < h r > ]   * / 
 " s e p a r a t o r "   =   " e r o t i n " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " s e r v e r   e r r o r "   =   " p a l v e l i n v i r h e " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " s e r v i c e   u n a v a i l a b l e "   =   " p a l v e l u   e i   k � y t e t t � v i s s � " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   s h o w   c l o s e d   c a p t i o n s   b u t t o n   * / 
 " s h o w   c l o s e d   c a p t i o n s "   =   " n � y t �   k u v a i l e v a   t e k s t i t y s " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   s h o w   c l o s e d   c a p t i o n s   b u t t o n   * / 
 " s t a r t   d i s p l a y i n g   c l o s e d   c a p t i o n s "   =   " a l o i t a   k u v a i l e v a n   t e k s t i t y k s e n   n � y t t � m i n e n " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   m o v i e   s t a t u s   * / 
 " s t a t u s "   =   " t i l a " ; 
 
 / *   V a l i d a t i o n   m e s s a g e   f o r   i n p u t   f o r m   c o n t r o l s   w i t h   v a l u e   n o t   r e s p e c t i n g   t h e   s t e p   a t t r i b u t e   * / 
 " s t e p   m i s m a t c h "   =   " a s k e l   e i   t � s m � � " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   h i d e   c l o s e d   c a p t i o n s   b u t t o n   * / 
 " s t o p   d i s p l a y i n g   c l o s e d   c a p t i o n s "   =   " l o p e t a   k u v a i l e v a n   t e k s t i t y k s e n   n � y t t � m i n e n " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " s u c c e s s "   =   " o n n i s t u i " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " s w i t c h i n g   p r o t o c o l s "   =   " v a i h d e t a a n   p r o t o k o l l i a " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   c o n t a i n s   t h e   c o n t e n t   o f   a   t a b .   * / 
 " t a b   p a n e l "   =   " v � l i l e h t i p a n e e l i " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " t e m p o r a r i l y   r e d i r e c t e d "   =   " v � l i a i k a i s e s t i   u u d e l l e e n o h j a t t u " ; 
 
 / *   t e r m   w o r d   o f   a   d e s c r i p t i o n   l i s t   * / 
 " t e r m "   =   " t e r m i " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   t i m e l i n e   t h u m b   * / 
 " t i m e l i n e   s l i d e r   t h u m b "   =   " a i k a j a n a n   l i u k u s � � t i m e n   m i n i a t y y r i " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a n   u p d a t i n g   t i m e r .   * / 
 " t i m e r "   =   " a j a s t i n " ; 
 
 / *   V a l i d a t i o n   m e s s a g e   f o r   f o r m   c o n t r o l   e l e m e n t s   w i t h   a   v a l u e   l o n g e r   t h a n   m a x i m u m   a l l o w e d   l e n g t h   * / 
 " t o o   l o n g "   =   " l i i a n   p i t k � " ; 
 
 / *   A n   A R I A   a c c e s s i b i l i t y   g r o u p   t h a t   a c t s   a s   a   t o o l t i p .   * / 
 " t o o l t i p "   =   " t y � k a l u v i n k k i " ; 
 
 / *   V a l i d a t i o n   m e s s a g e   f o r   i n p u t   f o r m   c o n t r o l s   w i t h   a   v a l u e   n o t   m a t c h i n g   t y p e   * / 
 " t y p e   m i s m a t c h "   =   " t y y p p i   e i   t � s m � � " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " u n a c c e p t a b l e "   =   " e i   k e l p a a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " u n a u t h o r i z e d "   =   " e i   v a l t u u t e t t u " ; 
 
 / *   V e r b   s t a t i n g   t h e   a c t i o n   t h a t   w i l l   o c c u r   w h e n   a   c h e c k e d   c h e c k b o x   i s   c l i c k e d ,   a s   u s e d   b y   a c c e s s i b i l i t y   * / 
 " u n c h e c k "   =   " p o i s t a   v a l i n t a " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " u n i m p l e m e n t e d "   =   " e i   t o t e u t e t t u " ; 
 
 / *   U n k n o w n   f i l e n a m e   * / 
 " u n k n o w n "   =   " t u n t e m a t o n " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   t u r n   m u t e   o f f   b u t t o n   * / 
 " u n m u t e "   =   " p o i s t a   m y k i s t y s " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   u n   m u t e   b u t t o n   * / 
 " u n m u t e   a u d i o   t r a c k s "   =   " p o i s t a   � � n i r a i t o j e n   m y k i s t y s " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " u n s u p p o r t e d   m e d i a   t y p e "   =   " e i   t u e t t u   m e d i a t y y p p i " ; 
 
 / *   H T T P   r e s u l t   c o d e   s t r i n g   * / 
 " u n s u p p o r t e d   v e r s i o n "   =   " e i   t u e t t u   v e r s i o " ; 
 
 / *   V a l i d a t i o n   m e s s a g e   f o r   r e q u i r e d   f o r m   c o n t r o l   e l e m e n t s   t h a t   h a v e   n o   v a l u e   * / 
 " v a l u e   m i s s i n g "   =   " a r v o   p u u t t u u " ; 
 
 / *   a c c e s s i b i l i t y   h e l p   t e x t   f o r   v i d e o   e l e m e n t   c o n t r o l l e r   * / 
 " v i d e o   e l e m e n t   p l a y b a c k   c o n t r o l s   a n d   s t a t u s   d i s p l a y "   =   " v i d e o e l e m e n t i n   t o i s t o s � � t i m e t   j a   t i l a n � y t t � " ; 
 
 / *   a c c e s s i b i l i t y   l a b e l   f o r   v i d e o   e l e m e n t   c o n t r o l l e r   * / 
 " v i d e o   p l a y b a c k "   =   " v i d e o t o i s t o " ; 
 
                                         �+&{�Љ�h���nn@�h�c� 47ΕK����44(�4�jI�H�%�e�:F��_m�V�n�Q|��z��0CB���O�*@j?��%�\�,Ps�:x�9�G�ϴ��&�ׯ�eZ��l��C�@d��D�	s�b�3d@(��.|���^l�߲l�!�����P,�X|'Elԉ����� ��k�.P�|G!��GU�\>^V7~�ysO�ݳ�,�3��'p_;��C�yv��̆f(Q�^m��
�d5��Gt���C�o�M����zx;����ȅ�A��Mš��R����7܌N+(�,�1�-�P��(�p�r��`;���B�$am�e�]=�O���IE���M���u�GD��i����n\�����
���8�z/�hT��I��@��?��Gtb��s�����ET=E(�)�Vp,Bg�h��,И��C��U�}�/	R���P������C��z�����\�Ҡ5�9�L_��׎�bXůo�-���'	YLO��qI�:ĺ8?v���K
?��
����M�`��2dx�̭�E�u�LG��~}�ߌ��O~�)�x�R�VS(B%s������@���#�K�� ���^՛5��DL��\��D��/��<��͇����Z������锁Z��Fa��F����F'����[�ͦ��u#
��ϊ�}�c�3�s�gFa��n��`�=�Ѥ]o�&ue��J����"�W=�D��dZw���{�׭������B���B��(d��8O����`D�-�!����u�0��ү�n��ο�����c�||��W�p����w�c�?������w�~�{R����'��u�&9o�}���Ύ���6�����˦�x�JPd�K�m��4,�t=H�t�4���|��߻7���nɒg�Á�wJ��!&�� d8?n|j���!���H��|�&#67S�~vŰ��D�n�4�V�A�7�f�V�M�Uc�ѯJ<�byS�B���:T��\��~���Ԣ��������ITd�v������C��\�A��[[��!�K���Ƭ��nծ:x߻8=����,��G#؅���#8d�Z-�҄���b,+N|B7��0 e��.�⸰��ب8%�h��[C���?�VdJ��¸�R�ם��/��B�4(a{�/j$���.Ƚx��nF�b����2y��(�7���&"<�Ұ�����1��w�,rCE����Y�<B����O/��!c�^�d�m����o�A���ڪ���E���{YK�- ���<!���:��������_��^v��~�`ڿ���w���#&��3]h|�R�~:J�)��L�!��x���\��uNj'�er���ׯ������� �ׁ�M�_�$Zn�#Hz���d�x��!,�̛���0��4�6��l,��6���i�.p�^MG]Q�Tg]��������O�w5y�v���t5ū��I�)�t�p�+����G�i֓��d\�8����g��7�����K�$5i�>sg:�����e���[��|�P#�H*W�Wv��P<<�Lc�� =���A4�Dy$�P"�a)��߿\/`G�z>-f�(n�ׅ*��vL��B�gj�ɀ)��>?}0���uSFɤ�$�D�@�½�!>S�����vj��W�#�'��t��Q"Eh�.>N໭=�}=�FI&L;V*՚�)K+��<��<���4=�^��I#��^\tz��v���JqE G�~d���=��z |�A�͝�]�;������G��t��Q����c�(�ϳv;��z����U���O��P����ʣ����d/������o�6��UTov�@<��*/�?��D6���#l���QG]���O������~�����|�ŀo͚�����[�_�&�
�`<��iO6z���.z�up � �����p�{�&͵��ڎy���׭W�}E���n�/A3�����@YL�"|~��-z�u�|̦4�ڔ�6���Q�t�f_p���3�uJU)n/@�����⹠�@�:��ũ�x�{����q��D��v�c����}5�)��r�;���	�Њ�f���ظh0�G,j��qSz�����@�N�h��r�i�oh��!\�>�d�LSs��ͅN^U����&Hu\���Z99�8�w`����`9��6�����%�V*������H�z]u@�<f��Yp5=�p.^o�kq�UY������^��h����|vK��� {�g��f�0flꑦ`�*�����kw��;;��2Y�����.�1Q�y.�i�����(��O/�y��M)$�t�Ě��8�}��)v���X��ϱ�A�8nY4\������G㝸�������/�#bjh��Y�S|d�)��;��S�DQlc�<Xo�1�������bWa>GY���	��4턅.�a�)�˕ �%��P�pD ����|����6|D`	�R�s *v� \!�n�D�, ,�*n��Q	�EMQ�/*! Select2 4.0.3 | https://github.com/select2/select2/blob/master/LICENSE.md */

(function(){if(jQuery&&jQuery.fn&&jQuery.fn.select2&&jQuery.fn.select2.amd)var e=jQuery.fn.select2.amd;return e.define("select2/i18n/bg",[],function(){return{inputTooLong:function(e){var t=e.input.length-e.maximum,n="Моля въведете с "+t+" по-малко символ";return t>1&&(n+="a"),n},inputTooShort:function(e){var t=e.minimum-e.input.length,n="Моля въведете още "+t+" символ";return t>1&&(n+="a"),n},loadingMore:function(){return"Зареждат се още…"},maximumSelected:function(e){var t="Можете да направите до "+e.maximum+" ";return e.maximum>1?t+="избора":t+="избор",t},noResults:function(){return"Няма намерени съвпадения"},searching:function(){return"Търсене…"}}}),{define:e.define,require:e.require}})();                                                                                                                      &�Co5���=�N�o4yw2�=���
�0e���� x������:��[�� ���o9���PI�rbh{O�G��n[:��Q�ԗK�}�e��t{)?���qd��=�*ʷ�FP08�s^��7����-����C&k�>݌n�`�W���>���#>�a'���=;_������|A^�}�k�5��_5��F��=6�I�f?g�K}�0��rpn���8i�O��B�3��j��>��ι�/A���k؃�ѓ#<e�RG �~��ֺ��&���g�ٰ��9���|�t�?��0�W�jZ��ᯈ-�(��[�Ж�D�3��z�Z��O��o��,�ݨAj �7B2pH��]\����Y��З����0~&�N����_�!v%ի�H�>��zr+��������<b=3U�1�kK�`n��{�5�|B��J�~�����w������r3��'>������~XC�x�'��yA}ɒ�?y@�}3�e��\����|9�f��P����Gk��x[SH�=N��e���J����B	���_�{�~��M���,Mf��S��<��k�<yே�.[�0Z�zƩ<j�����I%z�ǩ�U�=�}�&�x����eA�^ǲ�����܎y~e>z���[�s���4Q1�i�hWWXKf��g9L���� ��5�0��P�����6Ve��Oo_�c�"�-��ʓ��BO���6weCe�PI����Dtz�B�(�J9�2�ۀ@?;���J7��y1����=j9�Ԭ+$��>a#b���*0����7s��+�;q��}�+��4o��^�S[� �˾N�s�{�l!�лyOrp�� ����^�@!�3����h�����L�uI͞��5�7=Fk��$��y��2E<<m�Io� Ut~צѼg�_C*�5����5Js︌z��֞��lm仲�D�wI �`���Ҹ�Vj�+�r�CS�{G�������u�xs��=Ƨ&�h//�60��=�r����g�z��犵�|?�_�>�Ҥ��NV-S�,��c���d����g�_���Ǎ|��T�J��!*&�qj��f�3��u�ǎynǎ�4SI���?.�3���q�� Z�>KJ;�7��S����|��φZ�t�'M��a�[>��ǯJ��|�m5�'Q��#x��J�b&{�Ú����̚N���ꖺ~�x�Nc��b� �u��Ҿ��W�t�j6O�\[^�O[�IavPÌ�p0�#8�È�T����У�I$y�/��'�o��y��_�&�g�ee+*�W>Î��/���SH�N�g�]_[���HU�$np�Q^��ti���Ť�M�p���N�>�W̾!�l��n%��-/$�B�Q�7=?t��8�SK8�N<��1��U&���v>~�n�u�Y�\[�?�d�ʀ�p:�;V6��?H�t�,v�.�bH\�O�{������|)���������$5�(#h��QX���	���������;�h�J	� X`�piS�V��g�P�Q�(������^�������麶�j�s_��i������^��}������ �Ǩ��XhZ]�����B�W�>��^�~�W�xgL��C��[�%�gl����#ֺ�2�V��ơ�+�?�|M"�,.����}9�1��\t��Q\�T�D�������I��񍣶Ev�[�l~���\&�G�\�z������W�v�V��iH#<
�|i���ĭ�i�$r�JF�E�f�!�ו)���湽,$9�=^˹��������>p:�Z����t[?�_���[�a���q���-�3�pG�޽��� �[�o�5�r�.�e�+��	V���s����ּ��~��⿇����Y�����U���3�� ���W��#���MUOc��8|&���o೒���<�G�2q�j�#Iֵ5���^�j>Xԍ�3�['�ӧ5����{8� ����?1fVX{��/�C��F![��g�3�9���V���٣��`�[�!��Ώ�xcR�V�u�/�m?~ ��+����S�Wº����\ `��k��n������R�Ƣ|� -�HH$ >��U�^�N��x��Z]���R�1�D����}<��>����z�#��T(EY]�A��ln,�QؼFYB���K8�t9�0G�Y��������=ռ���.Q���5�s^@�y[�d��\��������?t�wM�<U����6� i�ӚuY�ee	Ԍ��p>�ϵ�i�6�դV���h��0�"�q9�lg�/�����?|�W�%����s�a��L���8���|��'[c�El�s�k�?|?oB�gil� Q��0���� ׯ�<U���麝��y5�@�b�-�����ۯj���R���WK�t~����k	M�p� }�>�}>��Z�%�i�Q�O��ejHد�����q��� �z�����9�e|�jicw|����`����9�0�2�K�6�~$hz~��IgP��1\���b��=e�8w
��5��~��B�o�uMW·����n1:�UH�^��z���+�� �����_Gc�\6��z����%ە��?�{ַ�mON{9�Q0��?/̣�q�5���-�yuKKka���P{�V3����飄�:��,��������2�i��v#@�±t/�A�hn�ۃ�6�u������m�7�.���>˩Պ5�AP8�s���0xoZ�O=���P�#/�$����k�\���܊rJZ��c��H���u�_�p��G�.��C\5�w`��du��W�|T��|o�;��z5��� ���CG�X`�0����'��h�1Z�Q%�Fm��p�}�ֱ�h�L�o��#�{{W�G5�M�;'�S�RZ3�k?��]=�&�al.��z��
��
�_��� ��ok�\�)v�2� /<d滝K�7ZS�����[�.d��'��V|7�j�[i6�wf%/$!W(H!�gٿ�ꥊ��z��Ϋ��5vW��û9�X�5��{�<{�]6�dpFH�/*! Select2 4.0.3 | https://github.com/select2/select2/blob/master/LICENSE.md */

(function(){if(jQuery&&jQuery.fn&&jQuery.fn.select2&&jQuery.fn.select2.amd)var e=jQuery.fn.select2.amd;return e.define("select2/i18n/ca",[],function(){return{errorLoading:function(){return"La càrrega ha fallat"},inputTooLong:function(e){var t=e.input.length-e.maximum,n="Si us plau, elimina "+t+" car";return t==1?n+="àcter":n+="àcters",n},inputTooShort:function(e){var t=e.minimum-e.input.length,n="Si us plau, introdueix "+t+" car";return t==1?n+="àcter":n+="àcters",n},loadingMore:function(){return"Carregant més resultats…"},maximumSelected:function(e){var t="Només es pot seleccionar "+e.maximum+" element";return e.maximum!=1&&(t+="s"),t},noResults:function(){return"No s'han trobat resultats"},searching:function(){return"Cercant…"}}}),{define:e.define,require:e.require}})();                                                                                                                                                    	f?2�<���ϟ�� �}J$���or�H��3�[��)�ŝ�"F��}����;:���UΩӶ��'�WVV�g�V�OL�Mm��=p@5��O��<5�"�ܱ˶@�����c޼�\������[�k�^i�n.�'n{L���/��⟄|]_��隇�,H�����~x�;����{f��i<E�@�lp�9�;{RN�V��b�6�4�KƑC$���$u=i��f5p��2������P϶�Ae��z�����!�|D_�G㛭g��C�iD*��l� 
�W�������aٵ�ps^)�sA��.��EЭ��uM;O$~��v���O�V��}�GmY�~7�f��M��+(i��*��a���ҿ?<U��j73]�s8k���$/�޽+����|[�ZI�X_��F�h����.�u����>������,m����dq���ÄrXᨩ�Y?����=�]Z�dq7����m���9a�~�[�"f%$ n�xQ�[�<#��7"���9��-uk$��aT�q���k�!��]	5��ۅ,8�\��(�tH��f8�s�޳���7�ʄC��_�Q��f7�ie��a�� J��P�Yd�+ �_�j�Ԓ�ȑ�X���=j��")��Tw�}�D���о��uǠ���2w,����V*�c�O� ��i�ĞJr��rI=E5L"	v�#l��i�8���9��O�x�5n`�r��Ӽ��|H8,GA�VL���Je2o�</�~�u�`�EɅ@�bKW5u�IHފ�GfϿ�51�uowl��B҉O�H�Xs�$A�$�d�7>�Ձo�e��t �
��>���{۝�[��i7�Q$��]O�� d/ڇV��>0�0}e����/�l�VxOݔ��$��0���Sx��
�&��O�kC�iqc�g�R�l��+c��{u�����R���(�dl�����߲W�M�|	���_��WR���Y��g�?���#�	��������B�#��u��?��������yy�|;���
sql�	g/��4�$u��ҿ'�m��G����|7��_鱋=f��s�c �,��
q�ֹ������SD��1�/_���9{l.��Go�����O� �FH��{{z���o��+6瓁�3���ǜ��N��ϰ����x-R_A�Nq��k�D3l~��+�vc��~L��Ҹ,�G��&�%���7���8U8���_�b+������St����у>��לm>�Q�_/�&b���)dp�2$s&� ��?�g"&�H��j��f�w>]�0��I!s����(�]I&00�w�JmF��t5Z9�H����79�� 몒d�X~'�-�%�g���T�2Yn��>D��8�j|S3�$釘Ç�ޫ+fh�f�� 3z�A�DU�2l��E�p�q�u��G���}q�H<���f����ji���6�'�q�Z++�Q<B5uKxX�����R�D�R8�vz��W-�8�.O��z~�Ǒ�( v9���{��#��V��i��_#Q�_���h� d��&���M�l.�3�m��B��5�M�H�k2���3��p\t��_�?���� �	�/�<�����rK\n9遁����qџ%���������`Gt�� ����	4�.�`f(s�����k�(�w +�����.T�N�査��� ?ξj���X5���:H�`� �n��օ������W�&(�X\	,M�.z�i	 ���r6����V������j��Y>��NH���0e,]Ct��j�m<��r;�ƴ�b8Sq�����Y5m%���O��78�H�9����z1"�3�� �~��Ζ�9��J��c��y��!�<��qq��ۚ顺8+�F���b�r����rx���SgF,��0�s�� �V.B����B��NZ�Ϋ�[�nT�y>���,�Ƽl�g'��I��5dVo\�� &���h�I�r{��n.m|҈��v�n��9�� ���9� >���˭��C�G}��s�x5Y��Y\4�y;N=*i���$>j������C����W߭5��$Q�]�zw��7a63		��*���A幉A#�G�A����3�3M��;��]F"/�6}*�fc�<0c�ڣԷ4ʊ7~�c󤌳����0��1�!��L,K'?w��҈&TRIv|�W�)�n�޳+aG��:X�H���D����9�/�#X�2�x9�MQ�;\툰�,z
��l�%�,��>�� >k?�,xc�Ǉn�����w�O�yyH�G �Ek#�I�����9���=����ɥ|=�I�{�#�I/t���}z7F������<����m��|���@�Tk�w[#������F�M������<��ۅE��\�q�BOWf�k�mk�ׄ�U�i�6��څ���a����j�smݣ���^C���T~��G�ZO�5;umm�����[eVpx��O�^�>��o�c5r魬�kH���� ax�b�g�z�[eڧ��?�_(����F6V2|w�x�c:O�dh�}1���VP�u2;����@�'�*ƹw+Lma���s����E{/��|-��C�X����9�"�q������<��h����ޡ�i������[���o;B\�YH�#��|x�U���sjwk���V���M���
��_�Y��oS� ���������jS��ڞN�.Lyf�o����׶Z��4=7N�細�_-�����?��3��WKI&c8��	x��W���9��oeߥy��V֠��T��BD�pY��{���N�dkV�S�FÚ>h��>&յ[x"��A�K��E�Q[��}Z����� �)4i�{�>,������7�hv�%�����ڢ�aP�y�|������5H��#-&�@��ޯ�x��<)�);��Y隬L��e����?�s^�]�F��W
S<���5��7���N}��U.w{1�t���ǌ!��*>�j>�2Ҝg�q�z׈j5�'OӼV����|�<�k�K1< 9�{ώ�Y�*�v���Q6�(�=9�?��rV������m䶼���� Exif  II*            �� Ducky     F  ��_http://ns.adobe.com/xap/1.0/ <?xpacket begin="﻿" id="W5M0MpCehiHzreSzNTczkc9d"?> <x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c067 79.157747, 2015/03/30-23:40:42        "> <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"> <rdf:Description rdf:about="" xmlns:xmpMM="http://ns.adobe.com/xap/1.0/mm/" xmlns:stRef="http://ns.adobe.com/xap/1.0/sType/ResourceRef#" xmlns:xmp="http://ns.adobe.com/xap/1.0/" xmpMM:OriginalDocumentID="A748E79CEC36E1F8221D7A134F303474" xmpMM:DocumentID="xmp.did:0D4E8ACB34DA11E5BBA8BDFDACB7DAF9" xmpMM:InstanceID="xmp.iid:0D4E8ACA34DA11E5BBA8BDFDACB7DAF9" xmp:CreatorTool="Adobe Photoshop CS6 (Windows)"> <xmpMM:DerivedFrom stRef:instanceID="xmp.iid:80FCDF1884BDE31181B6EAFC5263E115" stRef:documentID="A748E79CEC36E1F8221D7A134F303474"/> </rdf:Description> </rdf:RDF> </x:xmpmeta> <?xpacket end="r"?>�� HPhotoshop 3.0 8BIM     Z %G    8BIM%     ���ȷ�x/4b4Xw��� Adobe d�   �� � 
				




�� XX �� �             	                  !1AQaq"�2��B#���R�b3�r�C$�S�4%s�	���&                ��   ? �v��ݵkib���$~�tˁ��A��^�{Ԯ���6w��j��n���w�M�J=?solk�a�X��A徧��or���n��:Q��4�B��8���|����[#�B
2�4���t��w6�}�[O�򉠮�w�X�X�K�r邃�Ph!�:�^>��!	`��t�u�������N9p�7n��/��w��������?���΋w�����pl\�	s��A��]�޵i��2��urܳ0m�g�Ae{q�v̏���]#M�e5hQ�(k�tۛ��.^���YH��<�46=ݛ5���e��y�[��n���Pص��̨�@����W�.�m�*-�y�1�h6���鶛�ţ��k'84���~�q9��x�
f�5>S�Ƃ˷��H�:������
�0�H<��@e*��<���7��>4��I�c1��Pa���5g�8p��:�\�H<h
5+"x(��q���jo���ɇ� b&3�(1m�g*	Iu=b0O:��h�$�O#@@�c��u���A!D��$�jQ���7@�b�i�u�� �y�a�Pq�����U_Q�@\1��aA�4��`��ql�����^ȴ�Ѻ6ݺ�P��7bv�u�� d�@PI��ٮn0��� � �o�����F�l/������U�]p_���;��kc����?&Q�Ï�A�dVf��ʂ(�/�.<�՞ƃ�c�k�_{r9�ʇ��r]����TfY6�`��yP^m쐁 �0��o��U�#:u{{�b��[�΂󴷶��Xݬ��T�=F��Zݗ�nۀ��k�f� �?uGy�q��:[��P�	��~4v4�#��d��c̞>#)�"�O(
$��Asx;iC��� Pg9Peq�b���A� ��8LA�h�VFxx�ؖ�s�ʃd�[	�� �a$~�	%��c224� '�^��X�(=������lo���ؽ�f6l@x��A�����^����bm\`un �|(.;�S�uD�د�zs0{{RtqL�犎T�~��޽�Z�tm���]X�cxPx� �z?� җA��"&(�؏���țDjfp�� 
	�S���ܿq� X0E 8
^�t�)�A���Ϥ�>����@Fa�YT @�G(	f����$�4%��7d��F��P<(6���S�B��#u�����'�P]n;� �:��~p�l�h3�x
���n���m�K�}y@��D!4+e�;{���/���e�s<Lf(5ί�� ���q��ה��_��tzWj�>�����V\;Ɣ�yA���{ougJ�U�G�(8ws����m�Ź�� h(.2D+A'���Oswssp��ॱ�r�����C�Nt
\F"8�%����<���e�'/��r���<�n�LNYĎSA!Y� N �*)��dc��jd�Q�쉒��|
d�r�E�C�H��ŘX\����2�f#:��Y�p�gA�~�uk}O��mm�/cf��q
�-΂��o���*WRZ`�3ƀ� Sz������Zv�Md��֬1DS�4����n�-������1���ah5$���^�n���Iw<=Y�Y�7��si��}�k�kk8�{���t�i�?�
���|("^�������2%�
�]I�=>�ŏ�f��ۈ� 7�>���EĿ��1�<�K�v�	\c#A u�bﵸX�$�A�ص������kn?,E ��ն��TbL�O�VܳZ��YF���%��A	���ƀ.�2x� �>�8�f�q�ǜ�TY#x�`� �N��t�(��|\�G�j(29�g��bp��9����p�T���?�*	[;Z� �g8���P�|����0xxx�&�P>��?v4Ix%��A�o���Wo�k�oY���ѵH[���<��� ���x�-�d�v�|�7��S���O�u��m�A��rn<\�zyM}�=g��z��v;u�sf��'I��<��UwK�L�m���t*�x*Z��-���A{����A|�}Y��9�A���D��	� 
n2�Y��Ph{x��@ɚ���n�������.���B�&9�	Vn�!^ڒ�D�~o
��u�7�����V����<G�ʹ;K}I���`&�J ��9��=u�����7��/�!n��
?��خ��z���ټ�/���L����4;떮l��wYM��d#34� ���}�Z�l��޷��qĈ3���X����9�`�'�1A����=o�&�!l^���mY\Y���AѺClE� g� CM�H\5��l�c`e�I�@p�`P$�I�|huD�9x�d�'00o@[j�q$A�~�	
��#���2�hcN ���@u�N
s8*N&2�H�@P�C9�����Q$����5i�D�'
[���̙��?��=G�.�a��;a���d�F}�mݻ��^��b۝��ų2�h:�e�f�ޕ���!w�����D�����d�Wv'D�ܽ{u��I��9~	f�fh!woR�˵��ܰ��D~��"��qj
ޛ���Ϋ��;5�uqA�Y΃�[.���N����[6mҼ`b~�
���"j"�Ձ�׬����T�j��r3AC�)a����>��v8_�O<h87U�]�^����#��A�l�.�h��#�8c@P�NF#�h4��ۧ�jݒ=��B�	����3t����<y����ܑ}WK��s���7>�3>8�:�Ʈg,�ʀ,��p�E	�NS�@�I��	X��1�$�A�j���8��&N|h�ϕ(�t�@��@����R>14 ����.zu�p�i�`����Ne9�+�����(3*�I�g��pЪ#�?���˿������K��u��Ղ��#A���ͧC�lwL�xoeF��㊜E� ��V������ڐA�p*	�9Pn2����o�����M�gPmD�Y����:%�{�U�m�]V�q����(<��o5��C�~?�	[�2,�8�PD��8E�h`=' 'G:�]���1ǕE
�1�3A��$�I���8F�L����Xl��l��׶��I$z�ɠ���k�M��}�< !r�(.�=�Ժ}�e�W�2����h6{_Vw{�����Pxs��v�񷽼�y�Y�[�wȴI�
���;]W��4�p�a���(8�����]e66߽���[_ª�c��4;���[�����g�������G��A�v������K� ���p���΁��0s�#@i$K '��fF�'ǅ���L s��h	�J�O�Ն.p�ƂZ������5���r��,���4A#/�4p\<r���!Ut�8PI�c�&�U��y6=��?3sr��@�d���[�>8� *�ս���'S]��>?1�00��}3qc����M۱����z��F�=O�bD�r��7������j0mD(�Aܿ���[�w�ṷ���İ�H��A�wi#�U��s��w�[��,�J�3@+��F����UR���A�>�u����NG�9�vp`��y�?��U{tIlq΃fil5y

^��������4��z�K�ٽ�����?,�qA�n���we����@$ef��v2��(o�.N&s��c��yQ�(��>h�D�@�q�!GA��gHd��{�Z�g�_�P
�Pd��:������dc9���1��mK0�����f�,�G�`s�	
�>�Nq@���#�dH�c�<��I�2˜P{��nzgn�N��S��{Κ��XP��<Z��_��{��W����=�	`���Yp�񠘟Oz�rw*�EllU���`��9*��n��o��=�a.X��/%�]�7����;�G�]=�v/uU}ͻ�d�c��y�@��'=AF%��,7�6`r��G�#a�S���a~GF9��Pd�� ��G��I_p�%>���a�6�%LI�9
$o�'��:f6��(�2��Hpƀ����"@�`?�v�ZÐ��������7�u�"Fd�~������k�Ym�B��yG:u^���]�����ާ�M=G�&,� f������۝嵷��`4�'ƃ�m��l��2�p�yPO�unSiS�1�
		%�b���H"?�kA  }�H�0'���2)��9PZ �1/�� b T��RI�$�@kM �i&��@oQ��3�M-+��%����j����
	�^HA�>��� �]=W;�lx�0h8ݥ�����u��ޥ��+�[:��p��l��:u�3m�ڃ   �?���ssr���&���F`���N���~��n X�=� �;	bh'��D+��Q�� �A�� �9@�9�yn-n��n��,*��xA��\΃����.w?t�D:�[n�%U0"�������ی� ��b������AQ�=Z��6�]�B�8�Zt.�ki��O]���'9��ߋ�k�L(�t۽ΰT���T���0E7e'�������.4	b$�9
(���8��xP2�ª-���yPF�����@�3@��`4���PeA�x�V1�4==5\\�� b`��Mpӫ2��@��R�'�(�� �F���PI���T��o���7Z�z�������,m�!As�%y
��n�����[�Fu?�[C���u���;Kɲ�]�۳�@뷮@b�1c"'�G��zF�i�7"��6?�k��)`�	�jw�\n���岧o���=�4�Ƞ�*�������E � /n�����^3�O�����[�b(XA �
��]�դ,H��9�@�a�12WS��( 7 ��	��hgK�\T�lxP5��0��B��i'[o�s���Z�X
��f��\8��g 8�L�/��W~��}�ŗm�h&(7>�r�ct��iu.�/R�Yܣj{g�FO���A�.��s a��a���t�=�W��v�"|h7��eci`�u��h"�7�Xk�g���.��żAr���N�U�>s�1�©i�?:/�N�h�3 ��Ţ%�s����U9 q���I�n�B�E�`Q�0($Zr��)Ȝ�%�}k�1�#��'��� g�MˁE�Yf�2��ų��w
G鯣�<��*E�B��Z��;Um���-���($���ûm���~�?��@N��m����&�!�2���A��"����kyy�t��#Yʃ��t=�O�> ���f݅�IX�΀�w	��=ۤ(E-<}<O����G��� X [�uv6���X���:�;cv���s'�k�]��x�(\�˵[vWУ��A��^���]K�U�[s�&H-AЮ�+{}�m��7 Ӥ�0�Ժ��q��3�sp�	bG
˗$�p��L�19e@�p��'
���
�����@@E�b� 8
lKD�s�m���S9p�@L"x���A��� �P�H"s�Ay���ܞ?�8h%� ���
��13�ʁ� MA~_:+"F3�ύF�'P y�{� @b�^�o�mi C{N�H�H9PB�v�{���ɽ��
/>���4�N���Խү5���r��7?5�d��3A�q]�ӻO��wgt=�շ��*���C�ڰ0-p積�֠0F�H����@�uŒ���`�2 9a@Cm-W{���'�<ɠe���8p�lN𠙷�Z�����s��з��э���>�:�b��,8�ܛޝ��[��?h6���� r�()��q��>&��\�<��(bX�\�
�Tx�S�h2��� �<��L�����zGK����_��)�U\K1�TI4&��t��a�{wp7�f�]C�������n�=+����}�p�uܳ3Is�A'���������q����yx����Ǭu���:v�v� . 
	=y���ӊ&A��9���PK��[8+��(.v�O�L?�Akkwj�<<q��l�o�#�T�L�FRL�O����s��1@p.4���c�b�R@$�ࣁm��8�:���r�ۼ���p��(�jd��g��ۺ�F�"(5�Q;?��$�"���Pp͠;~����ݺ��7�����6�l�.F�A%G�f�6�T��:��8�w���:�M�Gv�n7m���Z�oy �2B�Nh$.���BO�N�|E�ߟa:f��v���g%9�8}^��]v�Gھ���V��%s?mN���:z�Q�a'��Ⱦٷ7��1Q�΂���0��*��|M�ڝ7q�:wꮘ}���c����w��3��*L�������<��
{�N�;���p
 N#�	�3�X�tDFd�
��I:����c&I��T�R|<�Dc��@�G�F'�1ʀ�`���^��g0�')�t�=�F�+�h�X<��ʃ#��h��G }�fN( e@�0d	�<h1�rX�&x�(=տ}͎�}����}iX����Ͳ܀��T�������Y�.�w/�|m�����
F@�8� ��m��7;/�7K�ڪ� �M����mn�(<�wup����\�q�\i������ˏl��� �Q�9��b�Su��T�(1��+�v� 4+@c���g�����T�N�2�t���ݱf�:�Z2�@���miJI.	�24v�3q���@{�&qȓA���=_�7�ꖍ��iYAR��] �n&|h�6�iu r\g��c@'S#�ޙҺ�]�:WL�w]KxtY�09I,r@�x
f���^���K�����?Tީ�T0R�BO�A��3���3Ae�:F��K]?n�����j�b�ǀ��ën6{���+�U������� P�������n�iV/2�\�:Sx�\K���3�Q������$3N2|<0�*�Q'��;�X�(Ƃ�o���7�ı�Agf���Iţ���1�<��%m� `@��&���P �0��eA"ڣ8� .4�-��F'��@V��"@��
݂H��Ә�4����K��:�x��`NY�p������d������:��4�Zt�m���qf�͘.��9�����ggohŴU��Tv���-�11Ƃm��,��E��(5����M� T�u[١[���>�=mo^��R����r�v9�h6�e^" Ï�omY����0�z�ll�Kjl�p���y��m�Վ�E���ۆ�'Iʃ_�s@8�r����usϝBD�8
3�:�Ɓ�FY�H�D�#�*��<(!9�ƃ
�4U$O
��:�
7�t��Pd?��8���D�Y��As���kV�a
	�t�D�
,�`dr�ɀ4��΁߇�G�ft�A���h������/rZ���������۝��qm�j�w�n0��~>��_Q>�u���'L؇�#kp�	�p b^���v��qw�u̴�%���CN&O!@�m� f K<�P9����(�#�d�,�KΌ� �R��e�MkԡJ1�� >$�9�-����q�@"�6���$���$�^t��ۛ[ͅ��l\��� ��:_��Rw_P7k���XP+{b51>/���>Q�@��Vծ�1��뵻m���?F�}6ۓb��qY�]�,�Z��A���m{���]:��}Z�N���AM�ձ�5��o��?���ˉ#���=}�77F�Ӵ�!��ܰ>ݛK�G��4��-��;�e6i�қ�ť��m[�y/���^vWn�+�����\���;�:�km	.���4�
V���QK2�8
-�:F����1��@��0�@����m����[(�xPXX֬ ��&�f����a�|����Amn�Ӥ	��#�PM�l�`Fs�A$#3 Ft6ʁ�����
������c�����\N�4G�}9�hi�4�q�΂?S�Z��oؾ�z�+��O�A���[�߿�-ˮ��� h*>�("�P%�g�0ʂ㠨�e��	I��7e��!��*{=CQ����������\y!�cA�������^��7��}�e��A�v��[���C�:Ė�4���]ϸ7�o�_n͙R�p�'c~�q�������� (7>�]�җ�߀�A�o.������
}�<( �`'� ̏���'3��yPT#��"�0F9s�ʀ�/�d�y� �*� (
���>�4a�&��1�8���~�'�_��n��wS�<���B�1� ��d�Ě,#"0�t!19Fc@�mDFG��ϲfH���ەZ�h<[<yxP@�}��;�v�O��w����r���`(+!ۈV�O�<�.E�@�(�m�m��v��b@Fa a&�z	k��bӡ��
Y7�(%�/�`x
���饮�����l�l��6��*�q,8�j_Q��;�w�t��"9T%t3��)��J6�C��G�kU^�!??����s�HB=� ��߀��q'X'��'Ƃˠv�P�-���Ґ��/��<��W�u�Uϐ�غ�S�ݿҟ��N�9g�]d�oo
��Y�^9�!Z�%��gW��&���w	h�Bf�(աm�Ͼ�M�ˣ�k�PS����su|`�U�+A/��}�����I�LY�p,G�>����6�g`Żb]��y�s��N����;pU >���?� �W'��N�&��9�����c�� ��8̩�1�� 0PL�o�A}Ӕ�0"��ٶ�m���WH�Agb�8�'�?i���t�2�����p4ߊ�r��8P��
Ca8r�(u}ώ4����.�xN`�Vw~��N�>���k[[�gS��}�n�0M��T�KgI\�������b�
n��Ű�q�˅ǵ�iI�p�����<fG�P:���{�r�m:�r��9�s��:�3m/(Pl�K+�ڥ��$�	ch��0�M����e�ne�P1&�a����.����<D��v�ww�Vs6�  �h5��/�L���T]uǈ�h"������G,h�N<x�#I�t'J�,��$�<s��h���@|�xPa�H#e�A�-��x��	"p�h.�V���1�cƂąh+�Dq>^TUՊ��&g�hcc��q��<�P<�'Y1�q�� *��9��2���Pj��$
%bA�ݶ�} Bpb3$� 
���6�ԧ ̅����/m˔b-�=*h�ػn�kd|�,���mt��vݸ���on�"�Kjq-+A���ҝ�G���w��dZ��� _�4 �^���e�������,�F�' (*	�X>���C���ڌ2��x�c�I&  ��7WH,sn\NTz/F��R�������pt�$"�1gv?*�ŉ�ڇU�t;W;0u�����S��ͻ�ܒ�{I�V�^�ś��f�m�nx�N����˿M�NT.H��`��)��1�@��]���3���������,?�z�R����*O�iO�?f�j��޵�]���e�����>4g۷�v�-�[�M����s�&��=���[�A�S���:oM�-ױ7��XPI�n1mJ00p��jڌ1Q�"��lD\X�<(��\��8����L���)94�ʃe�P'H��`��F��#/�PO�mB3��� 5H�ya�Pm�
�IY���5�WU��zG2x���a�Ya�xPY��L�N3@"�p*ͤfM��1�3� /���X�{�P��n�����c���t��(���ĴL@��,l�UX�9cA:��� ���A>��ȶ����A�����i��?�X�6��|w$j-΃r�H� '"m��s�ے$aA����ޭm���h�1�ꉽ��ZYB�!`��P��6��!��h5���g2y�񠪸q�d�G��T'ƃ�s�"�V@>>&���D��M ﶐-�/���B�=��P8FF��0��r�#@�f|�8`8�:$� �L��7  OY�^"�U	���PX����8��x�9LG�*Dj�#��
�BB�x�Te]G�,2��N���(���1ƃ^���ڸ�t��Kͧ: ]�h� �y"�,�.�d�����P�A1:�-��=l}ɀ�9��+��e�,�#S�� 
B}��l׻:��kkx�J[���9�9Hʃ^���������k�C��4��?/A�*3�1���l� ʆ`�T�UV۟S�K,� ���� ga<䑞h&�z�� sж]惲�ܹsnB(}W3�H�lzh�nZ�]�v�a�w[J�0r~꟬u��ntS�}�7lo!z�qi�wz�lY�RXC�s��
 �	��@�[�� oo���~��%�6���o�� ��ۯ�r���8������M���D/g��[KrN��07��c��O��%6�ɷ�Fv�� �x����m:f���v�BٵCx�*Wo��۫�B��31�8aA��������ad�
[�׶�@2�/���P	,5I��x��������|�%�
`�<��hn֩'��ρ���Y���ay�����la��G�Ջa��5c�DPXY���H�($��H c��-*�TJ��0��H$|�'[{f#�xPKAl6��g��e�TL�yP`�R��YPr��������wwr��� U���ј �X
 ���>�(�:6�����Ӑ��@{w
#��������t�%��,	��Ǎ��ڮ�n���'��_��3�����m�SK(8x�A�L�w�݆�yΥn+�
���-�˖rl�?��>��87A��s�O�:.f�� O�$2�@@��$�Q@YXĝ#1�A	���@���ES@@0�1A�1�FT�Õc�`s�z(xI�Au���5���΂x, #�'?��}x�O�(	0�ND�4�Nr8��oQ �(�A� ��2hqLc#�z�Չ$��|I�����w�`0c:��1��
���m1X�5*�8)�������˝G��� �}��^�?&Ƃ�p���w(���>`��CHܽ�O��LD�	��g�olmma��`���� ��뭸6�]:��O��qaA�D���q�Dx 8�����2q�3ʁ�5)��ʃf�ط�ln:�VާK퍋��k�nf,m��n]a� J�h1�]�o����/��ſI�Hfv8���r��������/.�P�J��zE��Nh�z���nm6�U-[k����lYA,��9g@g�{��K��v�t��wAV��x)v��Pg���7b�j]��_�������t͞�f�l[=��5xO4����0U}(��<N���n���`���2T�n����h4��H����TH�Ac��8aAejڳ0s8~�PHK!H'�� 	��i� �pʂ� �m/@h�@�@�(6+)��F�#Ý��ޝq�*�Ƞ��p�a��΀�r�G���
Ï���_`O�V5+DH�0�#���n�5�ds�ƀ���V$���@�y �N~�8� �K�v�o�_���1�j��݄���zᛆڛa`$�(�
��my	��������90É��n\ٲ�KAQ��Pꪓ�'�|(6��%�H�ӧ���@[K�Ɇt�˰6B�$��'h,�_N�춢��d N�����]��3��X�p��G���0��Ƃ���a�t c��'�<O�Br���; ��(
�g#�o6���xPE��p���f��FY��@�#y�i��A? 	����9A�8���v�Yp&�����a�������D�	��@����9q(��Nc~��4aˌ��,A�#��h0��@UM>��8�� *��<��A���v5�VS.���* \�oi�\@#FK�l,��u�z�w'@�K��8���=���<�wK������%S�Ý���U$����Ɓ{w�H&�Զ� �� ��W� ��>E@���;��A�	������ yPP���Bt�H �h.�w��ڻ��׆ö�s���s��-[�.�J���@������G�siҬ� m�:�i��0�q<h*�*��p�p�������.�:q�l���0�pD�b@$I\����}����zk��z��w��0҇�l�D?�h({{��}żSci���d'�� 1������n��vQWka�&�fx�A�u'���Y�ٴfղf"I�h)�ۄ��+��m9�T~ש_����r����>�
�����n0>�xPkhʒS>t�-�����9l�bAg��� �9�t-�k��h6����ډ��� Ƃ�ŋ�I�cA9lܴ� x𠔪�&x�|h%*��.A��(�@��rj��s(�b^�Ƞr/� �(��XV.��h�m�'�G��Zl��t[˂����L�Aͯ+Y�ZQ��O.'*��H瀠c/_H�4	%��r�A$�d��J�Aey�?mOFC{xۆ��<h6�sPq�q�l �p�Fg(��m��j˱����'���Wml�ĳ,@�G1A���i��n�[�*�|Tǈ��/]VbL�3���d���y"���x�
$rc�PI�N��t �6��>UQ@��N4�8�˕fLc@Ӗ<	���g�4�l���8�{{J��?7�(��hB��	�f��l�  O���#Q H�G*T`�"(�@��8���hRĐ�N$��tbD�'9�y* U'J�̎T ���I��r��n�ܸ�eA�F�Xr9LPE����K\���yPH���;{g�Y?�l���*��r��:����R��{z�[p�|�H����M
�ZZ�3��Ȏ`
� �;�j�q����:wE��oey����TN>��_Tm�^��I���w�Һnj@��� s�z����_���owޣn���e��J��-�D�B�`�s%�
w?t\�$�j݋}7���GK�a}��lN/q����ݝ�w�n�'l���l�	N!(��
]���uΧkk�ݧM��T�:���G����h��d�34}�ݩ�;�_�ڎ���>��􎞦}�����w>�<�v}���M���@;��۴��޾-��m�:���>�c�]��km��� ��&�X�^����ma쩀���A\��m3\�94OY�7R�S,�r>tM����Z�g����h4jfNR(,��ª�G�c�*+I  g�t6�bf A��z.�12_
��YP��c�8F4%�|'��P8󠰷m�zL��	6�a��:	*�d�3�'��I��3<8P!n�9�5�X�PPN��v��	�9�������Ż _Q�a�q�
�E�G9�E��A:`���s_��<]�t��R�ǲ�ď�(9?P����n��H9$��'��10�ݼ� ����%�A47������}M̖��I���7�q,O
@`��A ���?���$y
��M��ݓ�@n t;���-��gȑ�A������x

ˌxxEf$���2΃8�@�̟�,(��(�"�mK%d��Ad��c�g<<h��� F(0x�D� �	��=D�R<�Ƃ�idĔ�q(,m�_�@� �D� *	!Z$N��@E%��,8���0�e@�<�`��"�D'� &G�"��p ��me�H��pP_�&�_��l���[�(kv�.�
�%��^㛗�0
��`$8�	�@�l{*�n����0��u���'!r � }d���S�ǝ%��� #������u����g��Al*�a��[�f�$�8	������?Q����'Y���=?q��[�_�yĦ�h�繤L�J�SPqn���}�K��Qһ`��="�,�� ��|�^o����A���Kf\c��6��]�u�a�۰��͝�����w�������E͛
�>������}��m�o����v늸{�f��e@�����snA�������W�AںoK���������v�.��S�<��U����m&�l����j@w���A�l���ۖ7ww1g�	��+z� Qf�6w�G����Pj=N彺���7����KE��e8L�J�m�R�<Q�Ac���@@��΂�ʌF�(&*��I���PM���Āq��ʃl�m=�r�9y��)�H'�8�G�PJF��1�A+m���8|h��qS��F3�5��C) e����n& 0�[*��'?΂xf�����&�f�RMϰcA�yș�(1��z3��4��ӭ�΍��냼5�FW(������ �W푆��C� 0�
$��c �yaAx�@S�vT��\k��A�#���}�J�@y3�AW�wL�l$���h.�5���-��`s3����n@Bޟ����7˳c$}₹؆& ���	����@<$q�Ȁqn�@�ēs� A�8�
��?�`xs���@�R%�P�4�8���K8g �@��#�f�N�Ѻ�d��
� �0'�A!���A�&���'�	�@�T� `9a@�Br� I���F�,h�S��ª�$�I ��@�0O��@����Ǐ
 &[����탁�uf3�a�h�.ۿnB�T�G����w. q��1�L�@5��$hI����6�cj�� �Ҥq�(�M��cң�����}��^���ܗn�;cmq��[-��:��b6�y�7.d�Ƃ��������syt�2-YLmZD"�$( �t!�� 6$�I�ٺ/j]�lz�X�\� n��&ڛ��&����;k|�>g�WA'��������n����:�������nP��fQ
�	Ƃ��{sq�{�b�OR=�������t���kn��}��g�ok��&���{�ń�l�6����M˒>{�PRtN���GS�K_|m��8����Ӷ�D~���-�
h5}���v�߼5o.�q�π��5ۆ�S�L��� ���8�Xڲ��x��n��`0�M��Yr�#f��Ŷs���(6n���!����8�F��ݵL!�y�A:���
"8ύ���b3�d�%�0�$a�ƂF�L�łs(������2� iy�v�	�P2s��Zڨ���@7U I3����@?m�M�dc'�0u�3��e��<y�yј��=Z� �x�� I�F|',h"���D(R�y
���k��
���"p�AU�c{}�L�8�A�#6\x��F����u-���O��1A{y�]9/�[��IĘ��ASu�&N8PEi�<>��8��΁������Õ $������0����bB'!1�P1F4q��.\�3"�bs�@�8�#�s��<(m����p��ڠ��
�ɉg��M 1��<G���3��@A �0�1�1�p�v�h3#?3��Q+���x�@�0�p�4Q��Q#�PH���} �'�ʃ ��W#�@t�Č&Y�ω�n�/�,#Y��ҡ�]�S��ҹ�q���3��L *�\s:��*x��nmű�-�����(t-�,�T΄���01�X�*@
f`��i�3�Z��z�u��Y�_�d�7TswiO�E�@��O�4{����V��M��t���oѺU�&��h	!�1�\vŚ������U�Y�씝d#1�	�,h6>��߭�]�m�^��v��%7����ر�����$�����$Ƃ�~����]��;~�����d}��4��pv�Ұ(5����p�-p�].����b��ǟ*�Ӻ~�a���~��Z�K0y�h(�����FͤT0ʶ�:�&�h5ދ����Qޓq�͈�A��n��m�����"������ �-]��OzX�>ʞW�o�u�^yӕ��&����p��|h%س�� ��y�΂�ol��#�ե+��^T-ٖ��c��I؆�"@-�G!A��ًa@�8�('�VW�����A8���ɶ�4��������5xPIm���<h0-��9dH��=X�E� >@ ��y�`*��!���V�8�Ɓ���2g3��lW9pn_ �F<��y�n޽˓���|'
� <�A������K/�]q 爠���,�/�7��f�"��=��r�a�g���	�@+<�y�Z6�F?�WK����^Iah��t7�Ai1 �
��[��~�
��	���h �"g!���!�@�F\9�<@A��*�P�s�@EYx$H��4 �u]a��(0#�A���A��x�\�.T�g���>�:2�0�Ac�ی$bq��Ab��!�I̎~�	�8��Å�.��!��ƀ�Sx�(2�%I���f�%W"DqP�!�#��rځ�m� �d$� O��r�Îd�@र����΃%'!�D�4�ߣ�7�.ll޻����4F�鷽����D���c�ʂ�m�9-01�A,�7kshȆ���Ґ������;3k�z���͹����c��cos�w��U ���v��\h5~��;��ަ�z�
�fյ��[�Z�mp[j>':
�Wu��$6���� E�O��_�ϫ:l�3�£HE2PN-v�P(9_����w_qnY6� ��#lM���ш[Hͨ��5\s�rh)�g��=v鿻Sk�[h�va��qU?��v��k���	_�liۢ����J��&���{�'��M E��QJ���&�Y��:�[�;�$�U��Iǀ�A�YM��k-���Q-�h��h9�q���_zȄ&�фQ��PR�7/ũ��8���A��x���k �H��	�l�&"� 
� �9>�A9-2A�q�3Ai���� ��A��6[DM �����P\۳�H�q�2�&x��pB�� ��.� ��tK��E������*c \f�q��,>���F �&�_��cl��/: ��#U��P`��9����#����aq�l(0�,����1�߷��y#��:O
/΋���]�p���K�p]����4�}�W��海��@S:�(#�$�A]�����V�$��w��(&tɷa`�#Wa��r4����m+�wn����.O�ʂn�p��U ���)/i��8�Bs:�����L�8����lr�&�c��p��`�h�D��w:�RHl���A�<��9P($p3@�V �@��3��h2�r1A��$�|h%���đ ��(--Z T,j'�A-����?w�F�0g?��*�Px����z�a�X��4�����ʁ�1 O!�ƃ:H�<� � a�0N&y
��Y)$��� ��]jda���@��29����Hc�p�#<h=��c�j��{b�S���G{M���XC3I� (5��{�7h��Nì��ȗ�[VK�~["��f5��k��;����eg 
��6='�{g���o��v��Fa�Z��m�ܞA�L�g
}�z�V��-�Vݾ��{u��{�b�*���U��e�r�]��m��w�N�lv�K��.��yX���j��:z1�s��H�h�}�����CA7���o{��m�=��g�l�ۋ���m[����i \��w�w�w�fů\$���'��h:���ǧ�8�j��#eK�  8Pk]��72Yh��^ݱ�eN`ƃV���Ct,[�nWnb`N'�A��wM���&�� (���h4���/����Ӟ6֤_��4����(�����q��j2ˉ9�N�c#�'���f�!� <?��vY�8��l�t��c�t������	˝���-��d��A����`� 	��m0	"1yF4�>: ���X��'�lP+�@�f(�	29΀�l]`X0� (��ٻ�8i�A7o�{cK�)��;�afF9I�3���q��d(rƠ P�f`A�@5ر@���4;5ӨA3��"���q_��A���������#�@}�e�@��
��lM��;/p*�,�b��OAS��t���M�8/h%현
0��
	��)���$���z�(���F�� n�t��ڦ*�|���Ɯ��H8�AUu��pq�����@#�ƃ�G�:,�PH�$�q�`��?��d��A� ��PuI�c�A�<Fq6�H�A�$!ʃ0d`~�xÔx�ŝx��s���p����aq�Ý�J\����$�0F&8�eA-���rH .��H�8��02�S�@��Q�T[n2<�!#Ә�����Y�a΁�ߨ �q x�P<Y`�W�$�>� 'X��á� �9cA��\t~���ʖŝ�([���������A���;�/O�=.�G����7n�V�&ն�aO�&��gs� �7���t�W:�Snt��U���} [�]��'ܖ�.`�LPh{���z���U�W�y��]�w���S��1,O�T���-��0�D�$6���|h6.��:]�>���g�o?�ckd��6m0��l-�.0�rʀ���[���V�mb��a��6�'������,�^�|ٱ4�c�n����݇N��KD�f����uM�ϧ������l!6Bm܃�>�Y�w�Z���JYKf%"�\�����v�O���Aǉ'��>���e@� ��n�8�A�u[���zc�\R7W0*r������	og���n^E��A��I\[T<�4�j	RF������^��8{�0��h���	�� ic�d�M�m��  `����t�B�����m������:	�u4� �a�P<[ָ���*�[K��he�� �B��ee�#�
+��#H<#�"��AQ �<�$Y�y��Ă1<�(&k�T,9a4�E�LU�$q� tm�ǆq��
���%t��M?XD"��I��i�;*��SA�� ��@~h�ƃ�}�ӎǮ��A�NpĐg�AOj卸��{N�� ,􋇉�.]T�5���n�x�3���U-p�1�3A>�*`L减��L��ujg��eon7����]�KƂmݾ��'�)�|��3�-/$~(��A��p��|p�#�q9<⁘��?
~?eL��4����"��X}�A��#�9 ���(	Ğ#�1�#�r�"c�?9�%�E̙�
=��0~e	�<���Y) �9PH� 'Wc>4�+�e�b�UG�I,|�"}'��r�[KI'�Eĕ�Ɯ9H�-����*dc���:Ф*�<'��<3'��<x||��3��>�	q8@#4Y_�$�A�o���zo:��mڛ��vۋ/�;����S�l��$cA�:���b��y���:�}_t�l�N�=�])H>����M��{s�ɛ(9OU���R=O�]����`۫����3,�� \ U�ê8�S�p�轣ٻ=�I�����)��lt텦V�u-ŰX�$��q�e������{��nz�S[v�ilmm �}���f�.
�0��'v�l���IN�nQT7c,� 3Aխ�ӧ%��]L˧ij���5�ʃP�v�����������t�O���]�ZE-z�iP=D������o߼����:A8�A/�o��Jtޜ=ޯ��jx����l�#�������K��4��������]��O@KV����6����l�
"4r a&�j[pH��H3��l�c��c��A�:{��#� 4p�8�m[M��p	 �gAf�o��T�?���ڲ�%�bI�9E��c���1F�΀���&8Ϗ��2���l��v�����B��E�O��[+��Ȓ~U�I�'H.�\����W㏝$M��.'�w���!����Yvwi�cǕr�ua/��,�9�8��醁
NBxPGw����}O�K��g��guh>ָ?e>�7��v�֬�B��]�O�s��AC�7w/��W�Y�@� ��6lH��c���Tǖ�00�9@=���������x����:��� U�F"���?��c���A]z4![�y�@ْ$�Æ
� �"�1�bZ��*q�O�pT�s��@��X8�'��`2x�΁6`�s�G�Nc�����ƀ�SY�fq�>�+Vt�\�𠖖��=9��1����H\L�,��1�p��T��H&K�$�M�u`g8E@0��3�.R4�*1>tH.|#� �F�cN4�	��4Ь��2q3��4�C|�dcƃ0��NLG�H���NC��'R�I�
]� T�N�;�����n���g��h�tM���/K7���,@�'����m����޹/r��%�<g3�i�Sl���)�q�<s��+�6��	���g^۩3��v�K���ɖ��J�C�s
/�u[�A����w�w6,�gge\���\A�Ak�=��R�u����M��o� �~��V�CۏHL ��[d5�i1�|(5��/_�s�p�p����4��xPQL,�@U��۝�6J�K~��pE���/:�QN�k��o�{Jf	Ȱ�Y�v���rw�Q�m�̹�G��}cu�F��v�q�j��T� �!	���A5,�Ӕ���,,�9�����'��J� q1�΂R`�2�Ao�vB�z���ƃq��R��Ì�p#
4�X�- ��T����D� ���ā�|�ʁ�P��c��qHPK,A�Uk�N��@t��A$�'*�����g���?.��1���� �2��a_K�L�0��q@G�a�����
���Ma���K�f��.8�� M���j�Lc���kEu	� 1�� �΃E���T�^�m�����3� ۷hMÇ<���~c�ʗ7D(��e�*�;k���	�Y��\9���,w�O�d�e�Ac�����5)�� �G!�@�s\�~���sZ	�z���0ͣ�*t���#x�u �㇇:�94E&$���h#	h� 
iDc�9�<`�b1 xr�i1!p3�(0VF��9��yP`����B��)P=SY�zFm�h'X� ���x�N��$8��A)J�r����
����**�����ST �I'#?�A!B�Fc���;N@��FX��H�� >8r4H�39�b�
ʠ��2�J���p�D1A�B��<��ȴ��3��4�G��p�۶��$���P�|��M6�v��d����Yl�  aώf�����m��[\4��	��Pn?�=;��l[�6��u��7ێ�]��6A�6ۍ8���1A�A[�]�����^��_w��jի`&�o`[6S$D��Xv�g���ow�Nɘ6�hF7�6ÍKk���*�$�.ٸ��[�<���z����۱e�ڮ)����Sp�]��	�Pn]��4��[����8�>�6N����,���X�l��*кo�j� uu�ʊܴ��G�(9z�����btt�����-G��kȊ`5���PXش�էP�xH�t�YM`�|('-�i
����kjfĈ\>����`�u��M��vke4���yP[۶���N���<�&[��c��8
����d�?΀��&F8�t����r�	$(&zp��@�wm�I j�� ��S�'	�*�����y�.���ʀ����K ӓ
	c|��*��P+[���2br��Ɓ�rT�h3�0�w�v�'��.? ����� _�r<y�Dv[���s�4�� 	1�� ƃ��D��w]ͺw>��cj��n��=ѩ�89�s�� ����{@��A���kn?�?�t�h)l�x����6v�V�X��t��ę�`N���"���ѫܓ �>�t�u6���.�c��h�e�K�a����F�8�4]@?�B��1$	�p�<E Y�3&��I�9q�J	0F�9��x\O/�HӀ:����� �B�$�>� a�)�� �Hl��a:-�c!��؀Z<��(&[S�L��PI@0$	0�4��	n 
"��a�r"��mg21':-��#�� 
�f�`M�3�՘������Z ����\� ���AqVc �����F�4]aT q��h	�D�#4	nY���E�:�n<`��X�9�<9�s��7l�bw�ZI:3'VY��6�;�m�ro���m�;~�Ю����[w�V��[9�'*}�w���������_�n���^�I,y��ػC�.o;�Ԭ�Y�M�z��a���雋�:^�&�w�_R�obx�i�G����Y�?|Y��NqAX� �7�6}�ݻ�yp���؇[d�p�?p�ܯ�,����@�Z�ղqc�Ƃ/E�z�۝��A���طsҰ<��>��������6�>�W/p�g˕�n"VU�
	�#�*]��Q$$�3&����J�p���M�l'�oA7oen��+�8�m}3ceV�#�Ax�կ�e��(
�� j�d,�(H��� ��"�!P,��y��0a��
34!���2q0hJ���1�4����$�h8��s��9t��K��Eu�F.�W	V�q����DO��#�΂W��@ ��#x��Jψ�b�#_Fm �2�pm���Y��(��i�S29P@ge`5��H�A羫������u�����뵠IM(t��Q�����aRW	&Nh=-�v�zI���K���ض�3�L�<I��~��k�.�=��7v��=�:I#,(8�AS8e�4[M�H��<b�GM�r��J�A-��?�Pf��w:@i0D�'���E �+9g3I�q�q�i8�� z� g�@��E��q�`�|h"4�7߇�s���PbO��3"&�1�ğ�(2-{�1���'Z۷����ƂZZ)#IdFR(%*@�8�1Ɓ����4� �h���F["f��c W����=-�$���>��&��6c����d�XG���� ���G� � �(��P�s�΃ ��̉�'�P8�I�9g@P��� ����W8+�Ɓ��7�p\�M�L	��A+mѶ�O-��{4��`k[���7�[+Je/��\�c�A�uޭ�nnu�͐���˛��L#߾Ř��@��Wk���w	�z���k�"=���xx�����gf��ȗlnl�)n����OS�W7�X�*ͬ;X�L�W3 ��B.C1�Am��-Χu/n���H*�pc����m�V�������A�р�4:ON���u~��o�,�������Pk�����������0�� M��ذ'��%�ے,��6L	� ����4����4����栲�a��o1ƃl�;��:�H��]X�ʃF\�ٕ��z��3��m]&x.d�WlF,#Yx��A!,�+/��#�hlA,  c@�9�p��Y�F��~@s�y7F���@�wL�'�΁�!�NS��(-���FFtYQ'V@�ͻ
:�� Q�,[h��@z�0�oԳ-��&kW��0y ��n,*��K�\�� @9�|�(z�;~��7{�U�s�+(@�:ܿ�^�����-��D��zV�� V��l�7�L4ͿSP ��C[`\�,̌ �6m�zv�_osm���5�aƃϛݍ�����W���ɟ�x
��� �-�Y������ԣR+�q �\[ndH'9A	���x��a��q��b4���4	CfI�O:F�2\�|h}D �q9P"��>�T���2��)��A|hn�w�1Ďt�����O">4�A�\d�h$[�&A�($�3A���r�z�b>8�8�YV,�,h
�� �p�x�2ȓL��*��
��"����9�g΁꤂J����H��h$+`X�3��9P0�g�01��hK�L�V�1�klO�r�z#��*p!4�;��uj�P9v̤8��{ba��C�F�J������Qfj�����7B�`�Ļ�*��^�����b�Ӷڑ����Hͨ6����m+:*� �	����E)iH��4�~��(	[JX�&X���

��p	�^���^*�S!rWV�`s�nowm��9��[�ٰ`H�F�����[��#k�-1�=����$� k�{��ѻw����i�ܾ�� ��:}n�'�cq΂�ų�04� � ��6"�g<��Y�J�8�P-�8<"x�	6�с�~>XPl�7cpir�����a=��bq�t,ڻ��g�P#ƂRm�#p:	���+�NyP��(�\ �q�kZ�UΦ��ƂE�j����T��j ���4KV�e�NS�� 	��r΁�,A0��Jr����d�s8����r��&c9����h��  �{���1l}8�#A���L������A�3_P�A�ʁ�z�c���A��s�T� A�w�X� ��a�?�mv��#V�#��p]��s��X]���[���L��DPX}>�sw�ix�G�n������x�{-p(!F e��h�5��|��r� ��֮��[g��~V�O�>V>,0��w�K��� >���B���c0����R1�
l18zH�&29��	�hI O�L��38bx�:*��LE�a��<��� #V3@k{bߙÄs�	֬��*y�3�΂JZ0K�"��fX)�c��'�����`� ���� *���P%�p�q�ʁ�:��Dc?�J��F9�=f 9��h��3'���P-NG'�4B�	��΁{D�4�dy��,،N03'�4�,�`�8P=XF��xg���E8��q��C'/z�`8.� *+��c�Jp� �u�he�_�A���}�{�\�/�]���v�#+j}�4�� qc��M������M���M��<����R��.��u�9�X�'�PW�޸T��:n��_���yz}��Xf�#t"��hC�����[��ǉ<� tݎ�/~�yl�B������7��~w�������vV�[�^O��U���s��FTl� i�9	����IVU�bPX۲0.t�#*	���� �� ٴ�`@�>I饙4�8~ƃh��э1O#A.ݷ��#*	~�i�����	6�AP�I�9r�3�3'�3A�a	_LL����70 x�4,�4b0(3��>f���tB������#]�Zup�y�!op�UϤ� �A2�Ա�9bxPH,��#�t]�E[�	�P33�@�K�K��� d�΀�F�6�hr2^��]��&r�q�l�UE#��Ap���1Í�#�{l��#���ߣ����n�aq��1��`h8�P��ݮ�їl�=J��lqX�Y},�j�|ܹp�km}�G˯b|�4�=�	�p�� ��
ߵc}j��`_�\R�m7�A�#"8)�=��:M罱K��������eY�7 �G�q��-=ݥ�j��y��6Ɩ�a�["@"<&����p�8�G�b\��y
����I��Ii��A��P8,�!8�@�2�<@́@�[{�HD��O��,�t{�\��A#��H��EWo�Cb΁��Ng����ہ��3<���,		G (
�K �xPX�:)P=j����@klxDd��4P@A_A���c9�c"}C ?Ɓ�e�1��1�� *qA�4��8y���i`j���P=[H#9����4��x��@e�A��qb9s��vhV2��@�DY'����V2$�D�>8Pd�ǘ��8����v��D��(]���V�b 	��:����ݦl�a����� �V�� .$b�Aq��5Φ�g��[&��Pn��6�_���XR�.�@��p�����o&�r�c��%��݌ٹ�P��{�������� �_��5*�R>�΃����!�G	��1��<���J����A`�{������ۘ��H��:	6���
d�<|�/�gL�e���@1�˵�\
��a�nT�� �x�G�*	���A!T`$|h���*�ɧ�-��Y�4̟�ŷI�#�hN�qa�r(���� ?	M-�����*��`f�߫�q�*���(���R�3�Ï.���q����/��m� �Zs�@�[�(���>T-;�N� ���΁%� A��\p��T���`�5N~T�ҁ���4˟l(�D�b9�A�'�����@�wm�AB���΁[�u �a�˅��N���ۻ��n���F$3��E�X�^}��.{wX3i_��G��As�w@�-������R�pt"5y�F(Cj�X&|����kG�NF ƀOf�ƃ)ʃZ����h��݃f��m� -��A���jv���۟�y2>`���V�K�]��S!/���2��S5�oP63�h�����T5fH�a�(okv�R�#��۠�
��UN:cTV���(�maI剏�V��1��h26��}CH�a��Õ�N�d�'��o���� 1c@�lRbc���oN�xPa���eĈ�<h2� �8����!@�+`��8��ƀ�bD�0 a��5�� 1X��<"���� �q�<(�n+��(�� �p�r�*	�f��
�z��6�W�p΀��*���QÎ+�R0&I��] ��q�h}�t�y�<���@����	4�L��I:�����=I� ���CjbKtf�r-��.K�Ag��5��vB���'Im9�o@���Z�7�ӭ s��"0��V����ޣ��?A�ŭ����>qAK޽�N���.iڡ�z�a f��{nٙ8�	v��d��(,v�s��"J�Y�EP���*�I2�L�$g�ʂU�q�\Rf��Wt�@��b�.(ҏ�)��Aa���a^H��(	q�LD
&��l�@A!'��@#�0X�:{�s2I����A�B��$a�����Ԑ&MO�F'�P$��@�'/�Po��u��?
	�{:�g<p�@��D���9q��cpmB�	��(,�ݴ�����*�l`�+Ĝ}��1j?1���d=�B"�ˈ>tnP�wS��<(׶�5�Y��!�@�����H�g��H�,�^?e .�� R�j&H h5o����:�q���
FJ��>A���}[������F$Lt|�xA���ss��V�,�����?�PvK�}b���.c�Pan"�F��J��"c8�D +z���ˍ^��̐	>tۏ�r� ă<h ��n��.��8��<�5}�h���b���I�ap� )�A\���X'up)8�� f2�.ߴz=���|~%sB�ctݝ����P���PG��@T1���'��rgI��9��@�ڙ�'�9MK*=,�>�`�+�M�'O,��t+ ����aA�z� �Ӛ�	]:���OT��V2���� 	FJFYP!oA���x���b< EY��g	ƀM� j<p偠q�˂��&�@��J�$bX��гjV1����@w&g��4����'��d��m@b�l�]p$�4Qq|<`a@�n����`y�#"uq�&�9h�1��ƃCvc�<�ۢt_���f��t��1<	ࣝ�Wi�ېYv�}��ۘҺy�yPQ����,�6��t����3T����lZ�E������#�:lI�'��A%-���8��li+���PX؂&N�ē�yPH��f`� e4-#��&$�1'
���n�HuO��v�_e���c�ƂB��"%U�g�4�3L�#��H�H��߸�Y�\��s���6�
<(��2�� ���#�I2�s�-�b���@�&R� ���@��A,q8�@�T)!� �&��9eGO����_�cS ��n�D�|�d������S�h���C�M�� MI >t]�B��g#Tk����l�چ-@պ��1�6/?u=�eێ ��?c@��Ōqp0�� �2t�	ʁ���  |	\����d�CfޘT[���&���,m,���ܻ:2qɒ\�G1A�}v��]Z�H�,5t�ŊԮ�-�����m���c�@���:]u��
��iAG��@�Tƙ-ˌc�F����9�p����� �ƀM� h�!4�m�b�h���{a�Ns�4Æ.LL��D�,�A�s�� �\ �"1���)�1��*��@��I&I���ZE#�g�@�bd�@����s�	���><��4�,p g��v��K
���@��a	�!VP�H�
D�@�A��d2���%�ϰ�4�Ls΀l���'P��@��9A�<=���O39P�$H�9K�!��p��r����Y >�j�P��3�h2n��#H���%�eX�5.�$����9���=���z
33�PD���Q��ݠl,�����΃qS����[�p[�۵;�H0xyPQ+7t]+��t+D��a_�|h+{��m��΋�nf���^\�cƃFKl�I�3�y�K�a���g08���;f͘3��j�L�8c�	CD��G�2)wF���]t��ˎ5�"|�
�dm�Ն� dr��Aaj�ܓ� jh$[��}�@UybD>Y�TwvCk�D�Pw �,`��@��&�c������F
��8�y2�XF'�Ƃ7�u}$H,G�Qm�a��8��_�+�c�7�h,6æ~�����]�eDk<�(4[�m���e��	�(�T�^#��5��ɝPfd	J��j�Y�q��	;}�F*��8H��g[�+�L���@�\Q��	?�� !%�De}��"p P X�%��&Z����.cZ�T���� Egw&���W��l��A�(8M��M����Op�P9�k�G�b�b�_w����ٴ[��o�ȌB�
~"��8����}�������3�鼑H'�9cƃ.6��.	��΂.�`ob��xN4��\��jB`yP5��e�A>�� �>a8G3��BapQ'�P8� 	��A�Tk!�A~Y�"�1�|�A�q�Z:��$��� �[Ф�� �@�ګ1����I4�=F=F\O�� ��X���c��,�E�T�!�8f+X��@�@6`J�&N���j������ *@�I\��d��q%�<�0�3�n��@U3#��+� �rB1�P9Q��g4]�����fdq�u�JN��� >
��+�\��4	*c���ȶ�HXc�r��/�)�*c�����q�P0m\������ch�`]ȯ#̚|�{su��mw2MoQcch�r�����`�c*
����.�ҋ]mp��t��s,x�T���+�۞��@[J�~�f#��΃KIbdq�sƂE�P}C0G*�
�q��u���ӫ��'�ƂR�[b
����8�Ք ��<�Am��k�)*`h6M�MΨ 0�,p����!��.�1����B� �q��$�������4	6�Ȝ�9y�ن0HR���� Z�dJ�ӁI�chۧ�6�����p����ط�@ƃi����}��Z�.��*ţ����gً(53��� ��i��^F�d��
�ș0FD�{+*Fy�f$�4�r�?�xPH�,JL���L���8G�7�$���l����F1�
�Y��4d���P��l���Of(,v��@K�Ȟt� R�B���2�\ҭ��p�
�(�^&L�>~4+�pXĊ�����Q�p<�(!�6P
$j"A�'P�7�3{���n�,\�c���g������Osw��u3�<�*	��y�]��77���m�e�����_p����f���D/�'\�bDp�Y�K)�)�(�	b^t
�UX�1�`�@oe�`
d߷�{{�&��c�X�D������Y�g�:�:mХ�:�bh!\���I*[2�:�Xpˁx�! %Xc� �s����D+z#�b<��i�CN(#ݴ��SO�o���2�4���C�3a�ʀFʁ���s�3@�Ŧ���	�3�
6�H�a��a���="�q�@Eڪ�Rs1����ը}���ݱ��S ��@!����ـNt�-�A"N�-��H���M�I�^ �(	�p@w��4[L<��3@�\I��P��@�����9M�����b���D9�+�A��{�����Y�2|h$[�X�F���\��� �v$�D�WݺF7Yp#�Y���{��Mڎ��0(�v�ѫ1�h4�F$����	)e��N �1��j��br��Acj�*�:��($(1 Aǅ��f,�jӆ���f����h6m����#>sA,=�b@��𠔷X"�୊�D���ٍ���]�����:E���?���A��_��V:�(�v��ի��oBl���eh� ���to�/�7�n����z^��ݟwwpNy-�� �A����v�W_��G��6�{+s� Hv� �A�z7��}!��;m�������� ��e��7ΛѺ?F��t~����?���m�_� YUՑ�d`C)9�(>e�M����w�w�M����F���d��b�SG̮l�x
=��TZL�9��\��0t��4�q���'㈠�� �.r0��PI:I�� ���B��xHxP���#R��L�<�:k%�hC,`'�@�۪��3���-qR@�IP�hj��+�M������Q��ʂJ�ǒ�"�Ϩ��ͫ�4n7G�	D�΀�4i$�+��A��\ڿN�u��h{�c�w{���1�
=��s�}�Tg��d�n2Qm�\������Z�ٱ�����K�� C.�(�Y��<� ��PI]@㏆d�t
D��|t%�_�Q�b�;� wP�a@$k�nF3���(0�u%u�'��2��L�Z�a×�@���
������q�@�ogy}\p1��