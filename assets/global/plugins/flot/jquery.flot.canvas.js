/* Flot plugin for drawing all elements of a plot on the canvas.

Copyright (c) 2007-2014 IOLA and Ole Laursen.
Licensed under the MIT license.

Flot normally produces certain elements, like axis labels and the legend, using
HTML elements. This permits greater interactivity and customization, and often
looks better, due to cross-browser canvas text inconsistencies and limitations.

It can also be desirable to render the plot entirely in canvas, particularly
if the goal is to save it as an image, or if Flot is being used in a context
where the HTML DOM does not exist, as is the case within Node.js. This plugin
switches out Flot's standard drawing operations for canvas-only replacements.

Currently the plugin supports only axis labels, but it will eventually allow
every element of the plot to be rendered directly to canvas.

The plugin supports these options:

{
    canvas: boolean
}

The "canvas" option controls whether full canvas drawing is enabled, making it
possible to toggle on and off. This is useful when a plot uses HTML text in the
browser, but needs to redraw with canvas text when exporting as an image.

*/

(function($) {

	var options = {
		canvas: true
	};

	var render, getTextInfo, addText;

	// Cache the prototype hasOwnProperty for faster access

	var hasOwnProperty = Object.prototype.hasOwnProperty;

	function init(plot, classes) {

		var Canvas = classes.Canvas;

		// We only want to replace the functions once; the second time around
		// we would just get our new function back.  This whole replacing of
		// prototype functions is a disaster, and needs to be changed ASAP.

		if (render == null) {
			getTextInfo = Canvas.prototype.getTextInfo,
			addText = Canvas.prototype.addText,
			render = Canvas.prototype.render;
		}

		// Finishes rendering the canvas, including overlaid text

		Canvas.prototype.render = function() {

			if (!plot.getOptions().canvas) {
				return render.call(this);
			}

			var context = this.context,
				cache = this._textCache;

			// For each text layer, render elements marked as active

			context.save();
			context.textBaseline = "middle";

			for (var layerKey in cache) {
				if (hasOwnProperty.call(cache, layerKey)) {
					var layerCache = cache[layerKey];
					for (var styleKey in layerCache) {
						if (hasOwnProperty.call(layerCache, styleKey)) {
							var styleCache = layerCache[styleKey],
								updateStyles = true;
							for (var key in styleCache) {
								if (hasOwnProperty.call(styleCache, key)) {

									var info = styleCache[key],
										positions = info.positions,
										lines = info.lines;

									// Since every element at this level of the cache have the
									// same font and fill styles, we can just change them once
									// using the values from the first element.

									if (updateStyles) {
										context.fillStyle = info.font.color;
										context.font = info.font.definition;
										updateStyles = false;
									}

									for (var i = 0, position; position = positions[i]; i++) {
										if (position.active) {
											for (var j = 0, line; line = position.lines[j]; j++) {
												context.fillText(lines[j].text, line[0], line[1]);
											}
										} else {
											positions.splice(i--, 1);
										}
									}

									if (positions.length == 0) {
										delete styleCache[key];
									}
								}
							}
						}
					}
				}
			}

			context.restore();
		};

		// Creates (if necessary) and returns a text info object.
		//
		// When the canvas option is set, the object looks like this:
		//
		// {
		//     width: Width of the text's bounding box.
		//     height: Height of the text's bounding box.
		//     positions: Array of positions at which this text is drawn.
		//     lines: [{
		//         height: Height of this line.
		//         widths: Width of this line.
		//         text: Text on this line.
		//     }],
		//     font: {
		//         definition: Canvas font property string.
		//         color: Color of the text.
		//     },
		// }
		//
		// The positions array contains objects that look lik﻿<?xml version="1.0" encoding="utf-8"?>
<!-- (c) ammap.com | SVG map of Nigeria - High -->
<svg xmlns="http://www.w3.org/2000/svg" xmlns:amcharts="http://amcharts.com/ammap" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1">
	<defs>
		<style type="text/css">
			.land
			{
				fill: #CCCCCC;
				fill-opacity: 1;
				stroke:white;
				stroke-opacity: 1;
				stroke-width:0.5;
			}
		</style>

		<amcharts:ammap projection="mercator" leftLongitude="2.667421" topLatitude="13.892750" rightLongitude="14.680752" bottomLatitude="4.269658"></amcharts:ammap>

		<!-- All areas are listed in the line below. You can use this list in your script. -->
		<!--{id:"NG-AB"},{id:"NG-AD"},{id:"NG-AK"},{id:"NG-AN"},{id:"NG-BA"},{id:"NG-BE"},{id:"NG-BO"},{id:"NG-BY"},{id:"NG-CR"},{id:"NG-DE"},{id:"NG-EB"},{id:"NG-ED"},{id:"NG-EK"},{id:"NG-EN"},{id:"NG-FC"},{id:"NG-GO"},{id:"NG-IM"},{id:"NG-JI"},{id:"NG-KD"},{id:"NG-KE"},{id:"NG-KN"},{id:"NG-KO"},{id:"NG-KT"},{id:"NG-KW"},{id:"NG-LA"},{id:"NG-NA"},{id:"NG-NI"},{id:"NG-OG"},{id:"NG-ON"},{id:"NG-OS"},{id:"NG-OY"},{id:"NG-PL"},{id:"NG-RI"},{id:"NG-SO"},{id:"NG-TA"},{id:"NG-YO"},{id:"NG-ZA"}-->

	</defs>
	<g>
		<path id="NG-AB" title="Abia" class="land" d="M290.66,491.93L291.26,492.7L296.09,491.81L298.8,492.08L300.85,493.42L301.18,494.88L301.18,494.88L301.25,499.24L300.7,502.1L300.75,502.83L301.69,503.86L315.78,504.72L317.02,506.58L317.24,508.77L317.94,509.81L321.15,512.14L322.49,512.35L322.49,512.35L323.57,513.64L322.98,517.36L323.13,521.14L324.5,525.77L325.23,527.45L326.94,529.45L327.7,532L327.1,533.21L323.68,532.45L323.68,532.45L320.99,529.84L318.45,529L315.69,526.86L315.11,524.75L313.92,523.28L310.52,522.37L308.1,522.51L307.82,523.42L308.35,526.93L306.51,532.03L306.83,532.48L309.06,532.32L308.36,534.76L307.63,535.66L301.55,535.5L300.45,536.48L300.38,537.23L301.45,539.17L300.24,544.98L300.21,547.38L300.87,549.87L299.7,552.76L297.18,554.7L297.27,555.47L296.8,556.11L297.02,557.58L297.61,558.25L297.63,559.42L296.96,561.04L298.75,564.41L298.88,566.26L298.88,566.26L296.52,564.47L293.58,563.06L291.78,563.49L288.5,562.27L285.71,562.39L284.6,563.03L281.47,563.61L279.67,563.34L278.22,561.93L277.63,560.83L277.79,559.73L279.16,556.03L282.58,551.7L284.97,547.51L284.92,544.73L284.26,543.01L283.72,542.96L283.72,542.96L286.81,532.71L288.91,528.84L290.73,527.16L291.91,524.12L292.8,523.22L293.23,521.21L292.87,517.76L293.15,515.71L291.64,513.27L292.87,512.5L292.49,504.03L292.02,502.68L290.98,501.23L285.64,499.45L283.63,497.76L283.63,497.76L284.9,495.54L286.26,494.24z"/>
		<path id="NG-AD" title="Adamawa" class="land" d="M682.29,187.44L682.16,189.27L681.34,191.03L680.32,192.6L679.22,193.43L677.45,196.33L676.09,201.22L673.43,203.67L672.95,204.77L672.34,207.78L673.06,211.39L673.83,212.45L673.83,214.15L672.22,215.81L671.96,220.42L670.99,222.09L671.12,225.54L670.34,227.48L668.29,230.05L668.03,233.07L666.42,234.11L664.98,236.72L659.11,237.88L654.93,243.76L655.08,244.74L656.62,246.36L656.66,247L655.51,249.74L653.66,251.13L653.63,252.9L654.32,254.5L657.01,255.83L658.02,257.42L655.86,260.51L655.73,262.76L655.19,264.05L655.46,265.16L654.38,266.63L654.71,267.41L654.49,268.82L654.92,269.45L654.18,271.65L653.16,273.29L652.19,273.16L650.49,274.48L642.48,275.3L639.73,277.46L638.11,277.75L634.35,281.14L630.59,283.12L630.68,284.46L633.05,284.73L633.54,285.15L633.63,292.63L632.9,293.32L632.07,297.73L632.91,299.8L631.75,303.83L632.05,305.15L629.24,309.95L626.6,312.25L626.59,313.25L627.3,314.24L626.48,316.29L627.75,320.21L626.62,322.06L625.53,322.44L623.86,322.21L622.4,322.75L621.83,323.85L621.67,326.58L620.69,327.32L619.7,329.17L617.52,329.36L614.12,332.12L613.29,332.24L607.41,330.6L606.75,331.26L606.51,332.97L605.33,334.13L604.52,337.18L602.75,339.52L600.42,339.66L599.3,340.55L596.83,340.27L594.98,341.48L593.07,341.81L592.35,342.63L591.67,346.67L593.11,351.89L593.69,357.66L593.63,358.7L592.86,359.26L590.43,363.77L591.05,366.28L591.27,370.01L587.14,376.36L583.65,379.45L581.89,382.46L581.41,384.46L580.29,385.93L579.88,389.38L578.77,390.94L579.46,397.08L580.11,398.88L577.18,400.36L575.22,400.63L572.47,403.77L572.47,403.77L571.25,402.89L571.3,401.33L569.36,396.71L568.98,393.17L568.24,391.44L568.82,385.95L567.78,383.93L565.63,381.33L561.31,377.49L558.81,374.34L557.29,373.22L555.87,372.79L554.23,373.17L551.4,377.18L548.68,379.17L545.97,376.72L543.06,372.02L540.54,365.29L541.36,363.71L545.5,358.93L549.3,355.71L554.38,350.23L563.01,338.71L569.13,333.49L569.57,331.41L569.19,325.63L575.18,314.19L576.18,310.57L575.92,308.76L573.9,304.92L569.76,303.45L567.1,300.51L566.76,296.67L567.22,294.5L561.96,284.68L559.88,283.39L555.23,282.31L553.77,280.39L548.32,270.21L548.32,270.21L554.52,269.3L557.89,267.87L567.29,262.21L573.04,258.21L574.14,255.88L573.62,252.18L574.3,241.95L574.3,241.95L580.64,242.92L583.34,242.73L587.91,241.41L589.65,240.34L591.35,238.44L595.08,232.89L601.28,225.83L603.79,224.96L610.08,224.47L615.98,222.25L618.42,219.63L619.24,216.97L622.56,216.83L624.91,215.69L627.13,211.99L629.23,209.83L631.74,208.36L640.26,209.15L643.13,210.18L645.71,211.73L648.55,214.84L650.98,215.99L653.36,217.01L657.75,217L658.89,215.78L660.37,208.02L663.08,202.34L664.3,198.7L665.18,194.6L665.46,186.59L668.55,186.16L669.17,186.9L672.92,188.04L676.78,187.75L682.4,186.01L682.4,186.01z"/>
		<path id="NG-AK" title="Akwa Ibom" class="land" d="M302.56,585.59v0.01l0,0l-0.02,-0.02l0,0L302.56,585.59zM310.13,584.89l0.97,0.5l0.92,-0.19l0.88,-0.14l0.33,0.28l-3.58,0.75l-0.49,-0.59L310.13,584.89zM350.44,577.33l0.88,2.09l-0.76,0.82l-1.07,0.22l-0.