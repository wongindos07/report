* Web: http://www.amcharts.com/
* Facebook: https://www.facebook.com/amcharts
* Twitter: https://twitter.com/amcharts


## Changelog

### 1.0.8
* Added "headers" config variable which allows adding custom headers to HTTP requests

### 1.0.7
* Fixed an issue with the Pie chart when it is being loaded in inactive tab

### 1.0.6
* Added support for Gauge chart (loads "arrows" array)

### 1.0.5
* Fixed JS error if periodSelector was not defined in chart config
* Now all callback functions (complete, error, load) receive additional parameter: chart
* postProcess function will now have "this" context set to Data Loader object as well as receive chart reference as third paramater

### 1.0.4
* Added chart.dataLoader.loadData() function which can be used to manually trigger all data reload

### 1.0.3
* Fixed the bug where defaults were not being applied properly
* Fixed the bug with translations not being applied properly
* Cleaned up the code (to pass JSHint validation)

### 1.0.2
* Fixed the issue with modified Array prototypes

### 1.0.1
* Added "complete", "load" and "error" properties that can be set with function handlers to be invoked on load completion, successful file load or failed load respectively
* Fixed language container initialization bug
* Fixed bug that was causing parse errors not be displayed

### 1.0
* Added GANTT chart support

### 0.9.2
* Added global data load methods that can be used to load and parse data by code outside plugin
* Trim CSV column names
* Translation added: Lithuanian

### 0.9.1
* Fix chart animations not playing after asynchronous load

### 0.9
* Initial release                                                                                                                                                                                                                                                                                                                                                                                          ��@��s�~�؟�dr��g���g���	�Y�����Z�t�ޡ�[c��GK�@!�p�2n_X}io���N 59dJ0l��������P}{c�G@�W� ��xE�K#V��/�Q���_���QT�=���P`24�����F��|}p@L�t@��+-�K9�-�Z��*5�!����:	P/��s�ed�0��\ ��5 뙄�.��a�y���m�N���O����`-o�����[�����}ڼ
��:�9�?��+^������Ņ����hh|raeuqe�'������D�3t*��@��]�*.�,)��fQr�E� *HOï��Jffu1�槦�&U zi	�|�5�%�@��6�u�L^J
(<���R�F��/@�)� �;W��
*�2@Y�9P����N���,�C�S�_[���L ��@}ei��k �F
dU
�XViE
v1�d�Ǩ�C�������o��wo�%'����p@��W���W2�<�q;�lv�#�g!<\�'H�����v�7����߾/�v����9���=�Hop�<���)��Xm��^�ݤB�6��f H��VM��:�aw[&;�Ɲ�qG[�B�)�4i}z-�0��X�E�6F���]Ǧ���h����JJ��U)bUs^�@6���C�r`����Z�s�2��`%vae~kqfc16:�:����q�b�X��T���(�\��������)mDgh�"2����?h/B )z�q�:�/ M���?*������oR��6�l#v��`poz���i:���8:���C����x�w��{��~{�=ܼ�[=��]����&����Wa6n��3�y]ٙY��F��a~��ݗn�r��}�+|'�U؟��d8FI�����(u~p@�9 ���(�C��̊BPU�pz�D�#y>%3���_�⿒��=(�Mey���ԂL�w�X��-*C`V�P���6�RǶ�1;UB�c�%-HUY���`�< �
".�������yP �Xu��F�Y������_��k�G?�y���>�F�(����������0>������7����8�)~;��/
�?��/>�'